# nnnsim
NS-3 Module for the mobility aware, full network naming Information Centric Network Architecture, 3N.

For more information, including downloading and compilation instructions, please refer to the documents in doc/ folder.