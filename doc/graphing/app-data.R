#!/usr/bin/Rscript

# app-data.R
# Simple R script to make graphs from tracers - App delay
#
# Copyright (c) 2014 Waseda University, Sato Laboratory
# 
# app-data.R is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# app-data.R is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of              
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               
# GNU Affero Public License for more details.                                 
#                                                                             
# You should have received a copy of the GNU Affero Public License            
# along with app-data.R.  If not, see <http://www.gnu.org/licenses/>.
#
# Author: Jairo Eduardo Lopez <jairo@ruri.waseda.jp>
#                    

suppressPackageStartupMessages(library (ggplot2))
suppressPackageStartupMessages(library (scales))
suppressPackageStartupMessages(library (optparse))
suppressPackageStartupMessages(library (doBy))

# set some reasonable defaults for the options that are needed
option_list <- list (
  make_option(c("-f", "--file"), type="character", default="/home/jelfn/sim/ns-allinone-3.29/ns-3.29/scratch/nnn-intraas-push/results/IntraASPush-app-delays-dual-1-40-smart-p3N-cNO-no-no-partr-edge-mapm-02-001-006.txt",
              help="File which holds the raw app delay data.\n\t\t[Default \"%default\"]"),
  make_option(c("-d", "--delay"), action="store_true", default=FALSE,
              help="Tell the script to graph delay data"),
  make_option(c("-j", "--hop"), action="store_true", default=FALSE,
              help="Tell the script to graph hop data"),
  make_option(c("-r", "--retx"), action="store_true", default=FALSE,
              help="Tell the script to graph retransmission data"),
  make_option(c("-o", "--output"), type="character", default=".",
              help="Output directory for graphs.\n\t\t[Default \"%default\"]"),
  make_option(c("--txt"), action="store_true", default=FALSE,
              help="Flag to export data to space separated txt file\n\t\t[Default \"%default\"]"),
  make_option(c("-s", "--seq"), type="integer", default=10,
              help="Sets how apart the ticks are on graph.\n\t\t[Default \"%default\"]"),
  make_option(c("--goodput"), type="double", default="8.192",
              help="Graph good put with packets of a determined kilobit size.\n\t\t[Default \"%default kilobits\"]"),
  make_option(c("-e", "--node"), type="character", default="",
              help="Node data to graph. Can be a comma separated list.\n\t\tDefault graphs data for all nodes."),
  make_option(c("-t", "--title"), type="character", default="ICN App",
              help="Title for the graph")
)

# Load the parser
opt = parse_args(OptionParser(option_list=option_list, description="Creates graphs from App Delay Tracer data"))
data = read.table (opt$file, header=T, na.strings=c("-"))

if (nrow(data) == 0)
{
  cat(sprintf("No data in file\n"))
  quit()
}

data$Node = factor (data$Node)
data$Type = factor (data$Type)

data$TimeSec = 1 * ceiling (data$Time)

# exclude irrelevant types - CCN
data = subset (data, Type %in% c("FullDelay", "Loss"))

filnodes = unlist(strsplit(opt$node, ","))

# Filter for a particular node
if (nchar(opt$node) > 0) {
  
  data = subset (data, Node %in% filnodes)
}

many_nodes = nlevels (data$Node) > 1

# Set the theme for graph output
theme_set(theme_grey(base_size = 24) + 
    theme(axis.text = element_text(colour = "black")))

# Get the basename of the file
tmpname = strsplit(opt$file, "/")[[1]]
filename = tmpname[length(tmpname)]
# Get rid of the extension
noext = gsub("\\..*", "", filename)

# Copy data
data2 = data

# Sequence for ticks
tbreak = seq (0, max(data$TimeSec), opt$seq)

if (opt$delay) {
  if (many_nodes){
    data.total_delay = summaryBy (DelayS ~ Node + TimeSec + Type, data=data, FUN=mean)
  } else {
    data.total_delay = summaryBy (DelayS ~ TimeSec + Type, data=data, FUN=mean)
  }
  
  data.delay = subset(data.total_delay, Type %in% c("FullDelay"))
  cat ("Creating Average Network Delay graph\n")
  name = sprintf("%s Averaged 1 second Network Delay", opt$title)

  if (many_nodes) {
    g.all <- ggplot (data.delay, aes(x=TimeSec, y=DelayS.mean)) +
      geom_line (aes (colour=Node), size=1) +
      ggtitle (name) +
      ylab ("Delay [Seconds]") +
      xlab ("Time") +
      scale_x_continuous (breaks=tbreak) +
      scale_colour_discrete(name ="Nodes")
  } else {
    g.all <- ggplot (data.delay, aes(colour=Node)) +
      geom_line (aes (x=TimeSec, y=DelayS.mean, colour="Avg Delay"), size=1) +
      ggtitle (name) +
      ylab ("Delay [Seconds]") +
      xlab ("Time") +
      scale_x_continuous (breaks=tbreak)
  }
  
  outpng = sprintf("%s/%s-app-delay.png", opt$output, noext)
  
  png (outpng, width=1024, height=768)
  print (g.all)
  x = dev.off ()
  
  
  if (opt$txt)
  {
    outtxt = sprintf("%s/%s-app-delay.txt", opt$output, noext)
    write.table (data.delay, file=outtxt)
  }
}

if (opt$hop) {
  cat ("Creating Average Hopcount graph\n")
  name = sprintf("%s Average Packet Hop Count", opt$title)
  
  g.all <- ggplot (data, aes(colour=Legend)) +
    geom_line (aes (x=TimeSec, y=HopCount.mean, colour="Avg Hops"), size=1) +
    ggtitle (name) +
    ylab ("Hop Count") +
    xlab ("Time") +
    scale_x_continuous (breaks=tbreak)

  outpng = sprintf("%s/%s-app-hop-%s.png", opt$output, noext, opt$suffix)
  
  png (outpng, width=1024, height=768)
  print (g.all)
  x = dev.off ()
  
  if (opt$txt)
  {
    outtxt = sprintf("%s/%s-app-hop-%s.txt", opt$output, noext, opt$suffix)
    write.table (data.combined, file=outtxt)
  }
}

if (opt$retx) {
  cat ("Creating Average Transmission count graph\n")
  name = sprintf("%s Averaged 1 second Transmission Count", opt$title)
  
  if (many_nodes){
    data.total_rtx = summaryBy (RetxCount ~ Node + TimeSec + Type, data=data, FUN=mean)
  } else {
    data.total_rtx = summaryBy (RetxCount ~ TimeSec + Type, data=data, FUN=mean)
  }
  
  if (many_nodes) {
    g.all <- ggplot (data.total_rtx, aes(x=TimeSec, y=RetxCount.mean)) +
      geom_line (aes (colour=Node), size=1) +
      ggtitle (name) +
      ylab ("Retransmission Count") +
      xlab ("Time") +
      scale_x_continuous (breaks=tbreak) +
      scale_colour_discrete(name ="Nodes")
  } else {
    g.all <- ggplot (data.total_rtx, aes(colour=Legend)) +
      geom_line (aes (x=TimeSec, y=RetxCount.mean, colour="Avg Transmissions"), size=1) +
      ggtitle (name) +
      ylab ("Retransmission Count") +
      xlab ("Time") +
      scale_x_continuous (breaks=tbreak)
  }
  
  outpng = sprintf("%s/%s-app-retx-%s.png", opt$output, noext, opt$suffix)
  
  png (outpng, width=1024, height=768)
  print (g.all)
  x = dev.off ()
  
  if (opt$txt)
  {
    outtxt = sprintf("%s/%s-app-retx-%s.txt", opt$output, noext, opt$suffix)
    write.table (data.combined, file=outtxt)
  }
}

if (opt$goodput > 0)
{
  gmin = min(data2$TimeSec)
  gmax = max(data2$TimeSec)
  num = length(gmin:gmax)
  var = gmin:gmax
  
  total_num = nlevels(data$Node) * num
  
  data.kilobit <- data.frame(matrix(, nrow=total_num, ncol=4))
  
  n_nodes = nlevels(data$Node) -1

  colnames(data.kilobit) <- c("Node", "TimeSec", "Goodput", "Loss")

  for (n in 0:n_nodes) {
    for (i in 1:num) {
      mod = n*num
      
      subdata = subset (data2, TimeSec == var[i] & Type == "FullDelay" & Node == n)
      
      data.kilobit[i+mod,1] <- n
      data.kilobit[i+mod,2] = var[i]
      
      snum = nrow(subdata)
      kilobit <- 0
      if (snum > 1) {
        kilobit <- 1
        curr = subdata[1,4]
        
        for (j in 2:snum) {
          curr <- curr + 1
          if (is.null(subdata[j,4])) {
            next
          }
          
          if (is.na(subdata[j,4])) {
            next
          }
        
          if (curr == subdata[j,4]) {
            kilobit <- kilobit + 1
          } else {
            curr = subdata[j,4]
          }
        }
      }
      
      data.kilobit[i+mod,3] <- kilobit * opt$goodput
      
      subdata_loss = subset (data2, TimeSec == var[i] & Type == "Loss" & Node == n)
      lnum = nrow(subdata_loss)
      data.kilobit[i+mod,4] <- lnum * opt$goodput
    }
  }
  
  data.kilobit$Node = factor (data.kilobit$Node)
  
  cat ("Creating Average goodput\n")
  name = sprintf("%s Averaged 1 second goodput", opt$title)
  
  g.all <- ggplot (data.kilobit, aes(colour=Node)) +
    geom_line (aes (x=TimeSec, y=Goodput, linetype="Goodput"), size=1) +
    geom_line (aes (x=TimeSec, y=Loss, linetype="Loss"), size =1) +
    ggtitle (name) +
    ylab ("Goodput [kilobits/s]") +
    xlab ("Time") +
    scale_x_continuous (breaks=tbreak)+
    scale_linetype_discrete(name="Type")
    scale_colour_discrete(name ="Nodes")

  outpng = sprintf("%s/%s-app-goodput.png", opt$output, noext)

  png (outpng, width=1024, height=768)
  print (g.all)
  x = dev.off ()

  if (opt$txt)
  {
    outtxt = sprintf("%s/%s-app-goodput.txt", opt$output, noext)
    write.table (data.kilobit, file=outtxt)
  }
}
