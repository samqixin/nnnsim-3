# Get R 3.2 for Debian Jessie

1. Add the sources for R 3.2 to apt

        $ vi /etc/apt/sources.list

        deb http://ftp.yz.yamagata-u.ac.jp/pub/cran/bin/linux/debian jessie-cran3/

2. Should you need any proxy connections to connect to the outside, set the relevant environment variable

        $ export http_proxy=http://user@password:proxy.server:port

3. Get the key for the R-cran Debian repository

        $ gpg --keyserver subkeys.pgp.net --recv-key 381BA480

4. Export the key

        $ gpg -a --export 381BA480 > jranke_cran.asc

5. Add the key to the apt key-ring. From here on in, the root user is required.

        # apt-key add jranke_cran.asc

6. Update and upgrade the R packages

        # apt-get update
        # apt-get dist-upgrade

    It is recommended to also install the following packages which can be gotten by apt-get

        # apt-get install r-cran-multcomp r-cran-ggplot2 r-cran-optparse

    The doBy R package, used in comparison graphing scripts is currently not in the Debian facing repository.
    To obtain doBy, the package must be install directly within R. The installation should by done by the root user so that all users can access the library.
    If a proxy is required to connect to the extended network, then make sure to set the http_proxy environment variable before starting R.
    Then, start R

        # R
    
    If a proxy is required to connect to the extended network, make sure to run the following command line within R

        > Sys.setenv(http_proxy="http://user@password:proxy.server:port")
        
    With everything set, now we can install the required doBy package

        > install.packages('doBy')

