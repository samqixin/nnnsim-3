/* -*- Mode:C++; c-file-style:"gnu" -*- */
/*
 * Copyright (c) 2017 Jairo Eduardo Lopez
 *
 *   This file is part of nnnsim.
 *
 *  nnn-3n-icn-push-producer.h is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  nnn-3n-icn-push-producer.h is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with nnn-3n-icn-push-producer.h. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Author: Jairo Eduardo Lopez <jairo@ruri.waseda.jp>
 */

#ifndef MODEL_APPS_NNN_3N_ICN_PUSH_PRODUCER_H_
#define MODEL_APPS_NNN_3N_ICN_PUSH_PRODUCER_H_

#include "ns3/ptr.h"
#include "ns3/random-variable-stream.h"

#include "ns3/nnn-icn-producer-app.h"
#include "nnn-names-seen-container.h"

namespace ns3
{
  namespace nnn
  {
    class NNNAddress;

    class NNNICNPushProducer : public ICNProducerApp
    {
    public:
      static TypeId
      GetTypeId (void);

      NNNICNPushProducer ();

      virtual
      ~NNNICNPushProducer ();

      // Creates a completely new Data packet from the push stream
      Ptr<Packet>
      CreateData ();

      void
      SendPacket ();

      // Inherited from NnnApp
      void
      OnInterest (Ptr<Interest> interest);

      // Essentially to de-encapsulate NULLp
      void
      OnNULLp (Ptr<NULLp> nullpObject);

      // Essentially to de-encapsulate SO
      void
      OnSO (Ptr<SO> soObject);

      // Essentially to de-encapsulate DO
      void
      OnDO (Ptr<DO> doObject);

      // Essentially to de-encapsulate DU
      void
      OnDU (Ptr<DU> duObject);

      // Sets the Producer to push to the Edge
      // automatically turns off all other push methods
      void
      SetEdgePush (bool value);

      bool
      IsEdgePush () const;

      // Sets the Producer to push to a graph centroid
      // automatically turns off all other push methods
      void
      SetCentroidPush (bool value);

      bool
      IsCentroidPush () const;

      // Sets the Producer to push to a particular anchor node
      // automatically turns off all other push methods
      void
      SetAnchorPush (bool value);

      bool
      IsAnchorPush () const;

      // Adds a 3N name to be an anchor destination
      void
      AddAnchor (NNNAddress name);

      NNNAddress
      GetAnchor () const;

      // Signal for when the Producer obtains a name
      void
      GotName (Ptr<const NNNAddress> name);

      // Signal for when the Producer loses his name
      void
      NoName ();

      void
      FlowidEliminated (uint32_t flowid);

      void
      SendMapMeInterest ();

    protected:
      // inherited from Application base class.
      virtual void
      StartApplication ();    // Called at time specified by Start

      virtual void
      StopApplication ();     // Called at time specified by Stop

      /**
       * \brief Constructs the Data packet and sends it using a callback to the underlying NDN protocol
       */
      void
      ScheduleNextPacket ();

      /**
       * @brief Set type of frequency randomization
       * @param value Either 'none', 'uniform', or 'exponential'
       */
      void
      SetRandomizationType (const std::string &value);

      /**
       * @brief Get type of frequency randomization
       * @returns either 'none', 'uniform', or 'exponential'
       */
      std::string
      GetRandomizationType () const;

    private:

      // Function called when push type is changed
      void
      RecalculatePushDestination ();

      bool m_edge_push;                            ///< @brief Flags that the Producer should just push to the first PoA
      bool m_centroid_push;                        ///< @brief Flags that the Producer should push to a particular centroid point depending on the requesters
      bool m_anchor_push;                          ///< @brief Flags that the Producer should push to a particular anchor node in the network
      bool m_directInterest;                       ///< @brief Flags that the Producer should answer directly to Interest PDUs

      double m_frequency;                          ///< @brief How many Data packets should be pushed per second
      NamesSeenContainer m_possibleRequesters;     ///< @brief List of seen 3N requesters
      std::set<Ptr<const NNNAddress>, Ptr3NComp> m_push_destinations;  ///< @brief List of push destinations
      uint32_t m_seq;                              ///< @brief Current sequence number
      uint32_t m_seqMax;                           ///< @brief Maximum number for sequence number
      EventId  m_sendEvent;                        ///< @brief EventId of pending "send packet" event
      Time m_retxTimer;                            ///< @brief Currently estimated retransmission timer
      EventId m_retxEvent;                         ///< @brief Event to check whether or not retransmission should be performed

      bool m_firstTime;                            ///< @brief Flag to know whether this is the first packet to be scheduled
      bool useUni;                                 ///< @brief Flag to know if the random is using a uniform distribution
      Ptr<ExponentialRandomVariable> expRandom;    ///< @brief Reference to exponential distribution random number generator
      std::string m_randomType;                    ///< @brief String that holds the random number distribution used in scheduling packets
    };
  } /* namespace nnn */
} /* namespace ns3 */

#endif /* MODEL_APPS_NNN_3N_ICN_PUSH_PRODUCER_H_ */
