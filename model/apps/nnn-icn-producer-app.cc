/* -*- Mode:C++; c-file-style:"gnu" -*- */
/*
 * Copyright (c) 2017 Jairo Eduardo Lopez
 *
 *   This file is part of nnnsim.
 *
 *  nnn-icn-producer-app.cc is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  nnn-icn-producer-app.cc is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with nnn-icn-producer-app.cc. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Author: Jairo Eduardo Lopez <jairo@ruri.waseda.jp>
 */

#include "ns3/boolean.h"
#include "ns3/log.h"
#include "ns3/simulator.h"
#include "ns3/string.h"
#include "ns3/uinteger.h"

#include "nnn-icn-producer-app.h"
#include "ns3/nnn-fw-hop-count-tag.h"
#include "ns3/nnn-icn-app-face.h"
#include "ns3/nnn-pdus.h"
#include "ns3/nnn-forwarding-strategy.h"

namespace ns3
{
  NS_LOG_COMPONENT_DEFINE ("nnn.ICNProducerApp");

  namespace nnn
  {
    NS_OBJECT_ENSURE_REGISTERED (ICNProducerApp);

    TypeId
    ICNProducerApp::GetTypeId (void)
    {
      static TypeId tid = TypeId ("ns3::nnn::ICNProducerApp")
      		.SetGroupName ("Nnn")
		.SetParent<ICNApp> ()
		.AddConstructor<ICNProducerApp> ()
		.AddAttribute ("Prefix","Prefix, for which producer has the data",
			       StringValue ("/"),
			       MakeNameAccessor (&ICNProducerApp::m_prefix),
			       icn::MakeNameChecker ())
		.AddAttribute ("PayloadSize", "Virtual payload size for Content packets",
			       UintegerValue (1024),
			       MakeUintegerAccessor (&ICNProducerApp::m_virtualPayloadSize),
			       MakeUintegerChecker<uint32_t> ())
		.AddAttribute ("Freshness", "Freshness of data packets, if 0, then unlimited freshness",
			       TimeValue (Seconds (0)),
			       MakeTimeAccessor (&ICNProducerApp::m_freshness),
			       MakeTimeChecker ())
		.AddAttribute ("Signature", "Fake signature, 0 valid signature (default), other values application-specific",
			       UintegerValue (0),
			       MakeUintegerAccessor (&ICNProducerApp::m_signature),
			       MakeUintegerChecker<uint32_t> ())
		.AddAttribute ("KeyLocator", "Name to be used for key locator.  If root, then key locator is not used",
			       icn::NameValue (),
			       MakeNameAccessor (&ICNProducerApp::m_keyLocator),
			       icn::MakeNameChecker ())
		.AddAttribute ("LifeTime", "LifeTime for interest packet",
			       StringValue ("2s"),
			       MakeTimeAccessor (&ICNProducerApp::m_interestLifeTime),
			       MakeTimeChecker ())
		.AddAttribute ("useMapMe", "Flag to notify that Producer is using MapMe Interest packets",
			       BooleanValue (false),
			       MakeBooleanAccessor (&ICNProducerApp::m_useMapMe),
			       MakeBooleanChecker ())
		.AddAttribute ("MapMeRetransmit", "Time for a MapMe Interest packet without an ACK to attempt to retransmit",
			       StringValue ("25ms"),
			       MakeTimeAccessor (&ICNProducerApp::m_mapme_retransmit),
			       MakeTimeChecker ())
		;
      return tid;
    }

    ICNProducerApp::ICNProducerApp ()
    : ICNApp ()
    , m_useMapMe (false)
    , m_virtualPayloadSize (1024)
    , m_signature (0)
    , m_mapme_seq (0)
    , uniRandom (CreateObject<UniformRandomVariable> ())
    , m_firstMapMe (true)
    , m_3n_mapme_flowid (0)
    , m_3n_mapme_seq (0)
    , m_3n_mapme_window (1)
    {
    }

    ICNProducerApp::~ICNProducerApp ()
    {
    }

    Ptr<Packet>
    ICNProducerApp::CreateReturnData (Ptr<Interest> interest)
    {
      NS_LOG_FUNCTION (this << interest);

      // Obtain the name from the Interest packet
//      Ptr<icn::Name> dataName = Create<icn::Name> (interest->GetName ());

      // Create that Data packet using the Interest name
      Ptr<Data> data = Create<Data> (Create<Packet> (m_virtualPayloadSize));
      //dataName->append (m_postfix);
//      data->SetName (dataName);
      data->SetName (interest->GetNamePtr());
      data->SetFreshness (m_freshness);
      data->SetTimestamp (Simulator::Now());

      data->SetSignature (m_signature);
      if (m_keyLocator.size () > 0)
	{
	  data->SetKeyLocator (Create<icn::Name> (m_keyLocator));
	}

      NS_LOG_INFO ("Responding with Pkt: " << interest->GetName () << " Interest Name: " << data->GetName () << " SeqNo " << std::dec << data->GetName ().get (-1).toSeqNum ());

      // Echo back FwHopCountTag if exists
      FwHopCountTag hopCountTag;
      if (interest->GetPayload ()->PeekPacketTag (hopCountTag))
	{
	  data->GetPayload ()->AddPacketTag (hopCountTag);
	}

      m_transmittedDatas (data, this, m_face);
      return icn::Wire::FromData (data);
    }

    void
    ICNProducerApp::SendMapMeInterest ()
    {
      NS_LOG_FUNCTION (this);

      Ptr<icn::Name> name_prefix = Create<icn::Name> (m_prefix);
      name_prefix->appendSeqNum (m_mapme_seq);

      Ptr<Interest> interest = Create<Interest> ();
      interest->SetNonce               (uniRandom->GetValue ());
      interest->SetName                (name_prefix);
      interest->SetInterestLifetime    (m_interestLifeTime);
      interest->SetInterestType(Interest::MAP_ME_INTEREST_UPDATE);
      interest->SetSequenceNumber(m_mapme_seq);
      interest->SetRetransmissionTime(m_mapme_retransmit);

      NS_LOG_INFO ("Sending MapMe Interest with Interest name: " << *name_prefix << " SeqNo " << m_mapme_seq);
      NS_LOG_DEBUG ("Interest Type: " << unsigned(interest->GetInterestType()) << " MapMe Interest Nonce: " << interest->GetNonce());
      NS_LOG_DEBUG ("Lifetime: " << m_interestLifeTime << " Retransmission time: " << m_mapme_retransmit);
      // Log that we are to send out a Interest packet
      m_transmittedInterests (interest, this, m_face);

      // Encode the packet
      Ptr<Packet> retPkt = icn::Wire::FromInterest(interest, icn::Wire::WIRE_FORMAT_NDNSIM);

      NS_LOG_INFO ("Send MapMe Interest packet within NULLp");
      Ptr<NULLp> nullp_o = Create<NULLp> ();
      nullp_o->SetPDUPayloadType (ICN_NNN);
      nullp_o->SetLifetime (m_3n_lifetime);
      nullp_o->SetPayload (retPkt);

      if (m_face->ReceiveNULLp (nullp_o))
	{
	  // Everything went well, record the sending of the message
	  m_transmittedNULLps (nullp_o, this, m_face);
	  // Update the MapMe related Sequence number
	  m_mapme_seq++;
	}
      else
	{
	  NS_LOG_WARN ("Attempted to send NULLp and failed");
	}
    }

    void
    ICNProducerApp::StartApplication ()
    {
      NS_LOG_FUNCTION_NOARGS ();

      ICNApp::StartApplication ();

      if (m_useMapMe)
	{
	  NS_LOG_INFO ("Activating MapMe functionality");
	  m_fw->TraceConnectWithoutContext("PoAAssociation", MakeCallback (&ICNProducerApp::SendMapMeInterest, this));
	}
    }

    void
    ICNProducerApp::StopApplication ()
    {
      NS_LOG_FUNCTION_NOARGS ();

      ICNApp::StopApplication ();

      if (m_useMapMe)
	{
	  NS_LOG_INFO ("Disconnecting MapMe functionality");
	  Ptr<ForwardingStrategy> fw = GetNode ()->GetObject<ForwardingStrategy> ();
	  fw->TraceDisconnectWithoutContext("PoAAssociation", MakeCallback (&ICNProducerApp::SendMapMeInterest, this));
	}
    }
  } /* namespace nnn */
} /* namespace ns3 */
