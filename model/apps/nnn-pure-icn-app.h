/* -*- Mode:C++; c-file-style:"gnu" -*- */
/*
 * Copyright (c) 2016 Jairo Eduardo Lopez
 *
 *   This file is part of nnnsim.
 *
 *  nnn-pure-icn-app.h is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  nnn-pure-icn-app.h is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with nnn-pure-icn-app.h. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Author: Jairo Eduardo Lopez <jairo@ruri.waseda.jp>
 */

#ifndef NNN_PURE_ICN_APP_H_
#define NNN_PURE_ICN_APP_H_

#include "ns3/type-id.h"
#include "ns3/application.h"
#include "ns3/callback.h"
#include "ns3/traced-callback.h"
#include "ns3/ptr.h"

#include "ns3/nnn-icn-name.h"
#include "ns3/nnn-icn-pdus.h"
#include "ns3/nnn-icn-wire.h"
#include "ns3/wire-nnnsim-icn-naming.h"

namespace ns3
{
  namespace nnn
  {
    class Data;
    class Interest;

    class ICNAppFace;

    class PureICNApp : public Application
    {
    public:
      static TypeId GetTypeId ();

      PureICNApp ();
      virtual
      ~PureICNApp ();

      /**
       * @brief Get application ID (ID of applications face)
       */
      uint32_t
      GetId () const;

      void
      SetMobile (bool value);

      bool
      IsMobile () const;

      /**
       * @brief Method that will be called every time new Interest arrives
       * @param interest Interest header
       * @param packet   "Payload" of the interests packet. The actual payload should be zero, but packet itself
       *                 may be useful to get packet tags
       */
      virtual void
      OnInterest (Ptr<const Interest> interest);

      /**
       * @brief Method that will be called every time new NACK arrives
       * @param interest Interest header
       */
      virtual void
      OnNack (Ptr<const Interest> interest);

      /**
       * @brief Method that will be called every time new Data arrives
       * @param contentObject Data header
       * @param payload payload (potentially virtual) of the Data packet (may include packet tags of original packet)
       */
      virtual void
      OnData (Ptr<const Data> contentObject);

      /*
       * Functions that would be called from within a AppFace (pure)
       */
      virtual void
      OnICN (Ptr<const Packet> pkt);

      typedef void (* ReceivedInterestTracedCallback)
	  (const Ptr<const Interest>, const Ptr<PureICNApp>, const Ptr<ICNAppFace>);

      typedef void (* ReceivedNackTracedCallback)
	  (const Ptr<const Interest>, const Ptr<PureICNApp>, const Ptr<ICNAppFace>);

      typedef void (* ReceivedDataTracedCallback)
	  (const Ptr<const Data>, const Ptr<PureICNApp>, const Ptr<ICNAppFace>);

      typedef void (* TransmittedInterestTracedCallback)
	  (const Ptr<const Interest>, const Ptr<PureICNApp>, const Ptr<ICNAppFace>);

      typedef void (* TransmittedDataTracedCallback)
	  (const Ptr<const Data>, const Ptr<PureICNApp>, const Ptr<ICNAppFace>);

      typedef void (* AppBoolTracedCallback)
	  (bool);

    protected:
      /**
       * @brief Do cleanup when application is destroyed
       */
      virtual void
      DoDispose ();

      // inherited from Application base class. Originally they were private
      virtual void
      StartApplication ();     ///< @brief Called at time specified by Start

      virtual void
      StopApplication ();      ///< @brief Called at time specified by Stop

      bool m_active;           ///< @brief Flag to indicate that application is active (set by StartApplication and StopApplication)
      Ptr<ICNAppFace> m_face;  ///< @brief automatically created application face through which application communicates
      bool m_use3N;            ///< @brief Flag to indicate that the App uses 3N capabilities
      bool m_isMobile;         ///< @brief Flag to indicate that the App is of mobile type

      TracedCallback<Ptr<const Interest>,
      Ptr<PureICNApp>, Ptr<ICNAppFace> > m_receivedInterests;    ///< @brief App-level trace of received Interests

      TracedCallback<Ptr<const Interest>,
      Ptr<PureICNApp>, Ptr<ICNAppFace> > m_receivedNacks;        ///< @brief App-level trace of received NACKs

      TracedCallback<Ptr<const Data>,
      Ptr<PureICNApp>, Ptr<ICNAppFace> > m_receivedDatas;        ///< @brief App-level trace of received Data

      TracedCallback<Ptr<const Interest>,
      Ptr<PureICNApp>, Ptr<ICNAppFace> > m_transmittedInterests; ///< @brief App-level trace of transmitted Interests

      TracedCallback<Ptr<const Data>,
      Ptr<PureICNApp>, Ptr<ICNAppFace> > m_transmittedDatas; ///< @brief App-level trace of transmitted Data

      TracedCallback<bool> m_mobility;
    };
  } /* namespace nnn */
} /* namespace ns3 */

#endif /* NNN_PURE_ICN_APP_H_ */
