/* -*- Mode:C++; c-file-style:"gnu" -*- */
/*
 * Copyright (c) 2016 Jairo Eduardo Lopez
 *
 *   This file is part of nnnsim.
 *
 *  nnn-pure-icn-consumer-cbr.cc is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  nnn-pure-icn-consumer-cbr.cc is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with nnn-pure-icn-consumer-cbr.cc. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Author: Jairo Eduardo Lopez <jairo@ruri.waseda.jp>
 */

#include "nnn-pure-icn-consumer-cbr.h"

#include "ns3/boolean.h"
#include "ns3/callback.h"
#include "ns3/double.h"
#include "ns3/integer.h"
#include "ns3/log.h"
#include "ns3/names.h"
#include "ns3/packet.h"
#include "ns3/ptr.h"
#include "ns3/simulator.h"
#include "ns3/string.h"
#include "ns3/uinteger.h"

namespace ns3
{
  NS_LOG_COMPONENT_DEFINE ("nnn.PureICNConsumerCbr");

  namespace nnn
  {
    NS_OBJECT_ENSURE_REGISTERED (PureICNConsumerCbr);

    TypeId
    PureICNConsumerCbr::GetTypeId (void)
    {
      static TypeId tid = TypeId ("ns3::nnn::PureICNConsumerCbr")
   	    .SetGroupName ("Nnn")
	    .SetParent<PureICNConsumer> ()
	    .AddConstructor<PureICNConsumerCbr> ()
	    .AddAttribute ("Frequency", "Frequency of interest packets",
			   StringValue ("1.0"),
			   MakeDoubleAccessor (&PureICNConsumerCbr::m_frequency),
			   MakeDoubleChecker<double> ())
	    .AddAttribute ("Randomize", "Type of send time randomization: none (default), uniform, exponential",
			   StringValue ("none"),
			   MakeStringAccessor (&PureICNConsumerCbr::SetRandomize, &PureICNConsumerCbr::GetRandomize),
			   MakeStringChecker ())
	    .AddAttribute ("MaxSeq", "Maximum sequence number to request",
			   IntegerValue (std::numeric_limits<uint32_t>::max ()),
			   MakeIntegerAccessor (&PureICNConsumerCbr::m_seqMax),
			   MakeIntegerChecker<uint32_t> ())
			   ;
      return tid;
    }

    PureICNConsumerCbr::PureICNConsumerCbr ()
    : PureICNConsumer ()
    , m_frequency (1.0)
    , m_firstTime (true)
    , useUni      (false)
    , uniRandom   (CreateObject<UniformRandomVariable> ())
    , expRandom   (CreateObject<ExponentialRandomVariable> ())
    , m_randomType ("")
    {
      NS_LOG_FUNCTION_NOARGS ();
      m_seqMax = std::numeric_limits<uint32_t>::max ();
    }

    PureICNConsumerCbr::~PureICNConsumerCbr ()
    {
    }

    void
    PureICNConsumerCbr::ScheduleNextPacket ()
    {
      if (m_firstTime)
	{
	  m_sendEvent = Simulator::Schedule (Seconds (0.0),
					     &PureICNConsumer::SendPacket, this);
	  m_firstTime = false;
	}
      else if (!m_sendEvent.IsRunning ())
	{
	  double sched;
	  if (useUni)
	    {
	      sched = uniRandom->GetValue();
	    }
	  else
	    {
	      sched = expRandom->GetValue ();
	    }
	  m_sendEvent = Simulator::Schedule (
	      (m_randomType == "none") ? Seconds (1.0 / m_frequency) : Seconds (sched),
		  &PureICNConsumer::SendPacket, this);
	}
    }

    void
    PureICNConsumerCbr::SetRandomize (const std::string &value)
    {
      if (value == "uniform")
	{
	  uniRandom->SetAttribute ("Min", DoubleValue (0.0));
	  uniRandom->SetAttribute ("Max", DoubleValue (2 * 1.0 / m_frequency));
	  useUni = true;
	}
      else if (value == "exponential")
	{
	  expRandom->SetAttribute ("Mean", DoubleValue (1.0 / m_frequency));
	  expRandom->SetAttribute ("Bound", DoubleValue (50 * 1.0 / m_frequency));
	  useUni = false;
	}
      m_randomType = value;
    }

    std::string
    PureICNConsumerCbr::GetRandomize () const
    {
      return m_randomType;
    }
  } /* namespace nnn */
} /* namespace ns3 */
