/* -*- Mode:C++; c-file-style:"gnu" -*- */
/*
 * Copyright (c) 2016 Jairo Eduardo Lopez
 *
 *   This file is part of nnnsim.
 *
 *  nnn-pure-icn-consumer.cc is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  nnn-pure-icn-consumer.cc is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with nnn-pure-icn-consumer.cc. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Author: Jairo Eduardo Lopez <jairo@ruri.waseda.jp>
 */
#include "nnn-pure-icn-consumer.h"

#include "ns3/nnn-pdus.h"
#include "ns3/nnn-icn-app-face.h"
#include "ns3/nnn-rtt-estimator.h"
#include "ns3/nnn-icn-header-helper.h"
#include "ns3/nnn-fw-hop-count-tag.h"
#include "ns3/nnn-rtt-mean-deviation.h"

namespace ns3
{
  NS_LOG_COMPONENT_DEFINE ("nnn.PureICNConsumer");

  namespace nnn
  {
    NS_OBJECT_ENSURE_REGISTERED (PureICNConsumer);

    TypeId
    PureICNConsumer::GetTypeId (void)
    {
      static TypeId tid = TypeId ("ns3::nnn::PureICNConsumer")
     	      .SetGroupName ("Nnn")
	      .SetParent<PureICNApp> ()
	      .AddAttribute ("StartSeq", "Initial sequence number",
			     IntegerValue (0),
			     MakeIntegerAccessor(&PureICNConsumer::m_seq),
			     MakeIntegerChecker<int32_t>())
	      .AddAttribute ("Prefix","Name of the Interest",
			     StringValue ("/"),
			     MakeNameAccessor (&PureICNConsumer::m_interestName),
			     icn::MakeNameChecker ())
	      .AddAttribute ("LifeTime", "LifeTime for interest packet",
			     StringValue ("2s"),
			     MakeTimeAccessor (&PureICNConsumer::m_interestLifeTime),
			     MakeTimeChecker ())
	      .AddAttribute ("RetxTimer",
			     "Timeout defining how frequent retransmission timeouts should be checked",
			     StringValue ("50ms"),
			     MakeTimeAccessor (&PureICNConsumer::GetRetxTimer, &PureICNConsumer::SetRetxTimer),
			     MakeTimeChecker ())
	      .AddTraceSource ("LastRetransmittedInterestDataDelay", "Delay between last retransmitted Interest and received Data",
			       MakeTraceSourceAccessor (&PureICNConsumer::m_lastRetransmittedInterestDataDelay),
			       "ns3::nnn::PureICNConsumer::LastRetransmittedInterestDataDelayTracedCallback")
	      .AddTraceSource ("FirstInterestDataDelay", "Delay between first transmitted Interest and received Data",
			       MakeTraceSourceAccessor (&PureICNConsumer::m_firstInterestDataDelay),
			       "ns3::nnn::PureICNConsumer::FirstInterestDataDelayTracedCallback")
			       ;
      return tid;
    }

    PureICNConsumer::PureICNConsumer ()
    : PureICNApp()
    , m_rand   (CreateObject<UniformRandomVariable> ())
    , m_seq    (0)
    , m_seqMax (0) // don't request anything
    {
      NS_LOG_FUNCTION_NOARGS ();

      m_rtt = CreateObject<RttMeanDeviation> ();

      m_rand->SetAttribute ("Min", DoubleValue (0));
      m_rand->SetAttribute ("Max", DoubleValue (std::numeric_limits<uint32_t>::max ()));
    }

    PureICNConsumer::~PureICNConsumer ()
    {
    }

    void
    PureICNConsumer::OnTimeout (uint32_t sequenceNumber)
    {
      NS_LOG_FUNCTION (std::dec << sequenceNumber);
      // std::cout << Simulator::Now () << ", TO: " << sequenceNumber << ", current RTO: " << m_rtt->RetransmitTimeout ().ToDouble (Time::S) << "s\n";

      m_rtt->IncreaseMultiplier ();             // Double the next RTO
      m_rtt->SentSeq (SequenceNumber32 (sequenceNumber), 1); // make sure to disable RTT calculation for this sample
      m_retxSeqs.insert (sequenceNumber);
      ScheduleNextPacket ();
    }

    void
    PureICNConsumer::SendPacket ()
    {
      if (!m_active) return;

      NS_LOG_FUNCTION_NOARGS ();

      uint32_t seq=std::numeric_limits<uint32_t>::max (); //invalid
      bool retransmission = false;

      while (m_retxSeqs.size ())
	{
	  seq = *m_retxSeqs.begin ();
	  m_retxSeqs.erase (m_retxSeqs.begin ());
	  retransmission = true;
	  break;
	}

      if (seq == std::numeric_limits<uint32_t>::max ())
	{
	  if (m_seqMax != std::numeric_limits<uint32_t>::max ())
	    {
	      if (m_seq >= m_seqMax)
		{
		  return; // we are totally done
		}
	    }
	  seq = m_seq++;
	}

      if (retransmission)
	NS_LOG_INFO ("> Retransmitting Interest SeqNo " << std::dec << seq);

      Ptr<icn::Name> nameWithSequence = Create<icn::Name> (m_interestName);
      nameWithSequence->appendSeqNum (seq);

      Ptr<Interest> interest = Create<Interest> ();
      interest->SetNonce               (m_rand->GetValue ());
      interest->SetName                (nameWithSequence);
      interest->SetInterestLifetime    (m_interestLifeTime);

      WillSendOutInterest (seq);

      FwHopCountTag hopCountTag;
      interest->GetPayload ()->AddPacketTag (hopCountTag);

      bool ok = false;

      if (m_use3N)
	{
	  // Encode the packet
	  Ptr<Packet> retPkt = icn::Wire::FromInterest(interest, icn::Wire::WIRE_FORMAT_NDNSIM);
	  ok = m_face->SendIn3NPDU(nameWithSequence, retPkt);
	}
      else
	{
	  ok = m_face->ReceiveInterest(interest);
	}

      if (ok)
	{
	  m_transmittedInterests (interest, this, m_face);
	}

      ScheduleNextPacket ();
    }

    void
    PureICNConsumer::OnInterest (Ptr<const Interest> interest)
    {
      if (!m_active) return;

      NS_LOG_FUNCTION (this << interest);
    }

    void
    PureICNConsumer::OnNack (Ptr<const Interest> interest)
    {
      if (!m_active) return;
      PureICNApp::OnNack (interest);

      uint32_t seq = interest->GetName ().get (-1).toSeqNum ();
      NS_LOG_INFO ("< NACK for " << std::dec << seq);

      m_retxSeqs.insert (seq);
      m_seqTimeouts.erase (seq);
      m_rtt->IncreaseMultiplier ();             // Double the next RTO ??
      ScheduleNextPacket ();
    }

    void
    PureICNConsumer::OnData (Ptr<const Data> data)
    {
      if (!m_active) return;
      PureICNApp::OnData (data); // tracing inside

      NS_LOG_FUNCTION (this << data);

      uint32_t seq = data->GetName ().get (-1).toSeqNum ();
      NS_LOG_INFO ("< DATA for SeqNo " << std::dec << seq);

      int hopCount = -1;
      FwHopCountTag hopCountTag;
      if (data->GetPayload ()->PeekPacketTag (hopCountTag))
	{
	  hopCount = hopCountTag.Get ();
	}

      SeqTimeoutsContainer::iterator entry = m_seqLastDelay.find (seq);
      if (entry != m_seqLastDelay.end ())
	{
	  m_lastRetransmittedInterestDataDelay (this, seq, Simulator::Now () - entry->time, hopCount);
	}

      entry = m_seqFullDelay.find (seq);
      if (entry != m_seqFullDelay.end ())
	{
	  Time delay = Simulator::Now () - entry->time;
	  NS_LOG_INFO ("< DATA for SeqNo " << seq << " full delay: " << delay.GetSeconds());
	  m_firstInterestDataDelay (this, seq, delay, m_seqRetxCounts[seq], hopCount);
	}
      else
	{
	  NS_LOG_DEBUG ("< DATA for SeqNo " << seq << " we have probably already satisfied this request");
	}

      m_seqRetxCounts.erase (seq);
      m_seqFullDelay.erase (seq);
      m_seqLastDelay.erase (seq);

      m_seqTimeouts.erase (seq);
      m_retxSeqs.erase (seq);

      m_rtt->AckSeq (SequenceNumber32 (seq));
    }

    void
    PureICNConsumer::OnICN (Ptr<const Packet> pkt)
    {
      NS_LOG_FUNCTION (this);
      bool receivedInterest =false;
      bool receivedData = false;
      Ptr<Interest> interest;
      Ptr<Data> data;

      Ptr<Packet> icn_pdu = pkt->Copy();
      try {
	  icn::HeaderHelper::Type icnType = icn::HeaderHelper::GetICNHeaderType(icn_pdu);
	  switch (icnType)
	  {
	    case icn::HeaderHelper::INTEREST_ICN:
	      interest = ns3::icn::Wire::ToInterest (icn_pdu, ns3::icn::Wire::WIRE_FORMAT_NDNSIM);
	      receivedInterest = true;
	      break;
	    case icn::HeaderHelper::CONTENT_OBJECT_ICN:
	      data = ns3::icn::Wire::ToData (icn_pdu, ns3::icn::Wire::WIRE_FORMAT_NDNSIM);
	      receivedData = true;
	      break;
	    default:
	      NS_FATAL_ERROR ("Not supported ICN header");
	  }
      }
      catch (icn::UnknownHeaderException)
      {
	  NS_FATAL_ERROR ("Unknown ICN header. Should not happen");
      }

      // If the PDU is an Interest
      if (receivedInterest)
	{
	  uint8_t res = interest->GetInterestType();
	  // Within the Interests, we have possible NACKs, separate
	  if (Interest::MAP_ME_INTEREST_UPDATE <= res && res < Interest::NACK_LOOP)
	    {
	      if (res == Interest::MAP_ME_INTEREST_UPDATE_ACK)
		{
		  NS_LOG_INFO ("Obtained an Interest with MapMe ACK for " << interest->GetName ());
		}
	      else
		{
		  NS_LOG_INFO ("Obtained an Interest with MapMe information for " << interest->GetName ());
		}
	    }
	  else if (Interest::NACK_LOOP <= res)
	    {
	      NS_LOG_INFO ("Obtained an Interest with NACK information");
	      OnNack (interest);
	    }
	  else
	    {
	      NS_LOG_INFO ("Obtained a normal Interest packet");
	      OnInterest (interest);
	    }
	}

      // If the PDU is Data
      if (receivedData)
	{
	  NS_LOG_INFO ("Received Data");
	  OnData (data);
	}
    }

    void
    PureICNConsumer::WillSendOutInterest (uint32_t sequenceNumber)
    {
      NS_LOG_DEBUG ("Adding SeqNo " << std::dec << sequenceNumber << " to backlog queue. Transmission count: " <<  (m_seqRetxCounts[sequenceNumber] +1));
      NS_LOG_DEBUG ("Backlog of " << m_seqTimeouts.size () << " items");

      m_seqTimeouts.insert (SeqTimeout (sequenceNumber, Simulator::Now ()));
      m_seqFullDelay.insert (SeqTimeout (sequenceNumber, Simulator::Now ()));

      m_seqLastDelay.erase (sequenceNumber);
      m_seqLastDelay.insert (SeqTimeout (sequenceNumber, Simulator::Now ()));

      m_seqRetxCounts[sequenceNumber] ++;

      m_rtt->SentSeq (SequenceNumber32 (sequenceNumber), 1);
    }

    void
    PureICNConsumer::CheckRetxTimeout ()
    {
      Time now = Simulator::Now ();

      Time rto = m_rtt->RetransmitTimeout ();
      // NS_LOG_DEBUG ("Current RTO: " << rto.ToDouble (Time::S) << "s");

      while (!m_seqTimeouts.empty ())
	{
	  SeqTimeoutsContainer::index<i_timestamp>::type::iterator entry =
	      m_seqTimeouts.get<i_timestamp> ().begin ();
	  if (entry->time + rto <= now) // timeout expired?
	    {
	      uint32_t seqNo = entry->seq;
	      NS_LOG_DEBUG ("SeqNo " << seqNo << " has timed out, Entry: " << entry->time << " RTO: " << rto);
	      m_seqTimeouts.get<i_timestamp> ().erase (entry);
	      OnTimeout (seqNo);
	    }
	  else
	    break; // nothing else to do. All later packets need not be retransmitted
	}

      m_retxEvent = Simulator::Schedule (m_retxTimer,
					 &PureICNConsumer::CheckRetxTimeout, this);
    }

    void
    PureICNConsumer::SetRetxTimer (Time retxTimer)
    {
      m_retxTimer = retxTimer;
      if (m_retxEvent.IsRunning ())
	{
	  // m_retxEvent.Cancel (); // cancel any scheduled cleanup events
	  Simulator::Remove (m_retxEvent); // slower, but better for memory
	}

      // schedule even with new timeout
      m_retxEvent = Simulator::Schedule (m_retxTimer,
					 &PureICNConsumer::CheckRetxTimeout, this);
    }

    Time
    PureICNConsumer::GetRetxTimer () const
    {
      return m_retxTimer;
    }

    // Application Methods
    void
    PureICNConsumer::StartApplication () // Called at time specified by Start
    {
      NS_LOG_FUNCTION_NOARGS ();

      // do base stuff
      PureICNApp::StartApplication ();

      ScheduleNextPacket ();
    }

    void
    PureICNConsumer::StopApplication () // Called at time specified by Stop
    {
      NS_LOG_FUNCTION_NOARGS ();

      // cancel periodic packet generation
      Simulator::Cancel (m_sendEvent);

      // cleanup base stuff
      PureICNApp::StopApplication ();
    }
  } /* namespace nnn */
} /* namespace ns3 */
