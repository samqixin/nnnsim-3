/* -*- Mode:C++; c-file-style:"gnu" -*- */
/*
 * Copyright (c) 2016 Jairo Eduardo Lopez
 *
 *   This file is part of nnnsim.
 *
 *  nnn-pure-icn-consumer.h is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  nnn-pure-icn-consumer.h is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with nnn-pure-icn-consumer.h. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Author: Jairo Eduardo Lopez <jairo@ruri.waseda.jp>
 */
#ifndef NNN_PURE_ICN_CONSUMER_H
#define NNN_PURE_ICN_CONSUMER_H

#include "nnn-pure-icn-app.h"

#include <set>
#include <map>

#include "ns3/double.h"
#include "ns3/integer.h"
#include "ns3/nstime.h"
#include "ns3/random-variable-stream.h"
#include "ns3/log.h"
#include "ns3/packet.h"
#include "ns3/simulator.h"
#include "ns3/string.h"
#include "ns3/uinteger.h"

#include "ns3/nnn-rtt-estimator.h"

#include <boost/multi_index_container.hpp>
#include <boost/multi_index/tag.hpp>
#include <boost/multi_index/ordered_index.hpp>
#include <boost/multi_index/member.hpp>

namespace ns3
{
  namespace nnn
  {
    class PureICNConsumer : public PureICNApp
    {
    public:
      static TypeId
      GetTypeId (void);

      PureICNConsumer ();

      virtual
      ~PureICNConsumer ();

      virtual void
      OnTimeout (uint32_t sequenceNumber);

      virtual void
      SendPacket ();

      virtual void
      OnInterest (Ptr<const Interest> interest);

      virtual void
      OnNack (Ptr<const Interest> interest);

      virtual void
      OnData (Ptr<const Data> data);

      virtual void
      OnICN (Ptr<const Packet> pkt);

      virtual void
      WillSendOutInterest (uint32_t sequenceNumber);

      virtual void
      ScheduleNextPacket () = 0;

      virtual void
      CheckRetxTimeout ();

      void
      SetRetxTimer (Time retxTimer);

      Time
      GetRetxTimer () const;

      typedef void (* LastRetransmittedInterestDataDelayTracedCallback)
	  (const Ptr<PureICNApp>, const uint32_t, const Time, const int32_t);

      typedef void (* FirstInterestDataDelayTracedCallback)
	  (const Ptr<PureICNApp>, const uint32_t, const Time, const uint32_t);

    protected:
      virtual void
      StartApplication ();

      virtual void
      StopApplication ();

      Ptr<UniformRandomVariable> m_rand; ///< @brief nonce generator
      uint32_t m_seq;                    ///< @brief currently requested sequence number
      uint32_t m_seqMax;                 ///< @brief maximum number of sequence number
      EventId m_sendEvent;               ///< @brief EventId of pending "send packet" event
      Time m_retxTimer;                  ///< @brief Currently estimated retransmission timer
      EventId m_retxEvent;               ///< @brief Event to check whether or not retransmission should be performed
      Ptr<RttEstimator> m_rtt;           ///< @brief RTT estimator
      Time m_offTime;                    ///< \brief Time interval between packets
      icn::Name m_interestName;          ///< \brief ICN Name of the Interest (use Name)
      Time m_interestLifeTime;           ///< \brief LifeTime for interest packet

      /// @cond include_hidden
      /**
       * \struct This struct contains sequence numbers of packets to be retransmitted
       */
      struct RetxSeqsContainer :
	  public std::set<uint32_t> { };

      RetxSeqsContainer m_retxSeqs;             ///< \brief ordered set of sequence numbers to be retransmitted

      /**
       * \struct This struct contains a pair of packet sequence number and its timeout
       */
      struct SeqTimeout
      {
	SeqTimeout (uint32_t _seq, Time _time) : seq (_seq), time (_time) { }

	uint32_t seq;
	Time time;
      };
      /// @endcond

      /// @cond include_hidden
      class i_seq { };
      class i_timestamp { };
      /// @endcond

      /// @cond include_hidden
      /**
       * \struct This struct contains a multi-index for the set of SeqTimeout structs
       */
      struct SeqTimeoutsContainer :
	  public boost::multi_index::multi_index_container<
	      SeqTimeout,
	      boost::multi_index::indexed_by<
	          boost::multi_index::ordered_unique<
	              boost::multi_index::tag<i_seq>,
	              boost::multi_index::member<SeqTimeout, uint32_t, &SeqTimeout::seq>
                  >,
              boost::multi_index::ordered_non_unique<
                  boost::multi_index::tag<i_timestamp>,
                  boost::multi_index::member<SeqTimeout, Time, &SeqTimeout::time>
              >
          >
      > { } ;
      SeqTimeoutsContainer m_seqTimeouts;       ///< \brief multi-index for the set of SeqTimeout structs

      SeqTimeoutsContainer m_seqLastDelay;
      SeqTimeoutsContainer m_seqFullDelay;
      std::map<uint32_t, uint32_t> m_seqRetxCounts;

      // App, SeqNo, Delay, Hop Count
      TracedCallback<Ptr<PureICNApp>, uint32_t, Time, int32_t> m_lastRetransmittedInterestDataDelay;
      TracedCallback<Ptr<PureICNApp>, uint32_t, Time, uint32_t, int32_t> m_firstInterestDataDelay;
    };
  } /* namespace nnn */
} /* namespace ns3 */

#endif /* NNN_PURE_ICN_CONSUMER_H */
