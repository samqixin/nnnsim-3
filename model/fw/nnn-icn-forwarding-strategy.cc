/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil -*- */
/*
 * Copyright (c) 2015 Jairo Eduardo Lopez
 *
 *   This file is part of nnnsim.
 *
 *  nnn-icn-forwarding-strategy.cc is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  nnn-icn-forwarding-strategy.cc is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with nnn-icn-forwarding-strategy.cc. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Author: Jairo Eduardo Lopez <jairo@ruri.waseda.jp>
 */

// ICN data
#include "ns3/nnn-icn-naming.h"
#include "ns3/nnn-icn-pdus.h"
#include "ns3/nnn-nnnsim-icn-wire.h"
#include "ns3/nnn-icn-content-store.h"
#include "ns3/nnn-icn-header-helper.h"

#include "ns3/nnn-pit-entry-incoming-face.h"
#include "ns3/nnn-pit-entry-outgoing-face.h"
#include "ns3/nnn-fib.h"
#include "ns3/nnn-fib-entry.h"
#include "ns3/nnn-tfib.h"

#include "ns3/nnn-nnst.h"
#include "ns3/nnn-nnst-entry.h"
#include "ns3/nnn-nnst-entry-facemetric.h"
#include "ns3/nnn-nnpt.h"

#include "ns3/nnn-names-container.h"
#include "ns3/nnn-face-container.h"
#include "ns3/nnn-pdu-buffer.h"
#include "ns3/nnn-addr-aggregator.h"
#include "ns3/nnn-header-helper.h"
#include "ns3/nnn-face-container.h"
#include "ns3/nnn-rxbuffercontainer.h"

#include "ns3/log.h"
#include "ns3/string.h"
#include "nnn-icn-forwarding-strategy.h"

namespace ns3
{
  NS_LOG_COMPONENT_DEFINE ("nnn.fw.icn");

  namespace nnn
  {
    NS_OBJECT_ENSURE_REGISTERED (ICN3NForwardingStrategy);

    TypeId
    ICN3NForwardingStrategy::GetTypeId (void)
    {
      static TypeId tid = TypeId ("ns3::nnn::ICN3NForwardingStrategy")
	  .SetGroupName ("Nnn")
	  .SetParent<ForwardingStrategy> ()

	  ////////////////////////////////////////////////////////////////////

	  .AddTraceSource ("OutInterests", "OutInterests",
			   MakeTraceSourceAccessor (&ICN3NForwardingStrategy::m_outInterests),
			   "ns3::nnn::ForwardingStrategy::InterestTracedCallback")

	  .AddTraceSource ("InInterests", "InInterests",
			   MakeTraceSourceAccessor (&ICN3NForwardingStrategy::m_inInterests),
			   "ns3::nnn::ForwardingStrategy::InterestTracedCallback")

	  .AddTraceSource ("DropInterests", "DropInterests",
			   MakeTraceSourceAccessor (&ICN3NForwardingStrategy::m_dropInterests),
			   "ns3::nnn::ForwardingStrategy::InterestTracedCallback")

	  ////////////////////////////////////////////////////////////////////

	  .AddTraceSource ("OutData", "OutData",
			   MakeTraceSourceAccessor (&ICN3NForwardingStrategy::m_outData),
			   "ns3::nnn::ForwardingStrategy::DataTracedCallback")

	  .AddTraceSource ("InData", "InData",
			   MakeTraceSourceAccessor (&ICN3NForwardingStrategy::m_inData),
			   "ns3::nnn::ForwardingStrategy::DataTracedCallback")

	  .AddTraceSource ("DropData", "DropData",
			   MakeTraceSourceAccessor (&ICN3NForwardingStrategy::m_dropData),
			   "ns3::nnn::ForwardingStrategy::DataTracedCallback")

	  ////////////////////////////////////////////////////////////////////

	  .AddTraceSource ("OutNacks", "OutNacks",
			   MakeTraceSourceAccessor (&ICN3NForwardingStrategy::m_outNacks),
			   "ns3::nnn::ForwardingStrategy::InterestTracedCallback")

          .AddTraceSource ("InNacks", "InNacks",
			   MakeTraceSourceAccessor (&ICN3NForwardingStrategy::m_inNacks),
			   "ns3::nnn::ForwardingStrategy::InterestTracedCallback")

	  .AddTraceSource ("DropNacks", "DropNacks",
			   MakeTraceSourceAccessor (&ICN3NForwardingStrategy::m_dropNacks),
			   "ns3::nnn::ForwardingStrategy::InterestTracedCallback")

	  ////////////////////////////////////////////////////////////////////

	  .AddTraceSource ("SatisfiedInterests", "SatisfiedInterests",
			   MakeTraceSourceAccessor (&ICN3NForwardingStrategy::m_satisfiedInterests),
			   "ns3::nnn:ForwardingStrategy::PITEntryTracedCallback")

	  .AddTraceSource ("TimedOutInterests", "TimedOutInterests",
			   MakeTraceSourceAccessor (&ICN3NForwardingStrategy::m_timedOutInterests),
			   "ns3::nnn:ForwardingStrategy::PITEntryTracedCallback")

	  ////////////////////////////////////////////////////////////////////
	  ////////////////////////////////////////////////////////////////////
	  ////////////////////////////////////////////////////////////////////

	  .AddAttribute ("CacheUnsolicitedDataFromApps", "Cache unsolicited Data that has been pushed from applications",
			 BooleanValue (true),
			 MakeBooleanAccessor (&ICN3NForwardingStrategy::m_cacheUnsolicitedDataFromApps),
			 MakeBooleanChecker ())

	  .AddAttribute ("CacheUnsolicitedData", "Cache overheard Data that have not been requested",
			 BooleanValue (false),
			 MakeBooleanAccessor (&ICN3NForwardingStrategy::m_cacheUnsolicitedData),
			 MakeBooleanChecker ())

	  .AddAttribute ("DetectRetransmissions", "If non-duplicate Interest is received on the same face more than once, it is considered a retransmission",
			 BooleanValue (true),
			 MakeBooleanAccessor (&ICN3NForwardingStrategy::m_detectRetransmissions),
			 MakeBooleanChecker ())

	  .AddAttribute ("EnableNACKs", "Enabling support of NACKs",
			 BooleanValue (true),
			 MakeBooleanAccessor (&ICN3NForwardingStrategy::m_nacksEnabled),
			 MakeBooleanChecker ())

	  .AddAttribute ("EnableSharePoA", "Enables strategy to collect and use lower layer PoA information",
			 BooleanValue (false),
			 MakeBooleanAccessor(&ICN3NForwardingStrategy::IsSharingPoA, &ICN3NForwardingStrategy::SetSharePoA),
			 MakeBooleanChecker ())

	  .AddAttribute ("EnableNonAuthoritativeDU", "Enables nodes to send out DUs although they did not create content being sent",
			 BooleanValue (false),
			 MakeBooleanAccessor(&ICN3NForwardingStrategy::IsUsingNonAuthoritativeDU, &ICN3NForwardingStrategy::SetNonAuthoritativeDU),
			 MakeBooleanChecker ())

	  .AddAttribute ("StandardFIBMetric", "Standard metric for newly created FIB entries",
			 IntegerValue (1000000),
			 MakeIntegerAccessor (&ICN3NForwardingStrategy::m_icn_fib_metric),
			 MakeIntegerChecker<int32_t> ())

	  .AddAttribute ("EnableTFIB", "Enable support of MapMe TFib",
			 BooleanValue (false),
			 MakeBooleanAccessor (&ICN3NForwardingStrategy::HasTFIBEnabled, &ICN3NForwardingStrategy::EnableTFIB),
			 MakeBooleanChecker ())

	  .AddAttribute ("MapMeRetransmissions", "Number of times a Interest using MapMe will try to be retransmitted",
			IntegerValue (3),
			 MakeIntegerAccessor (&ICN3NForwardingStrategy::m_mapme_retrys),
			 MakeIntegerChecker<uint32_t>())

	  .AddAttribute ("EnableFIBUpdates", "Enable support for Interest PDUs to update the FIB. If EnableTFIB is enabled, this option should be enabled as well",
			 BooleanValue (true),
			 MakeBooleanAccessor (&ICN3NForwardingStrategy::IsEnablingFIBUpdates, &ICN3NForwardingStrategy::EnableFIBUpdates),
			 MakeBooleanChecker ())

	  .AddAttribute ("EnableSavingUnrequestedData", "Enables the saving of unrequested ICN Data packets in the local Content Store",
			 BooleanValue (true),
			 MakeBooleanAccessor (&ICN3NForwardingStrategy::IsEnablingSavingUnrequestedData, &ICN3NForwardingStrategy::EnableSavingUnrequestedData),
			 MakeBooleanChecker ())

	  .AddAttribute ("Enable3NFaceTimeSelect", "Enables the selection of Faces for interest propagation by 3N properties based on time",
			 BooleanValue (false),
			 MakeBooleanAccessor (&ICN3NForwardingStrategy::m_Enable_3N_face_time_select),
			 MakeBooleanChecker ())

	  .AddAttribute ("Anycast", "Permit the creation of 3N PDUs that go to all the most distant 3N names in an FIB entry",
			 BooleanValue (false),
			 MakeBooleanAccessor (&ICN3NForwardingStrategy::m_Enable_Anycast),
			 MakeBooleanChecker ())

	  .AddAttribute ("Multicast", "Permit the creation of 3N PDUs that go to all the most distant 3N names in an FIB entry",
			 BooleanValue (false),
			 MakeBooleanAccessor (&ICN3NForwardingStrategy::m_Enable_Multicast),
			 MakeBooleanChecker ())

	  .AddAttribute ("3NPartRouting", "Permit routing Interest PDUs using 3N names and FIB part numbers",
			 BooleanValue (false),
			 MakeBooleanAccessor (&ICN3NForwardingStrategy::m_Enable_FIB_part_select),
			 MakeBooleanChecker ())

	  .AddAttribute("FIBDstThreshold", "Maximum time to consider a 3N name in the FIB as a valid destination",
			StringValue("40s"),
			MakeTimeAccessor(&ICN3NForwardingStrategy::m_fib_3n_threshold),
			MakeTimeChecker ())

	  .AddConstructor<ICN3NForwardingStrategy> ()
	;
      return tid;
    }

    ICN3NForwardingStrategy::ICN3NForwardingStrategy ()
    : ForwardingStrategy ()
    , m_EnableTFIB (false)
    , m_sharePoAInfo (false)
    , m_EnableNonAuthoritativeDU (false)
    {
      m_node_names->RegisterCallbacks(
	  MakeCallback (&ICN3NForwardingStrategy::Reenroll, this),
	  MakeCallback (&ICN3NForwardingStrategy::Enroll, this)
      );

      // Must turn on the supported protocols
      m_n1minus_3n_support = true;
      m_n1minus_icn_support = true;

      // Creation of the separate PIT
      ObjectFactory pit_factory;
      pit_factory.SetTypeId("ns3::nnn::pit::Persistent");

      m_awaiting_mapme_acks = pit_factory.Create<Pit> ();

      m_awaiting_mapme_acks->SetFIB(m_fib);
    }

    ICN3NForwardingStrategy::~ICN3NForwardingStrategy ()
    {
    }

    void
    ICN3NForwardingStrategy::OnNULLp (Ptr<Face> face, Ptr<NULLp> null_p)
    {
      m_inNULLps (null_p, face);

      NS_LOG_INFO ("From Face: " << face->GetId () << " via PoA: " << face->GetAddress() << " on " << GetNode3NName () << " Flowid: " << null_p->GetFlowid () << " Sequence: " << null_p->GetSequence ());

      Ptr<Packet> icn_pdu = null_p->GetPayload ()->Copy ();

      Ptr<DATAPDU> dpdu = DynamicCast<DATAPDU> (null_p);

      bool forward = true;

      if (m_return_ACKP)
	{
	  forward = UpdateReceivedPDUs(dpdu, face, face->GetBroadcastAddress());
	}

      if (forward)
	ProcessICNPDU (dpdu, face, icn_pdu);
      else
	NS_LOG_DEBUG ("Flowid: " << dpdu->GetFlowid () << " Sequence: " << dpdu->GetSequence () << " has already been received");
    }

    void
    ICN3NForwardingStrategy::OnLayeredNULLp (Ptr<Face> face, Ptr<NULLp> null_p,
				 const Address& from, const Address& to)
    {
      m_inNULLps (null_p, face);

      NS_LOG_INFO ("From Face: " << face->GetId () << " via PoA: " << face->GetAddress() << " on " << GetNode3NName () << " got from PoA: " << from << " to PoA: " << to << " Flowid: " << null_p->GetFlowid () << " Sequence: " << null_p->GetSequence ());

      //Give us a rw copy of the packet
      Ptr<Packet> icn_pdu = null_p->GetPayload ()->Copy ();

      // To be able to simplify code, convert pointer to common type
      Ptr<DATAPDU> dpdu = DynamicCast<DATAPDU> (null_p);

      bool forward = true;
      // Send the ACKP
      if (m_return_ACKP)
	{
	  forward = UpdateReceivedPDUs(dpdu, face, from);
	}

      if (forward)
	ProcessLayeredICNPDU (dpdu, face, icn_pdu, from, to);
      else
	NS_LOG_DEBUG ("Flowid: " << dpdu->GetFlowid () << " Sequence: " << dpdu->GetSequence () << " has already been received");
    }

    void
    ICN3NForwardingStrategy::OnSO (Ptr<Face> face, Ptr<SO> so_p)
    {
      m_inSOs (so_p, face);

      NNNAddress myAddr = GetNode3NName ();
      NS_LOG_INFO ("From Face: " << face->GetId () << " via PoA: " << face->GetAddress() << " on " << myAddr << " got headed from " << so_p->GetName() << " Flowid: " << so_p->GetFlowid () << " Sequence: " << so_p->GetSequence ());

      // ---- Attempt to lower memory usage ----
      Ptr<const NNNAddress> src = so_p->GetNamePtr ();

      std::set<NNNAddress>::iterator it = m_names_in_ram.find(src->getName());

      if (it != m_names_in_ram.end ())
	{
	  so_p->SetName (Ptr<const NNNAddress> (&(*it)));
	}
      else
	{
	  m_names_in_ram.insert (*src);
	}
      // --------

      //Give us a rw copy of the packet
      Ptr<Packet> icn_pdu = so_p->GetPayload ()->Copy ();

      // To be able to simplify code, convert pointer to common type
      Ptr<DATAPDU> dpdu = DynamicCast<DATAPDU> (so_p);

      bool forward = true;
      // Send the ACKP
      if (m_return_ACKP)
	{
	  forward = UpdateReceivedPDUs(dpdu, face, face->GetBroadcastAddress());
	}

      if (forward)
	ProcessICNPDU (dpdu, face, icn_pdu);
      else
	NS_LOG_DEBUG ("Flowid: " << dpdu->GetFlowid () << " Sequence: " << dpdu->GetSequence () << " has already been received");
    }

    void
    ICN3NForwardingStrategy::OnLayeredSO (Ptr<Face> face, Ptr<SO> so_p, const Address& from,
			      const Address& to)
    {
      m_inSOs (so_p, face);
      NS_LOG_INFO ("From Face: " << face->GetId () << " via PoA: " << face->GetAddress() << " on " << GetNode3NName () << " got from PoA: " << from  << " to PoA: " << to << " headed from " << so_p->GetName() << " Flowid: " << so_p->GetFlowid () << " Sequence: " << so_p->GetSequence ());

      // ---- Attempt to lower memory usage ----
      Ptr<const NNNAddress> src = so_p->GetNamePtr ();

      std::set<NNNAddress>::iterator it = m_names_in_ram.find(src->getName());

      if (it != m_names_in_ram.end ())
	{
	  so_p->SetName (Ptr<const NNNAddress> (&(*it)));
	}
      else
	{
	  m_names_in_ram.insert (*src);
	}
      // --------

      //Give us a rw copy of the packet
      Ptr<Packet> icn_pdu = so_p->GetPayload ()->Copy ();

      // To be able to simplify code, convert pointer to common type
      Ptr<DATAPDU> dpdu = DynamicCast<DATAPDU> (so_p);

      bool forward = true;
      // Send the ACKP
      if (m_return_ACKP)
	{
	  forward = UpdateReceivedPDUs(dpdu, face, from);
	}

      if (forward)
	ProcessLayeredICNPDU (dpdu, face, icn_pdu, from, to);
      else
	NS_LOG_DEBUG ("Flowid: " << dpdu->GetFlowid () << " Sequence: " << dpdu->GetSequence () << " has already been received");
    }

    void
    ICN3NForwardingStrategy::OnDO (Ptr<Face> face, Ptr<DO> do_p)
    {
      m_inDOs (do_p, face);

      NS_LOG_INFO ("From Face: " << face->GetId () << " via PoA: " << face->GetAddress() << " on " << GetNode3NName () << " got headed to " << do_p->GetName() << " Flowid: " << do_p->GetFlowid () << " Sequence: " << do_p->GetSequence ());

      // ---- Attempt to lower memory usage ----
      Ptr<const NNNAddress> dst = do_p->GetNamePtr ();

      std::set<NNNAddress>::iterator it = m_names_in_ram.find(dst->getName());

      if (it != m_names_in_ram.end ())
	{
	  do_p->SetName (Ptr<const NNNAddress> (&(*it)));
	}
      else
	{
	  m_names_in_ram.insert (*dst);
	}
      // --------

      //Give us a rw copy of the packet
      Ptr<Packet> icn_pdu = do_p->GetPayload ()->Copy ();

      // To be able to simplify code, convert pointer to common type
      Ptr<DATAPDU> dpdu = DynamicCast<DATAPDU> (do_p);

      bool forward = true;
      // Send the ACKP
      if (m_return_ACKP)
	{
	  forward = UpdateReceivedPDUs(dpdu, face, face->GetBroadcastAddress ());
	}

      if (forward)
	ProcessICNPDU (dpdu, face, icn_pdu);
      else
	NS_LOG_DEBUG ("Flowid: " << dpdu->GetFlowid () << " Sequence: " << dpdu->GetSequence () << " has already been received");
    }

    void
    ICN3NForwardingStrategy::OnLayeredDO (Ptr<Face> face, Ptr<DO> do_p, const Address& from,
			      const Address& to)
    {
      m_inDOs (do_p, face);

      NNNAddress myAddr = GetNode3NName ();

      NS_LOG_INFO ("From Face: " << face->GetId () << " via PoA: " << face->GetAddress() << " on " << myAddr << " got from PoA: " << from   << " to PoA: " << to << " headed to " << do_p->GetName() << " Flowid: " << do_p->GetFlowid () << " Sequence: " << do_p->GetSequence ());

      // ---- Attempt to lower memory usage ----
      Ptr<const NNNAddress> dst = do_p->GetNamePtr ();

      std::set<NNNAddress>::iterator it = m_names_in_ram.find(dst->getName());

      if (it != m_names_in_ram.end ())
	{
	  do_p->SetName (Ptr<const NNNAddress> (&(*it)));
	}
      else
	{
	  m_names_in_ram.insert (*dst);
	}
      // --------

      //Give us a rw copy of the packet
      Ptr<Packet> icn_pdu = do_p->GetPayload ()->Copy ();

      // To be able to simplify code, convert pointer to common type
      Ptr<DATAPDU> dpdu = DynamicCast<DATAPDU> (do_p);

      bool forward = true;
      // Send the ACKP
      if (m_return_ACKP)
	{
	  forward = UpdateReceivedPDUs(dpdu, face, from);
	}

      if (forward)
	ProcessLayeredICNPDU (dpdu, face, icn_pdu, from, to);
      else
	NS_LOG_DEBUG ("Flowid: " << dpdu->GetFlowid () << " Sequence: " << dpdu->GetSequence () << " has already been received");
    }

    void
    ICN3NForwardingStrategy::OnDU (Ptr<Face> face, Ptr<DU> du_p)
    {
      m_inDUs (du_p, face);

      NNNAddress myAddr = GetNode3NName ();

      NS_LOG_INFO ("From Face: " << face->GetId () << " via PoA: " << face->GetAddress() << " on " << myAddr <<  " headed from " << du_p->GetSrcName () << " to " << du_p->GetDstName () << " Flowid: " << du_p->GetFlowid () << " Sequence: " << du_p->GetSequence ());

      // ---- Attempt to lower memory usage ----
      Ptr<const NNNAddress> src = du_p->GetSrcNamePtr ();
      Ptr<const NNNAddress> dst = du_p->GetDstNamePtr ();

      std::set<NNNAddress>::iterator it = m_names_in_ram.find(src->getName());

      if (it != m_names_in_ram.end ())
	{
	  du_p->SetSrcName (Ptr<const NNNAddress> (&(*it)));
	}
      else
	{
	  m_names_in_ram.insert (*src);
	}

      it = m_names_in_ram.find(dst->getName());

      if (it != m_names_in_ram.end ())
	{
	  du_p->SetDstName (Ptr<const NNNAddress> (&(*it)));
	}
      else
	{
	  m_names_in_ram.insert (*dst);
	}
      // --------

      //Give us a rw copy of the packet
      Ptr<Packet> icn_pdu = du_p->GetPayload ()->Copy ();

      // To be able to simplify code, convert pointer to common type
      Ptr<DATAPDU> dpdu = DynamicCast<DATAPDU> (du_p);

      bool forward = true;
      // Send the ACKP
      if (m_return_ACKP)
	{
	  forward = UpdateReceivedPDUs(dpdu, face, face->GetBroadcastAddress ());
	}

      if (forward)
	ProcessICNPDU (dpdu, face, icn_pdu);
      else
	NS_LOG_DEBUG ("Flowid: " << dpdu->GetFlowid () << " Sequence: " << dpdu->GetSequence () << " has already been received");
    }

    void
    ICN3NForwardingStrategy::OnLayeredDU (Ptr<Face> face, Ptr<DU> du_p, const Address& from,
			      const Address& to)
    {
      m_inDUs (du_p, face);
      NS_LOG_INFO ("From Face: " << face->GetId () << " via PoA: " << face->GetAddress() << " on " << GetNode3NName () << " got from PoA: " << from  << " to PoA: " << to << " headed from " << du_p->GetSrcName () << " to " << du_p->GetDstName () << " Flowid: " << du_p->GetFlowid () << " Sequence: " << du_p->GetSequence ());

      // ---- Attempt to lower memory usage ----
      Ptr<const NNNAddress> src = du_p->GetSrcNamePtr ();
      Ptr<const NNNAddress> dst = du_p->GetDstNamePtr ();

      std::set<NNNAddress>::iterator it = m_names_in_ram.find(src->getName());

      if (it != m_names_in_ram.end ())
	{
	  du_p->SetSrcName (Ptr<const NNNAddress> (&(*it)));
	}
      else
	{
	  m_names_in_ram.insert (*src);
	}

      it = m_names_in_ram.find(dst->getName());

      if (it != m_names_in_ram.end ())
	{
	  du_p->SetDstName (Ptr<const NNNAddress> (&(*it)));
	}
      else
	{
	  m_names_in_ram.insert (*dst);
	}
      // --------

      //Give us a rw copy of the packet
      Ptr<Packet> icn_pdu = du_p->GetPayload ()->Copy ();

      // To be able to simplify code, convert pointer to common type
      Ptr<DATAPDU> dpdu = DynamicCast<DATAPDU> (du_p);

      bool forward = true;
      // Send the ACKP
      if (m_return_ACKP)
	{
	  forward = UpdateReceivedPDUs(dpdu, face, from);
	}

      if (forward)
	ProcessLayeredICNPDU (dpdu, face, icn_pdu, from, to);
      else
	NS_LOG_DEBUG ("Flowid: " << dpdu->GetFlowid () << " Sequence: " << dpdu->GetSequence () << " has already been received");
    }

    void
    ICN3NForwardingStrategy::OnInterest (Ptr<Face> face, Ptr<Interest> interest_p)
    {
      NS_LOG_FUNCTION (this << face->GetId ());

      // Within the Interests, we have possible NACKs, separate
      // This is within the ICN flow, so the 3N PDU encapsulation is non-existent
      uint8_t res = interest_p->GetInterestType();
      if (Interest::MAP_ME_INTEREST_UPDATE <= res && res < Interest::NACK_LOOP)
	{
	  ProcessMapMe (0, face, interest_p);
	}
      else if (res >= Interest::NACK_LOOP)
	{
	  ProcessNack (0, face, interest_p);
	}
      else
	ProcessInterest (0, face, interest_p);
    }

    void
    ICN3NForwardingStrategy::OnLayeredInterest (Ptr<Face> face, Ptr<Interest> interest_p,
				    const Address& from, const Address& to)
    {
      NS_LOG_FUNCTION (this << face->GetId ());
      // Within the Interests, we have possible NACKs, separate
      // This is within the ICN flow, so the 3N PDU encapsulation is non-existent
      uint8_t res = interest_p->GetInterestType();
      if (Interest::MAP_ME_INTEREST_UPDATE <= res && res < Interest::NACK_LOOP)
	{
	  ProcessLayeredMapMe (0, face, interest_p, from);

	}
      else if (res >= Interest::NACK_LOOP)
	{
	  ProcessNack (0, face, interest_p);
	}
      else
	ProcessLayeredInterest (0, face, interest_p, from);
    }

    void
    ICN3NForwardingStrategy::OnData (Ptr<Face> face, Ptr<Data> data_p)
    {
      NS_LOG_FUNCTION (this << face->GetId ());
      // This is within the ICN flow, so the 3N PDU encapsulation is non-existent
      ProcessData (0, face, data_p);
    }

    void
    ICN3NForwardingStrategy::OnLayeredData (Ptr<Face> face, Ptr<Data> data_p,
				const Address& from, const Address& to)
    {
      NS_LOG_FUNCTION (this << face->GetId ());
      // This is within the ICN flow, so the 3N PDU encapsulation is non-existent
      ProcessLayeredData (0, face, data_p, from, to);
    }

    void
    ICN3NForwardingStrategy::ProcessICNPDU (Ptr<DATAPDU> pdu, Ptr<Face> face,
				Ptr<Packet> icn_pdu)
    {
      NS_LOG_FUNCTION (this << face->GetId ());
      bool receivedInterest =false;
      bool receivedData = false;
      Ptr<Interest> interest;
      Ptr<Data> data;

      try {
	  icn::HeaderHelper::Type icnType = icn::HeaderHelper::GetICNHeaderType(icn_pdu);
	  switch (icnType)
	  {
	    case icn::HeaderHelper::INTEREST_ICN:
	      interest = ns3::icn::Wire::ToInterest (icn_pdu, ns3::icn::Wire::WIRE_FORMAT_NDNSIM);
	      receivedInterest = true;
	      break;
	    case icn::HeaderHelper::CONTENT_OBJECT_ICN:
	      data = ns3::icn::Wire::ToData (icn_pdu, ns3::icn::Wire::WIRE_FORMAT_NDNSIM);
	      receivedData = true;
	      break;
	    default:
	      NS_FATAL_ERROR ("Not supported ICN header");
	  }
      }
      catch (icn::UnknownHeaderException)
      {
	  NS_FATAL_ERROR ("Unknown ICN header. Should not happen");
      }

      // If the PDU is an Interest
      if (receivedInterest)
	{
	  // Within the Interests, we have various possibilities
	  uint8_t res = interest->GetInterestType();

	  if (Interest::MAP_ME_INTEREST_UPDATE <= res && res < Interest::NACK_LOOP)
	    {
	      ProcessMapMe (pdu, face, interest);
	    }
	  else if (res >= Interest::NACK_LOOP)
	    {
	      ProcessNack (pdu, face, interest);
	    }
	  else
	    ProcessInterest (pdu, face, interest);
	}

      // If the PDU is Data
      if (receivedData)
	{
	  ProcessData (pdu, face, data);
	}
    }

    void
    ICN3NForwardingStrategy::ProcessLayeredICNPDU (Ptr<DATAPDU> pdu, Ptr<Face> face,
				       Ptr<Packet> icn_pdu, const Address& from,
				       const Address& to)
    {
      NS_LOG_FUNCTION (this << face->GetId ());
      bool receivedInterest =false;
      bool receivedData = false;
      Ptr<Interest> interest;
      Ptr<Data> data;

      try {
	  icn::HeaderHelper::Type icnType = icn::HeaderHelper::GetICNHeaderType(icn_pdu);
	  switch (icnType)
	  {
	    case icn::HeaderHelper::INTEREST_ICN:
	      interest = ns3::icn::Wire::ToInterest (icn_pdu, ns3::icn::Wire::WIRE_FORMAT_NDNSIM);
	      receivedInterest = true;
	      break;
	    case icn::HeaderHelper::CONTENT_OBJECT_ICN:
	      data = ns3::icn::Wire::ToData (icn_pdu, ns3::icn::Wire::WIRE_FORMAT_NDNSIM);
	      receivedData = true;
	      break;
	    default:
	      NS_FATAL_ERROR ("Not supported ICN header");
	  }
      }
      catch (icn::UnknownHeaderException)
      {
	  NS_FATAL_ERROR ("Unknown ICN header. Should not happen");
      }

      // If the PDU is an Interest
      if (receivedInterest)
	{
	  // Within the Interests, we have various possibilities
	  uint8_t res = interest->GetInterestType();

	  if (Interest::MAP_ME_INTEREST_UPDATE <= res && res < Interest::NACK_LOOP)
	    {
	      ProcessLayeredMapMe (pdu, face, interest, from);
	    }
	  else if (res >= Interest::NACK_LOOP)
	    {
	      ProcessNack (pdu, face, interest);
	    }
	  else
	    ProcessLayeredInterest (pdu, face, interest, from);
	}

      // If the PDU is Data
      if (receivedData)
	{
	  ProcessLayeredData (pdu, face, data, from, to);
	}
    }

    void
    ICN3NForwardingStrategy::UpdatePITEntry (Ptr<pit::Entry> pitEntry, Ptr<DATAPDU> pdu, Ptr<Face> face, Time lifetime)
    {
    }

    void
    ICN3NForwardingStrategy::UpdateLayeredPITEntry (Ptr<pit::Entry> pitEntry, Ptr<DATAPDU> pdu,
						    Ptr<Face> face, Time lifetime,
						    const Address& from)
    {
    }

    void
    ICN3NForwardingStrategy::UpdateFIBEntry (Ptr<DATAPDU> pdu, Ptr<Face> inface, Ptr<const icn::Name> interest)
    {
    }

    void
    ICN3NForwardingStrategy::UpdateLayeredFIBEntry (Ptr<DATAPDU> pdu, Ptr<Face> inface, Ptr<const icn::Name> interest, const Address& from, const Address& to)
    {
    }

    void
    ICN3NForwardingStrategy::ProcessInterest (Ptr<DATAPDU> pdu, Ptr<Face> face, Ptr<Interest> interest)
    {
      NS_LOG_FUNCTION (this << face->GetId () << face->GetFlags () << interest->GetName ());
      // Log the Interest PDU
      m_inInterests (interest, face);

      NNNAddress myAddr = GetNode3NName ();
      NS_LOG_INFO ("On " << myAddr << " processing Interest for SeqNo " << std::dec << interest->GetName ().get (-1).toSeqNum () <<
		   " in Flowid: " << pdu->GetFlowid() << " Sequence: " << pdu->GetSequence());

      // Search for the PIT with the Interest
      Ptr<pit::Entry> pitEntry = m_pit->Lookup (*interest);
      bool similarInterest = true;
      if (pitEntry == 0)
	{
	  // Flag that we have seen this Interest
	  similarInterest = false;
	  pitEntry = m_pit->Create (interest);
	  if (pitEntry != 0)
	    {
	      // Log the creation of a PIT entry
	      DidCreatePitEntry (face, interest, pitEntry);
	    }
	  else
	    {
	      // Log the failure to create a PIT entry
	      FailedToCreatePitEntry (face, interest);
	      // Finish processing
	      return;
	    }
	}

      bool isDuplicated = true;
      // Check if the Interest has been seen before
      if (!pitEntry->IsNonceSeen (interest->GetNonce ()))
	{
	  // Not seen, add the Nonce
	  pitEntry->AddSeenNonce (interest->GetNonce ());
	  // Flag that we have a duplicated object
	  isDuplicated = false;
	}

      if (isDuplicated)
	{
	  // Log that we received a duplicate Interest
	  DidReceiveDuplicateInterest (face, interest, pitEntry);
	  // Finish processing
	  return;
	}

      // Check if we have this particular Interest in the ContentStore
      Ptr<Data> contentObject = m_contentStore->Lookup (interest);
      if (contentObject != 0)
	{
	  NS_LOG_INFO ("CS has Interest, attempting to satisfy");
	  // Update the PIT
	  UpdatePITEntry(pitEntry, pdu, face, Seconds(1));

	  // Do data plane performance measurements
	  WillSatisfyPendingInterest (0, pitEntry);

	  // Actually satisfy pending interest
	  if (IsSharingPoA())
	    {
	      SatisfyPendingLayeredInterest(pdu, 0, contentObject, pitEntry);
	    }
	  else
	    {
	      SatisfyPendingInterest (pdu, 0, contentObject, pitEntry);
	    }
	  return;
	}

      // In the case of similar Interest, update the PIT entry
      if (similarInterest && ShouldSuppressIncomingInterest (face, interest, pitEntry))
	{
	  // Update the PIT
	  UpdatePITEntry(pitEntry, pdu, face, interest->GetInterestLifetime());

	  // Suppress this interest if we're still expecting data from some other face
	  NS_LOG_DEBUG ("Suppress interests");
	  m_dropInterests (interest, face);

	  DidSuppressSimilarInterest (face, interest, pitEntry);
	  return;
	}

      if (similarInterest)
	{
	  // Log that we obtained a similar Interest
	  DidForwardSimilarInterest (face, interest, pitEntry);
	}

      if (IsSharingPoA())
	{
	  PropagateLayeredInterest (pdu, face, interest, pitEntry, Address());
	}
      else
	{
	  PropagateInterest (pdu, face, interest, pitEntry);
	}
    }

    void
    ICN3NForwardingStrategy::ProcessLayeredInterest (Ptr<DATAPDU> pdu, Ptr<Face> face,
						     Ptr<Interest> interest,
						     const Address& from)
    {
      NS_LOG_FUNCTION (this << face->GetId () << face->GetFlags () << interest->GetName ());
      // Log the Interest PDU
      m_inInterests (interest, face);

      NNNAddress myAddr = GetNode3NName ();
      NS_LOG_INFO ("On " << myAddr << " processing Interest for SeqNo " << std::dec << interest->GetName ().getSeqNum () <<
		   " From PoA: " << from << " in Flowid: " << pdu->GetFlowid() << " Sequence: " << pdu->GetSequence());

      // Search for the PIT with the Interest
      Ptr<pit::Entry> pitEntry = m_pit->Lookup (*interest);
      bool similarInterest = true;
      if (pitEntry == 0)
	{
	  // Flag that we have seen this Interest
	  similarInterest = false;
	  pitEntry = m_pit->Create (interest);
	  if (pitEntry != 0)
	    {
	      // Log the creation of a PIT entry
	      DidCreatePitEntry (face, interest, pitEntry);
	    }
	  else
	    {
	      // Log the failure to create a PIT entry
	      FailedToCreatePitEntry (face, interest);
	      // Finish processing
	      return;
	    }
	}

      bool isDuplicated = true;
      // Check if the Interest has been seen before
      if (!pitEntry->IsNonceSeen (interest->GetNonce ()))
	{
	  // Not seen, add the Nonce
	  pitEntry->AddSeenNonce (interest->GetNonce ());
	  // Flag that we have a duplicated object
	  isDuplicated = false;
	}

      if (isDuplicated)
	{
	  // Log that we received a duplicate Interest
	  DidReceiveDuplicateLayeredInterest (pdu, face, interest, pitEntry, from);
	  // Finish processing
	  return;
	}

      // Check if we have this particular Interest in the ContentStore
      Ptr<Data> contentObject = m_contentStore->Lookup (interest);
      if (contentObject != 0)
	{
	  NS_LOG_INFO ("CS has Interest, attempting to satisfy");
	  // Update the PIT
	  UpdateLayeredPITEntry(pitEntry, pdu, face, Seconds(1), from);

	  // Do data plane performance measurements
	  WillSatisfyPendingLayeredInterest (0, pitEntry, from);

	  // Actually satisfy pending interest
	  SatisfyPendingLayeredInterest (pdu, 0, contentObject, pitEntry);
	  return;
	}

      // In the case of similar Interest, update the PIT entry
      if (similarInterest && ShouldSuppressIncomingInterest (face, interest, pitEntry))
	{
	  // Update the PIT
	  UpdateLayeredPITEntry(pitEntry, pdu, face, interest->GetInterestLifetime(), from);

	  // Suppress this interest if we're still expecting data from some other face
	  NS_LOG_DEBUG ("Suppress interests");
	  m_dropInterests (interest, face);

	  DidSuppressSimilarInterest (face, interest, pitEntry);
	  return;
	}

      if (similarInterest)
	{
	  // Log that we obtained a similar Interest
	  DidForwardSimilarInterest (face, interest, pitEntry);
	}

      PropagateLayeredInterest (pdu, face, interest, pitEntry, from);
    }

    void
    ICN3NForwardingStrategy::ProcessData (Ptr<DATAPDU> pdu, Ptr<Face> face, Ptr<Data> data)
    {
      NS_LOG_FUNCTION (this << face->GetId () << face->GetFlags () << data->GetName ());
      // Log the Data PDU
      m_inData (data, face);

      NNNAddress myAddr = GetNode3NName ();

      NS_LOG_INFO ("On " << myAddr << " processing DATA " << data->GetName () << " SeqNo " << std::dec << data->GetName ().get (-1).toSeqNum () <<
		   " in Flowid: " << pdu->GetFlowid() << " Sequence: " << pdu->GetSequence());

      // Lookup PIT entry
      Ptr<pit::Entry> pitEntry = m_pit->Lookup (*data);

      // Using 3N, we allow all data to be cached - will probably need to be discussed
      if (pitEntry != 0)
	{
	  // We could update the FIB table
	  if (m_EnableFIBUpdates)
	    UpdateFIBEntry (pdu, face, data->GetNamePtr ());

	  // Add to content store
	  m_contentStore->Add (data);
	  // Log that this node actually asked for this data
	  DidReceiveSolicitedData (face, data, true);
	}
      else
	{
	  // Log that this node is proactively caching this data
	  DidReceiveUnsolicitedData (face, data, true);
	  if (m_EnableSavingUnrequestedData)
	    {
	      // Add to content store
	      m_contentStore->Add (data);
	      NS_LOG_INFO ("Storing unsolicited name: " << data->GetName() << " SeqNo " << data->GetName ().get (-1).toSeqNum () << " in CS");
	    }

	  DoPropagateData(pdu, face, data);
	}

      while (pitEntry != 0)
	{
	  // Do data plane performance measurements
	  WillSatisfyPendingInterest (face, pitEntry);

	  if (IsSharingPoA())
	    {
	      NS_LOG_DEBUG ("Sharing PoA information");
	      SatisfyPendingLayeredInterest(pdu, face, data, pitEntry);
	    }
	  else
	    {
	      NS_LOG_DEBUG ("Not sharing PoA information");
	      // Actually satisfy pending interest
	      SatisfyPendingInterest (pdu, face, data, pitEntry);
	    }

	  // Lookup another PIT entry
	  pitEntry = m_pit->Lookup (*data);
	}
    }

    void
    ICN3NForwardingStrategy::ProcessLayeredData (Ptr<DATAPDU> pdu, Ptr<Face> face, Ptr<Data> data,
						 const Address& from, const Address& to)
    {
      // Log the Data PDU
      m_inData (data, face);

      NNNAddress myAddr = GetNode3NName ();

      NS_LOG_INFO ("On " << myAddr << " processing DATA " << data->GetName () << " SeqNo " << std::dec << data->GetName ().get (-1).toSeqNum () <<
		   " From PoA: " << from << " in Flowid: " << pdu->GetFlowid() << " Sequence: " << pdu->GetSequence());

      // If using MapMe or FIB table updates are available, update the FIB
      if (m_EnableFIBUpdates)
	UpdateLayeredFIBEntry (pdu, face, data->GetNamePtr (), from, to);

      // Lookup PIT entry
      Ptr<pit::Entry> pitEntry = m_pit->Lookup (*data);

      // Using 3N, we allow all data to be cached - will probably need to be discussed
      if (pitEntry != 0)
	{
	  // Add to content store
	  m_contentStore->Add (data);
	  // Log that this node actually asked for this data
	  DidReceiveSolicitedData (face, data, true);
	}
      else
	{
	  // Log that this node is proactively caching this data
	  DidReceiveUnsolicitedData (face, data, true);

	  if (m_EnableSavingUnrequestedData)
	    {
	      // Add to content store
	      m_contentStore->Add (data);
	      NS_LOG_INFO ("Storing unsolicited name: " << data->GetName() << " SeqNo " << data->GetName ().get (-1).toSeqNum () << " in CS");
	    }

	  DoPropagateData(pdu, face, data);
	}

      while (pitEntry != 0)
	{
	  // Do data plane performance measurements
	  WillSatisfyPendingLayeredInterest (face, pitEntry, from);

	  // Actually satisfy pending interest
	  SatisfyPendingLayeredInterest (pdu, face, data, pitEntry);

	  // Lookup another PIT entry
	  pitEntry = m_pit->Lookup (*data);
	}
    }

    void
    ICN3NForwardingStrategy::ProcessNack (Ptr<DATAPDU> pdu, Ptr<Face> face, Ptr<Interest> nack)
    {
      NS_LOG_FUNCTION (this << face->GetId () << face->GetFlags () << nack->GetName ());

      m_inNacks(nack, face);

      NNNAddress myAddr = GetNode3NName ();

      NS_LOG_INFO ("On " << myAddr << " processing NACK SeqNo " << std::dec << nack->GetName ().get (-1).toSeqNum ());

      // Pointers and flags for PDU types
      Ptr<NULLp> nullp_i;
      bool wasNULL = false;
      Ptr<SO> so_i;
      bool wasSO = false;
      Ptr<DO> do_i;
      bool wasDO = false;
      Ptr<DU> du_i;
      bool wasDU = false;

      if (pdu != 0)
	{
	  uint32_t pduid = pdu->GetPacketId();
	  switch(pduid)
	  {
	    case NULL_NNN:
	      // Convert pointer to NULLp PDU
	      nullp_i = DynamicCast<NULLp> (pdu);
	      wasNULL = true;
	      break;
	    case SO_NNN:
	      // Convert pointer to SO PDU
	      so_i = DynamicCast<SO> (pdu);
	      wasSO = true;
	      break;
	    case DO_NNN:
	      // Convert pointer to DO PDU
	      do_i = DynamicCast<DO> (pdu);
	      wasDO = true;
	      break;
	    case DU_NNN:
	      // Convert pointer to DU PDU
	      du_i = DynamicCast<DU> (pdu);
	      wasDU = true;
	      break;
	    default:
	      break;
	  }
	}

      Ptr<pit::Entry> pitEntry = m_pit->Lookup(*nack);
      if (pitEntry == 0)
	{
	  m_dropNacks (nack, face);
	  if (pdu != 0)
	    {
	      if (wasNULL)
		m_dropNULLps (nullp_i, face);
	      else if (wasSO)
		m_dropSOs (so_i, face);
	      else if (wasDO)
		m_dropDOs (do_i, face);
	      else if (wasDU)
		m_dropDUs (du_i, face);
	    }
	  return;
	}

      DidReceiveValidNack (pdu, face, nack->GetInterestType (), nack, pitEntry);
    }

    void
    ICN3NForwardingStrategy::ProcessMapMe (Ptr<DATAPDU> pdu, Ptr<Face> inFace, Ptr<Interest> mapme)
    {
      NS_LOG_FUNCTION (this);

      // Log the Interest packet
      m_inInterests (mapme, inFace);

      // Verify if the TFIB is enabled
      if (m_EnableTFIB)
	{
	  Ptr<const NNNAddress> gotten_src = 0;

	  if (mapme->GetInterestType () == Interest::MAP_ME_INTEREST_UPDATE)
	    {
	      NS_LOG_INFO ("Received an Interest with MapMe Update via Face " << inFace->GetId ());
	      NS_LOG_INFO ("Updating TFIB and FIB for " << mapme->GetName () << " with sequence " << mapme->GetSequenceNumber ());

	      Ptr<tfib::Entry> tfib_entry = m_tfib->Find (mapme->GetName ());

	      NS_LOG_DEBUG ("Previous " << *m_tfib);
	      bool forwardMapMe = false;
	      // If we find no information in the TFIB for this information, we
	      // can just go ahead and add the information to the FIB
	      if (tfib_entry == 0)
		{
		  NS_LOG_INFO ("Completely new info for prefix: " << mapme->GetName ());
		  NS_LOG_DEBUG ("Introducing name: " << mapme->GetName () << " Face: " << inFace->GetId ());
		  NS_LOG_DEBUG ("Sequence: " << mapme->GetSequenceNumber () << " Retransmission: " << mapme->GetRetransmissionTime ());
		  NS_LOG_DEBUG ("Part: " << mapme->GetPartNumber ());

		  m_tfib->Add(inFace, Address (), mapme, 0);

		  forwardMapMe = true;
		}
	      else
		{
		  if (tfib_entry->GetNewestSequence () < mapme->GetSequenceNumber ())
		    {
		      NS_LOG_INFO ("Updating new information for prefix: " << mapme->GetName ());
		      NS_LOG_DEBUG ("New sequence for for prefex: " << mapme->GetName () << " is: " << mapme->GetSequenceNumber ());

		      // Found new information, updating TFIB
		      tfib_entry->AddOrUpdate(inFace, Address (), mapme, 0);

		      forwardMapMe = true;
		    }
		  else
		    {
		      NS_LOG_WARN ("Obtained an MapMe Interest with an outdated sequence number");
		      m_dropInterests (mapme, inFace);
		    }
		}

	      NS_LOG_DEBUG ("Current " << *m_tfib);

	      // If we have new information, forward the Interest with MapMe information
	      if (forwardMapMe)
		{
		  // Now set up the ACKs and forward the information
		  // Search for the temporary PIT with the Interest
		  Ptr<pit::Entry> mapme_ack_Entry = m_awaiting_mapme_acks->Lookup (*mapme);
		  if (mapme_ack_Entry == 0)
		    {
		      mapme_ack_Entry = m_awaiting_mapme_acks->Create (mapme);
		      if (mapme_ack_Entry != 0)
			{
			  NS_LOG_INFO ("Created entry to wait for MapMe ACK for " << mapme->GetName () << " SeqN: " << mapme->GetPartNumber());
			}
		      else
			{
			  NS_LOG_WARN ("Couldn't create entry to wait for MapMe ACK for " << mapme->GetName () << " SeqN: " << mapme->GetPartNumber());
			  return;
			}
		    }
		  else
		    {
		      NS_LOG_WARN ("We already have an ACK waiting for " << mapme->GetName() << " SeqN: " << mapme->GetPartNumber() << " check logs");
		    }

		  // Transmit a MapMe Interest packet
		  RetransmitMapMe(pdu, inFace, mapme);

		  // Create a MapMe ACK to deal with the Awaiting PIT structure
		  TransmitMapMeAck(pdu, mapme_ack_Entry->GetInterest (), inFace);
		}
	    }
	  else if (mapme->GetInterestType () == Interest::MAP_ME_INTEREST_UPDATE_ACK)
	    {
	      NS_LOG_INFO ("Received an Interest with MapMe ACK via Face " << inFace->GetId ());
	      NS_LOG_INFO ("Obtained an ACK for Interest MapMe: " << mapme->GetName ());

	      MapMeAckProcess(pdu, inFace, mapme);
	    }
	  else
	    {
	      NS_LOG_WARN ("Obtained an MapMe Interest packet with unknown type!");
	      m_dropInterests (mapme, inFace);
	    }
	}
      else
	{
	  NS_LOG_WARN ("Obtained an MapMe Interest packet, but node does not support TFIB");
	  m_dropInterests (mapme, inFace);
	}
    }

    void
    ICN3NForwardingStrategy::MapMeAckProcess (Ptr<DATAPDU> pdu, Ptr<Face> face, Ptr<Interest> mapme)
    {
      NS_LOG_FUNCTION (this);

      Ptr<pit::Entry> pitEntry = m_awaiting_mapme_acks->Lookup (*mapme);

      if (pitEntry != 0)
	{
	  Ptr<tfib::Entry> tfib_entry = m_tfib->Find(mapme->GetName ());

	  if (tfib_entry == 0)
	    {
	      NS_LOG_WARN ("Did not find a TFIB entry for " << mapme->GetName ());
	      return;
	    }

	  uint32_t curr_seq = mapme->GetSequenceNumber ();

	  NS_LOG_DEBUG ("Current " << *m_tfib);

	  if (!tfib_entry->HasSequence(curr_seq))
	    {
	      NS_LOG_WARN ("Interest MapMe ACK is using a SeqN: " << curr_seq << " for which we have no information, returning");
	      return;
	    }
	  else
	    {
	      uint32_t nst_seq = tfib_entry->GetNewestSequence();
	      if (curr_seq != nst_seq)
		{
		  NS_LOG_WARN ("Interest MapMe ACK received with SeqN: " << curr_seq << " but newest is SeqN: " << nst_seq << " verify");
		}
	      else
		{
		  NS_LOG_DEBUG ("Interest MapMe ACK is using a SeqN: " << curr_seq);
		}
	    }

	  NS_LOG_DEBUG ("Removing Face " << face->GetId () << " from Interest MapMe ACK waiting list for " << mapme->GetName ());
	  // Remove the MapMe information from the PIT
	  pitEntry->RemoveOutgoing(face);

	  icn::Name mapmed_name = mapme->GetName ().getBaseName ();

	  if (pitEntry->GetOutgoingCount() == 0)
	    {
	      NS_LOG_DEBUG ("Have obtained all Interest MapMe ACKs, removing entries");
	      // If we have obtained all the ACKs we were waiting for, mark for erase
	      pitEntry->ClearIncoming ();
	      m_awaiting_mapme_acks->MarkErased(pitEntry);

	      // Having ACKed all MapMe information, proceed to update FIB
	      Ptr<fib::Entry> fib_entry = m_fib->Find(mapmed_name);

	      NS_LOG_DEBUG ("Previous " << *m_fib);

	      // Update FIB information
	      if (fib_entry == 0)
		{

		  NS_LOG_DEBUG ("Did not find information for " << mapmed_name << " introducing to FIB");
		  // Didn't find an exact match, introduce it
		  fib_entry = m_fib->Add(mapmed_name, tfib_entry->GetFace (curr_seq), m_icn_fib_metric);
		}
	      else
		{
		  NS_LOG_DEBUG ("Found match for " << mapmed_name << " updating FIB with Face: " << tfib_entry->GetFace (curr_seq));
		  // Found an exact match, update it
		  fib_entry->AddOrUpdateRoutingMetric(tfib_entry->GetFace (curr_seq), m_icn_fib_metric);
		}

	      fib_entry->UpdateStatus(tfib_entry->GetFace (curr_seq), fib::FaceMetric::ICN_FIB_GREEN);

	      NS_LOG_DEBUG ("Current " << *m_fib);
	    }
	  else
	    {
	      NS_LOG_DEBUG ("Still waiting for " << pitEntry->GetOutgoingCount () << " ACKs for SeqN: " << curr_seq);
	    }
	}
      else
	{
	  NS_LOG_WARN ("We are not waiting for an ACK for MapMe: " << mapme->GetName ());
	  m_dropInterests (mapme, face);
	}
    }

    void
    ICN3NForwardingStrategy::ProcessLayeredMapMe (Ptr<DATAPDU> pdu, Ptr<Face> inFace,
						  Ptr<Interest> mapme, const Address& from)
    {
    }

    void
    ICN3NForwardingStrategy::RetransmitMapMe (Ptr<DATAPDU> pdu, Ptr<Face> inFace,
					      Ptr<Interest> mapme)
    {
      NS_LOG_FUNCTION (this);
      // Check if the Entry continues to exist in the structure
      NS_LOG_DEBUG ("Verifying that we are waiting for an ACK for " << mapme->GetName ());
      Ptr<pit::Entry> mapme_ack_Entry = m_awaiting_mapme_acks->Lookup (*mapme);

      Ptr<Face> tmp;
      bool ok = false;

      if (mapme_ack_Entry != 0)
	{
	  NS_LOG_DEBUG ("We are waiting for a ACK, beginning forwarding");
	  // Forward the MapMe information
	  if (!pdu)
	    {
	      NS_LOG_INFO ("Attempting to send MapMe directly in ICN");
	      // This is a special case because the FIB was already modified when the
	      // Application was attached to the node
	      if (inFace->isAppFace())
		{
		  NS_LOG_INFO ("Received the MapMe Interest from Application");
		  // Transmit the MapMe through all the interfaces not of Application type
		  for (uint32_t i = 0; i < m_faces->GetN (); i++)
		    {
		      tmp = m_faces->Get (i);
		      // Check that the Face is not of type APPLICATION
		      if (!tmp->isAppFace ())
			{
			  if (tmp->IsNetworkCompatibilityEnabled ("ICN"))
			    {
			      NS_LOG_INFO ("Sending Interest MapMe Update via face " << tmp->GetId ());
			      if (m_useQueues)
				ok = EnqueueInterest(tmp, tmp->GetBroadcastAddress(), mapme);
			      else
				ok = tmp->SendInterest (mapme);

			      if (!ok)
				{
				  m_dropInterests (mapme, tmp);
				  NS_LOG_DEBUG ("Cannot satisfy Interest MapMe Update to " << tmp->GetId ());
				}
			      else
				{
				  // Log that a Interest packet was sent
				  m_outInterests (mapme, tmp);
				  mapme_ack_Entry->AddOutgoing (tmp);
				}
			    }
			  else
			    {
			      NS_LOG_WARN ("Attempted to send raw ICN through unsupported Face");
			    }
			}
		    }
		}
	      else
		{
		  NS_LOG_INFO ("Received the MapMe Interest from another node");
		  Ptr<icn::Name> mapmed_name = Create<icn::Name> (mapme->GetName ().getBaseName ());
		  NS_LOG_DEBUG ("Searching in FIB for " << *mapmed_name);
		  NS_LOG_DEBUG ("Current " << *m_fib);

		  Ptr<fib::Entry> fib_entry = m_fib->LongestPrefixMatch(mapmed_name);

		  if (!fib_entry)
		    {
		      NS_LOG_DEBUG ("No information in FIB for " << *mapmed_name << " returning");
		      return;
		    }

		  // Here we pick the next place to forward to using the ICN strategy
		  BOOST_FOREACH (const fib::FaceMetric &metricFace, fib_entry->m_faces.get<fib::i_metric> ())
		  {
		    tmp = metricFace.GetFace ();

		    if (tmp->isAppFace())
		      {
			continue;
		      }

		    if (tmp->GetId () == inFace->GetId ())
		      {
			continue;
		      }

		    if (tmp->IsNetworkCompatibilityEnabled ("ICN"))
		      {
			NS_LOG_INFO ("Sending MapMe Interest Update via Face " << tmp->GetId ());
			// Send out the Data PDU
			if (m_useQueues)
			  ok = EnqueueInterest(tmp, tmp->GetBroadcastAddress(), mapme);
			else
			  ok = tmp->SendInterest (mapme);

			if (!ok)
			  {
			    m_dropInterests (mapme, tmp);
			    NS_LOG_DEBUG ("Cannot satisfy Interest to " << tmp->GetId ());
			  }
			else
			  {
			    // Log that a Interest packet was sent
			    m_outInterests (mapme, tmp);
			    mapme_ack_Entry->AddOutgoing (tmp);
			  }
		      }
		    else
		      {
			NS_LOG_WARN ("Attempted to send raw ICN through unsupported Face");
		      }
		  }
		}
	    }
	  else
	    {
	      NS_LOG_INFO ("Attempting to send MapMe in 3N PDU");

	      NS_LOG_DEBUG ("Serializing the Interest");
	      // Encode the packet
	      Ptr<Packet> retPkt = icn::Wire::FromInterest(mapme, icn::Wire::WIRE_FORMAT_NDNSIM);

	      if (inFace->isAppFace())
		{
		  // This is a special case because the FIB was already modified when the
		  // Application was attached to the node
		  NS_LOG_INFO ("Received the Interest MapMe Update from Application");

		  NS_LOG_INFO ("Creating NULLp in which to send MapMe Interest");
		  Ptr<NULLp> nullp_o = Create<NULLp> ();
		  nullp_o->SetPDUPayloadType (ICN_NNN);
		  nullp_o->SetLifetime (m_3n_lifetime);
		  nullp_o->SetPayload (retPkt);

		  // Transmit the MapMe through all the interfaces not of Application type
		  for (uint32_t i = 0; i < m_faces->GetN (); i++)
		    {
		      tmp = m_faces->Get (i);
		      // Check that the Face is not of type APPLICATION
		      if (tmp->isAppFace ())
			{
			  continue;
			}

		      // We can't return the packet to the same Face
		      if (inFace->GetId () == tmp->GetId ())
			{
			  continue;
			}

		      if (m_ask_ACKP)
			{
			  int8_t n3ok = txPreparation (nullp_o, tmp, tmp->GetBroadcastAddress(), false);
			  ok = (n3ok >= 0);
			}
		      else
			{
			  txReset(nullp_o);
			  NS_LOG_INFO ("Sending Interest MapMe Update in NULLp via Face " << tmp->GetId ());
			  if (m_useQueues)
			    ok = EnqueueNULLp(tmp, tmp->GetBroadcastAddress(), nullp_o);
			  else
			    ok = tmp->SendNULLp (nullp_o);

			  UpdatePDUCountersB(nullp_o, tmp, ok, true);
			}

		      if (!ok)
			{
			  DidDropInterest(tmp, mapme);
			}
		      else
			{
			  NS_LOG_DEBUG ("Transmitted NULLp with MapMe Update via Face " << tmp->GetId ());
			  m_outInterests (mapme, tmp);
			  mapme_ack_Entry->AddOutgoing (tmp);
			}
		    }
		}
	      else
		{
		  NS_LOG_INFO ("Received the MapMe Interest from another node");
		  Ptr<icn::Name> mapmed_name = Create<icn::Name> (mapme->GetName ().getBaseName ());
		  NS_LOG_DEBUG ("Searching in FIB for " << mapmed_name);
		  NS_LOG_DEBUG ("Current " << *m_fib);

		  Ptr<fib::Entry> fib_entry = m_fib->LongestPrefixMatch(mapmed_name);
		  if (!fib_entry)
		    {
		      NS_LOG_DEBUG ("No information in FIB for " << *mapmed_name << " returning");
		      return;
		    }

		  Ptr<SO> so_o;
		  Ptr<NULLp> nullp_o;
		  Ptr<DATAPDU> pdu_o;
		  bool useSO = false;
		  //		  bool useNULL = false;
		  Ptr<DO> do_i;
		  Ptr<DU> du_i;
		  bool wasDO = false;
		  //		  bool wasDU = false;
		  bool forwardAsis = false;

		  // If I received a NULLp PDU, then we can become the main 3N node, send in SO
		  if (pdu->GetPacketId () == NULL_NNN)
		    {
		      if (Has3NName() && m_use3NName)
			{
			  NS_LOG_INFO ("" << GetNode3NName () << " will attempt to become temporal anchor for " << *mapmed_name);
			  so_o = Create<SO> ();
			  so_o->SetLifetime (m_3n_lifetime);
			  so_o->SetPDUPayloadType (ICN_NNN);
			  so_o->SetName (GetNode3NNamePtr ());
			  so_o->SetPayload (retPkt);
			  useSO = true;

			  pdu_o = DynamicCast <DATAPDU> (so_o);
			}
		      else
			{
			  NS_LOG_INFO ("Creating NULLp in which to send MapMe Interest");
			  nullp_o = Create<NULLp> ();
			  nullp_o->SetPDUPayloadType (ICN_NNN);
			  nullp_o->SetLifetime (m_3n_lifetime);
			  nullp_o->SetPayload (retPkt);

			  pdu_o = DynamicCast <DATAPDU> (nullp_o);
			}
		    }
		  else if (pdu->GetPacketId () == SO_NNN)
		    {
		      so_o = DynamicCast <SO> (pdu);
		      NS_LOG_INFO ("Forwarding SO received with " << so_o->GetName() << " as temporal anchor for " << *mapmed_name);
		      useSO = true;

		      pdu_o = DynamicCast <DATAPDU> (so_o);
		    }
		  else
		    {
		      switch (pdu->GetPacketId())
		      {
			case DO_NNN:
			  do_i = DynamicCast<DO> (pdu);
			  wasDO = true;
			  break;
			case DU_NNN:
			  du_i =DynamicCast<DU> (pdu);
			  //			  wasDU = true;
			  break;
			default:
			  NS_LOG_WARN ("Obtained unrecognized 3N PDU. Check code");
			  return;
		      }

		      if (wasDO)
			{
			  if (GoesBy3NName (do_i->GetNamePtr ()))
			    {
			      NS_LOG_INFO ("" << GetNode3NName () << " will attempt to become temporal anchor for " << *mapmed_name);
			      so_o = Create<SO> ();
			      so_o->SetLifetime (m_3n_lifetime);
			      so_o->SetPDUPayloadType (ICN_NNN);
			      so_o->SetName (GetNode3NNamePtr ());
			      so_o->SetPayload (retPkt);

			      useSO = true;
			      pdu_o = DynamicCast <DATAPDU> (so_o);
			    }
			  else
			    {
			      forwardAsis = true;
			      pdu_o = DynamicCast <DATAPDU> (do_i);
			    }
			}
		      else
			{
			  if (GoesBy3NName (du_i->GetDstNamePtr ()))
			    {
			      NS_LOG_INFO ("" << GetNode3NName () << " will attempt to become temporal anchor for " << *mapmed_name);
			      so_o = Create<SO> ();
			      so_o->SetLifetime (m_3n_lifetime);
			      so_o->SetPDUPayloadType (ICN_NNN);
			      so_o->SetName (GetNode3NNamePtr ());
			      so_o->SetPayload (retPkt);

			      useSO = true;
			      pdu_o = DynamicCast <DATAPDU> (so_o);
			    }
			  else
			    {
			      forwardAsis = true;
			      pdu_o = DynamicCast <DATAPDU> (du_i);
			    }
			}
		    }

		  // Here we pick the next place to forward to using the ICN strategy
		  BOOST_FOREACH (const fib::FaceMetric &metricFace, fib_entry->m_faces.get<fib::i_metric> ())
		  {
		    tmp = metricFace.GetFace ();

		    if (tmp->isAppFace())
		      {
			continue;
		      }

		    if (tmp->GetId () == inFace->GetId ())
		      {
			continue;
		      }

		    bool normalSend = true;

		    if (m_ask_ACKP)
		      {
			std::vector<Ptr<const NNNAddress> > ret = m_nnst->OneHopNameInfo(GetNode3NNamePtr(), tmp);

			if (!ret.empty())
			  {
			    normalSend = false;
			    int8_t n3ok = txPreparation (pdu_o, tmp, tmp->GetBroadcastAddress (), false);

			    ok = (n3ok >= 0);
			  }
			else
			  {
			    NS_LOG_WARN ("Face " << inFace->GetId () << " has no apparent 3N nodes, not expecting ACKPs");
			    SetupFlowidSeq(pdu_o, tmp, tmp->GetBroadcastAddress (), false);
			    UpdateSentPDUs(pdu_o, tmp, tmp->GetBroadcastAddress (), false);
			  }
		      }
		    else
		      {
			txReset(pdu_o);
		      }

		    if (normalSend)
		      {
			if (useSO)
			  {
			    NS_LOG_INFO ("Sending MapMe Interest MapMe Update in SO via Face " << tmp->GetId ());
			    if (m_useQueues)
			      ok = EnqueueSO(tmp, tmp->GetBroadcastAddress(), so_o);
			    else
			      ok = tmp->SendSO (so_o);
			  }
			else if (forwardAsis)
			  {
			    if (wasDO)
			      {
				NS_LOG_INFO ("Sending MapMe Interest MapMe Update in DO via Face " << tmp->GetId ());
				if (m_useQueues)
				  ok =  EnqueueDO(tmp, tmp->GetBroadcastAddress (), do_i);
				else
				  ok = tmp->SendDO (do_i);
			      }
			    else
			      {
				NS_LOG_INFO ("Sending MapMe Interest MapMe Update in DU via Face " << tmp->GetId ());
				if (m_useQueues)
				  ok =  EnqueueDU(tmp, tmp->GetBroadcastAddress (), du_i);
				else
				  ok = tmp->SendDU (du_i);
			      }
			  }
			else
			  {
			    NS_LOG_INFO ("Sending MapMe Interest MapMe Update in NULLp via Face " << tmp->GetId ());
			    if (m_useQueues)
			      ok = EnqueueNULLp(tmp, tmp->GetBroadcastAddress(), nullp_o);
			    else
			      ok = tmp->SendNULLp (nullp_o);
			  }

			UpdatePDUCountersB (pdu_o, tmp, ok, false);
		      }

		    if (!ok)
		      {
			m_dropInterests (mapme, tmp);
		      }
		    else
		      {
			if (useSO)
			  {
			    NS_LOG_DEBUG ("Transmitted SO with Interest MapMe Update via Face " << tmp->GetId ());
			  }
			else if (forwardAsis)
			  {
			    if (wasDO)
			      {
				NS_LOG_DEBUG ("Transmitted DO with Interest MapMe Update via Face " << tmp->GetId ());
			      }
			    else
			      {
				NS_LOG_DEBUG ("Transmitted DO with Interest MapMe Update via Face " << tmp->GetId ());
			      }
			  }
			else
			  {
			    NS_LOG_DEBUG ("Transmitted SO with Interest MapMe Update via Face " << tmp->GetId ());
			  }
			m_outInterests (mapme, tmp);
			mapme_ack_Entry->AddOutgoing (tmp);
		      }
		  }
		}
	    }

	  mapme_ack_Entry->IncreaseCurrentRetransmissions();
	  uint32_t currTransmissions = mapme_ack_Entry->GetCurrentRetransmissions ();
	  NS_LOG_DEBUG ("Current number of MapMe attempts for: " << mapme->GetName() << " at: " << currTransmissions);

	  if (currTransmissions <= m_mapme_retrys)
	    {
	      // Schedule retransmission of MapMe should we get no ACK
	      NS_LOG_DEBUG ("Scheduling retransmission of MapMe Interest, should we get no ACK in " << mapme->GetRetransmissionTime());
	      Simulator::Schedule(mapme->GetRetransmissionTime(), &ICN3NForwardingStrategy::RetransmitMapMe, this, pdu, inFace, mapme);
	    }
	  else
	    {
	      NS_LOG_DEBUG ("Exceeded permitted number of Interest with MapMe info retries at: " << m_mapme_retrys);

	      NS_LOG_DEBUG ("Clearing Faces which were waiting for ACKs");
	      mapme_ack_Entry->ClearOutgoing();

	      MapMeAckProcess(pdu, inFace, mapme);
	    }
	}
      else
	{
	  NS_LOG_INFO (mapme->GetName () << " MapMe Interest, has been acked, cancelling retransmission");
	  return;
	}
    }

    void
    ICN3NForwardingStrategy::RetransmitLayeredMapMe (Ptr<DATAPDU> pdu, Ptr<Face> face,
						     Ptr<Interest> mapme, const Address& from)
    {
    }

    void
    ICN3NForwardingStrategy::TransmitMapMeAck (Ptr<DATAPDU> pdu, Ptr<const Interest> mapme,
					       Ptr<Face> face)
    {
      NS_LOG_FUNCTION (this);

      if (face->isAppFace())
	{
	  NS_LOG_INFO ("Received ACK from Application, no need to ACK");
	  return;
	}

      Ptr<Interest> interest = Create<Interest> ();
      interest->SetNonce               (uniRandom->GetValue());
      interest->SetName                (mapme->GetName ());
      interest->SetInterestLifetime    (mapme->GetInterestLifetime ());
      interest->SetInterestType        (Interest::MAP_ME_INTEREST_UPDATE_ACK);
      interest->SetSequenceNumber      (mapme->GetSequenceNumber ());
      interest->SetRetransmissionTime  (mapme->GetRetransmissionTime ());
      interest->SetPartNumber          (mapme->GetPartNumber ());

      NS_LOG_INFO ("Transmitting Interest MapMe ACK with name: " << mapme->GetName () << " SeqN: " << mapme->GetSequenceNumber ());
      NS_LOG_DEBUG ("Attempting to transmit Interest MapMe ACK via Face " << face->GetId ());
      bool ok = false;

      // Check whether we obtained a Raw ICN packet or a 3N PDU
      if (pdu == 0)
	{
	  NS_LOG_DEBUG ("Checking if capable of direct ICN transmission");
	  if (face->IsNetworkCompatibilityEnabled ("ICN"))
	    {
	      // Send out the Interest packet
	      //	      Ptr<ICNFace> convertFace = DynamicCast<ICNFace> (face);
	      //	      ok = convertFace->SendInterest(interest);
	      if (m_useQueues)
		ok = EnqueueInterest(face, face->GetBroadcastAddress(), interest);
	      else
		ok = face->SendInterest (interest);

	      if (ok)
		{
		  NS_LOG_DEBUG ("Transmitted Interest MapMe ACK via Face " << face->GetId ());
		  m_outInterests (interest, face);
		}
	      else
		{
		  NS_LOG_DEBUG ("Could not transmit Interest MapMe ACK via Face " << face->GetId ());
		  m_dropInterests (interest, face);
		}
	    }
	  else
	    {
	      NS_LOG_WARN ("Received a raw ICN packet, but don't have one capable of transmission. Check code");
	    }
	}
      else
	{
	  NS_LOG_DEBUG ("Encoding Interest packet");
	  // Encode the packet
	  Ptr<Packet> retPkt = icn::Wire::FromInterest(interest, icn::Wire::WIRE_FORMAT_NDNSIM);

	  NS_LOG_DEBUG ("Creating NULLp for transmission");
	  Ptr<NULLp> null_p_o = Create<NULLp> ();
	  // Set the lifetime of the 3N PDU
	  null_p_o->SetLifetime (m_3n_lifetime);
	  // Configure payload for PDU
	  null_p_o->SetPayload (retPkt);
	  // Signal that the PDU had an ICN PDU as payload
	  null_p_o->SetPDUPayloadType (ICN_NNN);

	  bool normalSend = true;

	  if (m_ask_ACKP)
	    {
	      std::vector<Ptr<const NNNAddress> > ret = m_nnst->OneHopNameInfo(GetNode3NNamePtr(), face);

	      if (!ret.empty())
		{
		  normalSend = false;
		  int8_t n3ok = txPreparation (null_p_o, face, face->GetBroadcastAddress (), false);
		  ok = (n3ok >= 0);
		}
	      else
		{
		  NS_LOG_WARN ("Face " << face->GetId () << " has no apparent 3N nodes, not expecting ACKPs");
		  SetupFlowidSeq(null_p_o, face, face->GetBroadcastAddress (), false);
		  UpdateSentPDUs(null_p_o, face, face->GetBroadcastAddress (), false);
		}
	    }
	  else
	    {
	      txReset(null_p_o);
	    }

	  if (normalSend)
	    {
	      // Send out the NULL PDU
	      if (m_useQueues)
		ok = EnqueueNULLp(face, face->GetBroadcastAddress(), null_p_o);
	      else
		ok = face->SendNULLp (null_p_o);

	      UpdatePDUCountersB (null_p_o, face, ok, false);
	    }

	  if (!ok)
	    {
	      m_dropInterests (interest, face);
	    }
	  else
	    {
	      m_outInterests (interest, face);
	    }
	}
    }

    void
    ICN3NForwardingStrategy::TransmitLayeredMapMeAck (Ptr<DATAPDU> pdu, Ptr<const Interest> mapme,
						      Ptr<Face> face, const Address& from)
    {
    }

    void
    ICN3NForwardingStrategy::WillEraseTimedOutPendingInterest (Ptr<pit::Entry> pitEntry)
    {
      NS_LOG_FUNCTION (this);
      NS_LOG_DEBUG ("Will erase Timed Out Interest: " << pitEntry->GetPrefix () << " seq: " << std::dec << pitEntry->GetPrefix ().get (-1).toSeqNum ());

      for (pit::Entry::out_container::iterator face = pitEntry->GetOutgoing ().begin ();
	  face != pitEntry->GetOutgoing ().end ();
	  face ++)
	{
	  // NS_LOG_DEBUG ("Face: " << face->m_face);
	  pitEntry->GetFibEntry ()->UpdateStatus (face->m_face, fib::FaceMetric::ICN_FIB_YELLOW);
	}

      m_timedOutInterests (pitEntry);
    }

    void
    ICN3NForwardingStrategy::DidAddFibEntry (Ptr<fib::Entry> fibEntry)
    {
    }

    void
    ICN3NForwardingStrategy::WillRemoveFibEntry (Ptr<fib::Entry> fibEntry)
    {
    }

    Ptr<const NNNAddress>
    ICN3NForwardingStrategy::NNNForLongestPrefixMatch (Ptr<const icn::Name> name)
    {
      Ptr<fib::Entry> entry = m_fib->LongestPrefixMatch(name);

      if (!entry)
	{
	  return 0;
	}
      else
	{
	  return entry->FindNewest3NDestination();
	}
    }

    bool
    ICN3NForwardingStrategy::IsSharingPoA () const
    {
      return m_sharePoAInfo;
    }

    void
    ICN3NForwardingStrategy::SetSharePoA (bool value)
    {
      NS_LOG_FUNCTION (std::boolalpha << value);
      m_sharePoAInfo = value;
    }

    bool
    ICN3NForwardingStrategy::IsUsingNonAuthoritativeDU () const
    {
      return m_EnableNonAuthoritativeDU;
    }

    void
    ICN3NForwardingStrategy::SetNonAuthoritativeDU(bool value)
    {
      NS_LOG_FUNCTION (std::boolalpha << value);
      m_EnableNonAuthoritativeDU = value;
    }

    bool
    ICN3NForwardingStrategy::HasTFIBEnabled () const
    {
      return m_EnableTFIB;
    }

    void
    ICN3NForwardingStrategy::EnableTFIB (bool value)
    {
      NS_LOG_FUNCTION (std::boolalpha << value);
      m_EnableTFIB = value;
    }

    bool
    ICN3NForwardingStrategy::IsEnablingFIBUpdates () const
    {
      return m_EnableFIBUpdates;
    }

    void
    ICN3NForwardingStrategy::EnableFIBUpdates (bool value)
    {
      NS_LOG_FUNCTION (std::boolalpha << value);
      m_EnableFIBUpdates = value;
    }

    bool
    ICN3NForwardingStrategy::IsEnablingSavingUnrequestedData () const
    {
      return m_EnableSavingUnrequestedData;
    }

    void
    ICN3NForwardingStrategy::EnableSavingUnrequestedData (bool value)
    {
      NS_LOG_FUNCTION (std::boolalpha << value);
      m_EnableSavingUnrequestedData = value;
    }

    void
    ICN3NForwardingStrategy::DidCreatePitEntry (Ptr<Face> inFace,
						Ptr<const Interest> interest,
						Ptr<pit::Entry> pitEntry)
    {
      NS_LOG_FUNCTION (this);
    }

    void
    ICN3NForwardingStrategy::FailedToCreatePitEntry (Ptr<Face> inFace,
						     Ptr<const Interest> interest)
    {
      NS_LOG_FUNCTION (this);
      m_dropInterests (interest, inFace);
    }

    void
    ICN3NForwardingStrategy::DidReceiveDuplicateInterest (Ptr<Face> inFace,
							  Ptr<const Interest> interest,
							  Ptr<pit::Entry> pitEntry)
    {
      NS_LOG_FUNCTION (this);
      /////////////////////////////////////////////////////////////////////////////////////////
      //                                                                                     //
      // !!!! IMPORTANT CHANGE !!!! Duplicate interests will create incoming face entry !!!! //
      //                                                                                     //
      /////////////////////////////////////////////////////////////////////////////////////////
      pitEntry->AddIncoming (inFace);
      m_dropInterests (interest, inFace);
    }

    void
    ICN3NForwardingStrategy::DidReceiveDuplicateLayeredInterest (Ptr<DATAPDU> pdu,
								 Ptr<Face> inFace,
								 Ptr<const Interest> interest,
								 Ptr<pit::Entry> pitEntry,
								 const Address& from)
    {
      NS_LOG_FUNCTION (this);
      /////////////////////////////////////////////////////////////////////////////////////////
      //                                                                                     //
      // !!!! IMPORTANT CHANGE !!!! Duplicate interests will create incoming face entry !!!! //
      //                                                                                     //
      /////////////////////////////////////////////////////////////////////////////////////////
      if (!pdu)
	{
	  NS_LOG_INFO ("Duplicate Interest from " << inFace->GetId () << " PoA: " << from);
	  pitEntry->AddIncoming (inFace, from, true, false);
	}
      else
	{
	  bool wasNULL = false;
	  Ptr<SO> so_i;
	  bool wasSO = false;
	  bool wasDO = false;
	  Ptr<DU> du_i;
	  bool wasDU = false;

	  uint32_t pduid = pdu->GetPacketId();
	  switch(pduid)
	  {
	    case NULL_NNN:
	      wasNULL = true;
	      break;
	    case SO_NNN:
	      // Convert pointer to SO PDU
	      so_i = DynamicCast<SO> (pdu);
	      wasSO = true;
	      break;
	    case DO_NNN:
	      wasDO = true;
	      break;
	    case DU_NNN:
	      // Convert pointer to DU PDU
	      du_i = DynamicCast<DU> (pdu);
	      wasDU = true;
	      break;
	    default:
	      break;
	  }

	  if (wasNULL || wasDO)
	    {
	      NS_LOG_INFO ("Duplicate Interest from Face " << inFace->GetId () << " PoA: " << from << " in PDU with no SRC");
	      pitEntry->AddIncoming (inFace, from, false, true);
	    }
	  else if (wasSO || wasDU)
	    {
	      if (wasSO)
		{
		  NS_LOG_INFO ("Duplicate Interest from Face " << inFace->GetId () << " PoA: " << from << " in PDU from " << *(so_i->GetNamePtr ()));
		}
	      else
		{
		  NS_LOG_INFO ("Duplicate Interest from Face " << inFace->GetId () << " PoA: " << from << " in PDU from " << *(du_i->GetSrcNamePtr ()));
		}

	      pitEntry->AddIncoming (inFace, from, false, false);

	      if (wasSO)
		{
		  pitEntry->AddIncoming (inFace, so_i->GetNamePtr ());
		}
	      else
		{
		  pitEntry->AddIncoming (inFace, du_i->GetSrcNamePtr ());
		}
	    }
	}
    }

    void
    ICN3NForwardingStrategy::DidSuppressSimilarInterest (Ptr<Face> face,
							 Ptr<const Interest> interest,
							 Ptr<pit::Entry> pitEntry)
    {
      NS_LOG_FUNCTION (this);
    }

    void
    ICN3NForwardingStrategy::DidForwardSimilarInterest (Ptr<Face> inFace,
							Ptr<const Interest> interest,
							Ptr<pit::Entry> pitEntry)
    {
      NS_LOG_FUNCTION (this);
    }

    void
    ICN3NForwardingStrategy::DidExhaustForwardingOptions (Ptr<DATAPDU> pdu,
							  Ptr<Face> inFace,
							  Ptr<const Interest> interest,
							  Ptr<pit::Entry> pitEntry)
    {
      NS_LOG_FUNCTION (this);
      if (pitEntry->AreAllOutgoingInVain ())
	{
	  NS_LOG_DEBUG ("Outgoing are all in vain for Interest " << interest->GetName () << " take came in Face " << inFace->GetId ());
	  // Pointers and flags for PDU types
	  Ptr<NULLp> nullp_i;
	  bool wasNULL = false;
	  Ptr<SO> so_i;
	  bool wasSO = false;
	  Ptr<DO> do_i;
	  bool wasDO = false;
	  Ptr<DU> du_i;
	  bool wasDU = false;

	  if (pdu)
	    {
	      uint32_t pduid = pdu->GetPacketId();
	      switch(pduid)
	      {
		case NULL_NNN:
		  // Convert pointer to NULLp PDU
		  nullp_i = DynamicCast<NULLp> (pdu);
		  wasNULL = true;
		  break;
		case SO_NNN:
		  // Convert pointer to SO PDU
		  so_i = DynamicCast<SO> (pdu);
		  wasSO = true;
		  break;
		case DO_NNN:
		  // Convert pointer to DO PDU
		  do_i = DynamicCast<DO> (pdu);
		  wasDO = true;
		  break;
		case DU_NNN:
		  // Convert pointer to DU PDU
		  du_i = DynamicCast<DU> (pdu);
		  wasDU = true;
		  break;
		default:
		  break;
	      }
	    }

	  if (m_nacksEnabled)
	    {
	      // Create a NACK Interest to propagate the information
	      Ptr<Interest> nack = Create<Interest> (*interest);
	      nack->SetInterestType (Interest::NACK_GIVEUP_PIT);

	      // Convert the Nacked-Interest PDU into a NS-3 Packet
	      Ptr<Packet> icn_pdu = ns3::icn::Wire::FromInterest (nack);

	      // Create a NULL PDU to encapsulate the information - a more sophisticated
	      // PDU is not necessary, though might be interesting to consider
	      Ptr<NULLp> null_p_o = Create<NULLp> ();
	      // Set the lifetime of the 3N PDU
	      null_p_o->SetLifetime (m_3n_lifetime);
	      // Configure payload for PDU
	      null_p_o->SetPayload (icn_pdu);
	      // Signal that the PDU had an ICN PDU as payload
	      null_p_o->SetPDUPayloadType (ICN_NNN);

	      bool ok = false;
	      Ptr<Face> outFace;
	      bool attemptNull = false;
	      bool sentSomething = false;

	      // Send the NACK Interest to all known parties waiting for information
	      BOOST_FOREACH (const pit::IncomingFace &incoming, pitEntry->GetIncoming ())
	      {
		NS_LOG_DEBUG ("Attempting to send NACK for " << boost::cref (nack->GetName ()) <<
			      " SeqNo " << nack->GetName ().getSeqNum() << " via Face " << incoming.m_face->GetId ());

		// First Attempt to send out the information through all interfaces
		if (m_ask_ACKP)
		  {
		    int8_t n3ok = 0;
		    std::vector<Ptr<const NNNAddress> > ret = m_nnst->OneHopNameInfo(GetNode3NNamePtr(), incoming.m_face);

		    if (!ret.empty())
		      {
			n3ok = txPreparation (null_p_o, incoming.m_face, incoming.m_face->GetBroadcastAddress (), false);
			ok = (n3ok >= 0);
			sentSomething = ok;
		      }
		    else
		      {
			SetupFlowidSeq(null_p_o, incoming.m_face, incoming.m_face->GetBroadcastAddress (), false);
			NS_LOG_WARN ("Face " << incoming.m_face->GetId () << " has no apparent 3N nodes, not expecting ACKPs");
			UpdateSentPDUs(null_p_o, incoming.m_face, incoming.m_face->GetBroadcastAddress (), false);
			if (m_useQueues)
			  ok = EnqueueNULLp(incoming.m_face, incoming.m_face->GetBroadcastAddress(), null_p_o);
			else
			  ok = incoming.m_face->SendNULLp (null_p_o);
			sentSomething = ok;

			UpdatePDUCountersB(null_p_o, incoming.m_face, ok, false);
		      }
		  }
		else
		  {
		    txReset (null_p_o);
		    // Send out the NULL PDU
		    if (m_useQueues)
		      ok = EnqueueNULLp(incoming.m_face, incoming.m_face->GetBroadcastAddress(), null_p_o);
		    else
		      ok = incoming.m_face->SendNULLp (null_p_o);
		    sentSomething = ok;

		    UpdatePDUCountersB(null_p_o, incoming.m_face, ok, false);
		  }

		// 3N specific. If nothing has been sent, and we have at least seen a NULLp, return a NULLp
		if (!sentSomething)
		  {
		    if (incoming.SawNULL ())
		      {
			SetupFlowidSeq (null_p_o, incoming.m_face, incoming.m_face->GetBroadcastAddress(), false);
			NS_LOG_DEBUG ("Sending NACK in NULL");
			if (m_useQueues)
			  ok |= EnqueueNULLp(incoming.m_face, incoming.m_face->GetBroadcastAddress(), null_p_o);
			else
			  ok |= incoming.m_face->SendNULLp (null_p_o);
			attemptNull = ok;
		      }
		  }

		if (incoming.SawRawICN ())
		  {
		    NS_LOG_DEBUG ("Verifying if Face " << incoming.m_face->GetId () << " has ICN capability");
		    if (incoming.m_face->IsNetworkCompatibilityEnabled  ("ICN"))
		      {
			NS_LOG_DEBUG ("Sending raw NACK");
			if (m_useQueues)
			  ok |= EnqueueInterest(incoming.m_face, incoming.m_face->GetBroadcastAddress(), nack);
			else
			  ok |= incoming.m_face->SendInterest (nack);
		      }
		    else
		      {
			NS_LOG_WARN ("Selected Face " << incoming.m_face->GetId () << " has no direct ICN capability");
		      }
		  }

		if (ok)
		  {
		    m_outNacks (nack, incoming.m_face);

		    if (attemptNull)
		      {
			m_outNULLps (null_p_o, incoming.m_face);
		      }

		    NS_LOG_DEBUG ("Sent NACK for " << boost::cref (nack->GetName ()) << " via Face " << incoming.m_face->GetId ());
		  }
		else
		  {
		    m_dropNacks (nack, incoming.m_face);

		    if (attemptNull)
		      {
			m_dropNULLps (null_p_o, incoming.m_face);
		      }
		    NS_LOG_DEBUG ("Not able to propagate NACK via " << incoming.m_face->GetId ());
		  }
	      }
	    }

	  // Drop the information that generated all of this
	  m_dropInterests (interest, inFace);

	  if (pdu)
	    {
	      if (wasNULL)
		m_dropNULLps (nullp_i, inFace);
	      else if (wasSO)
		m_dropSOs (so_i, inFace);
	      else if (wasDO)
		m_dropDOs (do_i, inFace);
	      else if (wasDU)
		m_dropDUs (du_i, inFace);
	    }

	  // All incoming interests cannot be satisfied. Remove them
	  pitEntry->ClearIncoming ();

	  // Remove also outgoing
	  pitEntry->ClearOutgoing ();

	  // Set pruning timout on PIT entry (instead of deleting the record)
	  m_pit->MarkErased (pitEntry);
	}
    }

    void
    ICN3NForwardingStrategy::DidReceiveValidNack (Ptr<DATAPDU> pdu,
						  Ptr<Face> inFace,
						  uint32_t nackCode,
						  Ptr<const Interest> nack,
						  Ptr<pit::Entry> pitEntry)
    {
      NS_LOG_FUNCTION (this << boost::cref (*inFace));
      // Pointers and flags for PDU types
      Ptr<NULLp> nullp_i;
      bool wasNULL = false;
      Ptr<SO> so_i;
      bool wasSO = false;
      Ptr<DO> do_i;
      bool wasDO = false;
      Ptr<DU> du_i;
      bool wasDU = false;

      Ptr<DATAPDU> dataPdu;

      if (pdu)
	{
	  uint32_t pduid = pdu->GetPacketId();
	  switch(pduid)
	  {
	    case NULL_NNN:
	      // Convert pointer to NULLp PDU
	      nullp_i = DynamicCast<NULLp> (pdu);
	      wasNULL = true;
	      NS_LOG_DEBUG ("Received NULLp");
	      break;
	    case SO_NNN:
	      // Convert pointer to SO PDU
	      so_i = DynamicCast<SO> (pdu);
	      wasSO = true;
	      NS_LOG_DEBUG ("Received SO");
	      break;
	    case DO_NNN:
	      // Convert pointer to DO PDU
	      do_i = DynamicCast<DO> (pdu);
	      wasDO = true;
	      NS_LOG_DEBUG ("Received DO");
	      break;
	    case DU_NNN:
	      // Convert pointer to DU PDU
	      du_i = DynamicCast<DU> (pdu);
	      wasDU = true;
	      NS_LOG_DEBUG ("Received DU");
	      break;
	    default:
	      break;
	  }

	  // Regardless of the type, the PDU is a DATAPDU, treat it as such
	  dataPdu = DynamicCast<DATAPDU> (pdu);
	}
      else
	{
	  NS_LOG_DEBUG ("Received raw ICN packet");
	}

      NS_LOG_DEBUG ("nackCode: " << nackCode << " for [" << nack->GetName () << "]");

      // If NACK is NACK_GIVEUP_PIT, then neighbor gave up trying to propagate and removed it's PIT entry.
      // So, if we had an incoming entry to this neighbor, then we can remove it now
      if (nackCode == Interest::NACK_GIVEUP_PIT)
	{
	  NS_LOG_DEBUG ("Removing Face " << inFace->GetId () << " from PIT");
	  pitEntry->RemoveIncoming (inFace);
	}

      if (nackCode == Interest::NACK_LOOP ||
	  nackCode == Interest::NACK_CONGESTION ||
	  nackCode == Interest::NACK_GIVEUP_PIT)
	{
	  pitEntry->SetWaitingInVain (inFace);

	  if (!pitEntry->AreAllOutgoingInVain ()) // not all ougtoing are in vain
	    {
	      NS_LOG_DEBUG ("Not all outgoing are in vain");
	      // suppress
	      // Don't do anything, we are still expecting data from some other face
	      m_dropNacks (nack, inFace);

	      if (pdu)
		{
		  if (wasNULL)
		    m_dropNULLps (nullp_i, inFace);
		  else if (wasSO)
		    m_dropSOs (so_i, inFace);
		  else if (wasDO)
		    m_dropDOs (do_i, inFace);
		  else if (wasDU)
		    m_dropDUs (du_i, inFace);
		}
	      return;
	    }

	  Ptr<Interest> interest = Create<Interest> (*nack);
	  interest->SetInterestType (Interest::NORMAL_INTEREST);

	  // Convert the Interest PDU into a NS-3 Packet
	  Ptr<Packet> icn_pdu = ns3::icn::Wire::FromInterest (interest);

	  if (pdu != 0)
	    {
	      // Modify the payload of the NNNPDU
	      dataPdu->SetPayload (icn_pdu);
	    }

	  bool propagated;
	  if (pdu != 0)
	    {
	      propagated = DoPropagateInterest (dataPdu, inFace, interest, pitEntry);
	    }
	  else
	    {
	      propagated = DoPropagateInterest (pdu, inFace, interest, pitEntry);
	    }
	  if (!propagated)
	    {
	      if (pdu != 0)
		{
		  DidExhaustForwardingOptions (dataPdu, inFace, interest, pitEntry);
		}
	      else
		{
		  DidExhaustForwardingOptions (pdu, inFace, interest, pitEntry);
		}
	    }
	}

      if (inFace != 0 &&
	  (nackCode == Interest::NACK_CONGESTION ||
	      nackCode == Interest::NACK_GIVEUP_PIT))
	{
	  pitEntry->GetFibEntry ()->UpdateStatus (inFace, fib::FaceMetric::ICN_FIB_YELLOW);
	}
    }

    bool
    ICN3NForwardingStrategy::DetectRetransmittedInterest (Ptr<Face> inFace,
							  Ptr<const Interest> interest,
							  Ptr<pit::Entry> pitEntry)
    {
      NS_LOG_FUNCTION (this);
      pit::Entry::in_iterator existingInFace = pitEntry->GetIncoming ().find (inFace);

      bool isRetransmitted = false;

      if (existingInFace != pitEntry->GetIncoming ().end ())
	{
	  // this is almost definitely a retransmission. But should we trust the user on that?
	  isRetransmitted = true;
	}

      return isRetransmitted;
    }

    void
    ICN3NForwardingStrategy::WillSatisfyPendingInterest (Ptr<Face> inFace,
							 Ptr<pit::Entry> pitEntry)
    {
      NS_LOG_FUNCTION (this);
      if (inFace != 0)
	{
	  // Update metric status for the incoming interface in the corresponding FIB entry
	  pitEntry->GetFibEntry ()->UpdateStatus (inFace, fib::FaceMetric::ICN_FIB_GREEN);
	}

      pit::Entry::out_iterator out = pitEntry->GetOutgoing ().find (inFace);

      // If we have sent interest for this data via this face, then update stats.
      if (out != pitEntry->GetOutgoing ().end ())
	{
	  pitEntry->GetFibEntry ()->UpdateFaceRtt (inFace, Simulator::Now () - out->m_sendTime);
	}

      m_satisfiedInterests (pitEntry);
    }

    void
    ICN3NForwardingStrategy::WillSatisfyPendingLayeredInterest (Ptr<Face> inFace,
								Ptr<pit::Entry> pitEntry,
								const Address& from)
    {
      NS_LOG_FUNCTION (this);
      if (inFace != 0)
	{
	  // Update metric status for the incoming interface in the corresponding FIB entry
	  pitEntry->GetFibEntry ()->UpdateStatus (inFace, from, fib::FaceMetric::ICN_FIB_GREEN);
	}

      pit::Entry::out_iterator out = pitEntry->GetOutgoing ().find (inFace);

      // If we have sent interest for this data via this face, then update stats.
      if (out != pitEntry->GetOutgoing ().end ())
	{
	  pitEntry->GetFibEntry ()->UpdateFaceRtt (inFace, Simulator::Now () - out->m_sendTime);
	}

      m_satisfiedInterests (pitEntry);
    }

    void
    ICN3NForwardingStrategy::SatisfyPendingInterest (Ptr<DATAPDU> pdu,
						     Ptr<Face> inFace, // 0 allowed (from cache)
						     Ptr<const Data> data,
						     Ptr<pit::Entry> pitEntry)
    {
    }

    void
    ICN3NForwardingStrategy::SatisfyPendingLayeredInterest (Ptr<DATAPDU> pdu,
						     Ptr<Face> inFace, // 0 allowed (from cache)
						     Ptr<const Data> data,
						     Ptr<pit::Entry> pitEntry)
    {
    }

    void
    ICN3NForwardingStrategy::DidSendOutData (Ptr<Face> inFace,
					     Ptr<Face> outFace,
					     Ptr<const Data> data,
					     Ptr<pit::Entry> pitEntry)
    {
      NS_LOG_FUNCTION (this);
      m_outData (data, inFace == 0, outFace);
    }

    void
    ICN3NForwardingStrategy::DidReceiveSolicitedData (Ptr<Face> inFace,
						      Ptr<const Data> data,
						      bool didCreateCacheEntry)
    {
      NS_LOG_DEBUG ("Storing name: " << data->GetName() << " SeqNo " << data->GetName ().get (-1).toSeqNum () << " in CS");
    }

    void
    ICN3NForwardingStrategy::DidReceiveUnsolicitedData (Ptr<Face> inFace,
							Ptr<const Data> data,
							bool didCreateCacheEntry)
    {
      NS_LOG_FUNCTION (this);
    }

    bool
    ICN3NForwardingStrategy::ShouldSuppressIncomingInterest (Ptr<Face> inFace,
							     Ptr<const Interest> interest,
							     Ptr<pit::Entry> pitEntry)
    {
      NS_LOG_FUNCTION (this);
      bool isNew = pitEntry->GetIncoming ().size () == 0 && pitEntry->GetOutgoing ().size () == 0;

      if (isNew) return false; // never suppress new interests

      bool isRetransmitted = m_detectRetransmissions && // a small guard
	  DetectRetransmittedInterest (inFace, interest, pitEntry);

      if (pitEntry->GetOutgoing ().find (inFace) != pitEntry->GetOutgoing ().end ())
	{
	  NS_LOG_DEBUG ("Non duplicate interests from the face we have sent interest to. Don't suppress");
	  // got a non-duplicate interest from the face we have sent interest to
	  // Probably, there is no point in waiting data from that face... Not sure yet

	  // If we're expecting data from the interface we got the interest from ("producer" asks us for "his own" data)
	  // Mark interface YELLOW, but keep a small hope that data will come eventually.

	  // ?? not sure if we need to do that ?? ...

	  // pitEntry->GetFibEntry ()->UpdateStatus (inFace, fib::FaceMetric::ICN_FIB_YELLOW);
	}
      else
	if (!isNew && !isRetransmitted)
	  {
	    return true;
	  }

      return false;
    }

    bool
    ICN3NForwardingStrategy::CanSendOutInterest (Ptr<DATAPDU> pdu, Ptr<Face> inFace,
						 Ptr<Face> outFace,
						 Address addr,
						 Ptr<const Interest> interest,
						 Ptr<pit::Entry> pitEntry)
    {
      NS_LOG_FUNCTION (this);
      if (outFace == inFace)
	{
	  // Normally, sending out the same Face is prohibited, but if the PoA name given
	  // is not invalid, this is not necessarily an issue because it is using 3N information
	  if (addr.IsInvalid ())
	    {
	      NS_LOG_WARN ("Obtained same incoming and outgoing Face with an invalid PoA name. Check if this is desired effect");
	      return false; // same face as incoming, don't forward
	    }

	  if (addr == outFace->GetBroadcastAddress())
	    {
	      NS_LOG_WARN ("The incoming and outgoing Face is the same and intends to return with broadcast having no PoA information. Check if this is the desired effect");
	      return false;
	    }
	}

      pit::Entry::out_iterator outgoing =
	  pitEntry->GetOutgoing ().find (outFace);

      if (outgoing != pitEntry->GetOutgoing ().end ())
	{
	  if (pdu)
	    {
	      // Pointers for PDU types
	      Ptr<DU> du_i;
	      Ptr<DO> do_i;
	      bool got_du = false;
	      bool got_do = false;

	      // Find out the type of PDU we are dealing with
	      uint32_t pduid = pdu->GetPacketId ();
	      switch(pduid)
	      {
		case DU_NNN:
		  // Convert pointer to DU
		  du_i = DynamicCast<DU> (pdu);
		  got_du = true;
		  break;
		case DO_NNN:
		  // Convert pointer to DO
		  do_i = DynamicCast<DO> (pdu);
		  got_do = true;
		  break;
		default:
		  // Can't do anything with NULLp in FIB
		  break;
	      }

	      Ptr<const NNNAddress> addr;
	      if (got_do)
		{
		  addr = do_i->GetNamePtr ();
		}
	      else if (got_du)
		{
		  addr = du_i->GetDstNamePtr ();
		}

	      if (got_do || got_du)
		{
		  if (!pitEntry->OutgoingDestinationExists(outFace, addr))
		    {
		      return true;
		    }
		  else
		    {
		      NS_LOG_DEBUG ("Already sent Interest to " << *addr);
		    }
		}
	    }

	  if (!m_detectRetransmissions)
	    return false; // suppress
	  else if (outgoing->m_retxCount >= pitEntry->GetMaxRetxCount ())
	    {
	      NS_LOG_DEBUG ("Already forwarded before during this retransmission cycle (" <<outgoing->m_retxCount << " >= " << pitEntry->GetMaxRetxCount () << ")");
	      return false; // already forwarded before during this retransmission cycle
	    }
	}

      return true;
    }

    bool
    ICN3NForwardingStrategy::CanSendOutLayeredInterest (Ptr<DATAPDU> pdu, Ptr<Face> inFace, Ptr<Face> outFace,
							Address addr, Ptr<const Interest> interest,
							Ptr<pit::Entry> pitEntry, const Address& from)
    {
      NS_LOG_FUNCTION (this);
      if (outFace == inFace)
	{
	  // Normally, sending out the same Face is prohibited, but if the PoA name given
	  // is not invalid, this is not necessarily an issue because it is using 3N information
	  if (addr.IsInvalid ())
	    {
	      NS_LOG_ERROR("Obtained same incoming and outgoing Face with an invalid PoA name. Check if this is desired effect");
	      return false; // same face as incoming, don't forward
	    }

	  // What is generally not good is if the Address to send to is exactly the same as the from
	  if (addr == from)
	    {
	      NS_LOG_ERROR ("Obtained the same incoming and outgoing PoA name. Check if this is the desired effect");
	      return false;
	    }

	  if (addr == outFace->GetBroadcastAddress())
	    {
	      std::vector<Ptr<const NNNAddress> > ret = m_nnst->OneHopNameInfo(GetNode3NNamePtr(), outFace);

	      if (ret.size () > 1)
		{
		  NS_LOG_WARN ("The incoming and outgoing Face is the same but node has seen more than 1 node on Face: " << outFace->GetId () << ", permitting transmission");
		}
	      else
		{
		  return false;
		}
	    }
	}

      pit::Entry::out_iterator outgoing =
	  pitEntry->GetOutgoing ().find (outFace);

      if (outgoing != pitEntry->GetOutgoing ().end ())
	{
	  if (pdu)
	    {
	      // Pointers for PDU types
	      Ptr<DU> du_i;
	      Ptr<DO> do_i;
	      bool got_du = false;
	      bool got_do = false;

	      // Find out the type of PDU we are dealing with
	      uint32_t pduid = pdu->GetPacketId ();
	      switch(pduid)
	      {
		case DU_NNN:
		  // Convert pointer to DU
		  du_i = DynamicCast<DU> (pdu);
		  got_du = true;
		  break;
		case DO_NNN:
		  // Convert pointer to DO
		  do_i = DynamicCast<DO> (pdu);
		  got_do = true;
		  break;
		default:
		  // Can't do anything with NULLp in FIB
		  break;
	      }

	      Ptr<const NNNAddress> addr;
	      if (got_do)
		{
		  addr = do_i->GetNamePtr ();
		}
	      else if (got_du)
		{
		  addr = du_i->GetDstNamePtr ();
		}

	      if (got_do || got_du)
		{
		  if (!pitEntry->OutgoingDestinationExists(outFace, addr))
		    {
		      return true;
		    }
		  else
		    {
		      NS_LOG_DEBUG ("Already sent Interest to " << *addr);
		    }
		}
	    }

	  if (!m_detectRetransmissions)
	    return false; // suppress
	  else if (outgoing->m_retxCount >= pitEntry->GetMaxRetxCount ())
	    {
	      NS_LOG_DEBUG ("Already forwarded before during this retransmission cycle (" <<outgoing->m_retxCount << " >= " << pitEntry->GetMaxRetxCount () << ")");
	      return false; // already forwarded before during this retransmission cycle
	    }
	}

      return true;
    }

    bool
    ICN3NForwardingStrategy::TrySendOutInterest (Ptr<DATAPDU> pdu,
						 Ptr<Face> inFace,
						 Ptr<Face> outFace,
						 Address addr,
						 Ptr<const Interest> interest,
						 Ptr<pit::Entry> pitEntry)
    {
      NS_LOG_FUNCTION (this);

      // Check for empty Faces
      if (outFace == 0)
	return false;

      // Convert the Interest PDU into a NS-3 Packet
      Ptr<Packet> icn_pdu;

      // Pointers and flags for PDU types
      Ptr<NULLp> nullp_i;
      bool wasNULL = false;
      Ptr<SO> so_i;
      bool wasSO = false;
      Ptr<DO> do_i;
      bool wasDO = false;
      Ptr<DU> du_i;
      bool wasDU = false;
      Ptr<DATAPDU> datapdu;

      uint32_t outFaceId = outFace->GetId ();
      uint32_t inFaceId = inFace->GetId ();

      Address internal;
      if (addr.IsInvalid ())
	{
	  internal = outFace->GetBroadcastAddress();
	}
      else
	{
	  internal = addr;
	}

      uint32_t pduid;
      if (!pdu)
	{
	  NS_LOG_INFO ("Received raw Interest to attempt to propagate");
	}
      else
	{
	  pduid = pdu->GetPacketId();
	  switch(pduid)
	  {
	    case NULL_NNN:
	      // Convert pointer to NULLp PDU
	      nullp_i = DynamicCast<NULLp> (pdu);
	      datapdu = DynamicCast<DATAPDU> (nullp_i);
	      wasNULL = true;
	      break;
	    case SO_NNN:
	      // Convert pointer to SO PDU
	      so_i = DynamicCast<SO> (pdu);
	      datapdu = DynamicCast<DATAPDU> (so_i);
	      wasSO = true;
	      break;
	    case DO_NNN:
	      // Convert pointer to DO PDU
	      do_i = DynamicCast<DO> (pdu);
	      datapdu = DynamicCast<DATAPDU> (do_i);
	      wasDO = true;
	      break;
	    case DU_NNN:
	      // Convert pointer to DU PDU
	      du_i = DynamicCast<DU> (pdu);
	      datapdu = DynamicCast<DATAPDU> (du_i);
	      wasDU = true;
	      break;
	    default:
	      break;
	  }

	  icn_pdu = ns3::icn::Wire::FromInterest (interest);
	}

      // Check if we are allowed to retransmit through the selected Face
      if (!CanSendOutInterest (pdu, inFace, outFace, internal, interest, pitEntry))
	{
	  NS_LOG_DEBUG ("Cannot send PDU from Face " << inFaceId << " out via Face " << outFaceId);
	  return false;
	}

      // Flag to know if what we sent was successful
      bool successSend = false;
      bool outFaceIsAppFace = outFace->isAppFace();
      bool normalSend = true;

      if (m_ask_ACKP)
	{
	  int8_t ok = 0;
	  if (wasNULL || wasSO)
	    {
	      std::vector<Ptr<const NNNAddress> > ret = m_nnst->OneHopNameInfo(GetNode3NNamePtr(), outFace);

	      if (!ret.empty())
		{
		  normalSend = false;
		  ok = txPreparation (datapdu, outFace, internal, false);
		  successSend = (ok >= 0) ? true : false;
		}
	      else
		{
		  NS_LOG_WARN ("Face " << outFaceId << " has no apparent 3N nodes, not expecting ACKPs");
		  SetupFlowidSeq(datapdu, outFace, internal, false);
		  UpdateSentPDUs(datapdu, outFace, internal, false);
		}
	    }
	  else
	    {
	      normalSend = false;
	      ok = txPreparation (datapdu, outFace, internal, false);
	      successSend = (ok >= 0) ? true : false;
	    }
	}
      else
	{
	  txReset (datapdu);
	}

      if (normalSend)
	{
	  if (wasNULL)
	    if (m_useQueues)
	      successSend = EnqueueNULLp(outFace, outFace->GetBroadcastAddress(), nullp_i);
	    else
	      successSend = outFace->SendNULLp (nullp_i);
	  else if (wasSO)
	    if (m_useQueues)
	      successSend = EnqueueSO(outFace, outFace->GetBroadcastAddress(), so_i);
	    else
	      successSend = outFace->SendSO (so_i);
	  else if (wasDO)
	    {
	      // Application Faces have no names
	      if (outFaceIsAppFace)
		if (m_useQueues)
		  successSend =  EnqueueDO(outFace, Address (), do_i);
		else
		  successSend = outFace->SendDO (do_i);
	      else
		if (m_useQueues)
		  successSend =  EnqueueDO(outFace, internal, do_i);
		else
		  successSend = outFace->SendDO (do_i, internal);
	    }
	  else if (wasDU)
	    {
	      // Application Faces have no names
	      if (outFaceIsAppFace)
		if (m_useQueues)
		  successSend =  EnqueueDU(outFace, internal, du_i);
		else
		  successSend = outFace->SendDU (du_i);
	      else
		if (m_useQueues)
		  successSend =  EnqueueDU(outFace, internal, du_i);
		else
		  successSend = outFace->SendDU (du_i, internal);
	    }
	  else
	    {
	      NS_LOG_DEBUG ("Verifying if Face " << outFaceId  << " has ICN capability");
	      if (outFace->IsNetworkCompatibilityEnabled  ("ICN"))
		{
		  //	      Ptr<ICNFace> outICNFace = DynamicCast<ICNFace> (outFace);
		  //	      successSend = outICNFace->SendInterest (interest);
		  if (m_useQueues)
		    successSend = EnqueueInterest(outFace, outFace->GetBroadcastAddress(), interest);
		  else
		    successSend = outFace->SendInterest (interest);
		}
	      else
		{
		  NS_LOG_WARN ("Selected Face " << outFaceId << " has no direct ICN capability");
		}
	    }

	  if (pdu)
	    {
	      UpdatePDUCountersB(pdu, outFace, successSend, false);
	    }
	  else
	    {
	      if (successSend)
		{
		  NS_LOG_DEBUG ("Successfully sent out Interest via Face " << outFaceId);
		}
	    }
	}

      // Check if our sending was successful
      if (successSend)
	{
	  // Update the PIT Entry with the Outgoing selected Face
	  pitEntry->AddOutgoing (outFace);

	  if (pdu)
	    {
	      if (pdu->has3NDst ())
		{
		  pitEntry->AddOutgoingDestination(outFace, pdu->GetDstNamePtr ());
		}
	    }

	  // Log that an Interest PDU was forwarded
	  DidSendOutInterest (inFace, outFace, interest, pitEntry);
	}
      else
	{
	  DidDropInterest (outFace, interest);
	}

      return true;
    }

    bool
    ICN3NForwardingStrategy::TrySendOutLayeredInterest (Ptr<DATAPDU> pdu, Ptr<Face> inFace,
							Ptr<Face> outFace, Address addr,
							Ptr<const Interest> interest,
							Ptr<pit::Entry> pitEntry,
							const Address& from)
    {
      NS_LOG_FUNCTION (inFace->GetId () << from << outFace->GetId () << addr << interest->GetName ());

      // Check for empty Faces
      if (outFace == 0)
	return false;

      // Convert the Interest PDU into a NS-3 Packet
      Ptr<Packet> icn_pdu;

      // Pointers and flags for PDU types
      Ptr<NULLp> nullp_i;
      bool wasNULL = false;
      Ptr<SO> so_i;
      bool wasSO = false;
      Ptr<DO> do_i;
      bool wasDO = false;
      Ptr<DU> du_i;
      bool wasDU = false;
      bool wasRawICN = false;
      Ptr<DATAPDU> datapdu;

      uint32_t outFaceId = outFace->GetId ();
      uint32_t inFaceId = inFace->GetId ();

      Address internal;
      if (addr.IsInvalid ())
	{
	  internal = outFace->GetBroadcastAddress();
	}
      else
	{
	  internal = addr;
	}

      if (!pdu)
	{
	  NS_LOG_INFO ("Received raw Interest to attempt to propagate");
	  wasRawICN = true;
	}
      else
	{
	  uint32_t pduid = pdu->GetPacketId();
	  switch(pduid)
	  {
	    case NULL_NNN:
	      // Convert pointer to NULLp PDU
	      nullp_i = DynamicCast<NULLp> (pdu);
	      datapdu = DynamicCast<DATAPDU> (nullp_i);
	      wasNULL = true;
	      break;
	    case SO_NNN:
	      // Convert pointer to SO PDU
	      so_i = DynamicCast<SO> (pdu);
	      datapdu = DynamicCast<DATAPDU> (so_i);
	      wasSO = true;
	      break;
	    case DO_NNN:
	      // Convert pointer to DO PDU
	      do_i = DynamicCast<DO> (pdu);
	      datapdu = DynamicCast<DATAPDU> (do_i);
	      wasDO = true;
	      break;
	    case DU_NNN:
	      // Convert pointer to DU PDU
	      du_i = DynamicCast<DU> (pdu);
	      datapdu = DynamicCast<DATAPDU> (du_i);
	      wasDU = true;
	      break;
	    default:
	      break;
	  }

	  icn_pdu = ns3::icn::Wire::FromInterest (interest);
	}

      // Check if we are allowed to retransmit through the selected Face
      if (!CanSendOutLayeredInterest (pdu, inFace, outFace, internal, interest, pitEntry, from))
	{
	  NS_LOG_DEBUG ("Cannot send PDU from " << inFaceId << " from " << outFaceId);
	  return false;
	}

      // Flag to know if what we sent was successful
      bool successSend = false;
      bool normalSend = true;

      if (m_ask_ACKP)
	{
	  int8_t ok = 0;
	  if (wasNULL || wasSO)
	    {
	      std::vector<Ptr<const NNNAddress> > ret = m_nnst->OneHopNameInfo(GetNode3NNamePtr(), outFace);
	      if (!ret.empty())
		{
		  normalSend = false;
		  ok = txPreparation (datapdu, outFace, internal, false);
		  successSend = (ok >= 0) ? true : false;
		}
	      else
		{
		  NS_LOG_WARN ("Face " << outFaceId << " has no apparent 3N nodes, not expecting ACKPs");
		  SetupFlowidSeq(datapdu, outFace, internal, false);
		  UpdateSentPDUs(datapdu, outFace, internal, false);
		}
	    }
	  else
	    {
	      normalSend = false;
	      ok = txPreparation (datapdu, outFace, internal, false);
	      successSend = (ok >= 0) ? true : false;
	    }
	}
      else
	{
	  txReset (datapdu);
	}

      if (normalSend)
	{
	  // This will have to be modified when I completely severe the Apps from the 3N
	  // Depending on the PDU type, send
	  if (wasNULL)
	    {
	      // Application Faces have no names
	      if (outFace->isAppFace ())
		if (m_useQueues)
		  successSend = EnqueueNULLp(outFace, internal, nullp_i);
		else
		  successSend = outFace->SendNULLp (nullp_i);
	      else
		if (m_useQueues)
		  successSend = EnqueueNULLp(outFace, internal, nullp_i);
		else
		  successSend = outFace->SendNULLp (nullp_i, internal);
	    }
	  else if (wasSO)
	    {
	      // Application Faces have no names
	      if (outFace->isAppFace ())
		if (m_useQueues)
		  successSend = EnqueueSO(outFace, internal, so_i);
		else
		  successSend = outFace->SendSO (so_i);
	      else
		if (m_useQueues)
		  successSend = EnqueueSO(outFace, internal, so_i);
		else
		  successSend = outFace->SendSO (so_i, internal);
	    }
	  else if (wasDO)
	    {
	      // Application Faces have no names
	      if (outFace->isAppFace ())
		if (m_useQueues)
		  successSend = EnqueueDO(outFace, Address (), do_i);
		else
		  successSend = outFace->SendDO (do_i);
	      else
		if (m_useQueues)
		  successSend = EnqueueDO(outFace, internal, do_i);
		else
		  successSend = outFace->SendDO (do_i, internal);
	    }
	  else if (wasDU)
	    {
	      // Application Faces have no names
	      if (outFace->isAppFace ())
		if (m_useQueues)
		  successSend = EnqueueDU(outFace, internal, du_i);
		else
		  successSend = outFace->SendDU (du_i);
	      else
		if (m_useQueues)
		  successSend = EnqueueDU(outFace, internal, du_i);
		else
		  successSend = outFace->SendDU (du_i, internal);
	    }
	  else
	    {
	      NS_LOG_DEBUG ("Verifying if Face " << outFaceId  << " has ICN capability");
	      if (outFace->IsNetworkCompatibilityEnabled  ("ICN"))
		{
		  // Application Faces have no names!
		  if (outFace->isAppFace ())
		    if (m_useQueues)
		      successSend = EnqueueInterest(outFace, Address (), interest);
		    else
		      successSend = outFace->SendInterest (interest);
		  else
		    if (m_useQueues)
		      successSend = EnqueueInterest(outFace, internal, interest);
		    else
		      successSend = outFace->SendInterest (interest, internal);
		}
	      else
		{
		  NS_LOG_WARN ("Selected Face " << outFaceId << " has no direct ICN capability");
		}
	    }

	  if (pdu)
	    {
	      UpdatePDUCountersB(pdu, outFace, successSend, false);
	    }
	  else
	    {
	      if (successSend)
		{
		  NS_LOG_DEBUG ("Successfully sent out Interest via Face " << outFaceId);
		}
	    }
	}

      // Check if our sending was successful
      if (successSend)
	{
	  // Update the PIT Entry with the Outgoing selected Face
	  pitEntry->AddOutgoing (outFace, internal, wasRawICN);

	  if (pdu)
	    {
	      if (pdu->has3NDst ())
		{
		  pitEntry->AddOutgoingDestination(outFace, pdu->GetDstNamePtr ());
		}
	    }

	  // Log that an Interest PDU was forwarded
	  DidSendOutInterest (inFace, outFace, interest, pitEntry);
	}
      else
	{
	  DidDropInterest (outFace, interest);
	}

      return true;
    }

    void
    ICN3NForwardingStrategy::DidSendOutInterest (Ptr<Face> inFace,
						 Ptr<Face> outFace,
						 Ptr<const Interest> interest,
						 Ptr<pit::Entry> pitEntry)
    {
      NS_LOG_FUNCTION (this);
      m_outInterests (interest, outFace);
    }

    void
    ICN3NForwardingStrategy::DidDropInterest (Ptr<Face> face, Ptr<const Interest> interest)
    {
      NS_LOG_FUNCTION (this);
      m_dropInterests (interest, face);
    }

    void
    ICN3NForwardingStrategy::PropagateInterest (Ptr<DATAPDU> pdu,
						Ptr<Face> inFace,
						Ptr<const Interest> interest,
						Ptr<pit::Entry> pitEntry)
    {
      NS_LOG_FUNCTION (this);
      bool isRetransmitted = m_detectRetransmissions && // a small guard
	  DetectRetransmittedInterest (inFace, interest, pitEntry);

      // Update the PIT information with the PDU information
      if (pdu == 0)
	{
	  NS_LOG_DEBUG ("Adding Face " << inFace->GetId ());
	  // Saw raw ICN
	  pitEntry->AddIncoming (inFace, true);
	}
      else
	{
	  NS_LOG_DEBUG ("Adding Face " << inFace->GetId () << " with 3N information");
	  // Add the Incoming face as normal
	  pitEntry->AddIncoming (inFace/*, interest->GetInterestLifetime ()*/);
	  // Pointers and flags for PDU types
	  Ptr<SO> so_i;
	  Ptr<DU> du_i;

	  uint32_t pduid = pdu->GetPacketId();
	  switch(pduid)
	  {
	    case SO_NNN:
	      // Convert pointer to SO PDU
	      so_i = DynamicCast<SO> (pdu);
	      // Add the 3N name in the SO
	      pitEntry->AddIncoming(inFace, so_i->GetNamePtr());
	      break;
	    case DU_NNN:
	      // Convert pointer to DU PDU
	      du_i = DynamicCast<DU> (pdu);
	      // Add the 3N name in the DU
	      pitEntry->AddIncoming(inFace, du_i->GetSrcNamePtr());
	      break;
	    default:
	      break;
	  }
	}

      /// @todo Make lifetime per incoming interface
      pitEntry->UpdateLifetime (interest->GetInterestLifetime ());

      bool propagated = DoPropagateInterest (pdu, inFace, interest, pitEntry);

      if (!propagated && isRetransmitted) //give another chance if retransmitted
	{
	  // increase max number of allowed retransmissions
	  pitEntry->IncreaseAllowedRetxCount ();

	  NS_LOG_DEBUG ("Increased RetxCount, attempting again");

	  // try again
	  propagated = DoPropagateInterest (pdu, inFace, interest, pitEntry);
	}

      // if (!propagated)
      //   {
      //     NS_LOG_DEBUG ("++++++++++++++++++++++++++++++++++++++++++++++++++++++");
      //     NS_LOG_DEBUG ("+++ Not propagated ["<< interest->GetName () <<"], but number of outgoing faces: " << pitEntry->GetOutgoing ().size ());
      //     NS_LOG_DEBUG ("++++++++++++++++++++++++++++++++++++++++++++++++++++++");
      //   }

      // ForwardingStrategy will try its best to forward PDU to at least one interface.
      // If no interests was propagated, then there is not other option for forwarding or
      // ForwardingStrategy failed to find it.
      if (!propagated && pitEntry->AreAllOutgoingInVain ())
	{
	  DidExhaustForwardingOptions (pdu, inFace, interest, pitEntry);
	}
    }

    void
    ICN3NForwardingStrategy::PropagateLayeredInterest (Ptr<DATAPDU> pdu, Ptr<Face> inFace,
						       Ptr<const Interest> interest,
						       Ptr<pit::Entry> pitEntry,
						       const Address& from)
    {
      NS_LOG_FUNCTION (this);
      bool isRetransmitted = m_detectRetransmissions && // a small guard
	  DetectRetransmittedInterest (inFace, interest, pitEntry);

      UpdateLayeredPITEntry(pitEntry, pdu, inFace, interest->GetInterestLifetime (), from);

      bool propagated = DoPropagateLayeredInterest (pdu, inFace, interest, pitEntry, from);

      if (!propagated && isRetransmitted) //give another chance if retransmitted
	{
	  // Increase the maximum number of retransmissions allowed
	  pitEntry->IncreaseAllowedRetxCount ();

	  NS_LOG_DEBUG ("Increased RetxCount, attempting again");

	  // And try again
	  propagated = DoPropagateLayeredInterest (pdu, inFace, interest, pitEntry, from);
	}

      // ForwardingStrategy will try its best to forward PDU to at least one interface.
      // If no interests was propagated, then there is not other option for forwarding or
      // ForwardingStrategy failed to find it.
      if (!propagated && pitEntry->AreAllOutgoingInVain ())
	{
	  DidExhaustForwardingOptions (pdu, inFace, interest, pitEntry);
	}
    }

    bool
    ICN3NForwardingStrategy::DoPropagateInterest (Ptr<DATAPDU> pdu,
						  Ptr<Face> inFace,
						  Ptr<const Interest> interest,
						  Ptr<pit::Entry> pitEntry)
    {
      return false;
    }

    bool
    ICN3NForwardingStrategy::DoPropagateLayeredInterest (Ptr<DATAPDU> pdu, Ptr<Face> inFace,
							 Ptr<const Interest> interest,
							 Ptr<pit::Entry> pitEntry,
							 const Address& from)
    {
      return false;
    }

    bool
    ICN3NForwardingStrategy::DoPropagateData (Ptr<DATAPDU> pdu,
					      Ptr<Face> inFace,
					      Ptr<const Data> data)
    {
      return false;
    }


    bool
    ICN3NForwardingStrategy::TryToSendRawData (Ptr<Face> inFace,
					       Ptr<Face> outFace,
					       Ptr<const Data> data,
					       Ptr<pit::Entry> pitEntry)
    {
      NS_LOG_FUNCTION (this);

      bool ok = false;

      if (outFace->IsNetworkCompatibilityEnabled ("ICN"))
	{
	  // Send out the Data PDU
//	  Ptr<ICNFace> convertFace = DynamicCast<ICNFace> (outFace);
//	  ok = convertFace->SendData (data);
	  ok = outFace->SendData (data);

	  if (!ok)
	    {
	      m_dropData (data, outFace);
	      NS_LOG_DEBUG ("Cannot satisfy data to " << *outFace);
	    }
	  else
	    {
	      // Log that a Data PDU was sent
	      DidSendOutData (inFace, outFace, data, pitEntry);
	    }
	}
      else
	{
	  NS_LOG_INFO ("Saw a raw ICN PDU here, cannot satisfy using incompatible Face. Please check settings");
	}

      return ok;
    }

    bool
    ICN3NForwardingStrategy::TryToSendRawDataToPoA (Ptr<Face> inFace, Ptr<Face> outFace,
						    Ptr<const Data> data,  Ptr<pit::Entry> pitEntry,
						    const Address& to)
    {
      NS_LOG_FUNCTION (this);

      bool ok = false;

      if (outFace->IsNetworkCompatibilityEnabled ("ICN"))
	{
	  // Send out the Data PDU
	  //	  Ptr<ICNFace> convertFace = DynamicCast<ICNFace> (outFace);
	  //	  ok = convertFace->SendData (data);
	  if (m_useQueues)
	    ok = EnqueueData(outFace, to, data);
	  else
	    ok = outFace->SendData (data, to);

	  if (!ok)
	    {
	      m_dropData (data, outFace);
	      NS_LOG_DEBUG ("Cannot satisfy data to " << *outFace);
	    }
	  else
	    {
	      // Log that a Data PDU was sent
	      DidSendOutData (inFace, outFace, data, pitEntry);
	    }
	}
      else
	{
	  NS_LOG_INFO ("Saw a raw ICN PDU here, cannot satisfy using incompatible Face. Please check settings");
	}

      return ok;
    }

    bool
    ICN3NForwardingStrategy::TryToSendDataInNULL (uint32_t orig_flow,
						  Ptr<Face> inFace,
						  Ptr<Face> outFace,
						  Ptr<const Data> data,
						  Ptr<pit::Entry> pitEntry)
    {
      NS_LOG_FUNCTION (this);

      bool ok = false;

      // Convert the Data into payload
      Ptr<Packet> icn_pdu = ns3::icn::Wire::FromData (data);
      // Create the NULL PDU
      Ptr<NULLp> null_p_o = Create<NULLp> ();
      NS_LOG_INFO ("RF with Window: " << m_3n_max_pdu_window << " Rate: " << m_3n_default_rate << " kbit/s");
      // Set the lifetime of the 3N PDU
      null_p_o->SetLifetime (m_3n_lifetime);
      // Configure payload for PDU
      null_p_o->SetPayload (icn_pdu);
      // Signal that the PDU had an ICN PDU as payload
      null_p_o->SetPDUPayloadType (ICN_NNN);

      if (m_ask_ACKP)
	{
	  int8_t n3ok = 0;
	  std::vector<Ptr<const NNNAddress> > ret = m_nnst->OneHopNameInfo(GetNode3NNamePtr(), outFace);

	  if (!ret.empty())
	    {
	      n3ok = txPreparation (null_p_o, outFace, outFace->GetBroadcastAddress (), false);
	      ok = (n3ok >= 0);
	    }
	  else
	    {
	      SetupFlowidSeq(null_p_o, outFace, outFace->GetBroadcastAddress (), false);
	      NS_LOG_WARN ("Face " << outFace->GetId () << " has no apparent 3N nodes, not expecting ACKPs");
	      UpdateSentPDUs(null_p_o, outFace, outFace->GetBroadcastAddress (), false);
	      if (m_useQueues)
		ok = EnqueueNULLp(outFace, outFace->GetBroadcastAddress (), null_p_o);
	      else
		ok = outFace->SendNULLp (null_p_o);

	      UpdatePDUCountersB(null_p_o, outFace, ok, false);
	    }
	}
      else
	{
	  txReset (null_p_o);
	  // Send out the NULL PDU
	  if (m_useQueues)
	    ok = EnqueueNULLp(outFace, outFace->GetBroadcastAddress (), null_p_o);
	  else
	    ok = outFace->SendNULLp (null_p_o);

	  UpdatePDUCountersB(null_p_o, outFace, ok, false);
	}

      if (!ok)
	{
	  m_dropData (data, outFace);
	}
      else
	{
//	  m_3n_seq.find(s_flowid)->second++;
	  // Log that a Data PDU was sent
	  DidSendOutData (inFace, outFace, data, pitEntry);
	}

      return ok;
    }

    bool
    ICN3NForwardingStrategy::TryToSendDataInNULLToPoA (uint32_t orig_flow,
						       Ptr<Face> inFace, Ptr<Face> outFace,
						       Ptr<const Data> data,  Ptr<pit::Entry> pitEntry,
						       const Address& to)
    {
      NS_LOG_FUNCTION (this);

      bool ok = false;
//      uint32_t flowid = 0;
//      uint32_t seq_3n = 0;
//      bool first = true;
//
//      std::tie(flowid, seq_3n, first) = FindFlowidSeq (0, orig_flow);

      // Convert the Data into payload
      Ptr<Packet> icn_pdu = ns3::icn::Wire::FromData (data);
      // Create the NULL PDU
      Ptr<NULLp> null_p_o = Create<NULLp> ();
      // Set the lifetime of the 3N PDU
      null_p_o->SetLifetime (m_3n_lifetime);
      // Configure payload for PDU
      null_p_o->SetPayload (icn_pdu);
      // Signal that the PDU had an ICN PDU as payload
      null_p_o->SetPDUPayloadType (ICN_NNN);

      if (m_ask_ACKP)
	{
	  std::vector<Ptr<const NNNAddress> > ret = m_nnst->OneHopNameInfo(GetNode3NNamePtr(), outFace);

	  if (!ret.empty())
	    {
	      int8_t n3ok = txPreparation (null_p_o, outFace, to, false);
	      ok = (n3ok >= 0);
	    }
	  else
	    {
	      SetupFlowidSeq (null_p_o, outFace, to, false);
	      NS_LOG_WARN ("Face " << outFace->GetId () << " has no apparent 3N nodes, not expecting ACKPs");
	      UpdateSentPDUs(null_p_o, outFace, to, false);
	      if (m_useQueues)
		ok = EnqueueNULLp(outFace, to, null_p_o);
	      else
		ok = outFace->SendNULLp (null_p_o, to);

	      UpdatePDUCountersB (null_p_o, outFace, ok, false);
	    }
	}
      else
	{
	  txReset(null_p_o);
	  // Send out the NULL PDU
	  if (m_useQueues)
	    ok = EnqueueNULLp(outFace, to, null_p_o);
	  else
	    ok = outFace->SendNULLp (null_p_o, to);

	  UpdatePDUCountersB (null_p_o, outFace, ok, false);
	}

      if (!ok)
	{
	  m_dropData (data, outFace);
	}
      else
	{
//	  m_3n_seq.find(flowid)->second++;
	  DidSendOutData (inFace, outFace, data, pitEntry);
	}

      return ok;
    }

    bool
    ICN3NForwardingStrategy::TryToSendDataInDO (Ptr<DO> do_o, Address addr,
						Ptr<Face> inFace,
						Ptr<Face> outFace,
						Ptr<const Data> data,
						Ptr<pit::Entry> pitEntry)
    {
      NS_LOG_FUNCTION (this);
      bool ok = false;

      if (m_ask_ACKP)
	{
	  int8_t n3ok = txPreparation (do_o, outFace, addr, false);
	  ok = (n3ok >= 0);
	}
      else
	{
	  txReset(do_o);
	  if (addr.IsInvalid ())
	    if (m_useQueues)
	      ok = EnqueueDO(outFace, Address (), do_o);
	    else
	      ok = outFace->SendDO (do_o);
	  else
	    {
	      SetupFlowidSeq (do_o, outFace, addr, false);
	      if (m_useQueues)
		ok = EnqueueDO(outFace, addr, do_o);
	      else
		ok = outFace->SendDO (do_o, addr);
	    }

	  UpdatePDUCountersB(do_o, outFace, ok, false);
	}

      if (!ok)
	{
	  // Log Data drops
	  m_dropData (data, outFace);
	}
      else
	{
	  DidSendOutData (inFace, outFace, data, pitEntry);
	}

      return ok;
    }

    bool
    ICN3NForwardingStrategy::TryToSendDataInDU (Ptr<DU> du_o, Address addr,
						Ptr<Face> inFace,
						Ptr<Face> outFace,
						Ptr<const Data> data,
						Ptr<pit::Entry> pitEntry)
    {
      NS_LOG_FUNCTION (this);
      bool ok = false;

      if (m_ask_ACKP)
	{
	  int8_t n3ok = txPreparation (du_o, outFace, addr, false);
	  ok = (n3ok >= 0);
	}
      else
	{
	  txReset(du_o);
	  if (addr.IsInvalid ())
	    {
	      if (m_useQueues)
		ok = EnqueueDU(outFace, addr, du_o);
	      else
		ok = outFace->SendDU (du_o);
	    }
	  else
	    {
	      SetupFlowidSeq (du_o, outFace, addr, false);
	      if (m_useQueues)
		ok = EnqueueDU(outFace, addr, du_o);
	      else
		ok = outFace->SendDU (du_o, addr);
	    }

	  UpdatePDUCountersB (du_o, outFace, ok, false);
	}

      if (!ok)
	{
	  // Log Data drops
	  m_dropData (data, outFace);
	}
      else
	{
	  DidSendOutData (inFace, outFace, data, pitEntry);
	}

      return ok;
    }

    std::set<Ptr<const NNNAddress>, Ptr3NSizeMinComp>
    ICN3NForwardingStrategy::SelectMulticastDestinations (std::set<Ptr<const NNNAddress>, Ptr3NSizeMaxComp> data)
    {
      std::set<Ptr<const NNNAddress>, Ptr3NSizeMinComp> finalDests;

      std::set<Ptr<const NNNAddress>, Ptr3NSizeMinComp> parents;
      std::set<Ptr<const NNNAddress>, Ptr3NSizeMaxComp> leafs;
      std::set<Ptr<const NNNAddress>, Ptr3NSizeMaxComp> leftout;

      std::set<Ptr<const NNNAddress>, Ptr3NSizeMaxComp>::iterator it;

      Ptr<const NNNAddress> myAddr = GetNode3NNamePtr ();

      it = data.begin ();

      // Separate data into clearly Parents, Subsectors and outside names from current address
      while (it != data.end ())
	{
	  if (myAddr->isParentSectorSet(*it))
	    {
//	      NS_LOG_DEBUG ("Passing " << **it << " to leafs");
	      leafs.insert (*it);
	    }
	  else if (myAddr->isSubSectorSet(*it))
	    {
//	      NS_LOG_DEBUG ("Passing " << **it << " to parents");
	      parents.insert (*it);
	    }
	  else
	    {
//	      NS_LOG_DEBUG ("Passing " << **it << " to misfits");
	      leftout.insert (*it);
	    }
	  ++it;
	}

      // Filter out parents from leaf set
      std::set<Ptr<const NNNAddress>, Ptr3NSizeMaxComp> targetLeafs = NNNAddress::Filter3NParentNames(leafs);

//      NS_LOG_DEBUG ("Leaf destinations are:");
//      std::set<Ptr<const NNNAddress>, Ptr3NSizeMinComp>::iterator it3;
//
//      it3 = targetLeafs.begin ();
//      while (it3 != targetLeafs.end())
//	{
//	  NS_LOG_DEBUG ("Leaf to: " << **it3);
//	  ++it3;
//	}

      // Filter out parents from leftout set
      std::set<Ptr<const NNNAddress>, Ptr3NSizeMaxComp> targetMisfits = NNNAddress::Filter3NParentNames (leftout);

//      NS_LOG_DEBUG ("Misfit destinations are:");
//      std::set<Ptr<const NNNAddress>, Ptr3NSizeMinComp>::iterator it4;
//
//      it4 = targetMisfits.begin ();
//      while (it4 != targetMisfits.end())
//	{
//	  NS_LOG_DEBUG ("Misfit to: " << **it4);
//	  ++it4;
//	}

      if (!parents.empty())
	{
	  Ptr<const NNNAddress> parent = *(parents.begin ());
//	  NS_LOG_DEBUG ("Parent to test is: " << *parent);
	  bool add = true;
	  it = targetMisfits.begin ();
	  while (it != targetMisfits.end ())
	    {
	      if (myAddr->distance(**it) > myAddr->distance(*parent))
		{
//		  NS_LOG_DEBUG ("Sending to " << **it << " would take us past " << *parent << ", not inserting");
		  add = false;
		  break;
		}
	      ++it;
	    }

	  if (add)
	    finalDests.insert(parent);
	}

      // Now copy resulting addresses to finalDest
      std::set<Ptr<const NNNAddress>, Ptr3NSizeMaxComp>::iterator fin = targetMisfits.begin ();

      while (fin != targetMisfits.end ())
	{
	  finalDests.insert (*fin);
	  ++fin;
	}

      fin = targetLeafs.begin ();
      while (fin != targetLeafs.end ())
	{
	  finalDests.insert (*fin);
	  ++fin;
	}

      NS_LOG_DEBUG ("Final destinations to send to are:");
      std::set<Ptr<const NNNAddress>, Ptr3NSizeMinComp>::iterator it2;

      it2 = finalDests.begin ();
      while (it2 != finalDests.end())
	{
	  NS_LOG_DEBUG ("Final send to: " << **it2);
	  ++it2;
	}

      return finalDests;
    }

    NamesSeenContainer
    ICN3NForwardingStrategy::SelectMulticastDestinationsByTime (PartsSeenContainer con, Ptr<const NNNAddress> trigger)
    {
      NS_LOG_FUNCTION (this);

      std::set<Ptr<const NNNAddress>, Ptr3NSizeMaxComp> data;

      std::set<Ptr<const NNNAddress>, Ptr3NSizeMinComp> selection1;
      std::set<Ptr<const NNNAddress>, Ptr3NSizeMinComp>::iterator itselect;

      Time now = Simulator::Now ();

      // Obtain all the 3N name information from the NamesSeenContainer
      parts_seen_by_name& uname_index = con.get<p_3name> ();
      parts_seen_by_name::iterator it = uname_index.begin ();

      while (it != uname_index.end ())
	{
	  data.insert (it->m_name);
	  ++it;
	}

      // Include the trigger of this call
      data.insert (trigger);

      // Do the normal selection of destinations
      selection1 = SelectMulticastDestinations (data);

      itselect = selection1.begin ();

      NamesSeenContainer ret;

      parts_seen_by_name& name_index = con.get<p_3name> ();
      parts_seen_by_name::iterator itname, itname2;

      while (itselect != selection1.end ())
	{
	  std::tie(itname, itname2) = name_index.equal_range(*itselect);
	  if (itname != itname2)
	    {
	      ret.insert (NamesSeen (itname->m_name, itname->m_firstInserted, itname->m_lastSeen));
	    }
	  else
	    {
	      if (**itselect == *trigger)
		{
		  ret.insert (NamesSeen (*itselect));
		}
	      else
		{
		  NS_LOG_WARN ("Found name " << **itselect << "that wasn't in list or trigger. Check code!");
		}
	    }
	  ++itselect;
	}

      names_seen_by_uname& final_index = ret.get<i_uname> ();
      names_seen_by_uname::iterator it2 = final_index.begin ();

      NS_LOG_DEBUG ("Final destinations to send to are:");
      while (it2 != final_index.end ())
	{
	  NS_LOG_DEBUG ("Name: " << *(it2->name) << " Time: " << (it2->lastSeen).GetSeconds ());
	  ++it2;
	}

      return ret;
    }

    std::list<fib::FaceMetric>
    ICN3NForwardingStrategy::FindFacesBy3NDestinations (Ptr<fib::Entry> fibEntry)
    {
      std::list<fib::FaceMetric> ret;
      std::set<Ptr<Face> > seen;

      fib::FaceMetricContainer all = fibEntry->m_faces;

      PartsSeenContainer selection = fibEntry->Find3NDestinationsByTime(m_fib_3n_threshold);

      parts_seen_by_natural_order& name_time = selection.get<p_natural> ();
      parts_seen_by_natural_order::iterator itname = name_time.begin ();

      fib::faces_by_ptr& face_index =  all.get<fib::i_face> ();
      fib::faces_by_ptr::iterator itface;

      std::pair<Ptr<Face>, Address> tmp;
      Ptr<Face> foutFace = 0;
      Address destAddr;

      while (itname != name_time.end ())
	{
	  if (!itname->m_name || itname->m_name->isEmpty ())
	    {
	      ++itname;
	      continue;
	    }
	  tmp= m_nnst->ClosestSectorFaceInfo (itname->m_name, 0);

	  foutFace = tmp.first;
	  destAddr = tmp.second;

	  if (foutFace)
	    {
	      itface = face_index.find (foutFace);

	      if (itface != face_index.end ())
		{
		  if (seen.find (foutFace) == seen.end ())
		    {
		      seen.insert (foutFace);
		      ret.push_back(*itface);
		    }
		}
	    }
	  ++itname;
	}
      return ret;
    }




    std::pair<std::set<Ptr<Face> >,int>
    ICN3NForwardingStrategy::PropagateInterestinDO (Ptr<DO> pdu, Ptr<Face> inFace, Ptr<const Interest> interest,
			     Ptr<pit::Entry> pitEntry)
    {
      NS_LOG_FUNCTION (this);

      std::set<Ptr<Face> > ret;
      int propagatedCount = 0;
      Ptr<const NNNAddress> inDO3N = pdu->GetNamePtr();
      Ptr<const NNNAddress> tmpNewDest;
      bool nnptRedirect = false;
      Ptr<DATAPDU> pdu_o;
      Ptr<DO> do_o_spec;

      // Check if we are already at the destination (search through all the node names acquired)
      if (m_node_names->foundName(inDO3N))
	{
	  NS_LOG_INFO ("We have reached desired destination, looking for Apps");
	  Ptr<Face> tmpFace;
	  // We have reached the destination, look for Apps
	  for (uint32_t i = 0; i < m_faces->GetN (); i++)
	    {
	      tmpFace = m_faces->Get (i);
	      // Check that the Face is of type APPLICATION
	      if (tmpFace->isAppFace ())
		{
		  if (TrySendOutInterest(pdu, inFace, tmpFace, Address (), interest, pitEntry))
		    {
		      propagatedCount++;
		    }
		}
	    }
	  if (propagatedCount == 0)
	    {
	      // Specific case that sent a 3N PDU to a position with no Data knowingly
	      return std::make_pair(ret, -1);
	    }
	  else
	    {
	      return std::make_pair (ret, propagatedCount);
	    }
	}

      // We may have obtained a DEN so we need to check
      if (m_node_pdu_buffer->DestinationExists (inDO3N) && !m_nnpt->foundOldName(inDO3N))
	{
	  NS_LOG_INFO ("We are on " <<  GetNode3NName() << " we have been told to buffer this PDU to " << *inDO3N);
	  m_node_pdu_buffer->PushDO (*inDO3N, pdu);
	}

      // Check if the NNPT has any information for this particular 3N name
      if (m_in_transit_redirect && m_nnpt->foundOldName(inDO3N))
	{
	  // Retrieve the new 3N name destination and update variable
	  tmpNewDest = m_nnpt->findPairedNamePtr (inDO3N);
	  // Flag that the NNPT made a change
	  nnptRedirect = true;
	}
      else
	{
	  tmpNewDest = inDO3N;
	}

      if (nnptRedirect)
	{
	  NS_LOG_INFO ("On " << GetNode3NName() << " we create redirect from " << *inDO3N << " to " << *tmpNewDest);

	  // Create a new DO PDU to send the data
	  do_o_spec = Create<DO> ();
	  do_o_spec->SetFlowid(pdu->GetFlowid ());
	  do_o_spec->SetSequence(pdu->GetSequence ());
	  // Set the new 3N name
	  do_o_spec->SetName (tmpNewDest);
	  // Set the lifetime of the 3N PDU
	  do_o_spec->SetLifetime (m_3n_lifetime);
	  // Signal that the PDU had an ICN PDU as payload
	  do_o_spec->SetPDUPayloadType (ICN_NNN);
	  // Configure payload for PDU
	  do_o_spec->SetPayload (pdu->GetPayload ()->Copy ());

	  pdu_o = DynamicCast <DATAPDU> (do_o_spec);
	}
      else
	{
	  pdu_o = pdu;
	}

      // Roughly find the next hop
      std::pair<Ptr<Face>, Address> tmp = m_nnst->ClosestSectorFaceInfo (tmpNewDest, 0);

      Ptr<Face> foutFace = tmp.first;
      Address destAddr = tmp.second;

      if (!foutFace)
	{
	  NS_LOG_WARN ("On " << GetNode3NName() << " found no Face for " << *tmpNewDest);
	}
      else
	{
	  NS_LOG_INFO ("Attempting to send to " << *tmpNewDest << " via " << foutFace->GetId ());

	  if (TrySendOutInterest(pdu_o, inFace, foutFace, destAddr, interest, pitEntry))
	    {
	      ret.insert (foutFace);
	      propagatedCount++;
	    }
	}

      return std::make_pair(ret, propagatedCount);
    }



    std::pair<std::set<Ptr<Face> >,int>
    ICN3NForwardingStrategy::PropagateInterestinDU (Ptr<DU> pdu, Ptr<Face> inFace,
						    Ptr<const Interest> interest,
						    Ptr<pit::Entry> pitEntry)
    {
      NS_LOG_FUNCTION (this);

      std::set<Ptr<Face> > ret;

      int propagatedCount = 0;

      Ptr<const NNNAddress> inDU3N = pdu->GetDstNamePtr();
      Ptr<const NNNAddress> tmpNewDest;

      bool nnptRedirect = false;

      Ptr<DATAPDU> pdu_o;
      Ptr<DU> du_o_spec;

      // Check if we are already at the destination (search through all the node names acquired)
      if (m_node_names->foundName(inDU3N))
	{
	  NS_LOG_INFO ("We have reached desired destination, looking for Apps");
	  Ptr<Face> tmpFace;
	  // We have reached the destination, look for Apps
	  for (uint32_t i = 0; i < m_faces->GetN (); i++)
	    {
	      tmpFace = m_faces->Get (i);
	      // Check that the Face is of type APPLICATION
	      if (tmpFace->isAppFace ())
		{
		  if (TrySendOutInterest(pdu, inFace, tmpFace, Address (), interest, pitEntry))
		    {
		      ret.insert (tmpFace);
		      propagatedCount++;
		    }
		}
	    }
	  if (propagatedCount == 0)
	    {
	      // Specific case that sent a 3N PDU to a position with no Data knowingly
	      return std::make_pair(ret, -1);
	    }
	  else
	    {
	      return std::make_pair (ret, propagatedCount);
	    }
	}

      // We may have obtained a DEN so we need to check
      if (m_node_pdu_buffer->DestinationExists (inDU3N) && !m_nnpt->foundOldName(inDU3N))
	{
	  NS_LOG_INFO ("We are on " <<  GetNode3NName() << " we have been told to buffer this PDU to " << *inDU3N);
	  m_node_pdu_buffer->PushDU (*inDU3N, pdu);
	}

      // Check if the NNPT has any information for this particular 3N name
      if (m_in_transit_redirect && m_nnpt->foundOldName(inDU3N))
	{
	  // Retrieve the new 3N name destination and update variable
	  tmpNewDest = m_nnpt->findPairedNamePtr (inDU3N);
	  // Flag that the NNPT made a change
	  nnptRedirect = true;
	}
      else
	{
	  tmpNewDest = inDU3N;
	}

      if (nnptRedirect)
	{
	  NS_LOG_INFO ("On " << GetNode3NName() << " we create redirect from " << *inDU3N << " to " << *tmpNewDest);

	  // Create a new DU PDU to send the data
	  du_o_spec = Create<DU> ();
	  du_o_spec->SetFlowid(pdu->GetFlowid ());
	  du_o_spec->SetSequence(pdu->GetSequence ());
	  // Use the original DU's Src 3N name
	  du_o_spec->SetSrcName (pdu->GetSrcNamePtr ());
	  // Set the new 3N name destination
	  du_o_spec->SetDstName (tmpNewDest);
	  // Set the lifetime of the 3N PDU
	  du_o_spec->SetLifetime (m_3n_lifetime);
	  // Use the original DU's Src 3N name
	  du_o_spec->SetAuthoritative (pdu->IsAuthoritative ());
	  // Signal that the PDU had an ICN PDU as payload
	  du_o_spec->SetPDUPayloadType (ICN_NNN);
	  // Configure payload for PDU
	  du_o_spec->SetPayload (pdu->GetPayload ()->Copy ());

	  pdu_o = DynamicCast <DATAPDU> (du_o_spec);
	}
      else
	{
	  pdu_o = pdu;
	}

      // Roughly find the next hop
      std::pair<Ptr<Face>, Address> tmp = m_nnst->ClosestSectorFaceInfo (tmpNewDest, 0);

      Ptr<Face> foutFace = tmp.first;
      Address destAddr = tmp.second;

      if (!foutFace)
	{
	  NS_LOG_WARN ("On " << GetNode3NName() << " found no Face for " << *tmpNewDest);
	}
      else
	{
	  NS_LOG_INFO ("Attempting to send to " << *tmpNewDest << " via " << foutFace->GetId ());

	  if (TrySendOutInterest(pdu_o, inFace, foutFace, destAddr, interest, pitEntry))
	    {
	      ret.insert (foutFace);
	      propagatedCount++;
	    }
	}

      return std::make_pair(ret, propagatedCount);
    }

    std::tuple<uint32_t,uint32_t,bool>
    ICN3NForwardingStrategy::FindFlowidSeq (Ptr<DATAPDU> pdu, uint32_t count)
    {
      uint32_t r_flowid = 0;
      if (pdu)
	r_flowid = pdu->GetFlowid ();
      else
	r_flowid = count;

      uint32_t s_flowid = 0;
      uint32_t seq = 0;
      bool first = true;

      if (m_combo_flowids.left.find(r_flowid) == m_combo_flowids.left.end ())
	{
	  s_flowid = GenerateAppFlowid();
	  m_combo_flowids.insert (fw_flowid_combo(r_flowid, s_flowid));
	  m_3n_seq.insert(std::pair<uint32_t,uint32_t>(s_flowid, 0));
	}
      else
	{
	  s_flowid = m_combo_flowids.left.find(r_flowid)->second;
	  seq = m_3n_seq.find(s_flowid)->second;
	  first = false;
	}

      return std::make_tuple(s_flowid, seq, first);
    }

    void
    ICN3NForwardingStrategy::NotifyNewAggregate ()
    {
      NS_LOG_FUNCTION (this);

      ForwardingStrategy::NotifyNewAggregate ();

      if (m_pit == 0)
	{
	  m_pit = GetObject<Pit> ();
	}
      if (m_fib == 0)
	{
	  m_fib = GetObject<Fib> ();
	  m_awaiting_mapme_acks->SetFIB(m_fib);
	}
      if (m_contentStore == 0)
	{
	  m_contentStore = GetObject<ContentStore> ();
	}
      if (m_tfib == 0)
	{
	  m_tfib = GetObject<TFib> ();
	}
    }

    void
    ICN3NForwardingStrategy::DoDispose ()
    {
      m_pit = 0;
      m_fib = 0;
      m_contentStore = 0;
      m_tfib = 0;
      m_awaiting_mapme_acks = 0;

      ForwardingStrategy::DoDispose ();
    }

    bool
    ICN3NForwardingStrategy::EnqueueInterest (Ptr<Face> face, Address dst, Ptr<const Interest> pdu)
    {
      uint32_t faceid = face->GetId ();

      m_face_3n_address_queue[faceid].push(dst);
      m_face_3n_pdu_queue[faceid]->pushInterest(pdu, Simulator::Now());
      return true;
    }

    bool
    ICN3NForwardingStrategy::EnqueueData (Ptr<Face> face, Address dst, Ptr<const Data> pdu)
    {
      uint32_t faceid = face->GetId ();

      m_face_3n_address_queue[faceid].push(dst);
      m_face_3n_pdu_queue[faceid]->pushData(pdu, Simulator::Now());
      return true;
    }

  } /* namespace nnn */
} /* namespace ns3 */
