/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil -*- */
/*
 * Copyright (c) 2015 Jairo Eduardo Lopez
 *
 *   This file is part of nnnsim.
 *
 *  nnn-icn-smart-flooding.cc is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  nnn-icn-smart-flooding.cc is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with nnn-icn-smart-flooding.cc. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Author: Jairo Eduardo Lopez <jairo@ruri.waseda.jp>
 */

#include "nnn-icn-smart-flooding.h"

#include <boost/foreach.hpp>
#include <boost/lambda/bind.hpp>
#include <boost/lambda/lambda.hpp>
#include <boost/ref.hpp>
#include <boost/tuple/tuple.hpp>

#include "ns3/log.h"
#include "ns3/nnn-addr-aggregator.h"
#include "ns3/nnn-face.h"
#include "ns3/nnn-face-container.h"
#include "ns3/nnn-rxbuffercontainer.h"
#include "ns3/nnn-fib.h"
#include "ns3/nnn-fib-entry.h"
#include "ns3/nnn-tfib.h"
#include "ns3/nnn-header-helper.h"
#include "ns3/nnn-icn-content-store.h"
#include "ns3/nnn-icn-header-helper.h"
#include "ns3/nnn-icn-naming.h"
#include "ns3/nnn-icn-pdus.h"
#include "ns3/nnn-names-container.h"
#include "ns3/nnn-naming.h"
#include "ns3/nnn-nnnsim-icn-wire.h"
#include "ns3/nnn-nnpt.h"
#include "ns3/nnn-nnst.h"
#include "ns3/nnn-nnst-entry.h"
#include "ns3/nnn-nnst-entry-facemetric.h"
#include "ns3/nnn-pdu-buffer.h"
#include "ns3/nnn-rxbuffercontainer.h"
#include "ns3/nnn-pdus.h"
#include "ns3/nnn-pit-entry-incoming-face.h"
#include "ns3/nnn-pit-entry-outgoing-face.h"
#include "ns3/nnn-wire.h"
#include "ns3/wire-nnnsim-naming.h"

namespace ns3
{
  NS_LOG_COMPONENT_DEFINE ("nnn.fw.icn.sf");

  namespace nnn
  {
    NS_OBJECT_ENSURE_REGISTERED (ICN3NSmartFlooding);

    TypeId
    ICN3NSmartFlooding::GetTypeId (void)
    {
      static TypeId tid = TypeId ("ns3::nnn::ICN3NSmartFlooding")
	  .SetGroupName ("Nnn")
	  .SetParent<ICN3NForwardingStrategy> ()
	  .AddConstructor<ICN3NSmartFlooding> ()
	  ;
      return tid;
    }

    ICN3NSmartFlooding::ICN3NSmartFlooding ()
    {
      m_EnableTFIB = false;
      m_sharePoAInfo = false;
      m_EnableNonAuthoritativeDU = false;
      m_Enable_Flooding = false;

      m_node_names->RegisterCallbacks(
	  MakeCallback (&ICN3NForwardingStrategy::Reenroll, this),
	  MakeCallback (&ICN3NForwardingStrategy::Enroll, this)
      );

      // Must turn on the supported protocols
      m_n1minus_3n_support = true;
      m_n1minus_icn_support = true;

      // Creation of the separate PIT
      ObjectFactory pit_factory;
      pit_factory.SetTypeId("ns3::nnn::pit::Persistent");

      m_awaiting_mapme_acks = pit_factory.Create<Pit> ();

      m_awaiting_mapme_acks->SetFIB(m_fib);
    }

    ICN3NSmartFlooding::~ICN3NSmartFlooding ()
    {
    }

    void
    ICN3NSmartFlooding::UpdatePITEntry (Ptr<pit::Entry> pitEntry, Ptr<DATAPDU> pdu, Ptr<Face> face, Time lifetime)
    {
      NS_LOG_DEBUG ("Processing Interest SeqNo " << std::dec << pitEntry->GetPrefix ().get (-1).toSeqNum () << " Face: " << face->GetId ());

      if (pdu == 0)
	{
	  // Received a raw ICN PDU
	  pitEntry->AddIncoming(face, true);
	  pitEntry->UpdateLifetime (lifetime);
	  return;
	}

      // Pointers for PDU types
      Ptr<SO> so_i;
      Ptr<DU> du_i;

      // Find out the type of PDU we are dealing with
      uint32_t pduid = pdu->GetPacketId ();
      switch(pduid)
      {
	case SO_NNN:
	  // Convert pointer to SO
	  so_i = DynamicCast<SO> (pdu);
	  // Add the Face and 3N name
	  pitEntry->AddIncoming (face, so_i->GetNamePtr ());
	  break;
	case DU_NNN:
	  // Convert pointer to DU
	  du_i = DynamicCast<DU> (pdu);
	  // Add the Face and 3N name
	  pitEntry->AddIncoming (face, du_i->GetSrcNamePtr ());
	  break;
	default:
	  // For NULL or DO, just add the Face
	  pitEntry->AddIncoming(face);
	  break;
      }
      // Update PIT entry lifetime
      pitEntry->UpdateLifetime (lifetime);
    }

    void
    ICN3NSmartFlooding::UpdateLayeredPITEntry (Ptr<pit::Entry> pitEntry,
					       Ptr<DATAPDU> pdu, Ptr<Face> face,
					       Time lifetime,
					       const Address& from)
    {
      if (!pdu)
	{
	  NS_LOG_INFO ("Saving Interest SeqNo " << std::dec << pitEntry->GetPrefix ().get (-1).toSeqNum () <<
		       " from  Face: " << face->GetId () << " PoA: " << from);
	  // Received a raw ICN PDU
	  pitEntry->AddIncoming(face, from, true, false);
	  pitEntry->UpdateLifetime (lifetime);
	}
      else
	{
	  // Pointers for PDU types
	  bool wasNULL = false;
	  Ptr<SO> so_i;
	  bool wasSO = false;
	  bool wasDO = false;
	  Ptr<DU> du_i;
	  bool wasDU = false;

	  // Find out the type of PDU we are dealing with
	  uint32_t pduid = pdu->GetPacketId ();
	  switch(pduid)
	  {
	    case NULL_NNN:
	      wasNULL = true;
	      break;
	    case SO_NNN:
	      // Convert pointer to SO PDU
	      so_i = DynamicCast<SO> (pdu);
	      wasSO = true;
	      break;
	    case DO_NNN:
	      wasDO = true;
	      break;
	    case DU_NNN:
	      // Convert pointer to DU PDU
	      du_i = DynamicCast<DU> (pdu);
	      wasDU = true;
	      break;
	    default:
	      break;
	  }

	  if (wasNULL || wasDO)
	    {
	      NS_LOG_INFO ("Saving Interest SeqNo " << std::dec << pitEntry->GetPrefix ().get (-1).toSeqNum () <<
			   " from Face: " << face->GetId () << " PoA: " << from << " in PDU with no 3N SRC");
	      pitEntry->AddIncoming (face, from, false, true);
	    }
	  else if (wasSO || wasDU)
	    {
	      pitEntry->AddIncoming (face, from, false, false);

	      if (wasSO)
		{
		  NS_LOG_INFO ("Saving Interest SeqNo " << std::dec << pitEntry->GetPrefix ().get (-1).toSeqNum () <<
			       " from Face: " << face->GetId () << " PoA: " << from << " in PDU with 3N SRC " << *(so_i->GetNamePtr ()));
		  pitEntry->AddIncoming (face, so_i->GetNamePtr ());
		}
	      else
		{
		  NS_LOG_INFO ("Saving Interest SeqNo " << std::dec << pitEntry->GetPrefix ().get (-1).toSeqNum () <<
			       " from Face: " << face->GetId () << " PoA: " << from << " in PDU with 3N SRC " << *(du_i->GetSrcNamePtr ()));
		  pitEntry->AddIncoming (face, du_i->GetSrcNamePtr ());
		}
	    }
	  // Update PIT entry lifetime
	  pitEntry->UpdateLifetime (lifetime);
	}
    }

    // Test function to update FIB with 3N names
    void
    ICN3NSmartFlooding::UpdateFIBEntry (Ptr<DATAPDU> pdu, Ptr<Face> inface, Ptr<const icn::Name> interest)
    {
      NS_LOG_FUNCTION (inface->GetId () << interest->toUri ());

      // Cut the Interest to have only the basic data (we assume that the ending is the sequence as in ndnSIM)
      // Does not handle icn::Name postfix (which has been deactivated from Producer)
      icn::Name dirname = interest->getSubName (0, interest->size()-1);
      Ptr<icn::Name> ptr_dirname = Create<icn::Name> (dirname);

      uint32_t part = interest->getSeqNum();

      Ptr<fib::Entry> fibEntry;

//      NS_LOG_DEBUG ("Initial " << *m_fib);

      if (!pdu)
	{
	  NS_LOG_DEBUG ("There is no 3N information in ICN packets");
	  NS_LOG_INFO ("Updating FIB for " << dirname);

	  fibEntry = m_fib->Find (dirname);

	  if (!fibEntry)
	    {
	      NS_LOG_INFO ("No FIB found for " << dirname << " will create");
	      // I am heavily assuming that an application would have already
	      // installed an FIB entry. If an entry from an Application using 3N
	      // isn't stopped, then it might cause loops
	      if (!inface->isAppFace())
		{
		  NS_LOG_DEBUG ("Adding information for " << dirname << " via Face " << inface->GetId ());
		  fibEntry = m_fib->Add (dirname, inface, m_icn_fib_metric);
		}
	      else
		{
		  NS_LOG_WARN ("Received a ICN packet from an application. Probably a push. Please verify if this is correct");
		}
	    }
	  else
	    {
	      fibEntry->AddOrUpdateRoutingMetric(inface, m_icn_fib_metric);
	    }
	}
      else
	{
	  bool has3N = pdu->has3NSrc();
	  Ptr<const NNNAddress> newDest = 0;
	  bool newDestUD = false;
	  if (has3N)
	    {
	      newDest = pdu->GetSrcNamePtr ();

	      if (newDest->compare (this->GetNode3NName ()) != 0)
		{
		  NS_LOG_DEBUG ("There is an update to the 3N source to " << *newDest << " Part: " << part);
		  newDestUD = true;
		}
	    }

	  // Search for the FIB matching the Interest
	  fibEntry = m_fib->Find (dirname);

	  if (!fibEntry)
	    {
	      NS_LOG_INFO ("No FIB found for " << dirname << " will create");
	      // I am heavily assuming that an application would have already
	      // installed an FIB entry. If an entry from an Application using 3N
	      // isn't stopped, then it might cause loops
	      if (!inface->isAppFace())
		{
		  if (newDestUD)
		    {
		      NS_LOG_DEBUG ("Adding FIB for " << dirname << " with 3N name " << *newDest << " to Face " << inface->GetId ());
		      fibEntry = m_fib->Add (dirname, inface, m_icn_fib_metric, newDest);

		      m_fib->Update3NSrcDestination(ptr_dirname, newDest, part);
//		      fibEntry->Update3NSrcDestination(newDest, part);
		    }
		  else
		    {
		      NS_LOG_DEBUG ("Adding FIB for " << dirname << " to Face " << inface->GetId ());
		      fibEntry = m_fib->Add (dirname, inface, m_icn_fib_metric);
		    }
		}
	      else
		{
		  NS_LOG_WARN ("Received a ICN packet from an application. Probably a push. Please verify if this is correct");
		}
	    }
	  else
	    {
	      if (newDestUD)
		{
		  NS_LOG_INFO ("Found FIB for " << dirname << " updating to " << *newDest);
		  m_fib->Update3NSrcDestination(ptr_dirname, newDest, part);
//		  fibEntry->Update3NSrcDestination(newDest, part);
		}
	    }
	}
      NS_LOG_DEBUG ("Final " << *m_fib);
    }

    void
    ICN3NSmartFlooding::UpdateLayeredFIBEntry (Ptr<DATAPDU> pdu, Ptr<Face> inface, Ptr<const icn::Name> interest, const Address& from, const Address& to)
    {
      NS_LOG_FUNCTION (inface->GetId () << interest->toUri () << from << to);

      Ptr<fib::Entry> fibEntry;

//      NS_LOG_DEBUG ("Initial " << *m_fib);

      // Cut the Interest to have only the basic data (we assume that the ending is the sequence as in ndnSIM)
      // Does not handle icn::Name postfix (which has been deactivated from Producer)
      icn::Name dirname = interest->getSubName (0, interest->size()-1);
      Ptr<icn::Name> ptr_dirname = Create<icn::Name> (dirname);
      uint32_t part = interest->getSeqNum();

      if (!pdu)
	{
	  NS_LOG_DEBUG ("There is no 3N information in ICN packets");

	  NS_LOG_INFO ("Updating FIB for " << dirname);

	  fibEntry = m_fib->Find (dirname);

	  if (!fibEntry)
	    {
	      NS_LOG_INFO ("No FIB found for " << dirname << " will create");
	      // I am heavily assuming that an application would have already
	      // installed an FIB entry. If an entry from an Application using 3N
	      // isn't stopped, then it might cause loops
	      if (!inface->isAppFace())
		{
		  NS_LOG_DEBUG ("Adding information for " << dirname << " via Face " << inface->GetId () << " PoA: " << from);
		  fibEntry = m_fib->Add (dirname, inface, m_icn_fib_metric, from);
		  NS_LOG_DEBUG ("Updating PoA " << from << " Part: " << part << " table");
//		  fibEntry->UpdatePoADestination (inface, from, part, m_icn_fib_metric);
		  m_fib->UpdatePoASrcDestination(ptr_dirname, inface, from, part, m_icn_fib_metric);
		}
	      else
		{
		  NS_LOG_WARN ("Received a ICN packet from an application. Probably a push. Please verify if this is correct");
		}
	    }
	  else
	    {
	      NS_LOG_DEBUG ("Updating PoA " << from << " Part: " << part << " table");
//	      fibEntry->UpdatePoADestination (inface, from, part, m_icn_fib_metric);
	      m_fib->UpdatePoASrcDestination(ptr_dirname, inface, from, part, m_icn_fib_metric);
	    }
	}
      else
	{
	  bool has3N = pdu->has3NSrc();
	  Ptr<const NNNAddress> newDest = 0;
	  bool newDestUD = false;
	  if (has3N)
	    {
	      newDest = pdu->GetSrcNamePtr ();

	      if (newDest->compare (this->GetNode3NName ()) != 0)
		{
		  NS_LOG_DEBUG ("There is an update to the 3N source to " << *newDest << " Part: " << part);
		  newDestUD = true;
		}
	    }

	  // Search for the FIB matching the Interest
	  fibEntry = m_fib->Find (dirname);

	  if (!fibEntry)
	    {
	      NS_LOG_INFO ("No FIB found for " << dirname << " will create");
	      // I am heavily assuming that an application would have already
	      // installed an FIB entry. If an entry from an Application using 3N
	      // isn't stopped, then it might cause loops
	      if (!inface->isAppFace())
		{
		  if (newDestUD)
		    {
		      NS_LOG_DEBUG ("Adding FIB for " << dirname << " with 3N name " << *newDest << " to Face " << inface->GetId ());
		      fibEntry = m_fib->Add (dirname, inface, m_icn_fib_metric, newDest);
//		      fibEntry->Update3NSrcDestination(newDest, part);
		      m_fib->Update3NSrcDestination(ptr_dirname, newDest, part);
		      NS_LOG_DEBUG ("Updating PoA " << from << " Part: " << part << " table");
//		      fibEntry->UpdatePoADestination (inface, from, part, m_icn_fib_metric);
		      m_fib->UpdatePoASrcDestination(ptr_dirname, inface, from, part, m_icn_fib_metric);
		    }
		  else
		    {
		      NS_LOG_DEBUG ("Adding FIB for " << dirname << " to Face " << inface->GetId ());
		      fibEntry = m_fib->Add (dirname, inface, m_icn_fib_metric);
		      NS_LOG_DEBUG ("Updating PoA " << from << " Part: " << part << " table");
//		      fibEntry->UpdatePoADestination (inface, from, part, m_icn_fib_metric);
		      m_fib->UpdatePoASrcDestination(ptr_dirname, inface, from, part, m_icn_fib_metric);
		    }
		}
	      else
		{
		  NS_LOG_WARN ("Received a ICN packet from an application. Probably a push. Please verify if this is correct");
		}
	    }
	  else
	    {
	      if (newDestUD)
		{
		  NS_LOG_INFO ("Found FIB for " << dirname << " updating to " << *newDest);
		  //		  fibEntry->Update3NSrcDestination(newDest, part);
		  m_fib->Update3NSrcDestination(ptr_dirname, newDest, part);
		}
//	      fibEntry->UpdatePoADestination (inface, from, part, m_icn_fib_metric);
	      m_fib->UpdatePoASrcDestination(ptr_dirname, inface, from, part, m_icn_fib_metric);
	    }
	}

      NS_LOG_DEBUG ("Final " << *m_fib);
    }

    void
    ICN3NSmartFlooding::ProcessLayeredMapMe (Ptr<DATAPDU> pdu, Ptr<Face> inFace,
					      Ptr<Interest> mapme, const Address& from)
    {
      NS_LOG_FUNCTION (this);

      // Log the Interest packet
      m_inInterests (mapme, inFace);

      // Verify if the TFIB is enabled
      if (m_EnableTFIB)
	{
	  Ptr<const NNNAddress> gotten_src = 0;

	  if (pdu->has3NSrc ())
	    gotten_src = pdu->GetSrcNamePtr();

	  if (mapme->GetInterestType () == Interest::MAP_ME_INTEREST_UPDATE)
	    {
	      NS_LOG_INFO ("Received an Interest with MapMe Update via Face " << inFace->GetId () << " PoA: " << from);

	      if (pdu->has3NSrc ())
		NS_LOG_INFO ("Updating TFIB and FIB for " << mapme->GetName () << " with sequence " << mapme->GetSequenceNumber () << " 3N Src: " << *gotten_src);
	      else
		NS_LOG_INFO ("Updating TFIB and FIB for " << mapme->GetName () << " with sequence " << mapme->GetSequenceNumber ());

	      Ptr<tfib::Entry> tfib_entry = m_tfib->Find (mapme->GetName ());

	      NS_LOG_DEBUG ("Previous " << *m_tfib);
	      bool forwardMapMe = false;
	      // If we find no information in the TFIB for this information, we
	      // can just go ahead and add the information to the FIB
	      if (tfib_entry == 0)
		{
		  NS_LOG_INFO ("Completely new info for prefix: " << mapme->GetName ());
		  NS_LOG_DEBUG ("Introducing name: " << mapme->GetName () << " Face: " << inFace->GetId () << " PoA: " << from);
		  NS_LOG_DEBUG ("Sequence: " << mapme->GetSequenceNumber () << " Retransmission: " << mapme->GetRetransmissionTime ());
		  NS_LOG_DEBUG ("Part: " << mapme->GetPartNumber ());
		  if (pdu->has3NSrc ())
		    NS_LOG_DEBUG ("3N Src: " << *gotten_src);

		  m_tfib->Add(inFace, from, mapme, gotten_src);

		  forwardMapMe = true;
		}
	      else
		{
		  if (tfib_entry->GetNewestSequence () < mapme->GetSequenceNumber ())
		    {
		      NS_LOG_INFO ("Updating new information for prefix: " << mapme->GetName () << " PoA: " << from);
		      NS_LOG_DEBUG ("New sequence for for prefex: " << mapme->GetName () << " is: " << mapme->GetSequenceNumber ());
		      if (pdu->has3NSrc ())
			NS_LOG_DEBUG ("3N Src: " << *gotten_src);
		      // Found new information, updating TFIB
		      tfib_entry->AddOrUpdate(inFace, from, mapme, gotten_src);

		      forwardMapMe = true;
		    }
		  else
		    {
		      NS_LOG_WARN ("Obtained an MapMe Interest with an outdated sequence number");
		      m_dropInterests (mapme, inFace);
		    }
		}

	      NS_LOG_DEBUG ("Current " << *m_tfib);

	      // If we have new information, forward the Interest with MapMe information
	      if (forwardMapMe)
		{
		  // Now set up the ACKs and forward the information
		  // Search for the temporary PIT with the Interest
		  Ptr<pit::Entry> mapme_ack_Entry = m_awaiting_mapme_acks->Lookup (*mapme);
		  if (mapme_ack_Entry == 0)
		    {
		      mapme_ack_Entry = m_awaiting_mapme_acks->Create (mapme);
		      if (mapme_ack_Entry != 0)
			{
			  NS_LOG_INFO ("Created entry to wait for MapMe ACK for " << mapme->GetName () << " SeqN: " << mapme->GetPartNumber());
			}
		      else
			{
			  NS_LOG_WARN ("Couldn't create entry to wait for MapMe ACK for " << mapme->GetName () << " SeqN: " << mapme->GetPartNumber());
			  return;
			}

		      UpdateLayeredPITEntry (mapme_ack_Entry, pdu, inFace, Seconds (1.0), from);
		    }
		  else
		    {
		      NS_LOG_WARN ("We already have an ACK waiting for " << mapme->GetName() << " SeqN: " << mapme->GetPartNumber() << " check logs");
		    }

		  // Transmit a MapMe Interest packet
		  RetransmitLayeredMapMe (pdu, inFace, mapme, from);

		  // Create a MapMe ACK to deal with the Awaiting PIT structure
		  TransmitLayeredMapMeAck(pdu, mapme_ack_Entry->GetInterest (), inFace, from);
		}
	    }
	  else if (mapme->GetInterestType () == Interest::MAP_ME_INTEREST_UPDATE_ACK)
	    {
	      NS_LOG_INFO ("Received an Interest with MapMe ACK via Face " << inFace->GetId () << " PoA: " << from);
	      NS_LOG_INFO ("Obtained an ACK for Interest MapMe: " << mapme->GetName ());

	      LayeredMapMeAckProcess(pdu, inFace, mapme, from);
	    }
	  else
	    {
	      NS_LOG_WARN ("Obtained an MapMe Interest packet with unknown type!");
	      m_dropInterests (mapme, inFace);
	    }
	}
      else
	{
	  NS_LOG_WARN ("Obtained an MapMe Interest packet, but node does not support TFIB");
	  m_dropInterests (mapme, inFace);
	}
    }

    void
    ICN3NSmartFlooding::LayeredMapMeAckProcess (Ptr<DATAPDU> pdu, Ptr<Face> face, Ptr<Interest> mapme,
						const Address& from)
    {
      NS_LOG_FUNCTION (this);

      Ptr<pit::Entry> pitEntry = m_awaiting_mapme_acks->Lookup (*mapme);

      if (pitEntry != 0)
	{
	  Ptr<tfib::Entry> tfib_entry = m_tfib->Find(mapme->GetName ());

	  if (tfib_entry == 0)
	    {
	      NS_LOG_WARN ("Did not find a TFIB entry for " << mapme->GetName ());
	      return;
	    }

	  uint32_t curr_seq = mapme->GetSequenceNumber ();

	  NS_LOG_DEBUG ("Current " << *m_tfib);

	  if (!tfib_entry->HasSequence(curr_seq))
	    {
	      NS_LOG_WARN ("Interest MapMe ACK is using a SeqN: " << curr_seq << " for which we have no information, returning");
	      return;
	    }
	  else
	    {
	      uint32_t nst_seq = tfib_entry->GetNewestSequence();
	      if (curr_seq != nst_seq)
		{
		  NS_LOG_WARN ("Interest MapMe ACK received with SeqN: " << curr_seq << " but newest is SeqN: " << nst_seq << " verify");
		}
	      else
		{
		  NS_LOG_DEBUG ("Interest MapMe ACK is using a SeqN: " << curr_seq);
		}
	    }

	  NS_LOG_DEBUG ("Removing Face " << face->GetId () << " from Interest MapMe ACK waiting list for " << mapme->GetName ());
	  // Remove the MapMe information from the PIT
	  pitEntry->RemoveOutgoing(face);

	  icn::Name mapmed_name = mapme->GetName ().getBaseName ();

	  Ptr<icn::Name> mapmed_ptr = Create<icn::Name> (mapmed_name);

	  if (pitEntry->GetOutgoingCount () == 1)
	    {
	      // Double check that we actually have a destination on the Face given
	      pit::Entry::out_container tmp = pitEntry->GetOutgoing();

	      pit::Entry::out_iterator it = tmp.begin ();

	      pit::OutgoingFace itface = *it;

	      std::vector<Ptr<const NNNAddress> > ret = m_nnst->OneHopNameInfo(GetNode3NNamePtr(), itface.m_face);

	      if (ret.empty ())
		{
		  NS_LOG_WARN ("Remaining Face: " << itface.m_face->GetId() << " for " << mapmed_name << " has no apparent PoAs, removing and continuing");
		  pitEntry->RemoveOutgoing (itface.m_face);
		}
	      else
		{
		  std::string names = "";

		  std::vector<Ptr<const NNNAddress> >::iterator it3n;

		  for (it3n = ret.begin() ; it3n != ret.end (); it3n++)
		    {
		      names += (*it3n)->toDotHexPrt();
		    }

		  NS_LOG_DEBUG ("For remaining Face: " << itface.m_face->GetId () << " waiting for " << mapmed_name << " from nodes: " << names);
		}
	    }

	  if (pitEntry->GetOutgoingCount() == 0)
	    {
	      NS_LOG_DEBUG ("Have obtained all Interest MapMe ACKs, removing entries");
	      // If we have obtained all the ACKs we were waiting for, mark for erase
	      pitEntry->ClearIncoming ();
	      m_awaiting_mapme_acks->MarkErased(pitEntry);

	      // Having ACKed all MapMe information, proceed to update FIB
	      Ptr<fib::Entry> fib_entry = m_fib->Find(mapmed_name);

	      NS_LOG_DEBUG ("Previous " << *m_fib);

	      bool have3N = false;
	      if (!(tfib_entry->Get3NNamePtr()))
		{
		  NS_LOG_DEBUG ("Will update "<< mapmed_name << " Face " << tfib_entry->GetFace (curr_seq)->GetId () <<
				" PoA: " << tfib_entry->GetPoA (curr_seq) << " using only PoA");
		}
	      else
		{
		  have3N = true;
		  NS_LOG_DEBUG ("Will update "<< mapmed_name << " Face " << tfib_entry->GetFace (curr_seq)->GetId () <<
				" PoA: " << tfib_entry->GetPoA (curr_seq) << " 3N Src: " << *(tfib_entry->Get3NNamePtr (curr_seq)));
		}

	      if (fib_entry == 0)
		{
		  if (have3N)
		    {
		      NS_LOG_DEBUG ("Did not find information for " << mapmed_name <<
				    " introducing to FIB with 3N Src: " << *(tfib_entry->Get3NNamePtr()) << " Part: " << tfib_entry->GetPart(curr_seq));
		      fib_entry = m_fib->Add(mapmed_name, tfib_entry->GetFace (curr_seq), m_icn_fib_metric, tfib_entry->Get3NNamePtr(curr_seq), tfib_entry->GetPoA (curr_seq));
//		      fib_entry->Update3NSrcDestination(tfib_entry->Get3NNamePtr(curr_seq), tfib_entry->GetPart(curr_seq));
		      m_fib->Update3NSrcDestination(mapmed_ptr, tfib_entry->Get3NNamePtr(curr_seq), tfib_entry->GetPart(curr_seq));
//		      fib_entry->UpdatePoADestination(tfib_entry->GetFace (curr_seq), tfib_entry->GetPoA (curr_seq), m_icn_fib_metric);
		      m_fib->UpdatePoASrcDestination(mapmed_ptr, tfib_entry->GetFace (curr_seq), tfib_entry->GetPoA (curr_seq),
						     tfib_entry->GetPart(curr_seq), m_icn_fib_metric);
		    }
		  else
		    {
		      NS_LOG_DEBUG ("Did not find information for " << mapmed_name <<
				    " introducing to FIB using PoA: " << tfib_entry->GetPoA (curr_seq) << " Part: " << tfib_entry->GetPart(curr_seq));
		      // Didn't find an exact match, introduce it
		      fib_entry = m_fib->Add(mapmed_name, tfib_entry->GetFace (curr_seq), m_icn_fib_metric, tfib_entry->GetPoA (curr_seq));
//		      fib_entry->UpdatePoADestination(tfib_entry->GetFace (curr_seq), tfib_entry->GetPoA (curr_seq),
//						      tfib_entry->GetPart(curr_seq), m_icn_fib_metric);
		      m_fib->UpdatePoASrcDestination(mapmed_ptr, tfib_entry->GetFace (curr_seq), tfib_entry->GetPoA (curr_seq),
						     tfib_entry->GetPart(curr_seq), m_icn_fib_metric);
		    }
		}
	      else
		{
		  if (have3N)
		    {
		      NS_LOG_DEBUG ("Found match for " << mapmed_name << " updating FIB with 3N Src: " <<
				    *(tfib_entry->Get3NNamePtr(curr_seq)) << " Part: " <<  tfib_entry->GetPart(curr_seq));
//		      fib_entry->Update3NSrcDestination(tfib_entry->Get3NNamePtr(), tfib_entry->GetPart(curr_seq));
		      m_fib->Update3NSrcDestination(mapmed_ptr, tfib_entry->Get3NNamePtr(curr_seq), tfib_entry->GetPart(curr_seq));
//		      fib_entry->UpdatePoADestination(tfib_entry->GetFace (curr_seq), tfib_entry->GetPoA (curr_seq), m_icn_fib_metric);
		      m_fib->UpdatePoASrcDestination(mapmed_ptr, tfib_entry->GetFace (curr_seq), tfib_entry->GetPoA (curr_seq),
						     tfib_entry->GetPart(curr_seq), m_icn_fib_metric);
		    }
		  else
		    {
		      NS_LOG_DEBUG ("Found match for " << mapmed_name << " updating FIB with PoA: " <<
				    tfib_entry->GetPoA (curr_seq)  << "Part: " << tfib_entry->GetPart (curr_seq));
//		      fib_entry->UpdatePoADestination(tfib_entry->GetFace (curr_seq), tfib_entry->GetPoA (curr_seq),
//						      tfib_entry->GetPart(curr_seq), m_icn_fib_metric);
		      m_fib->UpdatePoASrcDestination(mapmed_ptr, tfib_entry->GetFace (curr_seq), tfib_entry->GetPoA (curr_seq),
						     tfib_entry->GetPart(curr_seq), m_icn_fib_metric);
		    }

		  // Found an exact match, update it
		  fib_entry->AddOrUpdateRoutingMetric(tfib_entry->GetFace (curr_seq), m_icn_fib_metric);
		}
	      fib_entry->UpdateStatus(tfib_entry->GetFace (curr_seq), tfib_entry->GetPoA (curr_seq), fib::FaceMetric::ICN_FIB_GREEN);

	      NS_LOG_DEBUG ("Current " << *m_fib);
	    }
	  else
	    {
	      NS_LOG_DEBUG ("Still waiting for " << pitEntry->GetOutgoingCount () << " ACKs for SeqN: " << curr_seq);
	    }
	}
      else
	{
	  NS_LOG_WARN ("We are not waiting for an ACK for MapMe: " << mapme->GetName ());
	  m_dropInterests (mapme, face);
	}
    }

    void
    ICN3NSmartFlooding::RetransmitLayeredMapMe (Ptr<DATAPDU> pdu, Ptr<Face> face,
						Ptr<Interest> mapme, const Address& from)
    {
      NS_LOG_FUNCTION (this);
      // Check if the Entry continues to exist in the structure
      NS_LOG_DEBUG ("Verifying that we are waiting for an ACK for " << mapme->GetName ());
      Ptr<pit::Entry> mapme_ack_Entry = m_awaiting_mapme_acks->Lookup (*mapme);

      Ptr<Face> tmp;
      bool ok = false;

      if (mapme_ack_Entry != 0)
	{
	  NS_LOG_DEBUG ("We are waiting for a ACK, beginning forwarding");
	  // Forward the MapMe information
	  if (!pdu)
	    {
	      NS_LOG_INFO ("Attempting to send MapMe directly in ICN");
	      // This is a special case because the FIB was already modified when the
	      // Application was attached to the node
	      if (face->isAppFace())
		{
		  NS_LOG_INFO ("Received the MapMe Interest from Application");
		  // Transmit the MapMe through all the interfaces not of Application type
		  for (uint32_t i = 0; i < m_faces->GetN (); i++)
		    {
		      tmp = m_faces->Get (i);
		      // Check that the Face is not of type APPLICATION
		      if (!tmp->isAppFace ())
			{
			  if (tmp->IsNetworkCompatibilityEnabled ("ICN"))
			    {
			      NS_LOG_INFO ("Sending Interest MapMe Update via face " << tmp->GetId ());
			      ok = tmp->SendInterest (mapme);

			      if (!ok)
				{
				  m_dropInterests (mapme, tmp);
				  NS_LOG_DEBUG ("Cannot satisfy Interest MapMe Update to " << tmp->GetId ());
				}
			      else
				{
				  // Log that a Interest packet was sent
				  m_outInterests (mapme, tmp);
				  mapme_ack_Entry->AddOutgoing (tmp);
				}
			    }
			  else
			    {
			      NS_LOG_WARN ("Attempted to send raw ICN through unsupported Face");
			    }
			}
		    }
		}
	      else
		{
		  NS_LOG_INFO ("Received the MapMe Interest from another node");
		  Ptr<icn::Name> mapmed_name = Create<icn::Name> (mapme->GetName ().getBaseName ());
		  NS_LOG_DEBUG ("Searching in FIB for " << *mapmed_name);
		  NS_LOG_DEBUG ("Current " << *m_fib);

		  Ptr<fib::Entry> fib_entry = m_fib->LongestPrefixMatch(mapmed_name);

		  if (!fib_entry)
		    {
		      NS_LOG_DEBUG ("No information in FIB for " << *mapmed_name << " returning");
		      return;
		    }

		  // Here we pick the next place to forward to using the ICN strategy
		  BOOST_FOREACH (const fib::FaceMetric &metricFace, fib_entry->m_faces.get<fib::i_metric> ())
		  {
		    tmp = metricFace.GetFace ();

		    if (tmp->isAppFace())
		      {
			continue;
		      }

		    if (tmp->GetId () == face->GetId ())
		      {
			continue;
		      }

		    if (tmp->IsNetworkCompatibilityEnabled ("ICN"))
		      {
			NS_LOG_INFO ("Sending MapMe Interest Update via Face " << tmp->GetId ());
			ok = tmp->SendInterest (mapme);

			if (!ok)
			  {
			    m_dropInterests (mapme, tmp);
			    NS_LOG_DEBUG ("Cannot satisfy Interest to " << tmp->GetId ());
			  }
			else
			  {
			    // Log that a Interest packet was sent
			    m_outInterests (mapme, tmp);
			    mapme_ack_Entry->AddOutgoing (tmp);
			  }
		      }
		    else
		      {
			NS_LOG_WARN ("Attempted to send raw ICN through unsupported Face");
		      }
		  }
		}
	    }
	  else
	    {
	      NS_LOG_INFO ("Attempting to send MapMe in 3N PDU");
	      NS_LOG_DEBUG ("Serializing the Interest");
	      // Encode the packet
	      Ptr<Packet> retPkt = icn::Wire::FromInterest(mapme, icn::Wire::WIRE_FORMAT_NDNSIM);

	      Address t_poa;

	      if (face->isAppFace())
		{
		  // This is a special case because the FIB was already modified when the
		  // Application was attached to the node
		  NS_LOG_INFO ("Received the Interest MapMe Update from Application");

		  NS_LOG_INFO ("Creating NULLp in which to send MapMe Interest");
		  Ptr<NULLp> nullp_o = Create<NULLp> ();
		  nullp_o->SetPDUPayloadType (ICN_NNN);
		  nullp_o->SetLifetime (m_3n_lifetime);
		  nullp_o->SetPayload (retPkt);

		  // Transmit the MapMe through all the interfaces not of Application type
		  for (uint32_t i = 0; i < m_faces->GetN (); i++)
		    {
		      tmp = m_faces->Get (i);
		      // Check that the Face is not of type APPLICATION
		      if (tmp->isAppFace ())
			{
			  continue;
			}

		      // We can't return the packet to the same Face
		      if (face->GetId () == tmp->GetId ())
			{
			  continue;
			}

		      // In this case, we have no PoA information, so send in broadcast
		      NS_LOG_INFO ("Sending Interest MapMe Update in NULLp via Face " << tmp->GetId ());
		      ok = tmp->SendNULLp (nullp_o);

		      if (!ok)
			{
			  m_dropInterests (mapme, tmp);
			  m_dropNULLps (nullp_o, tmp);
			  NS_LOG_DEBUG ("Cannot satisfy data to " << tmp->GetId ());
			}
		      else
			{
			  NS_LOG_DEBUG ("Transmitted NULLp with MapMe Update via Face " << tmp->GetId ());
			  m_outNULLps (nullp_o, tmp);
			  m_outInterests (mapme, tmp);
			  mapme_ack_Entry->AddOutgoing (tmp);
			}
		    }
		}
	      else
		{
		  NS_LOG_INFO ("Received the MapMe Interest from another node");
		  Ptr<icn::Name> mapmed_name = Create<icn::Name> (mapme->GetName ().getBaseName ());
		  NS_LOG_DEBUG ("Searching in FIB for " << *mapmed_name);
		  NS_LOG_DEBUG ("Current " << *m_fib);

		  Ptr<fib::Entry> fib_entry = m_fib->LongestPrefixMatch(mapmed_name);
		  if (!fib_entry)
		    {
		      NS_LOG_DEBUG ("No information in FIB for " << *mapmed_name << " returning");
		      return;
		    }

		  Ptr<SO> so_o;
		  Ptr<NULLp> nullp_o;
		  Ptr<DATAPDU> pdu_o;
		  bool useSO = false;
//		  bool useNULL = false;
		  Ptr<DO> do_i;
		  Ptr<DU> du_i;
		  bool wasDO = false;
//		  bool wasDU = false;
		  bool forwardAsis = false;

		  // If I received a NULLp PDU, then we can become the main 3N node, send in SO
		  if (pdu->GetPacketId () == NULL_NNN)
		    {
		      if (Has3NName() && m_use3NName)
			{
			  NS_LOG_INFO ("" << GetNode3NName () << " will attempt to become temporal anchor for " << *mapmed_name);
			  so_o = Create<SO> ();
			  so_o->SetLifetime (m_3n_lifetime);
			  so_o->SetPDUPayloadType (ICN_NNN);
			  so_o->SetName (GetNode3NNamePtr ());
			  so_o->SetPayload (retPkt);

			  useSO = true;
			  pdu_o = DynamicCast <DATAPDU> (so_o);
			}
		      else
			{
			  NS_LOG_INFO ("Creating NULLp in which to send MapMe Interest");
			  nullp_o = Create<NULLp> ();
			  nullp_o->SetPDUPayloadType (ICN_NNN);
			  nullp_o->SetLifetime (m_3n_lifetime);
			  nullp_o->SetPayload (retPkt);

			  pdu_o = DynamicCast <DATAPDU> (nullp_o);
			}
		    }
		  else if (pdu->GetPacketId () == SO_NNN)
		    {
		      so_o = DynamicCast <SO> (pdu);
		      NS_LOG_INFO ("Forwarding SO received with " << so_o->GetName() << " as temporal anchor for " << *mapmed_name);
		      useSO = true;

		      pdu_o = DynamicCast <DATAPDU> (so_o);
		    }
		  else
		    {
		      switch (pdu->GetPacketId())
		      {
			case DO_NNN:
			  do_i = DynamicCast<DO> (pdu);
			  wasDO = true;
			  break;
			case DU_NNN:
			  du_i =DynamicCast<DU> (pdu);
//			  wasDU = true;
			  break;
			default:
			  NS_LOG_WARN ("Obtained unrecognized 3N PDU. Check code");
			  return;
		      }

		      if (wasDO)
			{
			  if (GoesBy3NName (do_i->GetNamePtr ()))
			    {
			      NS_LOG_INFO ("" << GetNode3NName () << " will attempt to become temporal anchor for " << *mapmed_name);
			      so_o = Create<SO> ();
			      so_o->SetLifetime (m_3n_lifetime);
			      so_o->SetPDUPayloadType (ICN_NNN);
			      so_o->SetName (GetNode3NNamePtr ());
			      so_o->SetPayload (retPkt);

			      useSO = true;
			      pdu_o = DynamicCast <DATAPDU> (so_o);
			    }
			  else
			    {
			      forwardAsis = true;
			      pdu_o = DynamicCast <DATAPDU> (do_i);
			    }
			}
		      else
			{
			  if (GoesBy3NName (du_i->GetDstNamePtr ()))
			    {
			      NS_LOG_INFO ("" << GetNode3NName () << " will attempt to become temporal anchor for " << *mapmed_name);
			      so_o = Create<SO> ();
			      so_o->SetLifetime (m_3n_lifetime);
			      so_o->SetPDUPayloadType (ICN_NNN);
			      so_o->SetName (GetNode3NNamePtr ());
			      so_o->SetPayload (retPkt);

			      useSO = true;
			      pdu_o = DynamicCast <DATAPDU> (so_o);
			    }
			  else
			    {
			      forwardAsis = true;
			      pdu_o = DynamicCast <DATAPDU> (du_i);
			    }
			}
		    }

		  // Here we pick the next place to forward to using the ICN strategy
		  BOOST_FOREACH (const fib::FaceMetric &metricFace, fib_entry->m_faces.get<fib::i_metric> ())
		  {
		    tmp = metricFace.GetFace ();
		    t_poa = metricFace.GetBestPoA();

		    if (tmp->isAppFace())
		      {
			continue;
		      }

		    if (tmp->GetId () == face->GetId ())
		      {
			continue;
		      }

		    if (useSO)
		      NS_LOG_INFO ("Sending MapMe Interest MapMe Update in SO via Face " << tmp->GetId () << " PoA: " << t_poa);
		    else if (forwardAsis)
		      {
			if (wasDO)
			  NS_LOG_INFO ("Sending MapMe Interest MapMe Update in DO via Face " << tmp->GetId () << " PoA: " << t_poa);
			else
			  NS_LOG_INFO ("Sending MapMe Interest MapMe Update in DU via Face " << tmp->GetId () << " PoA: " << t_poa);
		      }
		    else
		      NS_LOG_INFO ("Sending MapMe Interest MapMe Update in NULLp via Face " << tmp->GetId () << " PoA: " << t_poa);

		    bool normalSend = true;
		    // Double check Address is valid
		    if (t_poa.IsInvalid())
		      {
			if (m_ask_ACKP)
			  {
			    std::vector<Ptr<const NNNAddress> > ret = m_nnst->OneHopNameInfo(GetNode3NNamePtr(), tmp);

			    if (!ret.empty())
			      {
				normalSend = false;
				int8_t n3ok = txPreparation (pdu_o, tmp, tmp->GetBroadcastAddress(), false);
				ok = (n3ok >= 0);
			      }
			    else
			      {
				NS_LOG_WARN ("Face " << tmp->GetId () << " has no apparent 3N nodes, not expecting ACKPs");
				SetupFlowidSeq(pdu_o, tmp, tmp->GetBroadcastAddress (), false);
				UpdateSentPDUs(pdu_o, tmp, tmp->GetBroadcastAddress (), false);
			      }
			  }
			else
			  {
			    txReset(pdu_o);
			  }

			NS_LOG_DEBUG ("PoA information in FIB seems invalid, sending in broadcast");
			if (normalSend)
			  {
			    if (useSO)
			      if (m_useQueues)
				ok = EnqueueSO(tmp, tmp->GetBroadcastAddress(), so_o);
			      else
			      ok = tmp->SendSO (so_o);
			    else if (forwardAsis)
			      {
				if (wasDO)
				  if (m_useQueues)
				    ok = EnqueueDO(tmp, tmp->GetBroadcastAddress(), do_i);
				  else
				    ok = tmp->SendDO (do_i);
				else
				  if (m_useQueues)
				    ok = EnqueueDU(tmp, tmp->GetBroadcastAddress(), du_i);
				  else
				    ok = tmp->SendDU (du_i);
			      }
			    else
			      if (m_useQueues)
				ok = EnqueueNULLp(tmp, tmp->GetBroadcastAddress(), nullp_o);
			      else
				ok = tmp->SendNULLp (nullp_o);

			    UpdatePDUCountersB(pdu_o, tmp, ok, false);
			  }
		      }
		    else
		      {
			if (m_ask_ACKP)
			  {
			    std::vector<Ptr<const NNNAddress> > ret = m_nnst->OneHopNameInfo(GetNode3NNamePtr(), tmp);

			    if (!ret.empty())
			      {
				normalSend = false;
				int8_t n3ok = txPreparation (pdu_o, tmp, t_poa, false);
				ok = (n3ok >= 0);
			      }
			    else
			      {
				NS_LOG_WARN ("Face " << face->GetId () << " has no apparent 3N nodes, not expecting ACKPs");
				SetupFlowidSeq(pdu_o, tmp, t_poa, false);
				UpdateSentPDUs(pdu_o, tmp, t_poa, false);
			      }
			  }
			else
			  {
			    txReset(pdu_o);
			  }

			if (normalSend)
			  {
			    if (useSO)
			      if (m_useQueues)
				ok = EnqueueSO(tmp, t_poa, so_o);
			      else
				ok = tmp->SendSO (so_o, t_poa);
			    else if (forwardAsis)
			      {
				if (wasDO)
				  if (m_useQueues)
				    ok = EnqueueDO(tmp, t_poa, do_i);
				  else
				    ok = tmp->SendDO (do_i, t_poa);
				else
				  if (m_useQueues)
				    ok = EnqueueDU(tmp, t_poa, du_i);
				  else
				    ok = tmp->SendDU (du_i, t_poa);
			      }
			    else
			      if (m_useQueues)
				ok = EnqueueNULLp(tmp, t_poa, nullp_o);
			      else
				ok = tmp->SendNULLp (nullp_o, t_poa);

			    UpdatePDUCountersB(pdu_o, tmp, ok, false);
			  }
		      }

		    if (!ok)
		      {
			m_dropInterests (mapme, tmp);
		      }
		    else
		      {
			if (useSO)
			  {
			    NS_LOG_DEBUG ("Transmitted SO with Interest MapMe Update via Face " << tmp->GetId ());
			  }
			else if (forwardAsis)
			  {
			    if (wasDO)
			      {
				NS_LOG_DEBUG ("Transmitted DO with Interest MapMe Update via Face " << tmp->GetId ());
			      }
			    else
			      {
				NS_LOG_DEBUG ("Transmitted DO with Interest MapMe Update via Face " << tmp->GetId ());
			      }
			  }
			else
			  {
			    NS_LOG_DEBUG ("Transmitted NULL with Interest MapMe Update via Face " << tmp->GetId ());
			  }
			m_outInterests (mapme, tmp);
			mapme_ack_Entry->AddOutgoing (tmp);
		      }
		  }
		}
	    }

	  mapme_ack_Entry->IncreaseCurrentRetransmissions();
	  uint32_t currTransmissions = mapme_ack_Entry->GetCurrentRetransmissions ();
	  NS_LOG_DEBUG ("Current number of MapMe attempts for: " << mapme->GetName() << " at: " << currTransmissions);

	  if (currTransmissions <= m_mapme_retrys)
	    {
	      // Schedule retransmission of MapMe should we get no ACK
	      NS_LOG_DEBUG ("Scheduling retransmission of MapMe Interest, should we get no ACK in " << mapme->GetRetransmissionTime());
	      Simulator::Schedule (mapme->GetRetransmissionTime(), &ICN3NSmartFlooding::RetransmitLayeredMapMe, this, pdu, face, mapme, from);
	    }
	  else
	    {
	      NS_LOG_DEBUG ("Exceeded permitted number of Interest with MapMe info retries at: " << m_mapme_retrys);

	      NS_LOG_DEBUG ("Clearing Faces which were waiting for ACKs");
	      mapme_ack_Entry->ClearOutgoing();

	      LayeredMapMeAckProcess (pdu, face, mapme, from);
	    }
	}
      else
	{
	  NS_LOG_INFO (mapme->GetName () << " MapMe Interest, has been acked, cancelling retransmission");
	  return;
	}
    }

    void
    ICN3NSmartFlooding::TransmitLayeredMapMeAck (Ptr<DATAPDU> pdu, Ptr<const Interest> mapme,
						 Ptr<Face> face, const Address& from)
    {
      NS_LOG_FUNCTION (this);

      if (face->isAppFace())
	{
	  NS_LOG_INFO ("Received ACK from Application, no need to ACK");
	  return;
	}

      Ptr<Interest> interest = Create<Interest> ();
      interest->SetNonce               (uniRandom->GetValue());
      interest->SetName                (mapme->GetName ());
      interest->SetInterestLifetime    (mapme->GetInterestLifetime ());
      interest->SetInterestType        (Interest::MAP_ME_INTEREST_UPDATE_ACK);
      interest->SetSequenceNumber      (mapme->GetSequenceNumber ());
      interest->SetRetransmissionTime  (mapme->GetRetransmissionTime ());
      interest->SetPartNumber          (mapme->GetPartNumber ());

      NS_LOG_INFO ("Transmitting Interest MapMe ACK with name: " << mapme->GetName () << " SeqN: " << mapme->GetSequenceNumber () << " PoA: " << from);
      NS_LOG_DEBUG ("Attempting to transmit Interest MapMe ACK via Face " << face->GetId ());
      bool ok = false;

      // Check whether we obtained a Raw ICN packet or a 3N PDU
      if (pdu == 0)
	{
	  NS_LOG_DEBUG ("Checking if capable of direct ICN transmission");
	  if (face->IsNetworkCompatibilityEnabled ("ICN"))
	    {
	      if (m_useQueues)
		ok = EnqueueInterest(face, from, interest);
	      else
		ok = face->SendInterest (interest, from);
	      if (ok)
		{
		  NS_LOG_DEBUG ("Transmitted Interest MapMe ACK via Face " << face->GetId ());
		  m_outInterests (interest, face);
		}
	      else
		{
		  NS_LOG_DEBUG ("Could not transmit Interest MapMe ACK via Face " << face->GetId ());
		  m_dropInterests (interest, face);
		}
	    }
	  else
	    {
	      NS_LOG_WARN ("Received a raw ICN packet, but don't have one capable of transmission. Check code");
	    }
	}
      else
	{
	  NS_LOG_DEBUG ("Encoding Interest packet");
	  // Encode the packet
	  Ptr<Packet> retPkt = icn::Wire::FromInterest(interest, icn::Wire::WIRE_FORMAT_NDNSIM);

	  NS_LOG_DEBUG ("Creating NULLp for transmission");
	  Ptr<NULLp> null_p_o = Create<NULLp> ();
	  // Set the lifetime of the 3N PDU
	  null_p_o->SetLifetime (m_3n_lifetime);
	  // Configure payload for PDU
	  null_p_o->SetPayload (retPkt);
	  // Signal that the PDU had an ICN PDU as payload
	  null_p_o->SetPDUPayloadType (ICN_NNN);

	  bool normalSend = true;

	  if (m_ask_ACKP)
	    {
	      std::vector<Ptr<const NNNAddress> > ret = m_nnst->OneHopNameInfo(GetNode3NNamePtr(), face);

	      if (!ret.empty())
		{
		  normalSend = false;
		  int8_t n3ok = txPreparation (null_p_o, face, from, false);
		  ok = (n3ok >= 0);
		}
	      else
		{
		  NS_LOG_WARN ("Face " << face->GetId () << " has no apparent 3N nodes, not expecting ACKPs");
		  SetupFlowidSeq (null_p_o, face, from, false);
		  UpdateSentPDUs(null_p_o, face, from, false);
		}
	    }
	  else
	    {
	      txReset(null_p_o);
	    }

	  if (normalSend)
	    {
	      // Send out the NULL PDU
	      if (m_useQueues)
		ok = EnqueueNULLp(face, from, null_p_o);
	      else
		ok = face->SendNULLp (null_p_o, from);

	      UpdatePDUCountersB(null_p_o, face, ok, false);
	    }

	  if (!ok)
	    {
	      m_dropInterests (interest, face);
	    }
	  else
	    {
	      NS_LOG_DEBUG ("Transmitted NULLp with Interest MapMe ACK via Face " << face->GetId ());
	      m_outInterests (interest, face);
	    }
	}
    }

    void
    ICN3NSmartFlooding::SatisfyPendingInterest (Ptr<DATAPDU> pdu,
						Ptr<Face> inFace,
						Ptr<const Data> data,
						Ptr<pit::Entry> pitEntry)
    {
      NNNAddress myAddr = GetNode3NName ();

      NS_LOG_INFO ("On " << myAddr << " Satisfying pending Interests for " << data->GetName());

      if (inFace != 0)
	pitEntry->RemoveIncoming (inFace);

      if (m_isMobile)
	NS_LOG_INFO ("Node is mobile");

      // Convert the Data PDU into a NS-3 Packet
      Ptr<Packet> icn_pdu = ns3::icn::Wire::FromData (data);

      // Pointers and flags for PDU types
      Ptr<NULLp> nullp_i;
      bool wasNULL = false;
      Ptr<SO> so_i;
      bool wasSO = false;
      Ptr<DO> do_i;
      bool wasDO = false;
      Ptr<DU> du_i;
      bool wasDU = false;
      bool wasRawData = false;

      if (pdu != 0)
	{
	  uint32_t pduid = pdu->GetPacketId ();
	  switch(pduid)
	  {
	    case NULL_NNN:
	      // Convert pointer to NULLp PDU
	      nullp_i = DynamicCast<NULLp> (pdu);
	      wasNULL = true;
	      NS_LOG_INFO ("Received a NULLp");
	      break;
	    case SO_NNN:
	      // Convert pointer to SO PDU
	      so_i = DynamicCast<SO> (pdu);
	      wasSO = true;
	      NS_LOG_INFO ("Received a SO src: " << so_i->GetName ());
	      break;
	    case DO_NNN:
	      // Convert pointer to DO PDU
	      do_i = DynamicCast<DO> (pdu);
	      wasDO = true;
	      NS_LOG_INFO ("Received a DO dst: " << do_i->GetName ());
	      break;
	    case DU_NNN:
	      // Convert pointer to DU PDU
	      du_i = DynamicCast<DU> (pdu);
	      wasDU = true;
	      NS_LOG_INFO ("Received a " << std::boolalpha << du_i->IsAuthoritative () << " DU src: " << du_i->GetSrcName () << " - dst: " << du_i->GetDstName ());
	      break;
	    default:
	      break;
	  }
	}
      else
	{
	  NS_LOG_INFO ("Received a raw Data PDU");
	  wasRawData = true;
	}

      // Check where the Data PDU came from and divide them as such
      if (inFace == 0)
	{
	  NS_LOG_INFO ("On " << myAddr << " satisfying Interest from local CS");

	  BOOST_FOREACH(const pit::IncomingFace &incoming, pitEntry->GetIncoming ()) {

	    bool ok = false;
	    bool sentSomething = false;
	    uint32_t incomingID = incoming.m_face->GetId ();
	    bool sawRawICN = incoming.SawRawICN ();
	    bool sawNULL = incoming.SawNULL ();

	    if (incoming.m_face->isAppFace ())
	      NS_LOG_INFO ("Satisfying an Interest from an Application Face " << incomingID);

	    std::vector<Ptr<NNNAddress> > distinct = incoming.m_addrs->GetDistinctDestinations ();

	    if (distinct.empty ())
	      {
		NS_LOG_INFO ("PIT Entry has no 3N names aggregated");

		// In case the structure has seen a NULL PDU
		if (sawNULL)
		  {
		    NS_LOG_INFO ("Satisfying using NULLp out Face " << incomingID);
		    sentSomething = TryToSendDataInNULL (pdu->GetFlowid (), inFace, incoming.m_face, data, pitEntry);
		  }

		// In case we have seen raw ICN PDUs
		if (sawRawICN)
		  {
		    NS_LOG_INFO ("Attempting to satisfy using ICN Data out Face " << incomingID);
		    sentSomething = TryToSendRawData(inFace, incoming.m_face, data, pitEntry);
		  }
	      }
	    else
	      {
		NS_LOG_INFO ("PIT Entry has 3N names aggregated");
		BOOST_FOREACH (Ptr<NNNAddress> j, distinct) {
		  bool subSector = m_node_names->foundName(j);
		  // Obtain all the 3N names aggregated in this sector
		  std::vector<Ptr<NNNAddress> > addrs = incoming.m_addrs->GetCompleteDestinations (j);
		  BOOST_FOREACH (Ptr<NNNAddress> i, addrs) {
		    // If the aggregation is the same as the 3N Name the node is using, then
		    // everything aggregated is probably connected to it
		    if (subSector)
		      {
			NS_LOG_INFO ("We are satisfying Interests to nodes that are directly connected to us " << *j << ", continue");
		      }
		    // We are pushing to a different sector
		    else
		      {
			NS_LOG_INFO ("On " << myAddr << " satisfying for 3N names in different subsector " << *j << ", destination: " << *i);
			if (sentSomething)
			  {
			    NS_LOG_INFO ("On " << myAddr << ", we seem to have already sent to subsector " << *j << " about to skip");
			    continue;
			  }
		      }

		    bool sentToAddr = false;

		    // Within this particular case, we have the destinations we
		    // wish to send to, but can't give source information
		    // because we are reading from the local CS. Thus we can
		    // only send DOs
		    bool redirect = false;
		    Ptr<const NNNAddress> searchDest;
		    NNNAddress endDest;

		    if (m_in_transit_redirect)
		      {
			redirect = m_nnpt->foundOldName (i);
			searchDest = m_nnpt->findPairedNamePtr (i);
			endDest = searchDest->getName ();

			if (redirect)
			  {
			    NS_LOG_DEBUG ("On " << myAddr << *m_nnpt);
			    NS_LOG_INFO ("We are redirecting " << *i << " to " << endDest);
			  }
		      }
		    else
		      {
			searchDest = i;
			endDest = i->getName();
		      }

		    // Need routing information, attempt handshake
		    if (m_useHR)
		      AskHandshake (searchDest);

		    Ptr<DU> du_o_spec;
		    Ptr<DO> do_o_spec;
		    Ptr<DATAPDU> o_spec;

		    // Create the PDU that needs to be sent
		    if (m_EnableNonAuthoritativeDU)
		      {
			NS_LOG_INFO ("Non-authorative DU usage allowed, creating");
			du_o_spec = Create<DU> ();
			// Set this node's 3N name
			du_o_spec->SetSrcName (GetNode3NNamePtr ());
			// Set the new 3N name
			du_o_spec->SetDstName (searchDest);
			// Set the DU as non-authoritative
			du_o_spec->SetAuthoritative (false);
			// Set the lifetime of the 3N PDU
			du_o_spec->SetLifetime (m_3n_lifetime);
			// Configure payload for PDU
			du_o_spec->SetPayload (icn_pdu);
			// Signal that the PDU had an ICN PDU as payload
			du_o_spec->SetPDUPayloadType (ICN_NNN);

			o_spec = DynamicCast <DATAPDU> (du_o_spec);
		      }
		    else
		      {
			// Create the new DO PDU to send
			do_o_spec = Create<DO> ();
			// Set the new 3N name
			do_o_spec->SetName (searchDest);
			// Set the lifetime of the 3N PDU
			do_o_spec->SetLifetime (m_3n_lifetime);
			// Configure payload for PDU
			do_o_spec->SetPayload (icn_pdu);
			// Signal that the PDU had an ICN PDU as payload
			do_o_spec->SetPDUPayloadType (ICN_NNN);

			o_spec = DynamicCast <DATAPDU> (do_o_spec);
		      }

		    NS_LOG_DEBUG ("On " << myAddr << " " << *m_nnst);

		    // Verify if we have a table to send things with
		    if (m_buffer_during_disenroll && (m_hasDisenrolled || m_nnst->isEmpty()))
		      {
			NS_LOG_INFO ("Node is in dis-enroll or NNST is blank, queuing PDU for later");

			if (m_EnableNonAuthoritativeDU)
			  {
			    NS_LOG_DEBUG ("Buffering DU");
			    m_node_pdu_queue->pushDU(du_o_spec, Seconds(0));
			  }
			else
			  {
			    NS_LOG_DEBUG ("Buffering DO");
			    m_node_pdu_queue->pushDO(do_o_spec, Seconds(0));
			  }

			continue;
		      }

		    // Roughly pick the next hop that would bring us closer to endDest
		    std::pair<Ptr<Face>, Address> tmp = m_nnst->ClosestSectorFaceInfo (endDest, 0);

		    Ptr<Face> outFace = tmp.first;
		    Address destAddr = tmp.second;

		    if (outFace == 0)
		      {
			NS_LOG_WARN ("Attempted to send, didn't find an interface, defaulting");
			outFace = incoming.m_face;
			destAddr = outFace->GetBroadcastAddress();
		      }

		    uint32_t outgoingID = outFace->GetId ();

		    // We may have obtained a DEN for this destination so we need to check
		    if (m_node_pdu_buffer->DestinationExists (endDest) && !redirect)
		      {
			if (!endDest.isDirectSubSector (myAddr))
			  {
			    NS_LOG_INFO ("On " << myAddr << ", " << endDest << " is not a direct subsector, buffering and not forwarding PDU");
			    m_node_pdu_buffer->PushDATAPDU(endDest, o_spec);
			    if (m_3n_only_buffer)
			      continue;
			  }
		      }

		    if (m_EnableNonAuthoritativeDU)
		      {
			// We now check that we aren't the destination
			if (m_node_names->foundName (searchDest))
			  {
			    NS_LOG_INFO ("We are the desired 3N named node destination");

			    if (incoming.m_face->isAppFace ())
			      {
				NS_LOG_INFO ("Satisfying using DU out Application Face " << incomingID);
				ok = TryToSendDataInDU (du_o_spec, Address (), inFace, incoming.m_face, data, pitEntry);
				sentToAddr = ok;
			      }
			    else
			      {
				NS_LOG_DEBUG ("Not an Application Face, something is wrong");
			      }
			  }

			if (!sentToAddr)
			  {
			    NS_LOG_INFO ("Satisfying using " << std::boolalpha << du_o_spec->IsAuthoritative () << " authoritative DU to " << endDest << " out Face " << outgoingID);
			    ok = TryToSendDataInDU (du_o_spec, destAddr, inFace, outFace, data, pitEntry);

			    if (outgoingID == incomingID)
			      sentSomething = ok;
			  }
		      }
		    else
		      {
			// We now check that we aren't the destination
			if (m_node_names->foundName (searchDest))
			  {
			    NS_LOG_INFO ("We are the desired 3N named node destination");

			    if (incoming.m_face->isAppFace ())
			      {
				NS_LOG_INFO ("Satisfying using DO out Application Face " << incomingID);
				ok = TryToSendDataInDO (do_o_spec, Address (), inFace, incoming.m_face, data, pitEntry);
				sentToAddr = ok;
			      }
			    else
			      {
				NS_LOG_DEBUG ("Not an Application Face, something is wrong");
			      }
			  }

			if (!sentToAddr)
			  {
			    NS_LOG_INFO ("Satisfying using DO to " << endDest << " out Face " << outgoingID);
			    ok = TryToSendDataInDO (do_o_spec, destAddr, inFace, outFace, data, pitEntry);
			    if (outgoingID == incomingID)
			      sentSomething = ok;
			  }
		      }
		  }
		}
	      }

	    // If we haven't yet pushed anything out the incoming face
	    // registered in the PIT, we should do it now
	    if (!sentSomething)
	      {
		NS_LOG_INFO ("Satisfying using NULLp out Face " << incomingID);

		TryToSendDataInNULL (pdu->GetFlowid (), inFace, incoming.m_face, data, pitEntry);
	      }

	    // Since dealing with raw ICN PDUs, check if we have seen any here
	    if (incoming.SawRawICN ())
	      {
		NS_LOG_INFO ("Attempting to satisfy using ICN Data out Face " << incomingID);

		sentSomething = TryToSendRawData(inFace, incoming.m_face, data, pitEntry);
	      }
	  }
	}
      else
	{
	  NS_LOG_INFO ("On " << myAddr << " received PDU is satisfying Interest");

	  BOOST_FOREACH(const pit::IncomingFace &incoming, pitEntry->GetIncoming ()) {

	    bool ok = false;
	    bool sentSomething = false;
	    uint32_t incomingID = incoming.m_face->GetId ();
	    bool sawRawICN = incoming.SawRawICN ();
	    bool sawNULL = incoming.SawNULL ();
	    if (incoming.m_face->isAppFace ())
	      NS_LOG_INFO ("Satisfying an Interest from an Application Face " << incomingID);

	    std::vector<Ptr<NNNAddress> > distinct = incoming.m_addrs->GetDistinctDestinations ();

	    if (distinct.empty ())
	      {
		NS_LOG_INFO ("PIT Entry has no 3N names aggregated");

		Ptr<DO> do_o_spec;
		Ptr<DU> du_o_spec;

		// In the case of a PDU having the information satisfying an
		// Interest, and the PIT having no 3N names aggregated, the
		// NULLp and the SO is just forwarded through the PIT data. If
		// you received a DO or DU then we make sure that the original
		// PDU doesn't need to be redirected or buffered
		if (wasNULL)
		  {
		    NS_LOG_INFO ("Satisfying using NULLp out Face " << incomingID);
		    sentSomething = TryToSendDataInNULL (pdu->GetFlowid (), inFace, incoming.m_face, data, pitEntry);
		  }
		else if (wasSO)
		  {
		    NS_LOG_INFO ("Satisfying using SO out Face " << incomingID);

		    bool normalSend = true;
		    if (m_ask_ACKP)
		      {
			std::vector<Ptr<const NNNAddress> > ret = m_nnst->OneHopNameInfo(GetNode3NNamePtr(), incoming.m_face);

			if (!ret.empty())
			  {
			    normalSend = false;
			    int8_t n3ok = txPreparation (so_i, incoming.m_face, incoming.m_face->GetBroadcastAddress(), false);
			    ok = (n3ok >= 0);
			  }
			else
			  {
			    NS_LOG_WARN ("Face " << incomingID << " has no apparent PoAs, not expecting ACKPs");
			    SetupFlowidSeq (so_i, incoming.m_face, incoming.m_face->GetBroadcastAddress(), false);
			  }
		      }
		    else
		      {
			txReset(so_i);
		      }

		    if (normalSend)
		      {
			if (m_useQueues)
			  ok = EnqueueSO(incoming.m_face, incoming.m_face->GetBroadcastAddress(), so_i);
			else
			  ok = incoming.m_face->SendSO (so_i);
			UpdatePDUCountersB (so_i, incoming.m_face, ok, false);
		      }

		    if (!ok)
		      {
			// Log Data drops
			m_dropData (data, incoming.m_face);
		      }
		    else
		      {
			// Log that a Data PDU was sent
			DidSendOutData (inFace, incoming.m_face, data, pitEntry);
			sentSomething = true;
		      }
		  }
		else if (wasDO || wasDU)
		  {
		    // Verify that the PDUs don't need to be redirected
		    Ptr<const NNNAddress> pdu3N = pdu->GetDstNamePtr();

		    bool redirect = false;
		    NNNAddress endDest;
		    Ptr<const NNNAddress> searchDest;

		    if (m_in_transit_redirect)
		      {
			redirect = m_nnpt->foundOldName (pdu3N);
			searchDest = m_nnpt->findPairedNamePtr (pdu3N);
			endDest = searchDest->getName ();

			if (redirect)
			  {
			    NS_LOG_DEBUG ("On " << myAddr << *m_nnpt);
			    NS_LOG_INFO ("We are redirecting " << *pdu3N << " to " << endDest);
			  }
		      }
		    else
		      {
			searchDest = pdu3N;
			endDest = pdu3N->getName();
		      }

		    // Need routing information, attempt handshake
		    if (m_useHR)
		      AskHandshake (searchDest);

		    // If redirecting, recreate the DO PDU
		    if (redirect)
		      {
			Ptr<Face> old_face;
			Address old_dst;

			if (wasDO)
			  {
			    // Create the new DO PDU to send
			    do_o_spec = Create<DO> ();
			    // Set the new 3N name
			    do_o_spec->SetName (searchDest);
			    // Set the lifetime of the 3N PDU
			    do_o_spec->SetLifetime (m_3n_lifetime);
			    // Configure payload for PDU
			    do_o_spec->SetPayload (icn_pdu);
			    // Signal that the PDU had an ICN PDU as payload
			    do_o_spec->SetPDUPayloadType (ICN_NNN);
			  }
			else
			  {
			    du_o_spec = Create<DU> ();
			    // Use the original DU's Src 3N name
			    du_o_spec->SetSrcName (du_i->GetSrcNamePtr ());
			    // Set the new 3N name destination
			    du_o_spec->SetDstName (searchDest);
			    // Set the lifetime of the 3N PDU
			    du_o_spec->SetLifetime (m_3n_lifetime);
			    // Use the original setting for authoritative
			    du_o_spec->SetAuthoritative(du_i->IsAuthoritative ());
			    // Configure payload for PDU
			    du_o_spec->SetPayload (icn_pdu);
			    // Signal that the PDU had an ICN PDU as payload
			    du_o_spec->SetPDUPayloadType (ICN_NNN);
			  }
		      }

		    NS_LOG_DEBUG ("On " << myAddr << " " << *m_nnst);

		    if (m_buffer_during_disenroll && (m_hasDisenrolled || m_nnst->isEmpty()))
		      {
			NS_LOG_INFO ("Node is in dis-enroll or NNST is blank, queuing PDU for later");

			if (wasDO)
			  {
			    if (redirect)
			      {
				m_node_pdu_queue->pushDO(do_o_spec, Seconds(0));
			      }
			    else
			      {
				m_node_pdu_queue->pushDO(do_i, Seconds(0));
			      }
			  }
			else if (wasDU)
			  {
			    if (redirect)
			      {
				m_node_pdu_queue->pushDU(du_o_spec, Seconds(0));
			      }
			    else
			      {
				m_node_pdu_queue->pushDU(du_i, Seconds(0));
			      }
			  }
			else
			  {
			    NS_LOG_WARN ("Attempted check of PDU type. None found, check if this is desired effect");
			  }

			continue;
		      }

		    // Roughly pick the next hop that would bring us closer to endDest
		    std::pair<Ptr<Face>, Address> tmp = m_nnst->ClosestSectorFaceInfo (endDest, 0);

		    Ptr<Face> outFace = tmp.first;
		    Address destAddr = tmp.second;

		    if (outFace == 0)
		      {
			NS_LOG_WARN ("Attempted to send, didn't find an interface, defaulting");
			outFace = incoming.m_face;
			destAddr = outFace->GetBroadcastAddress();
		      }

		    uint32_t outgoingID = outFace->GetId ();

		    // We may have obtained a DEN for this destination so we need to check
		    if (m_node_pdu_buffer->DestinationExists (endDest) && !redirect)
		      {
			if (!endDest.isDirectSubSector (myAddr))
			  {
			    NS_LOG_INFO ("On " << myAddr << ", " << endDest << " is not a direct subsector, buffering and not forwarding PDU");
			    if (wasDO)
			      {
				m_node_pdu_buffer->PushDO (endDest, do_i);
			      }
			    else
			      {
				m_node_pdu_buffer->PushDU (endDest, du_i);
			      }
			    if (m_3n_only_buffer)
			      continue;
			  }
		      }

		    // We check that we are the destination
		    if (m_node_names->foundName (searchDest))
		      {
			NS_LOG_INFO ("We are the desired 3N named node destination");

			if (incoming.m_face->isAppFace ())
			  {
			    if (wasDO)
			      {
				NS_LOG_INFO ("Satisfying using DO to " << endDest << " out Face " << incomingID);
				if (redirect)
				  {
				    ok = TryToSendDataInDO (do_o_spec, Address (), inFace, incoming.m_face, data, pitEntry);
				  }
				else
				  {
				    ok = TryToSendDataInDO (do_i, Address (), inFace, incoming.m_face, data, pitEntry);
				  }
			      }
			    else
			      {
				NS_LOG_INFO ("Satisfying using " << std::boolalpha << du_o_spec->IsAuthoritative () << " authoritative DU to " << endDest << " out Face " << incomingID);
				if (redirect)
				  {
				    ok = TryToSendDataInDU (du_o_spec, Address (), inFace, incoming.m_face, data, pitEntry);
				  }
				else
				  {
				    ok = TryToSendDataInDU (du_i, Address (), inFace, incoming.m_face, data, pitEntry);
				  }
			      }
			    sentSomething = ok;
			  }
			else
			  {
			    NS_LOG_DEBUG ("Not an Application Face, something is wrong");
			  }
		      }

		    if (wasDO)
		      {
			NS_LOG_INFO ("Satisfying using DO to " << endDest << " out Face " << outgoingID);
			if (redirect)
			  {
			    ok = TryToSendDataInDO (do_o_spec, destAddr, inFace, outFace, data, pitEntry);
			  }
			else
			  {
			    ok = TryToSendDataInDO (do_i, destAddr, inFace, outFace, data, pitEntry);
			  }
		      }
		    else
		      {
			NS_LOG_INFO ("Satisfying using DU to " << endDest << " out Face " << outgoingID);
			if (redirect)
			  {
			    ok = TryToSendDataInDU (du_o_spec, destAddr, inFace, outFace, data, pitEntry);
			  }
			else
			  {
			    ok = TryToSendDataInDU (du_i, destAddr, inFace, outFace, data, pitEntry);
			  }
		      }

		    if (outgoingID == incomingID)
		      sentSomething = ok;
		  }
		else if (wasRawData)
		  {
		    // In the case of being a raw ICN Data PDU, then much like the NULL and SO, just push using the PIT
		    if (sawNULL)
		      {
			NS_LOG_INFO ("Satisfying using NULLp out Face " << incomingID);
			sentSomething = TryToSendDataInNULL (pdu->GetFlowid (), inFace, incoming.m_face, data, pitEntry);
		      }

		    // In case we have seen raw ICN PDUs
		    if (sawRawICN)
		      {
			NS_LOG_INFO ("Attempting to satisfy using ICN Data out Face " << incomingID);

			sentSomething = TryToSendRawData (inFace, incoming.m_face, data, pitEntry);
		      }
		  }
	      }
	    else
	      {
		NS_LOG_INFO ("PIT Entry has 3N names aggregated");
		BOOST_FOREACH (Ptr<NNNAddress> j, distinct) {
		  bool subSector = m_node_names->foundName(j);
		  NNNAddress newdst;
		  // Obtain all the 3N names aggregated in this sector
		  std::vector<Ptr<NNNAddress> > addrs = incoming.m_addrs->GetCompleteDestinations (j);
		  BOOST_FOREACH (Ptr<NNNAddress> i, addrs) {
		    // If the aggregation is the same as the 3N Name the node is using, then
		    // everything aggregated is probably connected to it
		    if (subSector)
		      {
			NS_LOG_INFO ("We are satisfying Interests to nodes that are directly connected to us " << *j << ", continue");
		      }
		    // We are pushing to a different sector
		    else
		      {
			NS_LOG_INFO ("On " << myAddr << " satisfying for 3N names in different subsector " << *j << ", destination: " << *i);
			if (sentSomething)
			  {
			    NS_LOG_INFO ("On " << myAddr << ", we seem to have already sent to subsector " << *j << " about to skip");
			    continue;
			  }
		      }

		    bool sentToAddr = false;

		    // Temporary PDUs
		    Ptr<DO> do_o_spec;
		    Ptr<DU> du_o_spec;
		    Ptr<DATAPDU> o_spec;
		    // In the case of a PDU having the information satisfying an Interest and the PIT
		    // having 3N names aggregated, we always have a 3N name as the destination. We
		    // have the following cases:
		    //
		    // In -> Out:
		    // NULLp -> DO    - DO uses PIT DST
		    // DO(a) -> DO(b) - (b) uses PIT DST, no info from (a)
		    // SO -> DU       - DU uses SO SRC info and PIT DST
		    // DU(a) -> DU(b) - (b) uses SRC info from (a) and PIT DST
		    //
		    // As always, we make sure that the PDU doesn't need to be redirected or buffered

		    bool redirect = false;
		    Ptr<const NNNAddress> searchDest;
		    NNNAddress endDest;

		    if (m_in_transit_redirect)
		      {
			redirect = m_nnpt->foundOldName (i);
			searchDest = m_nnpt->findPairedNamePtr (i);
			endDest = searchDest->getName ();

			if (redirect)
			  {
			    NS_LOG_DEBUG ("On " << myAddr << *m_nnpt);
			    NS_LOG_INFO ("We are redirecting " << *i << " to " << endDest);
			  }
		      }
		    else
		      {
			searchDest = i;
			endDest = i->getName();
		      }

		    // Start creation of the PDU to send
		    // Test whether the PDU we saw was a NULLp || DO or SO || DU
		    if (wasNULL || wasDO)
		      {
			// Create the new DO PDU to send
			do_o_spec = Create<DO> ();
			// Set the new 3N name
			do_o_spec->SetName (searchDest);
			// Set the lifetime of the 3N PDU
			do_o_spec->SetLifetime (m_3n_lifetime);
			// Configure payload for PDU
			do_o_spec->SetPayload (icn_pdu);
			// Signal that the PDU had an ICN PDU as payload
			do_o_spec->SetPDUPayloadType (ICN_NNN);

			o_spec = DynamicCast<DATAPDU> (do_o_spec);
		      }
		    else
		      {
			Ptr<const NNNAddress> src3N;
			bool isAuthoritative = true;
			if (wasSO)
			  {
			    src3N = so_i->GetNamePtr ();
			  }
			else
			  {
			    isAuthoritative = du_i->IsAuthoritative ();
			    src3N = du_i->GetSrcNamePtr ();
			  }

			du_o_spec = Create<DU> ();
			// Use the original DU's Src 3N name
			du_o_spec->SetSrcName (src3N);
			// Set the new 3N name destination
			du_o_spec->SetDstName (searchDest);
			// Set the lifetime of the 3N PDU
			du_o_spec->SetLifetime (m_3n_lifetime);
			// Use the original setting for authoritative
			du_o_spec->SetAuthoritative(isAuthoritative);
			// Configure payload for PDU
			du_o_spec->SetPayload (icn_pdu);
			// Signal that the PDU had an ICN PDU as payload
			du_o_spec->SetPDUPayloadType (ICN_NNN);

			o_spec = DynamicCast<DATAPDU> (du_o_spec);
		      }

		    // We may have obtained a DEN for this destination so we need to check
		    if (m_node_pdu_buffer->DestinationExists (endDest) && !redirect)
		      {
			if (!endDest.isDirectSubSector (myAddr))
			  {
			    NS_LOG_INFO ("On " << myAddr << ", " << endDest << " is not a direct subsector, buffering and not forwarding PDU");
			    m_node_pdu_buffer->PushDATAPDU (endDest, o_spec);
			    if (m_3n_only_buffer)
			      continue;
			  }
		      }

		    if (m_node_names->foundName (searchDest))
		      {
			NS_LOG_INFO ("We are the desired 3N named node destination");

			if (incoming.m_face->isAppFace ())
			  {
			    if (wasNULL || wasDO)
			      {
				NS_LOG_INFO ("Satisfying using DO out Face " << incomingID);
				ok = TryToSendDataInDO (do_o_spec, Address (), inFace, incoming.m_face, data, pitEntry);

			      }
			    else
			      {
				NS_LOG_INFO ("Satisfying using DU out Face " << incomingID);
				ok = TryToSendDataInDU (du_o_spec, Address (), inFace, incoming.m_face, data, pitEntry);
			      }

			    sentSomething = ok;
			    sentToAddr = ok;
			  }
			else
			  {
			    NS_LOG_DEBUG ("Not an Application Face, something is wrong");
			  }
		      }

		    // We have checked buffering and redirection. Attempt to finally send
		    if (!sentToAddr)
		      {
			// Need routing information, attempt handshake
			if (m_useHR)
			  AskHandshake (searchDest);

			NS_LOG_DEBUG ("On " << myAddr << " " << *m_nnst);

			if (m_buffer_during_disenroll && (m_hasDisenrolled || m_nnst->isEmpty()))
			  {
			    NS_LOG_INFO ("Node is in dis-enroll or NNST is blank, queuing PDU for later");

			    if (wasNULL || wasDO)
			      {
				m_node_pdu_queue->pushDO(do_o_spec, Seconds(0));
			      }
			    else
			      {
				m_node_pdu_queue->pushDU(du_o_spec, Seconds(0));
			      }

			    continue;
			  }

			// Roughly pick the next hop that would bring us closer to endDest
			std::pair<Ptr<Face>, Address> tmp = m_nnst->ClosestSectorFaceInfo (endDest, 0);

			Ptr<Face> outFace = tmp.first;
			Address destAddr = tmp.second;

			if (outFace == 0)
			  {
			    NS_LOG_WARN ("Attempted to send, didn't find an interface, defaulting");
			    outFace = incoming.m_face;
			    destAddr = outFace->GetBroadcastAddress();
			  }

			uint32_t outgoingID = outFace->GetId ();

			if (wasNULL || wasDO)
			  {
			    NS_LOG_INFO ("Satisfying using DO to " << endDest << " out Face " << outgoingID);
			    ok = TryToSendDataInDO (do_o_spec, destAddr, inFace, outFace, data, pitEntry);
			    sentToAddr = ok;
			    if (outgoingID == incomingID)
			      sentSomething = ok;
			  }
			else
			  {
			    NS_LOG_INFO ("Satisfying using " << std::boolalpha << du_o_spec->IsAuthoritative () << " authoritative DU to " << endDest << " out Face " << outgoingID);
			    ok = TryToSendDataInDU(du_o_spec, destAddr, inFace, outFace, data, pitEntry);
			    sentToAddr = ok;
			    if (outgoingID == incomingID)
			      sentSomething = ok;
			  }
		      }
		  }
		}
	      }

	    // If we haven't yet pushed anything out the incoming face
	    // registered in the PIT, we should do it now
	    if (!sentSomething)
	      {
		NS_LOG_INFO ("Have tested all possibilities, yet haven't yet satisfied PIT's Face " << incomingID);
		NS_LOG_INFO ("Satisfying using NULLp out Face " << incomingID);
		TryToSendDataInNULL (pdu->GetFlowid (), inFace, incoming.m_face, data, pitEntry);
	      }

	    // In case we have seen raw ICN PDUs
	    if (sawRawICN)
	      {
		NS_LOG_INFO ("Attempting to satisfy using ICN Data out Face " << incomingID);

		sentSomething = TryToSendRawData (inFace, incoming.m_face, data, pitEntry);
	      }
	  }
	}

      NS_LOG_INFO ("Finished satisfying, clearing PIT Entry");

      // All incoming interests are satisfied. Remove them
      pitEntry->ClearIncoming ();

      // Remove all outgoing faces
      pitEntry->ClearOutgoing ();

      // Set pruning timeout on PIT entry (instead of deleting the record)
      m_pit->MarkErased (pitEntry);
    }

    void
    ICN3NSmartFlooding::SatisfyPITEntryFromCache (Ptr<DATAPDU> pdu,
						  Ptr<Face> inFace, // 0 allowed (from cache)
						  Ptr<const Data> data,
						  Ptr<pit::Entry> pitEntry)
    {
      // Convert the Data PDU into a NS-3 Packet
      Ptr<Packet> icn_pdu = ns3::icn::Wire::FromData (data);

      BOOST_FOREACH(const pit::IncomingFace &incoming, pitEntry->GetIncoming ()) {

	bool ok = false;
	bool sentSomething = false;
	bool isAppFace = false;
	uint32_t incomingID = incoming.m_face->GetId ();
	std::map<Address,pit::PoAInInfo> poa_map = incoming.GetPoADestinations();
	std::map<Address,pit::PoAInInfo>::iterator it;

	if (incoming.m_face->isAppFace ())
	  {
	    NS_LOG_INFO ("Satisfying an Interest from an Application Face " << incomingID);
	    isAppFace = true;
	  }

	std::vector<Ptr<NNNAddress> > distinct = incoming.m_addrs->GetDistinctDestinations ();

	if (distinct.empty ())
	  {
	    NS_LOG_INFO ("PIT Entry has no 3N names aggregated");

	    for (it = poa_map.begin (); it != poa_map.end (); ++it)
	      {
		 // In case the structure has seen a NULL PDU
		if ((it->second).m_saw_null)
		  {
		    if (isAppFace)
		      {
			NS_LOG_INFO ("Satisfying using NULL out App Face " << incomingID);
			sentSomething = TryToSendDataInNULL (pdu->GetFlowid (), inFace, incoming.m_face,
							     data, pitEntry);
		      }
		    else
		      {
			NS_LOG_INFO ("Satisfying using NULLp out Face " << incomingID <<
							 " PoA: " << it->first);
			sentSomething = TryToSendDataInNULLToPoA (pdu->GetFlowid (), inFace, incoming.m_face,
								  data, pitEntry,
								  it->first);
		      }
		  }

		// In case we have seen raw ICN PDUs
		if ((it->second).m_raw_icn)
		  {
		    if (isAppFace)
		      {
			NS_LOG_INFO ("Satisfying using raw ICN Data out App Face " << incomingID);
			sentSomething = TryToSendRawData (inFace, incoming.m_face,
							  data, pitEntry);
		      }
		    else
		      {
			NS_LOG_INFO ("Attempting to satisfy using raw ICN Data out Face " << incomingID <<
				     " PoA: " << it->first);
			sentSomething = TryToSendRawDataToPoA (inFace, incoming.m_face,
							       data, pitEntry,
							       it->first);
		      }
		  }
	      }
	  }
	else
	  {
	    NS_LOG_INFO ("PIT Entry has 3N names aggregated");
	    BOOST_FOREACH (Ptr<NNNAddress> j, distinct) {
	      bool subSector = m_node_names->foundName(j);
	      // Obtain all the 3N names aggregated in this sector
	      std::vector<Ptr<NNNAddress> > addrs = incoming.m_addrs->GetCompleteDestinations (j);
	      BOOST_FOREACH (Ptr<NNNAddress> i, addrs) {
		// If the aggregation is the same as the 3N Name the node is using, then
		// everything aggregated is probably connected to it
		if (subSector)
		  {
		    NS_LOG_INFO ("We are satisfying Interests to nodes that are directly connected to us " << *j << ", continue");
		  }
		// We are pushing to a different sector
		else
		  {
		    NS_LOG_INFO ("Satisfying for 3N names in different subsector " << *j << ", destination: " << *i);
		    if (sentSomething)
		      {
			NS_LOG_INFO ("We seem to have already sent to subsector " << *j << " about to skip");
			continue;
		      }
		  }

		bool sentToAddr = false;

		// Within this particular case, we have the destinations we
		// wish to send to, but can't give source information
		// because we are reading from the local CS. Thus we can
		// only send DOs
		bool redirect = false;
		Ptr<const NNNAddress> searchDest;
		NNNAddress endDest;

		if (m_in_transit_redirect)
		  {
		    redirect = m_nnpt->foundOldName (i);
		    searchDest = m_nnpt->findPairedNamePtr (i);
		    endDest = searchDest->getName ();

		    if (redirect)
		      {
			NS_LOG_DEBUG ("On " << GetNode3NName () << *m_nnpt);
			NS_LOG_INFO ("We are redirecting " << *i << " to " << endDest);
		      }
		  }
		else
		  {
		    searchDest = i;
		    endDest = i->getName();
		  }

		// Need routing information, attempt handshake
		if (m_useHR)
		  AskHandshake (searchDest);

		Ptr<DU> du_o_spec;
		Ptr<DO> do_o_spec;
		Ptr<DATAPDU> o_spec;

		if (m_EnableNonAuthoritativeDU)
		  {
		    NS_LOG_INFO ("Non-authoritative DU usage allowed, creating from " << GetNode3NName () << " to " << endDest);
		    du_o_spec = Create<DU> ();
		    // Set this node's 3N name
		    du_o_spec->SetSrcName (GetNode3NNamePtr ());
		    // Set the new 3N name
		    du_o_spec->SetDstName (searchDest);
		    // Set the DU as non-authoritative
		    du_o_spec->SetAuthoritative(false);
		    // Set the lifetime of the 3N PDU
		    du_o_spec->SetLifetime (m_3n_lifetime);
		    // Configure payload for PDU
		    du_o_spec->SetPayload (icn_pdu);
		    // Signal that the PDU had an ICN PDU as payload
		    du_o_spec->SetPDUPayloadType (ICN_NNN);

		    o_spec = DynamicCast<DATAPDU> (du_o_spec);
		  }
		else
		  {
		    NS_LOG_INFO ("Allowed to return in DO to " << endDest);
		    // Create the new DO PDU to send
		    do_o_spec = Create<DO> ();
		    // Set the new 3N name
		    do_o_spec->SetName (searchDest);
		    // Set the lifetime of the 3N PDU
		    do_o_spec->SetLifetime (m_3n_lifetime);
		    // Configure payload for PDU
		    do_o_spec->SetPayload (icn_pdu);
		    // Signal that the PDU had an ICN PDU as payload
		    do_o_spec->SetPDUPayloadType (ICN_NNN);

		    o_spec = DynamicCast<DATAPDU> (do_o_spec);
		  }

		NS_LOG_DEBUG ("On " << GetNode3NName() << " " << *m_nnst);

		if (m_buffer_during_disenroll && (m_hasDisenrolled || m_nnst->isEmpty()))
		  {
		    NS_LOG_INFO ("Node is in dis-enroll or NNST is blank, queuing PDU for later");

		    if (m_EnableNonAuthoritativeDU)
		      {
			m_node_pdu_queue->pushDU(du_o_spec, Seconds(0));
		      }
		    else
		      {
			m_node_pdu_queue->pushDO(do_o_spec, Seconds(0));
		      }
		    continue;
		  }

		// Roughly pick the next hop that would bring us closer to endDest
		std::pair<Ptr<Face>, Address> tmp = m_nnst->ClosestSectorFaceInfo (endDest, 0);

		Ptr<Face> outFace = tmp.first;
		Address destAddr = tmp.second;

		if (outFace == 0)
		  {
		    NS_LOG_WARN ("Attempted to send, didn't find an interface, defaulting");
		    outFace = incoming.m_face;
		    destAddr = outFace->GetBroadcastAddress();
		  }

		uint32_t outgoingID = outFace->GetId ();

		// We may have obtained a DEN for this destination so we need to check
		if (m_node_pdu_buffer->DestinationExists (endDest) && !redirect)
		  {
		    if (!endDest.isDirectSubSector (GetNode3NName()))
		      {
			NS_LOG_INFO ("On " << GetNode3NName() << ", " << endDest << " is not a direct subsector, buffering and not forwarding PDU");
			m_node_pdu_buffer->PushDATAPDU (endDest, o_spec);
			if (m_3n_only_buffer)
			  continue;
		      }
		  }

		if (m_EnableNonAuthoritativeDU)
		  {
		    // We now check that we aren't the destination
		    if (m_node_names->foundName (searchDest))
		      {
			NS_LOG_INFO ("We are the desired 3N named node destination");

			if (incoming.m_face->isAppFace ())
			  {
			    NS_LOG_INFO ("Satisfying using DU out Application Face " << incomingID);
			    ok = TryToSendDataInDU (du_o_spec, Address (), inFace, incoming.m_face, data, pitEntry);
			    sentToAddr = ok;
			  }
			else
			  {
			    NS_LOG_DEBUG ("Not an Application Face, something is wrong");
			  }
		      }

		    if (!sentToAddr)
		      {
			NS_LOG_INFO ("Satisfying using " << std::boolalpha << du_o_spec->IsAuthoritative () << " authoritative DU to " << endDest << " out Face " << outgoingID);
			ok = TryToSendDataInDU (du_o_spec, destAddr, inFace, outFace, data, pitEntry);

			if (outgoingID == incomingID)
			  sentSomething = ok;
		      }
		  }
		else
		  {
		    // We now check that we aren't the destination
		    if (m_node_names->foundName (searchDest))
		      {
			NS_LOG_INFO ("We are the desired 3N named node destination");

			if (incoming.m_face->isAppFace ())
			  {
			    NS_LOG_INFO ("Satisfying using DO out Application Face " << incomingID);
			    ok = TryToSendDataInDO (do_o_spec, Address (), inFace, incoming.m_face, data, pitEntry);
			    sentToAddr = ok;
			  }
			else
			  {
			    NS_LOG_DEBUG ("Not an Application Face, something is wrong");
			  }
		      }

		    if (!sentToAddr)
		      {
			NS_LOG_INFO ("Satisfying using DO to " << endDest << " out Face " << outgoingID);
			ok = TryToSendDataInDO (do_o_spec, destAddr, inFace, outFace, data, pitEntry);

			if (outgoingID == incomingID)
			  sentSomething = ok;
		      }
		  }
	      }
	    }
	  }

	// If we haven't yet pushed anything out the incoming face
	// registered in the PIT, we should do it now
	if (!sentSomething)
	  {
	    for (it = poa_map.begin (); it != poa_map.end (); ++it)
	      {
		// In case the structure has seen a NULL PDU
		if ((it->second).m_saw_null)
		  {
		    if (isAppFace)
		      {
			NS_LOG_INFO ("Satisfying using NULL out App Face " << incomingID);
			sentSomething = TryToSendDataInNULL (pdu->GetFlowid (), inFace, incoming.m_face,
							     data, pitEntry);
		      }
		    else
		      {
			NS_LOG_INFO ("Satisfying using NULLp out Face " << incomingID <<
				     " PoA: " << it->first);
			sentSomething = TryToSendDataInNULLToPoA (pdu->GetFlowid (), inFace, incoming.m_face,
								  data, pitEntry,
								  it->first);
		      }
		  }

		// In case we have seen raw ICN PDUs
		if ((it->second).m_raw_icn)
		  {
		    if (isAppFace)
		      {
			NS_LOG_INFO ("Satisfying using raw ICN Data out App Face " << incomingID);
			sentSomething = TryToSendRawData (inFace, incoming.m_face,
							  data, pitEntry);
		      }
		    else
		      {
			NS_LOG_INFO ("Attempting to satisfy using ICN Data out Face " << incomingID <<
				     " PoA: " << it->first);
			sentSomething = TryToSendRawDataToPoA (inFace, incoming.m_face,
							       data, pitEntry,
							       it->first);
		      }
		  }
	      }
	  }
      }
    }

    void
    ICN3NSmartFlooding::SatisfyPendingLayeredInterest (Ptr<DATAPDU> pdu,
						       Ptr<Face> inFace, // 0 allowed (from cache)
						       Ptr<const Data> data,
						       Ptr<pit::Entry> pitEntry)
    {
      NNNAddress myAddr = GetNode3NName ();

      NS_LOG_INFO ("On " << myAddr << " Satisfying pending Interests for " << data->GetName() << " SeqNo " << data->GetName ().get (-1).toSeqNum ());

      if (inFace != 0)
	pitEntry->RemoveIncoming (inFace);

      if (m_isMobile)
	NS_LOG_INFO ("Node is mobile");

      // Convert the Data PDU into a NS-3 Packet
      Ptr<Packet> icn_pdu = ns3::icn::Wire::FromData (data);

      // Pointers and flags for PDU types
      Ptr<NULLp> nullp_i;
      bool wasNULL = false;
      Ptr<SO> so_i;
      bool wasSO = false;
      Ptr<DO> do_i;
      bool wasDO = false;
      Ptr<DU> du_i;
      bool wasDU = false;
      bool wasRawData = false;

      if (pdu != 0)
	{
	  uint32_t pduid = pdu->GetPacketId ();
	  switch(pduid)
	  {
	    case NULL_NNN:
	      // Convert pointer to NULLp PDU
	      nullp_i = DynamicCast<NULLp> (pdu);
	      wasNULL = true;
	      NS_LOG_INFO ("Received a NULLp");
	      break;
	    case SO_NNN:
	      // Convert pointer to SO PDU
	      so_i = DynamicCast<SO> (pdu);
	      wasSO = true;
	      NS_LOG_INFO ("Received a SO src: " << so_i->GetName ());
	      break;
	    case DO_NNN:
	      // Convert pointer to DO PDU
	      do_i = DynamicCast<DO> (pdu);
	      wasDO = true;
	      NS_LOG_INFO ("Received a DO dst: " << do_i->GetName ());
	      break;
	    case DU_NNN:
	      // Convert pointer to DU PDU
	      du_i = DynamicCast<DU> (pdu);
	      wasDU = true;
	      NS_LOG_INFO ("Received a " << std::boolalpha << du_i->IsAuthoritative() << " authoritative DU src: " << du_i->GetSrcName () << " - dst: " << du_i->GetDstName ());
	      break;
	    default:
	      break;
	  }
	}
      else
	{
	  NS_LOG_INFO ("Received a raw Data PDU");
	  wasRawData = true;
	}

      // Check where the Data PDU came from and divide them as such
      if (inFace == 0)
	{
	  SatisfyPITEntryFromCache(pdu, inFace, data, pitEntry);
	}
      else
	{
	  NS_LOG_INFO ("On " << myAddr << " received PDU is satisfying Interest");

	  BOOST_FOREACH(const pit::IncomingFace &incoming, pitEntry->GetIncoming ()) {

	    bool ok = false;
	    bool sentSomething = false;
	    bool isAppFace = false;
	    uint32_t incomingID = incoming.m_face->GetId ();
	    std::map<Address,pit::PoAInInfo> poa_map = incoming.GetPoADestinations();
	    std::map<Address,pit::PoAInInfo>::iterator it;

	    if (incoming.m_face->isAppFace ())
	      {
		NS_LOG_INFO ("Satisfying an Interest from an Application Face " << incomingID);
		isAppFace = true;
	      }

	    std::vector<Ptr<NNNAddress> > distinct = incoming.m_addrs->GetDistinctDestinations ();

	    if (distinct.empty ())
	      {
		NS_LOG_INFO ("PIT Entry has no 3N names aggregated");

		Ptr<DO> do_o_spec;
		Ptr<DU> du_o_spec;

		// In the case of a PDU having the information satisfying an
		// Interest, and the PIT having no 3N names aggregated, the
		// NULLp and the SO is just forwarded through the PIT data. If
		// you received a DO or DU then we make sure that the original
		// PDU doesn't need to be redirected or buffered
		if (wasNULL)
		  {
		    for (it = poa_map.begin (); it != poa_map.end (); ++it)
		      {
			// In case the structure has seen a NULL PDU
			if ((it->second).m_saw_null)
			  {
			    if (isAppFace)
			      {
				NS_LOG_INFO ("Satisfying using NULLp out App Face: " << incomingID <<
					     " DATA " << data->GetName() << " SeqNo " << data->GetName ().get (-1).toSeqNum ());
				sentSomething = TryToSendDataInNULL (pdu->GetFlowid (), inFace, incoming.m_face,
								     data, pitEntry);
			      }
			    else
			      {
				NS_LOG_INFO ("Satisfying using NULLp out Face: " << incomingID << " PoA: " << it->first <<
					     " DATA " << data->GetName() << " SeqNo " << data->GetName ().get (-1).toSeqNum ());
				sentSomething = TryToSendDataInNULLToPoA (pdu->GetFlowid (), inFace, incoming.m_face,
									  data, pitEntry,
									  it->first);
			      }
			  }
		      }
		  }
		else if (wasSO)
		  {
		    for (it = poa_map.begin (); it != poa_map.end (); ++it)
		      {
			if (isAppFace)
			  {
			    NS_LOG_INFO ("Satisfying using SO out App Face: " << incomingID <<
					 " DATA " << data->GetName() << " SeqNo " << data->GetName ().get (-1).toSeqNum ());
			    if (m_useQueues)
			      ok = EnqueueSO(incoming.m_face, incoming.m_face->GetBroadcastAddress(), so_i);
			    else
			      ok = incoming.m_face->SendSO (so_i);
			  }
			else
			  {
			    NS_LOG_INFO ("Satisfying using SO out Face: " << incomingID << " PoA: " << it->first <<
					 " DATA " << data->GetName() << " SeqNo " << data->GetName ().get (-1).toSeqNum ());

			    bool normalSend = true;
			    if (m_ask_ACKP)
			      {
				std::vector<Ptr<const NNNAddress> > ret = m_nnst->OneHopNameInfo(GetNode3NNamePtr(), incoming.m_face);

				if (!ret.empty())
				  {
				    normalSend = false;
				    int8_t n3ok = txPreparation (so_i, incoming.m_face, it->first, false);
				    ok = (n3ok >= 0);
				  }
				else
				  {
				    NS_LOG_WARN ("Face " << incomingID << " has no apparent PoAs, not expecting ACKPs");
				    SetupFlowidSeq(so_i, incoming.m_face, it->first, false);
				  }
			      }
			    else
			      {
				txReset(so_i);
			      }

			    if (normalSend)
			      {
				if (m_useQueues)
				  ok = EnqueueSO(incoming.m_face, it->first, so_i);
				else
				  ok = incoming.m_face->SendSO (so_i, it->first);
				UpdatePDUCountersB (so_i, incoming.m_face, ok, false);
			      }
			  }

			if (!ok)
			  {
			    // Log Data drops
			    m_dropData (data, incoming.m_face);
			  }
			else
			  {
			    // Log that a Data PDU was sent
			    DidSendOutData (inFace, incoming.m_face, data, pitEntry);
			    sentSomething = true;
			  }
		      }
		  }
		else if (wasDO || wasDU)
		  {
		    // Verify that the PDUs don't need to be redirected
		    Ptr<const NNNAddress> pdu3N;

		    if (wasDO)
		      pdu3N = do_i->GetNamePtr ();
		    else
		      pdu3N = du_i->GetDstNamePtr ();

		    bool redirect = false;
		    NNNAddress endDest;
		    Ptr<const NNNAddress> searchDest;

		    if (m_in_transit_redirect)
		      {
			redirect = m_nnpt->foundOldName (pdu3N);
			searchDest = m_nnpt->findPairedNamePtr (pdu3N);
			endDest = searchDest->getName ();

			if (redirect)
			  {
			    NS_LOG_DEBUG ("On " << myAddr << *m_nnpt);
			    NS_LOG_INFO ("We are redirecting " << *pdu3N << " to " << endDest);
			  }
		      }
		    else
		      {
			searchDest = pdu3N;
			endDest = pdu3N->getName();
		      }

		    // Need routing information, attempt handshake
		    if (m_useHR)
		      AskHandshake (searchDest);


		    // If redirecting, recreate the DO PDU
		    if (redirect)
		      {
			if (wasDO)
			  {
			    // Create the new DO PDU to send
			    do_o_spec = Create<DO> ();
			    // Set the new 3N name
			    do_o_spec->SetName (searchDest);
			    // Set the lifetime of the 3N PDU
			    do_o_spec->SetLifetime (m_3n_lifetime);
			    // Configure payload for PDU
			    do_o_spec->SetPayload (icn_pdu);
			    // Signal that the PDU had an ICN PDU as payload
			    do_o_spec->SetPDUPayloadType (ICN_NNN);
			  }
			else
			  {
			    du_o_spec = Create<DU> ();
			    // Use the original DU's Src 3N name
			    du_o_spec->SetSrcName (du_i->GetSrcNamePtr ());
			    // Set the new 3N name destination
			    du_o_spec->SetDstName (searchDest);
			    // Set the lifetime of the 3N PDU
			    du_o_spec->SetLifetime (m_3n_lifetime);
			    // Use the original setting for authoritative
			    du_o_spec->SetAuthoritative(du_i->IsAuthoritative ());
			    // Configure payload for PDU
			    du_o_spec->SetPayload (icn_pdu);
			    // Signal that the PDU had an ICN PDU as payload
			    du_o_spec->SetPDUPayloadType (ICN_NNN);
			  }
		      }

		    NS_LOG_DEBUG ("On " << myAddr << " " << *m_nnst);

		    if (m_buffer_during_disenroll && (m_hasDisenrolled || m_nnst->isEmpty()))
		      {
			NS_LOG_INFO ("Node is in dis-enroll or NNST is blank, queuing PDU for later");

			if (wasDO)
			  {
			    if (redirect)
			      {
				m_node_pdu_queue->pushDO(do_o_spec, Seconds(0));
			      }
			    else
			      {
				m_node_pdu_queue->pushDO(do_i, Seconds(0));
			      }
			  }
			else
			  {
			    if (redirect)
			      {
				m_node_pdu_queue->pushDU(du_o_spec, Seconds(0));
			      }
			    else
			      {
				m_node_pdu_queue->pushDU(du_i, Seconds(0));
			      }
			  }
			continue;
		      }

		    // Roughly pick the next hop that would bring us closer to endDest
		    std::pair<Ptr<Face>, Address> tmp = m_nnst->ClosestSectorFaceInfo (endDest, 0);

		    Ptr<Face> outFace = tmp.first;
		    Address destAddr = tmp.second;

		    if (outFace == 0)
		      {
			NS_LOG_WARN ("Attempted to send, didn't find an interface, defaulting");
			outFace = incoming.m_face;
			destAddr = outFace->GetBroadcastAddress();
		      }

		    uint32_t outgoingID = outFace->GetId ();

		    // We may have obtained a DEN for this destination so we need to check
		    if (m_node_pdu_buffer->DestinationExists (endDest) && !redirect)
		      {
			if (!endDest.isDirectSubSector (myAddr))
			  {
			    NS_LOG_INFO ("On " << myAddr << ", " << endDest << " is not a direct subsector, buffering and not forwarding PDU");
			    if (wasDO)
			      {
				m_node_pdu_buffer->PushDO (endDest, do_i);
			      }
			    else
			      {
				m_node_pdu_buffer->PushDU (endDest, du_i);
			      }
			    if (m_3n_only_buffer)
			      continue;
			  }
		      }

		    // We check that we are the destination
		    if (m_node_names->foundName (searchDest))
		      {
			NS_LOG_INFO ("We are the desired 3N named node destination");

			if (incoming.m_face->isAppFace ())
			  {
			    if (wasDO)
			      {
				NS_LOG_INFO ("Satisfying using DO to " << endDest << " out Face:  " << incomingID <<
					     " DATA " << data->GetName() << " SeqNo " << data->GetName ().get (-1).toSeqNum ());
				if (redirect)
				  {
				    ok = TryToSendDataInDO (do_o_spec, Address (), inFace, incoming.m_face, data, pitEntry);
				  }
				else
				  {
				    ok = TryToSendDataInDO (do_i, Address (), inFace, incoming.m_face, data, pitEntry);
				  }
			      }
			    else
			      {
				NS_LOG_INFO ("Satisfying using DU to " << endDest << " out Face: " << incomingID <<
					     " DATA " << data->GetName() << " SeqNo " << data->GetName ().get (-1).toSeqNum ());
				if (redirect)
				  {
				    ok = TryToSendDataInDU (du_o_spec, Address (), inFace, incoming.m_face, data, pitEntry);
				  }
				else
				  {
				    ok = TryToSendDataInDU (du_i, Address (), inFace, incoming.m_face, data, pitEntry);
				  }
			      }
			    sentSomething = ok;
			  }
			else
			  {
			    NS_LOG_DEBUG ("Not an Application Face, something is wrong");
			  }
		      }

		    if (wasDO)
		      {
			NS_LOG_INFO ("Satisfying using DO to " << endDest << " out Face: " << outgoingID <<
				     " DATA " << data->GetName() << " SeqNo " << data->GetName ().get (-1).toSeqNum ());
			if (redirect)
			  {
			    ok = TryToSendDataInDO (do_o_spec, destAddr, inFace, outFace, data, pitEntry);
			  }
			else
			  {
			    ok = TryToSendDataInDO (do_i, destAddr, inFace, outFace, data, pitEntry);
			  }
		      }
		    else
		      {
			NS_LOG_INFO ("Satisfying using DU to " << endDest << " out Face: " << outgoingID <<
				     " DATA " << data->GetName() << " SeqNo " << data->GetName ().get (-1).toSeqNum ());
			if (redirect)
			  {
			    ok = TryToSendDataInDU (du_o_spec, destAddr, inFace, outFace, data, pitEntry);
			  }
			else
			  {
			    ok = TryToSendDataInDU (du_i, destAddr, inFace, outFace, data, pitEntry);
			  }
		      }

		    if (outgoingID == incomingID)
		      sentSomething = ok;

		  }
		else if (wasRawData)
		  {
		    for (it = poa_map.begin (); it != poa_map.end (); ++it)
		      {
			// In case the structure has seen a NULL PDU
			if ((it->second).m_saw_null)
			  {
			    if (isAppFace)
			      {
				NS_LOG_INFO ("Satisfying using NULLp out App Face: " << incomingID <<
					     " DATA " << data->GetName() << " SeqNo " << data->GetName ().get (-1).toSeqNum ());
				sentSomething = TryToSendDataInNULL (pdu->GetFlowid (), inFace, incoming.m_face,
								     data, pitEntry);
			      }
			    else
			      {
				NS_LOG_INFO ("Satisfying using NULLp out Face: " << incomingID << " PoA: " << it->first <<
					     " DATA " << data->GetName() << " SeqNo " << data->GetName ().get (-1).toSeqNum ());
				sentSomething = TryToSendDataInNULLToPoA (pdu->GetFlowid (), inFace, incoming.m_face,
									  data, pitEntry,
									  it->first);
			      }
			  }

			// In case we have seen raw ICN PDUs
			if ((it->second).m_raw_icn)
			  {
			    if (isAppFace)
			      {
				NS_LOG_INFO ("Satisfying using raw ICN Data out App Face " << incomingID <<
					     " DATA " << data->GetName() << " SeqNo " << data->GetName ().get (-1).toSeqNum ());
				sentSomething = TryToSendRawData (inFace, incoming.m_face,
								  data, pitEntry);
			      }
			    else
			      {
				NS_LOG_INFO ("Attempting to satisfy using raw ICN Data out Face " << incomingID <<
					     " PoA: " << it->first << " DATA " << data->GetName() << " SeqNo " << data->GetName ().get (-1).toSeqNum ());
				sentSomething = TryToSendRawDataToPoA (inFace, incoming.m_face,
								       data, pitEntry,
								       it->first);
			      }
			  }
		      }
		  }
	      }
	    else
	      {
		NS_LOG_INFO ("PIT Entry has 3N names aggregated");
		BOOST_FOREACH (Ptr<NNNAddress> j, distinct) {
		  bool subSector = m_node_names->foundName(j);
		  NNNAddress newdst;
		  // Obtain all the 3N names aggregated in this sector
		  std::vector<Ptr<NNNAddress> > addrs = incoming.m_addrs->GetCompleteDestinations (j);
		  BOOST_FOREACH (Ptr<NNNAddress> i, addrs) {
		    // If the aggregation is the same as the 3N Name the node is using, then
		    // everything aggregated is probably connected to it
		    if (subSector)
		      {
			NS_LOG_INFO ("We are satisfying Interests to nodes that are directly connected to us " << *j << ", continue");
		      }
		    // We are pushing to a different sector
		    else
		      {
			NS_LOG_INFO ("On " << myAddr << " satisfying for 3N names in different subsector " << *j << ", destination: " << *i);
			if (sentSomething)
			  {
			    NS_LOG_INFO ("On " << myAddr << ", we seem to have already sent to subsector " << *j << " about to skip");
			    continue;
			  }
		      }

		    bool sentToAddr = false;

		    // Temporary PDUs
		    Ptr<DO> do_o_spec;
		    Ptr<DU> du_o_spec;
		    Ptr<DATAPDU> o_spec;
		    // In the case of a PDU having the information satisfying an Interest and the PIT
		    // having 3N names aggregated, we always have a 3N name as the destination. We
		    // have the following cases:
		    //
		    // In -> Out:
		    // NULLp -> DO    - DO uses PIT DST
		    // DO(a) -> DO(b) - (b) uses PIT DST, no info from (a)
		    // SO -> DU       - DU uses SO SRC info and PIT DST
		    // DU(a) -> DU(b) - (b) uses SRC info from (a) and PIT DST
		    //
		    // As always, we make sure that the PDU doesn't need to be redirected or buffered
		    bool redirect = false;
		    Ptr<const NNNAddress> searchDest;
		    NNNAddress endDest;

		    if (m_in_transit_redirect)
		      {
			redirect = m_nnpt->foundOldName (i);
			searchDest = m_nnpt->findPairedNamePtr (i);
			endDest = searchDest->getName ();

			if (redirect)
			  {
			    NS_LOG_DEBUG ("On " << myAddr << *m_nnpt);
			    NS_LOG_INFO ("We are redirecting " << *i << " to " << endDest);
			  }
		      }
		    else
		      {
			searchDest = i;
			endDest = i->getName();
		      }

		    // Start creation of the PDU to send
		    // Test whether the PDU we saw was a NULLp || DO or SO || DU
		    if (wasNULL || wasDO)
		      {
			// Create the new DO PDU to send
			do_o_spec = Create<DO> ();
			// Set the new 3N name
			do_o_spec->SetName (searchDest);
			// Set the lifetime of the 3N PDU
			do_o_spec->SetLifetime (m_3n_lifetime);
			// Configure payload for PDU
			do_o_spec->SetPayload (icn_pdu);
			// Signal that the PDU had an ICN PDU as payload
			do_o_spec->SetPDUPayloadType (ICN_NNN);

			o_spec = DynamicCast<DATAPDU> (do_o_spec);
		      }
		    else
		      {
			Ptr<const NNNAddress> src3N;
			bool isAuthoritative = true;
			if (wasSO)
			  {
			    src3N = so_i->GetNamePtr ();
			  }
			else
			  {
			    isAuthoritative = du_i->IsAuthoritative ();
			    src3N = du_i->GetSrcNamePtr ();
			  }

			du_o_spec = Create<DU> ();
			// Use the original DU's Src 3N name
			du_o_spec->SetSrcName (src3N);
			// Set the new 3N name destination
			du_o_spec->SetDstName (searchDest);
			// Set the lifetime of the 3N PDU
			du_o_spec->SetLifetime (m_3n_lifetime);
			// Use the original setting for authoritative
			du_o_spec->SetAuthoritative(isAuthoritative);
			// Configure payload for PDU
			du_o_spec->SetPayload (icn_pdu);
			// Signal that the PDU had an ICN PDU as payload
			du_o_spec->SetPDUPayloadType (ICN_NNN);

			o_spec = DynamicCast<DATAPDU> (du_o_spec);
		      }

		    // We may have obtained a DEN for this destination so we need to check
		    if (m_node_pdu_buffer->DestinationExists (endDest) && !redirect)
		      {
			if (!endDest.isDirectSubSector (myAddr))
			  {
			    NS_LOG_INFO ("On " << myAddr << ", " << endDest << " is not a direct subsector, buffering and not forwarding PDU");
			    m_node_pdu_buffer->PushDATAPDU (endDest, o_spec);
			    if (m_3n_only_buffer)
			      continue;
			  }
		      }

		    NS_LOG_DEBUG (*m_node_names);

		    if (m_node_names->foundName (searchDest))
		      {
			NS_LOG_INFO ("We are the desired 3N named node destination");

			if (incoming.m_face->isAppFace ())
			  {
			    if (wasNULL || wasDO)
			      {
				NS_LOG_INFO ("Satisfying using DO out Face: " << incomingID <<
					     " DATA " << data->GetName() << " SeqNo " << data->GetName ().get (-1).toSeqNum ());
				ok = TryToSendDataInDO (do_o_spec, Address (), inFace, incoming.m_face, data, pitEntry);

			      }
			    else
			      {
				NS_LOG_INFO ("Satisfying using DU out Face: " << incomingID <<
					     " DATA " << data->GetName() << " SeqNo " << data->GetName ().get (-1).toSeqNum ());
				ok = TryToSendDataInDU (du_o_spec, Address (), inFace, incoming.m_face, data, pitEntry);
			      }

			    sentSomething = ok;
			    sentToAddr = ok;
			  }
			else
			  {
			    NS_LOG_DEBUG ("Not an Application Face, something is wrong");
			  }
		      }

		    // We have checked buffering and redirection. Attempt to finally send
		    if (!sentToAddr)
		      {
			// Need routing information, attempt handshake
			if (m_useHR)
			  AskHandshake (searchDest);

			NS_LOG_DEBUG ("On " << myAddr << " " << *m_nnst);

			if (m_buffer_during_disenroll && (m_hasDisenrolled || m_nnst->isEmpty()))
			  {
			    NS_LOG_INFO ("Node is in dis-enroll or NNST is blank, queuing PDU for later");

			    if (wasNULL || wasDO)
			      {
				m_node_pdu_queue->pushDO(do_o_spec, Seconds(0));
			      }
			    else if (wasDU)
			      {
				m_node_pdu_queue->pushDU(du_o_spec, Seconds(0));
			      }
			    else
			      {
				NS_LOG_WARN ("Attempted check of PDU type. None found, check if this is desired effect");
			      }
			    continue;
			  }

			// Roughly pick the next hop that would bring us closer to endDest
			std::pair<Ptr<Face>, Address> tmp = m_nnst->ClosestSectorFaceInfo (endDest, 0);

			Ptr<Face> outFace = tmp.first;
			Address destAddr = tmp.second;

			if (outFace == 0)
			  {
			    NS_LOG_WARN ("Attempted to send, didn't find an interface, defaulting");
			    outFace = incoming.m_face;
			    destAddr = outFace->GetBroadcastAddress();
			  }

			uint32_t outgoingID = outFace->GetId ();

			if (wasNULL || wasDO)
			  {
			    NS_LOG_INFO ("Satisfying using DO to " << endDest << " out Face: " << outgoingID <<
					 " DATA " << data->GetName() << " SeqNo " << data->GetName ().get (-1).toSeqNum ());
			    ok = TryToSendDataInDO (do_o_spec, destAddr, inFace, outFace, data, pitEntry);
			    sentToAddr = ok;
			    if (outgoingID == incomingID)
			      sentSomething = ok;
			  }
			else
			  {
			    NS_LOG_INFO ("Satisfying using " << std::boolalpha << du_o_spec->IsAuthoritative () << " authoritative DU to " << endDest << " out Face: " << outgoingID <<
					 " DATA " << data->GetName() << " SeqNo " << data->GetName ().get (-1).toSeqNum ());
			    ok = TryToSendDataInDU(du_o_spec, destAddr, inFace, outFace, data, pitEntry);
			    sentToAddr = ok;
			    if (outgoingID == incomingID)
			      sentSomething = ok;
			  }
		      }
		  }
		}
	      }

	    // If we haven't yet pushed anything out the incoming face
	    // registered in the PIT, we should do it now
	    if (!sentSomething)
	      {
		NS_LOG_INFO ("Have tested all possibilities, yet haven't yet satisfied PIT's Face " << incomingID);
		for (it = poa_map.begin (); it != poa_map.end (); ++it)
		  {
		    // In case the structure has seen a NULL PDU
		    if ((it->second).m_saw_null)
		      {
			if (isAppFace)
			  {
			    NS_LOG_INFO ("Satisfying using NULLp out App Face " << incomingID << " DATA " << data->GetName() << " SeqNo " << data->GetName ().get (-1).toSeqNum ());
			    sentSomething = TryToSendDataInNULL (pdu->GetFlowid (), inFace, incoming.m_face,
								 data, pitEntry);
			  }
			else
			  {
			    NS_LOG_INFO ("Satisfying using NULLp out Face " << incomingID <<
					 " PoA: " << it->first << " DATA " << data->GetName() << " SeqNo " << data->GetName ().get (-1).toSeqNum ());
			    sentSomething = TryToSendDataInNULLToPoA (pdu->GetFlowid (), inFace, incoming.m_face,
								      data, pitEntry,
								      it->first);
			  }
		      }

		    // In case we have seen raw ICN PDUs
		    if ((it->second).m_raw_icn)
		      {
			if (isAppFace)
			  {
			    NS_LOG_INFO ("Satisfying using raw ICN Data out App Face " << incomingID <<
					 " DATA " << data->GetName() << " SeqNo " << data->GetName ().get (-1).toSeqNum ());
			    sentSomething = TryToSendRawData (inFace, incoming.m_face,
							      data, pitEntry);
			  }
			else
			  {
			    NS_LOG_INFO ("Attempting to satisfy using ICN Data out Face " << incomingID <<
					 " PoA: " << it->first << " DATA " << data->GetName() << " SeqNo " << data->GetName ().get (-1).toSeqNum ());
			    sentSomething = TryToSendRawDataToPoA (inFace, incoming.m_face,
								   data, pitEntry,
								   it->first);
			  }
		      }
		  }
	      }
	  }
	}

      NS_LOG_INFO ("Finished satisfying, clearing PIT Entry");

      // All incoming interests are satisfied. Remove them
      pitEntry->ClearIncoming ();

      // Remove all outgoing faces
      pitEntry->ClearOutgoing ();

      // Set pruning timeout on PIT entry (instead of deleting the record)
      m_pit->MarkErased (pitEntry);
    }

    std::pair<std::set<Ptr<Face> >, int>
    ICN3NSmartFlooding::DoSelectDOPropagations (Ptr<DO> pdu, Ptr<Face> inFace, Ptr<const Interest> interest,
						Ptr<pit::Entry> pitEntry)
    {
      NS_LOG_FUNCTION (this);
      std::set<Ptr<Face> > ret;
      int propagatedCount = 0;
      Ptr<const NNNAddress> inDO3N = pdu->GetNamePtr();
      std::set<Ptr<const NNNAddress>, Ptr3NSizeMinComp>::iterator it;

      //      NS_LOG_DEBUG ("Current " << *m_fib);

      if (m_Enable_Multicast)
	{
	  NamesSeenContainer selection  = SelectMulticastDestinationsByTime (pitEntry->GetFibEntry ()->Find3NDestinationsByTime (m_fib_3n_threshold), inDO3N);

	  names_seen_by_uname& final_index = selection.get<i_uname> ();
	  names_seen_by_uname::iterator it = final_index.begin ();

	  size_t dests = final_index.size ();

	  uint32_t num = 0;
	  Ptr<const NNNAddress> tmp_name = 0;
	  while (it != final_index.end ())
	    {
	      tmp_name = it->name;
	      if (*tmp_name == *inDO3N)
		{
		  NS_LOG_DEBUG ("Sending original DO #" << (num+1) << "/" << dests << " to " << *tmp_name);
		  std::pair<std::set<Ptr<Face> >,int> tmp1 = PropagateInterestinDO (pdu, inFace, interest, pitEntry);
		  ret.insert(tmp1.first.begin (), tmp1.first.end ());
		  propagatedCount += tmp1.second;
		}
	      else
		{
		  // Create a new DO PDU to send the data
		  Ptr<DO> do_o_spec = Create<DO> ();
		  do_o_spec->SetFlowid(pdu->GetFlowid ());
		  do_o_spec->SetSequence(pdu->GetSequence ());
		  // Set the new 3N name
		  do_o_spec->SetName (tmp_name);
		  // Set the lifetime of the 3N PDU
		  do_o_spec->SetLifetime (m_3n_lifetime);
		  // Signal that the PDU had an ICN PDU as payload
		  do_o_spec->SetPDUPayloadType (ICN_NNN);
		  // Configure payload for PDU
		  do_o_spec->SetPayload (pdu->GetPayload ()->Copy ());

		  NS_LOG_DEBUG ("Sending DO #" << (num+1) << "/" << dests << " to " << *tmp_name);
		  std::pair<std::set<Ptr<Face> >,int> tmp1 = PropagateInterestinDO (do_o_spec, inFace, interest, pitEntry);
		  ret.insert(tmp1.first.begin (), tmp1.first.end ());
		  propagatedCount += tmp1.second;
		}
	      ++it;
	      num++;
	    }
	  return make_pair (ret, propagatedCount);
	}
      else if (m_Enable_FIB_part_select)
	{
	  Ptr<const icn::Name> interest_name = interest->GetNamePtr ();
	  uint32_t curr_part = interest_name->getSeqNum ();

	  NS_LOG_DEBUG ("For ICN Name: " << *interest_name << " searching Part: " << curr_part);
	  PartsSeenContainer selection  = pitEntry->GetFibEntry ()->Find3NDestinationsByTime ();

	  if (selection.size () > 0)
	    {
	      parts_seen_by_natural_order& final_index = selection.get<p_natural> ();
	      parts_seen_by_natural_order::iterator it = final_index.begin ();
	      Ptr<const NNNAddress> tmp_name = 0;
	      bool normal = true;

	      while (it != final_index.end ())
		{
		  Part3NSeen tmp = *it;

		  NS_LOG_DEBUG ("Have seen for " << *interest_name << " 3N source: " << *(tmp.m_name) << " Time: " << tmp.m_lastSeen.GetSeconds () << " Part: " << tmp.m_part);
		  if (tmp.m_part <= curr_part)
		    {
		      tmp_name = tmp.m_name;
		      if (*tmp_name != *inDO3N)
			{
			  NS_LOG_DEBUG ("Modifying DO PDU to 3N source: " << *(tmp.m_name));
			  // Create a new DO PDU to send the data
			  Ptr<DO> do_o_spec = Create<DO> ();
			  do_o_spec->SetFlowid(pdu->GetFlowid ());
			  do_o_spec->SetSequence(pdu->GetSequence ());
			  // Set the new 3N name
			  do_o_spec->SetName (tmp_name);
			  // Set the lifetime of the 3N PDU
			  do_o_spec->SetLifetime (m_3n_lifetime);
			  // Signal that the PDU had an ICN PDU as payload
			  do_o_spec->SetPDUPayloadType (ICN_NNN);
			  // Configure payload for PDU
			  do_o_spec->SetPayload (pdu->GetPayload ()->Copy ());

			  std::pair<std::set<Ptr<Face> >,int> tmp1 = PropagateInterestinDO (do_o_spec, inFace, interest, pitEntry);
			  ret.insert(tmp1.first.begin (), tmp1.first.end ());
			  propagatedCount += tmp1.second;
			  normal = false;
			}
		      else
			{
			  NS_LOG_DEBUG ("No need to change destination. Normal forwarding");
			}
		      // Break the while
		      break;
		    }
		  else
		    {
		      // Haven't found a viable destination, continue
		      ++it;
		    }
		}
	      if (!normal)
		return make_pair (ret, propagatedCount);
	    }
	  else
	    {
	      NS_LOG_DEBUG ("Found no FIB data for Time or Parts. Normal forwarding");
	    }
	}

      NS_LOG_DEBUG ("Sending original DO # 1/1 to " << *inDO3N);
      std::pair<std::set<Ptr<Face> >,int> tmp1 = PropagateInterestinDO (pdu, inFace, interest, pitEntry);
      ret.insert(tmp1.first.begin (), tmp1.first.end ());
      propagatedCount += tmp1.second;

      return make_pair (ret, propagatedCount);
    }

    std::pair<std::set<Ptr<Face> >, int>
    ICN3NSmartFlooding::DoSelectDUPropagations (Ptr<DU> pdu, Ptr<Face> inFace, Ptr<const Interest> interest,
						     Ptr<pit::Entry> pitEntry)
    {
      NS_LOG_FUNCTION (this);
      std::set<Ptr<Face> > ret;
      int propagatedCount = 0;
      Ptr<const NNNAddress> inDU3N = pdu->GetDstNamePtr();
      std::set<Ptr<const NNNAddress>, Ptr3NSizeMaxComp> tmp;
      std::set<Ptr<const NNNAddress>, Ptr3NSizeMinComp> destinations;
      std::set<Ptr<const NNNAddress>, Ptr3NSizeMinComp>::iterator it;

      uint32_t num = 0;

      //      NS_LOG_DEBUG ("Current " << *m_fib);

      if (m_Enable_Multicast)
	{
	  bool sentToOriginalDest = false;
	  NamesSeenContainer selection = SelectMulticastDestinationsByTime (pitEntry->GetFibEntry ()->Find3NDestinationsByTime (m_fib_3n_threshold), inDU3N);

	  // We only want to send a DU to the newest destination, we send DOs to the rest in case we have information in different branches
	  names_seen_by_uname& final_index = selection.get<i_uname> ();
	  names_seen_by_uname::iterator it2 = final_index.begin ();

	  size_t dests = final_index.size ();

	  // Give priority to the name given in by the original PDU
	  names_seen_by_name& name_index = selection.get<i_name> ();
	  names_seen_by_name::iterator it3 = name_index.find (inDU3N);

	  if (it3 != name_index.end ())
	    {
	      NS_LOG_DEBUG ("Sending original DU #" << (num+1) << "/" << dests << " to " << *(it3->name));
	      std::pair<std::set<Ptr<Face> >,int> tmp1 = PropagateInterestinDU (pdu, inFace, interest, pitEntry);
	      ret.insert(tmp1.first.begin (), tmp1.first.end ());
	      propagatedCount += tmp1.second;
	      sentToOriginalDest = true;
	      num++;
	    }

	  while (it2 != final_index.end ())
	    {
	      if (num == 0 && !sentToOriginalDest)
		{
		  // Create a new DU PDU to send the data
		  Ptr<DU> du_o_spec = Create<DU> ();
		  du_o_spec->SetFlowid(pdu->GetFlowid ());
		  du_o_spec->SetSequence(pdu->GetSequence ());
		  // Use the original DU's Src 3N name
		  du_o_spec->SetSrcName (pdu->GetSrcNamePtr ());
		  // Set the new 3N name destination
		  du_o_spec->SetDstName (it2->name);
		  // Set the lifetime of the 3N PDU
		  du_o_spec->SetLifetime (m_3n_lifetime);
		  // Use the original DU's Src 3N name
		  du_o_spec->SetAuthoritative (pdu->IsAuthoritative ());
		  // Signal that the PDU had an ICN PDU as payload
		  du_o_spec->SetPDUPayloadType (ICN_NNN);
		  // Configure payload for PDU
		  du_o_spec->SetPayload (pdu->GetPayload ()->Copy ());

		  NS_LOG_DEBUG ("Sending DU #" << (num+1) << "/" << dests << " to " << *(it2->name));
		  std::pair<std::set<Ptr<Face> >,int> tmp1 = PropagateInterestinDU (du_o_spec, inFace, interest, pitEntry);
		  ret.insert(tmp1.first.begin (), tmp1.first.end ());
		  propagatedCount += tmp1.second;
		  ++it2;
		  num++;
		}
	      else
		{
		  if (*(it2->name) != *inDU3N)
		    {
		      // Create a new DO PDU to send the data
		      Ptr<DO> do_o_spec = Create<DO> ();
		      do_o_spec->SetFlowid(pdu->GetFlowid ());
		      do_o_spec->SetSequence(pdu->GetSequence ());
		      // Set the new 3N name
		      do_o_spec->SetName (it2->name);
		      // Set the lifetime of the 3N PDU
		      do_o_spec->SetLifetime (m_3n_lifetime);
		      // Signal that the PDU had an ICN PDU as payload
		      do_o_spec->SetPDUPayloadType (ICN_NNN);
		      // Configure payload for PDU
		      do_o_spec->SetPayload (pdu->GetPayload ()->Copy ());

		      NS_LOG_DEBUG ("Sending DO #" << (num+1) << "/" << dests << " to " << *(it2->name));
		      std::pair<std::set<Ptr<Face> >,int> tmp1 = PropagateInterestinDO (do_o_spec, inFace, interest, pitEntry);
		      ret.insert(tmp1.first.begin (), tmp1.first.end ());
		      propagatedCount += tmp1.second;
		      num++;
		    }

		  ++it2;
		}
	    }
	  return make_pair (ret, propagatedCount);
	}
      else if (m_Enable_FIB_part_select)
	{
	  Ptr<const icn::Name> interest_name = interest->GetNamePtr ();
	  uint32_t curr_part = interest_name->getSeqNum ();
	  NS_LOG_DEBUG ("For ICN Name: " << *interest_name << " searching Part: " << curr_part);
	  PartsSeenContainer selection  = pitEntry->GetFibEntry ()->Find3NDestinationsByTime ();

	  if (selection.size () > 0)
	    {
	      parts_seen_by_natural_order& final_index = selection.get<p_natural> ();
	      parts_seen_by_natural_order::iterator it = final_index.begin ();
	      Ptr<const NNNAddress> tmp_name = 0;
	      bool normal = true;

	      while (it != final_index.end ())
		{
		  Part3NSeen tmp = *it;

		  NS_LOG_DEBUG ("Have seen for " << *interest_name << " 3N source: " << *(tmp.m_name) << " Time: " << tmp.m_lastSeen.GetSeconds () << " Part: " << tmp.m_part);
		  if (tmp.m_part <= curr_part)
		    {
		      tmp_name = tmp.m_name;
		      if (*tmp_name != *inDU3N)
			{
			  NS_LOG_DEBUG ("Modifying DU PDU to 3N source: " << *(tmp.m_name));
			  // Create a new DO PDU to send the data
			  Ptr<DU> du_o_spec = Create<DU> ();
			  du_o_spec->SetFlowid(pdu->GetFlowid ());
			  du_o_spec->SetSequence(pdu->GetSequence ());
			  // Use the original DU's Src 3N name
			  du_o_spec->SetSrcName (pdu->GetSrcNamePtr ());
			  // Set the new 3N name destination
			  du_o_spec->SetDstName (tmp_name);
			  // Set the lifetime of the 3N PDU
			  du_o_spec->SetLifetime (m_3n_lifetime);
			  // Use the original DU's Src 3N name
			  du_o_spec->SetAuthoritative (pdu->IsAuthoritative ());
			  // Signal that the PDU had an ICN PDU as payload
			  du_o_spec->SetPDUPayloadType (ICN_NNN);
			  // Configure payload for PDU
			  du_o_spec->SetPayload (pdu->GetPayload ()->Copy ());

			  std::pair<std::set<Ptr<Face> >,int> tmp1 = PropagateInterestinDU (du_o_spec, inFace, interest, pitEntry);
			  ret.insert(tmp1.first.begin (), tmp1.first.end ());
			  propagatedCount += tmp1.second;
			  normal = false;
			}
		      else
			{
			  NS_LOG_DEBUG ("No need to change destination. Begin forwarding");
			}
		      // Break the while
		      break;
		    }
		  else
		    {
		      // Haven't found a viable destination, continue
		      ++it;
		    }
		}
	      if (!normal)
		return make_pair (ret, propagatedCount);
	    }
	  else
	    {
	      NS_LOG_DEBUG ("Found no FIB data for Time or Parts. Normal forwarding");
	    }
	}

      NS_LOG_DEBUG ("Sending DU #1/1 to " << *inDU3N);
      std::pair<std::set<Ptr<Face> >,int> tmp1 = PropagateInterestinDU (pdu, inFace, interest, pitEntry);
      ret.insert(tmp1.first.begin (), tmp1.first.end ());
      propagatedCount += tmp1.second;

      return make_pair (ret, propagatedCount);
    }

    std::pair<std::set<Ptr<Face> >,int>
    ICN3NSmartFlooding::PropagateInterestwith3NLessDst (Ptr<DATAPDU> pdu, Ptr<Face> inFace, std::set<Ptr<Face> > sent,
							Ptr<const Interest> interest, Ptr<pit::Entry> pitEntry,
							const Address& from)
    {
      NS_LOG_FUNCTION (inFace->GetId () << interest->GetName() << from);
      int propagatedCount = 0;

      Ptr<SO> so_i;
      bool wasSO = false;
      Ptr<NULLp> null_i;
      bool wasNULL = false;
      bool wasDO = false;
      bool wasDU = false;

      std::set<Ptr<Face> > ret;
      std::set<Ptr<Face> >::iterator itf = sent.begin ();

      while (itf != sent.end ())
	{
	  NS_LOG_DEBUG ("Face " << (*itf)->GetId () << " will be skipped");
	  ret.insert(*itf);
	  ++itf;
	}

      std::pair<std::set<Ptr<Face> >,int> tmp1;

      // Check if we have been given a raw ICN PDU
      if (pdu != 0)
	{
	  uint32_t pduid = pdu->GetPacketId();
	  switch(pduid)
	  {
	    case NULL_NNN:
	      null_i = DynamicCast<NULLp> (pdu);
	      wasNULL = true;
	      break;
	    case SO_NNN:
	      so_i = DynamicCast<SO> (pdu);
	      wasSO = true;
	      break;
	    case DO_NNN:
	      wasDO = true;
	      break;
	    case DU_NNN:
	      wasDU = true;
	      break;
	    default:
	      NS_LOG_WARN ("Was given an unsupported PDU ID " << pduid << ". Check code!");
	      return std::make_pair(ret, 0);
	  }
	}

      if (wasSO)
	NS_LOG_INFO ("Propagating SO from " << so_i->GetName());
      else if (wasNULL)
	NS_LOG_INFO ("Propagating NULLp");
      else if (wasDO)
    	NS_LOG_INFO ("Propagating DO");
      else if (wasDU)
    	NS_LOG_INFO ("Propagating DU");
      else
	NS_LOG_INFO ("Propagating raw ICN Interest");

      NS_LOG_DEBUG ("On " << GetNode3NName () << " current " << *m_fib);
      NS_LOG_DEBUG ("On " << GetNode3NName () << " current " << *m_nnst);

      std::list<fib::FaceMetric> res = FindFacesBy3NDestinations (pitEntry->GetFibEntry ());
      if (m_Enable_3N_face_time_select && !res.empty())
	{
	  NS_LOG_DEBUG ("Utilizing Time ordering");
	  bool attempt = false;
	  std::list<fib::FaceMetric>::iterator itlist;

	  if (!(propagatedCount > 0))
	    {
	      itlist = res.begin ();

	      while (itlist != res.end ())
		{
		  NS_LOG_DEBUG ("Trying timed ordered Green and Yellow 3N based Face: " << itlist->GetFace()->GetId ());
		  attempt = false;
		  if (itlist->GetStatus() == fib::FaceMetric::ICN_FIB_GREEN || itlist->GetStatus() == fib::FaceMetric::ICN_FIB_YELLOW)
		    attempt = true;

		  attempt &= (ret.find (itlist->GetFace()) == ret.end ());

		  if (attempt)
		    {
		      ret.insert(itlist->GetFace());
		      if (TrySendOutLayeredInterest (pdu, inFace, itlist->GetFace(), itlist->GetBestPoA(), interest, pitEntry, from))
			{
			  propagatedCount++;
			}
		    }
		  ++itlist;
		}
	    }
	}
      else if (m_Enable_Flooding)
	{
	  NS_LOG_DEBUG ("Utilizing Flooding");
	  // Flood the Faces not connected to the FIB
	  Ptr<Face> tmp;
	  for (uint32_t i = 0; i < m_faces->GetN (); i++)
	    {
	      tmp = m_faces->Get (i);
	      if (ret.find (tmp) != ret.end ())
		{
		  continue;
		}

	      ret.insert(tmp);
	      if (!TrySendOutLayeredInterest (pdu, inFace, tmp, tmp->GetBroadcastAddress(), interest, pitEntry, from))
		{
		  continue;
		}

	      propagatedCount++;
	    }
	}
      else if (m_Enable_FIB_part_select)
	{
	  NS_LOG_DEBUG ("Utilizing PoA part selection");

	  Ptr<const icn::Name> interest_name = interest->GetNamePtr ();
	  uint32_t curr_part = interest_name->getSeqNum ();

	  NS_LOG_DEBUG ("For ICN Name: " << *interest_name << " searching Part: " << curr_part);
	  PartsPoASeenContainer selection = pitEntry->GetFibEntry ()->FindPoADestinationsByTime ();

	  if (selection.size () > 0)
	    {
	      parts_poa_seen_by_natural_order& final_index = selection.get<p_natural> ();
	      parts_poa_seen_by_natural_order::iterator it = final_index.begin ();

	      Ptr<Face> tmp_face;
	      Address tmp_poa;

	      while (it != final_index.end ())
		{
		  PartPoASeen tmp = *it;

		  tmp_face = tmp.m_face;

		  if (tmp_face->IsPointToPoint())
		    {
		      tmp_poa = tmp.m_poa;
		    }
		  else
		    {
		      tmp_poa = tmp_face->GetBroadcastAddress();
		    }

		  NS_LOG_DEBUG ("Have seen for " << *interest_name << " Face: " << tmp_face->GetId() <<
				" from PoA: " << tmp_poa << " Time: " << tmp.m_lastSeen.GetSeconds () <<
				" Part: " << tmp.m_part);
		  if (tmp.m_part <= curr_part)
		    {
		      ret.insert (tmp_face);
		      if (!TrySendOutLayeredInterest (pdu, inFace, tmp_face, tmp_poa, interest, pitEntry, from))
			{
			  NS_LOG_ERROR ("Could not send out Face: " << tmp_face->GetId () << " although FIB considers this the location!");
			  break;
			}
		    }
		  else
		    {
		      ++it;
		      continue;
		    }

		  propagatedCount++;
		  break;
		}
	    }
	  else
	    {
	      NS_LOG_DEBUG ("Found no FIB data for Time or Parts. Normal forwarding");
	    }
	}
      else
	{
	  NS_LOG_DEBUG ("Utilizing Smart Flooding");

	  std::set<Ptr<Face> > yellow;

	  // Here we pick the next place to forward to using the ICN strategy
	  BOOST_FOREACH (const fib::FaceMetric &metricFace, pitEntry->GetFibEntry ()->m_faces.get<fib::i_fib_lastseen> ())
	  {
	    if (metricFace.GetStatus () == fib::FaceMetric::ICN_FIB_RED)
	      {
		NS_LOG_DEBUG ("Not using Red Face " << metricFace.GetFace ()->GetId ());
		ret.insert(metricFace.GetFace ());
		continue;
	      }

	    if (metricFace.GetStatus () == fib::FaceMetric::ICN_FIB_YELLOW)
	      {
		NS_LOG_DEBUG ("Not using Yellow Face " << metricFace.GetFace ()->GetId ());
		continue;
	      }

	    NS_LOG_DEBUG ("Trying Green Face: " << metricFace.GetFace ()->GetId ());
	    // Verify that the Face is not in the set we were given
	    if (ret.find (metricFace.GetFace ()) != ret.end())
	      {
		NS_LOG_DEBUG ("Face: " << metricFace.GetFace ()->GetId () << " was found in the given set");
		continue;
	      }

	    ret.insert(metricFace.GetFace ());
	    // Now we actually attempt to forward the PDU
	    if (!TrySendOutLayeredInterest (pdu, inFace, metricFace.GetFace (), metricFace.GetBestPoA(), interest, pitEntry, from))
	      {
		continue;
	      }

	    propagatedCount++;
	    return std::make_pair(ret, propagatedCount);
	  }

	  BOOST_FOREACH (const fib::FaceMetric &metricFace, pitEntry->GetFibEntry ()->m_faces.get<fib::i_fib_lastseen> ())
	  {
	    if (metricFace.GetStatus () == fib::FaceMetric::ICN_FIB_RED)
	      continue;

	    NS_LOG_DEBUG ("Trying Yellow Face: " << metricFace.GetFace ()->GetId ());
	    // Verify that the Face is not in the set we were given
	    if (ret.find (metricFace.GetFace ()) != ret.end())
	      {
		NS_LOG_DEBUG ("Face: " << metricFace.GetFace ()->GetId () << " was found in the given set");
		continue;
	      }

	    ret.insert(metricFace.GetFace ());
	    if (!TrySendOutLayeredInterest (pdu, inFace, metricFace.GetFace (), metricFace.GetBestPoA(), interest, pitEntry, from))
	      {
		continue;
	      }

	    propagatedCount++;
	  }

	  std::set<Ptr<Face> > all = GetAllFaceSet();
	  std::set<Ptr<Face> > diff;
	  std::set_difference(all.begin (), all.end (), ret.begin (), ret.end (), std::inserter(diff, diff.begin ()));
	  int counter = 0;
	  if (!diff.empty())
	    {
	      std::set<Ptr<Face> >::iterator it;

	      for (it = diff.begin (); it != diff.end (); ++it)
		{
		  NS_LOG_DEBUG ("Trying FIB not connected Face: " << (*it)->GetId ());
		  ret.insert(*it);
		  if (!TrySendOutLayeredInterest (pdu, inFace, *it, (*it)->GetBroadcastAddress (), interest, pitEntry, from))
		    {
		      continue;
		    }
		  counter++;
		}
	    }

	  if ((counter > 0) && !(propagatedCount > 0))
	    {
	      return std::make_pair(ret, 0);
	    }
	}
      return std::make_pair(ret, propagatedCount);
    }


    std::pair<std::set<Ptr<Face> >,int>
    ICN3NSmartFlooding::PropagateInterestwithoutPoAs (Ptr<DATAPDU> pdu, Ptr<Face> inFace, std::set<Ptr<Face> > sent,
						      Ptr<const Interest> interest, Ptr<pit::Entry> pitEntry)
    {
      NS_LOG_FUNCTION (this);
      int propagatedCount = 0;

      Ptr<SO> so_i;
      bool wasSO = false;
      Ptr<NULLp> null_i;
      bool wasNULL = false;
      bool wasDO = false;
      bool wasDU = false;

      std::set<Ptr<Face> > ret;
      std::set<Ptr<Face> >::iterator itf = sent.begin ();

      while (itf != sent.end ())
	{
	  NS_LOG_DEBUG ("Face " << (*itf)->GetId () << " will be skipped");
	  ret.insert(*itf);
	  ++itf;
	}

      std::pair<std::set<Ptr<Face> >,int> tmp1;

      // Check if we have been given a raw ICN PDU
      if (pdu != 0)
	{
	  uint32_t pduid = pdu->GetPacketId();
	  switch(pduid)
	  {
	    case NULL_NNN:
	      null_i = DynamicCast<NULLp> (pdu);
	      wasNULL = true;
	      break;
	    case SO_NNN:
	      so_i = DynamicCast<SO> (pdu);
	      wasSO = true;
	      break;
	    case DO_NNN:
	      wasDO = true;
	      break;
	    case DU_NNN:
	      wasDU = true;
	      break;
	    default:
	      NS_LOG_WARN ("Was given an unsupported PDU ID " << pduid << ". Check code!");
	      return std::make_pair(ret, 0);
	  }
	}

      if (wasSO)
	NS_LOG_INFO ("Propagating SO from " << so_i->GetName());
      else if (wasNULL)
	NS_LOG_INFO ("Propagating NULLp");
      else if (wasDO)
    	NS_LOG_INFO ("Propagating DO");
      else if (wasDU)
    	NS_LOG_INFO ("Propagating DU");
      else
	NS_LOG_INFO ("Propagating raw ICN Interest");

      NS_LOG_DEBUG ("On " << GetNode3NName () << " current FIB:" << std::endl << *m_fib);
      NS_LOG_DEBUG ("On " << GetNode3NName () << " current " << *m_nnst);

      std::list<fib::FaceMetric> res = FindFacesBy3NDestinations (pitEntry->GetFibEntry ());
      if (m_Enable_3N_face_time_select && !res.empty())
	{
	  NS_LOG_DEBUG ("Utilizing Time ordering");
	  bool attempt = false;
	  std::list<fib::FaceMetric>::iterator itlist;

	  if (!(propagatedCount > 0))
	    {
	      itlist = res.begin ();

	      while (itlist != res.end ())
		{
		  attempt = false;
		  if (itlist->GetStatus() == fib::FaceMetric::ICN_FIB_GREEN || itlist->GetStatus() == fib::FaceMetric::ICN_FIB_YELLOW)
		    attempt = true;

		  attempt &= (ret.find (itlist->GetFace()) == ret.end ());

		  if (attempt)
		    {
		      if (TrySendOutInterest (pdu, inFace, itlist->GetFace(), Address(), interest, pitEntry))
			{
			  ret.insert(itlist->GetFace());
			  propagatedCount++;
			}
		    }
		  ++itlist;
		}
	    }
	}
      else if (m_Enable_Flooding)
	{
	  NS_LOG_DEBUG ("Utilizing Flooding");
	  BOOST_FOREACH (const fib::FaceMetric &metricFace, pitEntry->GetFibEntry ()->m_faces.get<fib::i_fib_lastseen> ())
	  {
	    if (ret.find (metricFace.GetFace ()) != ret.end())
	      {
		NS_LOG_DEBUG ("Face: " << metricFace.GetFace ()->GetId () << " was found in the given set");
		continue;
	      }

	    ret.insert(metricFace.GetFace ());
	    // Now we actually attempt to forward the PDU
	    if (!TrySendOutInterest (pdu, inFace, metricFace.GetFace (), Address(), interest, pitEntry))
	      {
		continue;
	      }

	    propagatedCount++;
	  }

	  Ptr<Face> tmp;
	  NS_LOG_DEBUG ("Flooding Faces not directly connected to FIB");
	  for (uint32_t i = 0; i < m_faces->GetN (); i++)
	    {
	      tmp = m_faces->Get (i);
	      if (ret.find (tmp) != ret.end ())
		{
		  continue;
		}

	      ret.insert(tmp);
	      if (!TrySendOutInterest (pdu, inFace, tmp, Address (), interest, pitEntry))
		{
		  continue;
		}

	      propagatedCount++;
	    }
	}
      else
	{
	  NS_LOG_DEBUG ("Utilizing Smart Flooding");

	  std::set<Ptr<Face> > fib_faces;
	  std::set<Ptr<Face> > all_seen;

	  // Here we pick the next place to forward to using the ICN strategy
	  BOOST_FOREACH (const fib::FaceMetric &metricFace, pitEntry->GetFibEntry ()->m_faces.get<fib::i_fib_lastseen> ())
	  {
	    if (metricFace.GetStatus () == fib::FaceMetric::ICN_FIB_RED)
	      {
		NS_LOG_DEBUG ("Not using Red Face " << metricFace.GetFace ()->GetId ());
		ret.insert(metricFace.GetFace ());
		continue;
	      }

	    if (metricFace.GetStatus () == fib::FaceMetric::ICN_FIB_YELLOW)
	      {
		NS_LOG_DEBUG ("Not using Yellow Face " << metricFace.GetFace ()->GetId ());
		continue;
	      }

	    NS_LOG_DEBUG ("Trying Green Face: " << metricFace.GetFace ()->GetId ());
	    // Verify that the Face is not in the set we were given
	    if (ret.find (metricFace.GetFace ()) != ret.end())
	      {
		NS_LOG_DEBUG ("Face: " << metricFace.GetFace ()->GetId () << " was found in the given set");
		continue;
	      }

	    ret.insert(metricFace.GetFace ());
	    // Now we actually attempt to forward the PDU
	    if (!TrySendOutInterest (pdu, inFace, metricFace.GetFace (), Address(), interest, pitEntry))
	      {
		continue;
	      }

	    propagatedCount++;
	    return std::make_pair(ret, propagatedCount);
	  }

	  BOOST_FOREACH (const fib::FaceMetric &metricFace, pitEntry->GetFibEntry ()->m_faces.get<fib::i_fib_lastseen> ())
	  {
	    if (metricFace.GetStatus () == fib::FaceMetric::ICN_FIB_RED)
	      continue;

	    NS_LOG_DEBUG ("Trying Yellow Face: " << metricFace.GetFace ()->GetId ());
	    // Verify that the Face is not in the set we were given
	    if (ret.find (metricFace.GetFace ()) != ret.end())
	      {
		NS_LOG_DEBUG ("Face " << metricFace.GetFace ()->GetId () << " was found in the given set");
		continue;
	      }

	    ret.insert(metricFace.GetFace ());
	    if (!TrySendOutInterest (pdu, inFace, metricFace.GetFace (), Address(), interest, pitEntry))
	      {
		continue;
	      }

	    propagatedCount++;
	  }

	  std::set<Ptr<Face> > all = GetAllFaceSet();
	  std::set<Ptr<Face> > diff;
	  std::set_difference(all.begin (), all.end (), ret.begin (), ret.end (), std::inserter(diff, diff.begin ()));
	  int counter = 0;
	  if (!diff.empty())
	    {
	      std::set<Ptr<Face> >::iterator it;

	      for (it = diff.begin (); it != diff.end (); ++it)
		{
		  NS_LOG_DEBUG ("Trying FIB not connected Face: " << (*it)->GetId ());
		  ret.insert(*it);
		  if (!TrySendOutInterest (pdu, inFace, *it, Address(), interest, pitEntry))
		    {
		      continue;
		    }
		  counter++;
		}
	    }

	  if ((counter > 0) && !(propagatedCount > 0))
	    {
	      return std::make_pair(ret, 0);
	    }
	}

      return std::make_pair(ret, propagatedCount);
    }

    bool
    ICN3NSmartFlooding::DoPropagateInterest (Ptr<DATAPDU> pdu,
					     Ptr<Face> inFace,
					     Ptr<const Interest> interest,
					     Ptr<pit::Entry> pitEntry)
    {
      NS_LOG_DEBUG ("Processing Interest SeqNo " << std::dec << interest->GetName ().get (-1).toSeqNum () << " Face: " << inFace->GetId ());
      NS_ASSERT_MSG (m_pit != 0, "PIT should be aggregated with forwarding strategy");

      int propagatedCount = 0;

      // Pointers and flags for PDU types
      Ptr<NNNPDU> pdu_i;

      Ptr<DO> do_i;
      bool wasDO = false;
      Ptr<DU> du_i;
      bool wasDU = false;
//      bool useanycast = false;

      std::set<Ptr<Face> > ret;
      std::set<Ptr<Face> > tmp1;
      std::pair<std::set<Ptr<Face> >,int> tmp2;

      // Since we are using a combination of ICN/3N, we copy the pointer and
      // see if 3N gives us a different Face to use. This will be used
      // specifically when the PDU has a destination, in other words when using
      // DO and DU PDUs

      // ICN PDU to packet conversion
      Ptr<Packet> icn_pdu;

      // Check if we have been given a raw ICN PDU
      if (pdu != 0)
	{
	  uint32_t pduid = pdu->GetPacketId();
	  switch(pduid)
	  {
	    case DO_NNN:
	      // Convert pointer to DO PDU
	      do_i = DynamicCast<DO> (pdu);
	      wasDO = true;
	      break;
	    case DU_NNN:
	      // Convert pointer to DU PDU
	      du_i = DynamicCast<DU> (pdu);
	      wasDU = true;
	      break;
	    default:
	      break;
	  }

	  // Convert the Interest PDU into a NS-3 Packet
	  icn_pdu = ns3::icn::Wire::FromInterest (interest);
	}

      NS_LOG_DEBUG ("Current " << *m_fib);

      // The propagation rules are a little different if using a DO or DU PDU
      if (wasDO)
	{
	  tmp2 = DoSelectDOPropagations(do_i, inFace, interest, pitEntry);

	  tmp1 = tmp2.first;

	  propagatedCount += tmp2.second;
	}
      else if (wasDU)
	{
	  tmp2 = DoSelectDUPropagations(du_i, inFace, interest, pitEntry);

	  tmp1 = tmp2.first;

	  propagatedCount += tmp2.second;
	}

      if (propagatedCount == 0)
	{
	  // For everything else, propagate like always
	  tmp2 = PropagateInterestwithoutPoAs (pdu, inFace, std::set<Ptr<Face> > (),
					       interest, pitEntry);
	  propagatedCount += tmp2.second;
	}

      return propagatedCount > 0;
    }

    bool
    ICN3NSmartFlooding::DoPropagateLayeredInterest (Ptr<DATAPDU> pdu,
						    Ptr<Face> inFace,
						    Ptr<const Interest> interest,
						    Ptr<pit::Entry> pitEntry,
						    const Address& from)
    {
      NS_LOG_DEBUG ("Processing Interest SeqNo " << std::dec << interest->GetName ().get (-1).toSeqNum () << " Face: " << inFace->GetId ());
      NS_ASSERT_MSG (m_pit != 0, "PIT should be aggregated with forwarding strategy");

      int propagatedCount = 0;

      // Pointers and flags for PDU types
      Ptr<NNNPDU> pdu_i;

      Ptr<DO> do_i;
      bool wasDO = false;
      Ptr<DU> du_i;
      bool wasDU = false;
//      bool useanycast = false;

      std::set<Ptr<Face> > ret;
      std::set<Ptr<Face> > tmp1;
      std::pair<std::set<Ptr<Face> >,int> tmp2;

      // Since we are using a combination of ICN/3N, we copy the pointer and
      // see if 3N gives us a different Face to use. This will be used
      // specifically when the PDU has a destination, in other words when using
      // DO and DU PDUs

      // ICN PDU to packet conversion
      Ptr<Packet> icn_pdu;

      // Check if we have been given a raw ICN PDU
      if (pdu != 0)
	{
	  uint32_t pduid = pdu->GetPacketId();
	  switch(pduid)
	  {
	    case DO_NNN:
	      // Convert pointer to DO PDU
	      do_i = DynamicCast<DO> (pdu);
	      wasDO = true;
	      break;
	    case DU_NNN:
	      // Convert pointer to DU PDU
	      du_i = DynamicCast<DU> (pdu);
	      wasDU = true;
	      break;
	    default:
	      break;
	  }

	  // Convert the Interest PDU into a NS-3 Packet
	  icn_pdu = ns3::icn::Wire::FromInterest (interest);
	}

      // The propagation rules are a little different if using a DO or DU PDU
      if (wasDO)
	{
	  tmp2 = DoSelectDOPropagations(do_i, inFace, interest, pitEntry);

	  tmp1 = tmp2.first;

	  propagatedCount += tmp2.second;
	}
      else if (wasDU)
	{
	  tmp2 = DoSelectDUPropagations(du_i, inFace, interest, pitEntry);

	  tmp1 = tmp2.first;
	  propagatedCount += tmp2.second;
	}

      if (propagatedCount == 0)
	{
	  // For everything else, propagate like always
	  tmp2 = PropagateInterestwith3NLessDst (pdu, inFace, std::set<Ptr<Face> > (),
						 interest, pitEntry, from);
	  propagatedCount += tmp2.second;
	}

      return propagatedCount > 0;
    }

    bool
    ICN3NSmartFlooding::DoPropagateData(Ptr<DATAPDU> pdu, Ptr<Face> inFace, Ptr<const Data> data)
    {
      if (pdu == 0)
	{
	  NS_LOG_WARN ("Received a raw ICN PDU. Can do nothing with this");
	  return false;
	}
//      NS_LOG_DEBUG ("Processing DATA SeqNo " << std::dec << data->GetName ().get (-1).toSeqNum () << " Face: " << inFace->GetId ());
      // Convert the Interest PDU into a NS-3 Packet
      Ptr<Packet> icn_pdu = ns3::icn::Wire::FromData(data);

      // Pointers and flags for PDU types
      Ptr<DO> do_i;
      bool wasDO = false;
      Ptr<DU> du_i;
      bool wasDU = false;
      bool ok = false;

      uint32_t pduid = pdu->GetPacketId();
      switch(pduid)
      {
	case DO_NNN:
	  // Convert pointer to DO PDU
	  do_i = DynamicCast<DO> (pdu);
	  wasDO = true;
	  break;
	case DU_NNN:
	  // Convert pointer to DU PDU
	  du_i = DynamicCast<DU> (pdu);
	  wasDU = true;
	  break;
	default:
	  break;
      }

      Ptr<Face> foutFace;

      // Pointers to use when we have DO or DU PDUs
      std::pair<Ptr<Face>, Address> tmp;
      Address destAddr;
      NNNAddress newdst;
      Ptr<NNNAddress> newdstPtr;
      Ptr<const NNNAddress> constdstPtr;
      Ptr<const NNNAddress> tmpNewdst;
      Ptr<DO> do_o_spec;
      Ptr<DU> du_o_spec;
      bool nnptRedirect = false;

      NNNAddress myAddr = GetNode3NName ();

      if (wasDO || wasDU)
	{
	  // First obtain the name
	  if (wasDO)
	    {
	      newdst = do_i->GetName ();
	      constdstPtr = do_i->GetNamePtr();
	      tmpNewdst = do_i->GetNamePtr();
	      NS_LOG_INFO ("Propagating Data DO heading to " << newdst << " Flowid: " << do_i->GetFlowid () << " Sequence: " << do_i->GetSequence ());
	    }
	  else if (wasDU)
	    {
	      newdst = du_i->GetDstName ();
	      constdstPtr = du_i->GetDstNamePtr();
	      tmpNewdst = du_i->GetDstNamePtr();
	      NS_LOG_INFO ("Propagating Data DU from " << du_i->GetSrcName() << "  heading to " << newdst << " Flowid: " << du_i->GetFlowid () << " Sequence: " << du_i->GetSequence ());
	    }

	  // Check if we are already at the destination (search through all the node names acquired)
	  if (m_node_names->foundName(constdstPtr))
	    {
	      NS_LOG_INFO ("DATA SeqNo " << std::dec << data->GetName ().get (-1).toSeqNum () << " has reached desired destination, looking for Apps");
	      Ptr<Face> tmpFace;
	      // We have reached the destination, look for Apps
	      for (uint32_t i = 0; i < m_faces->GetN (); i++)
		{
		  tmpFace = m_faces->Get (i);
		  // Check that the Face is of type APPLICATION
		  if (tmpFace->isAppFace ())
		    {
		      if (wasDO)
			{
			  if (m_useQueues)
			    ok = EnqueueDO(tmpFace, Address (), do_i);
			  else
			    ok = tmpFace->SendDO (do_i);
			}
		      else if (wasDU)
			{
			  if (m_useQueues)
			    ok = EnqueueDU(tmpFace, Address (), du_i);
			  else
			    ok = tmpFace->SendDU (du_i);
			}

		      if (!ok)
			{
			  // Log Data drops
			  m_dropData (data, tmpFace);
			  if (wasDO)
			    m_dropDOs (do_i, tmpFace);
			  else if (wasDU)
			    m_dropDUs (du_i, tmpFace);

			  NS_LOG_DEBUG ("Cannot satisfy data to " << *tmpFace);
			}
		      else
			{
			  // Log that a Data PDU was sent
			  m_outData (data, false, tmpFace);

			  if (wasDO)
			    {
			      NS_LOG_INFO ("Satisfying with DO");
			      m_outDOs (do_i, tmpFace);
			    }
			  else if (wasDU)
			    {
			      NS_LOG_INFO ("Satisfying with DU");
			      m_outDUs (du_i, tmpFace);
			    }
			}
		    }
		}
	      return ok;
	    }

	  // We may have obtained a DEN so we need to check
	  if (m_node_pdu_buffer->DestinationExists (newdst) && !m_nnpt->foundOldName(constdstPtr))
	    {
	      if (!newdst.isDirectSubSector (myAddr))
		{
		  NS_LOG_INFO ("On " << myAddr << ", " << newdst << " is not a direct subsector, buffering and not forwarding PDU");
		  if (wasDO)
		    {
		      m_node_pdu_buffer->PushDO (newdst, do_i);
		    }
		  else if (wasDU)
		    {
		      m_node_pdu_buffer->PushDU (newdst, du_i);
		    }
		  if (m_3n_only_buffer)
		    return true;
		}
	    }

	  // Check if the NNPT has any information for this particular 3N name
	  if (m_in_transit_redirect && m_nnpt->foundOldName(constdstPtr))
	    {
	      // Retrieve the new 3N name destination and update variable
	      tmpNewdst = m_nnpt->findPairedNamePtr (constdstPtr);
	      newdst = tmpNewdst->getName ();
	      // Flag that the NNPT made a change
	      nnptRedirect = true;
	    }

	  if (nnptRedirect)
	    {
	      NS_LOG_INFO ("On " << myAddr << " we are redirecting from " << *constdstPtr << " to " << newdst);
	      NS_LOG_DEBUG ("On " << myAddr << " " << *m_nnpt);
	      if (wasDO)
		{
		  // Create a new DO PDU to send the data
		  do_o_spec = Create<DO> ();
		  // Set the new 3N name
		  do_o_spec->SetName (tmpNewdst);
		  // Set the lifetime of the 3N PDU
		  do_o_spec->SetLifetime (m_3n_lifetime);
		  // Configure payload for PDU
		  do_o_spec->SetPayload (icn_pdu);
		  // Signal that the PDU had an ICN PDU as payload
		  do_o_spec->SetPDUPayloadType (ICN_NNN);
		}
	      else if (wasDU)
		{
		  // Create a new DU PDU to send the data
		  du_o_spec = Create<DU> ();
		  // Use the original DU's Src 3N name
		  du_o_spec->SetSrcName (du_i->GetSrcNamePtr ());
		  // Set the new 3N name destination
		  du_o_spec->SetDstName (tmpNewdst);
		  // Set the lifetime of the 3N PDU
		  du_o_spec->SetLifetime (m_3n_lifetime);
		  // Use the original DU's Src 3N name
		  du_o_spec->SetAuthoritative (du_i->IsAuthoritative ());
		  // Configure payload for PDU
		  du_o_spec->SetPayload (icn_pdu);
		  // Signal that the PDU had an ICN PDU as payload
		  du_o_spec->SetPDUPayloadType (ICN_NNN);
		}
	    }

	  // Need routing information, attempt handshake
	  if (m_useHR)
	    AskHandshake (tmpNewdst);

	  NS_LOG_DEBUG ("On " << myAddr << " " << *m_nnst);

	  // We have an dis-enroll procedure
	  if (m_buffer_during_disenroll && (m_hasDisenrolled || m_nnst->isEmpty()))
	    {
	      NS_LOG_INFO ("Node is in dis-enroll or NNST might be blank, queuing PDU for later");

	      if (wasDO)
		{
		  if (nnptRedirect)
		    {
		      m_node_pdu_queue->pushDO(do_o_spec, Seconds(0));
		    }
		  else
		    {
		      m_node_pdu_queue->pushDO(do_i, Seconds (0));
		    }
		}
	      else if (wasDU)
		{

		  if (nnptRedirect)
		    {
		      m_node_pdu_queue->pushDU(du_o_spec, Seconds(0));
		    }
		  else
		    {
		      m_node_pdu_queue->pushDU(du_i, Seconds (0));
		    }
		}
	      else
		{
		  NS_LOG_WARN ("Attempted check of PDU type. None found, check if this is desired effect");
		}

	      return true;
	    }

	  // Roughly find the next hop
	  tmp = m_nnst->ClosestSectorFaceInfo (newdst, 0);

	  // Update the variables for Face and PoA name
	  foutFace = tmp.first;
	  destAddr = tmp.second;

	  if (foutFace == 0)
	    {
	      NS_LOG_WARN ("For some reason the NNST has returned an empty Face, skipping");
	    }
	  else
	    {
	      if (nnptRedirect)
		{
		  if (wasDO)
		    {
		      if (m_ask_ACKP)
			{
			  int8_t n3ok = txPreparation (do_o_spec, foutFace, destAddr, false);
			  ok = (n3ok >= 0);
			}
		      else
			{
			  txReset(do_o_spec);
			  if (m_useQueues)
			    ok = EnqueueDO(foutFace, destAddr, do_o_spec);
			  else
			    ok = foutFace->SendDO (do_o_spec, destAddr);
			  UpdatePDUCountersB (do_o_spec, foutFace, ok, false);
			}
		    }
		  else if (wasDU)
		    {
		      if (m_ask_ACKP)
			{
			  int8_t n3ok = txPreparation (du_o_spec, foutFace, destAddr, false);
			  ok = (n3ok >= 0);
			}
		      else
			{
			  txReset(du_o_spec);
			  if (m_useQueues)
			    ok = EnqueueDU(foutFace, destAddr, du_o_spec);
			  else
			    ok = foutFace->SendDU (du_o_spec, destAddr);
			  UpdatePDUCountersB (du_o_spec, foutFace, ok, false);
			}
		    }

		  if (!ok)
		    {
		      // Log Data drops
		      m_dropData (data, foutFace);
		    }
		  else
		    {
		      // Log that a Data PDU was sent
		      m_outData (data, false, foutFace);
		    }
		}
	      else
		{
		  if (wasDO)
		    {
		      if (m_ask_ACKP)
			{
			  int8_t n3ok = txPreparation (do_i, foutFace, destAddr, false);
			  ok = (n3ok >= 0);
			}
		      else
			{
			  txReset(do_i);
			  if (m_useQueues)
			    ok = EnqueueDO(foutFace, destAddr, do_i);
			  else
			    ok = foutFace->SendDO (do_i, destAddr);
			  UpdatePDUCountersB (do_i, foutFace, ok, false);
			}
		    }
		  else if (wasDU)
		    {
		      if (m_ask_ACKP)
			{
			  int8_t n3ok = txPreparation (du_i, foutFace, destAddr, false);
			  ok = (n3ok >= 0);
			}
		      else
			{
			  txReset(du_i);
			  if (m_useQueues)
			    ok = EnqueueDU(foutFace, destAddr, du_i);
			  else
			    ok = foutFace->SendDU (du_i, destAddr);
			  UpdatePDUCountersB (du_i, foutFace, ok, false);
			}
		    }

		  if (!ok)
		    {
		      // Log Data drops
		      m_dropData (data, foutFace);
		    }
		  else
		    {
		      // Log that a Data PDU was sent
		      m_outData (data, false, foutFace);
		    }
		}
	    }
	}
      else
	{
	  NS_LOG_INFO ("Received either NULL or SO. Without a PIT entry we can do nothing with this information");
	}

      return ok;
    }

  } /* namespace nnn */
} /* namespace ns3 */

