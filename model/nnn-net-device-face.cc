/* -*- Mode:C++; c-file-style:"gnu" -*- */
/*
 * Copyright (c) 2014 Waseda University, Sato Laboratory
 *
 *   This file is part of nnnsim.
 *
 *  nnn-net-device-face.cc is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  nnn-net-device-face.cc is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with nnn-net-device-face.cc. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Author: Zhu Li <phillipszhuli1990@gmail.com>
 *          Jairo Eduardo Lopez <jairo@ruri.waseda.jp>
 */

#include "ns3/channel.h"
#include "ns3/net-device.h"
#include "ns3/log.h"
#include "ns3/packet.h"
#include "ns3/node.h"
#include "ns3/pointer.h"
#include "ns3/point-to-point-net-device.h"

#include "nnn-net-device-face.h"

#include "ns3/nnn-address.h"
#include "ns3/nnn-l3-protocol.h"
#include "ns3/nnn-forwarding-strategy.h"

namespace ns3
{
  NS_LOG_COMPONENT_DEFINE ("nnn.NetDeviceFace");

  namespace nnn
  {
    NS_OBJECT_ENSURE_REGISTERED (NetDeviceFace);

    TypeId
    NetDeviceFace::GetTypeId ()
    {
      static TypeId tid = TypeId ("ns3::nnn::NetDeviceFace")
    		    .SetParent<Face> ()
    		    .SetGroupName ("Nnn")
    		    ;
      return tid;
    }

    /**
     * By default, Face are created in the "down" state.  Before
     * becoming useable, the user must invoke SetUp on the face
     */
    NetDeviceFace::NetDeviceFace (Ptr<Node> node, const Ptr<NetDevice> &netDevice)
    : Face (node)
    , m_netDevice (netDevice)
    {
      NS_LOG_FUNCTION (this << netDevice);

      SetPoASharing (false);
      SetMetric (5); // default metric
      SetAddress (m_netDevice->GetAddress());
      InsertAddress (m_netDevice->GetBroadcast ());

      NS_ASSERT_MSG (m_netDevice != 0, "NetDeviceFace needs to be assigned a valid NetDevice");
    }

    NetDeviceFace::NetDeviceFace (Ptr<Node> node, const Ptr<NetDevice> &netDevice, bool sharing)
    : Face (node)
    , m_netDevice (netDevice)
    {
      NS_LOG_FUNCTION (this << netDevice);
      SetPoASharing (sharing);
      SetMetric (5); // default metric
      SetAddress (m_netDevice->GetAddress());
      InsertAddress (m_netDevice->GetBroadcast ());

      NS_ASSERT_MSG (m_netDevice != 0, "NetDeviceFace needs to be assigned a valid NetDevice");
    }

    NetDeviceFace::~NetDeviceFace ()
    {
      NS_LOG_FUNCTION_NOARGS ();
    }

    NetDeviceFace& NetDeviceFace::operator= (const NetDeviceFace &)
    {
      return *this;
    }

    Ptr<NetDevice>
    NetDeviceFace::GetNetDevice () const
    {
      return m_netDevice;
    }

    void
    NetDeviceFace::RegisterProtocolHandlers (Ptr<ForwardingStrategy> fw)
    {
      NS_LOG_FUNCTION (this);

      Face::RegisterProtocolHandlers (fw);

      if (fw->SupportsDirect3N ())
	{
	  m_node->RegisterProtocolHandler (MakeCallback (&NetDeviceFace::ReceiveFromNetDevice, this),
					   L3Protocol::NNN_ETHERNET_FRAME_TYPE, m_netDevice, false/*promiscuous mode*/);
	}

      if (fw->SupportsDirectICN ())
	{
	  m_node->RegisterProtocolHandler (MakeCallback (&NetDeviceFace::ReceiveFromNetDevice, this),
					   L3Protocol::ICN_ETHERNET_FRAME_TYPE, m_netDevice, true/*promiscuous mode*/);
	}
    }

    void
    NetDeviceFace:: UnRegisterProtocolHandlers ()
    {
      m_node->UnregisterProtocolHandler (MakeCallback (&NetDeviceFace::ReceiveFromNetDevice, this));
      Face::UnRegisterProtocolHandlers ();
    }

    bool
    NetDeviceFace::Send3N (Ptr<Packet> packet)
    {
      if (!Face::Send3N (packet))
	{
	  return false;
	}

      NS_LOG_FUNCTION (this << packet);

      NS_ASSERT_MSG (packet->GetSize () <= m_netDevice->GetMtu (),
                     "Packet size " << packet->GetSize () << " exceeds device MTU "
                     << m_netDevice->GetMtu ()
                     << " for 3N; fragmentation not supported");

      bool ok = m_netDevice->Send (packet, m_netDevice->GetBroadcast (),
                                   L3Protocol::NNN_ETHERNET_FRAME_TYPE);
      return ok;
    }

    bool
    NetDeviceFace::Send3N (Ptr<Packet> packet, Address addr)
    {
      if (!Face::Send3N (packet, addr))
	{
	  return false;
	}

      NS_LOG_FUNCTION (this << packet << " going from " << m_netDevice->GetAddress() << " to " << addr);

      NS_ASSERT_MSG (packet->GetSize () <= m_netDevice->GetMtu (),
                     "Packet size " << packet->GetSize () << " exceeds device MTU "
                     << m_netDevice->GetMtu ()
                     << " for 3N; fragmentation not supported");

      bool ok = m_netDevice->Send (packet, addr, L3Protocol::NNN_ETHERNET_FRAME_TYPE);
      return ok;
    }

    bool
    NetDeviceFace::SendICN (Ptr<Packet> packet)
    {
      if (!Face::SendICN (packet))
	{
	  return false;
	}

      NS_LOG_FUNCTION (this << packet);

      NS_ASSERT_MSG (packet->GetSize () <= m_netDevice->GetMtu (),
		     "Packet size " << packet->GetSize () << " exceeds device MTU "
		     << m_netDevice->GetMtu ()
		     << " for 3N; fragmentation not supported");

      bool ok = m_netDevice->Send (packet, m_netDevice->GetBroadcast (),
				   L3Protocol::ICN_ETHERNET_FRAME_TYPE);
      return ok;
    }

    bool
    NetDeviceFace::SendICN (Ptr<Packet> packet, Address addr)
    {
      if (!Face::SendICN (packet, addr))
	{
	  return false;
	}

      NS_LOG_FUNCTION (this << packet << " going from " << m_netDevice->GetAddress() << " to " << addr);

      NS_ASSERT_MSG (packet->GetSize () <= m_netDevice->GetMtu (),
		     "Packet size " << packet->GetSize () << " exceeds device MTU "
		     << m_netDevice->GetMtu ()
		     << " for 3N; fragmentation not supported");

      bool ok = m_netDevice->Send (packet, addr, L3Protocol::ICN_ETHERNET_FRAME_TYPE);
      return ok;
    }

    Address
    NetDeviceFace::GetBroadcastAddress () const
    {
      return m_netDevice->GetBroadcast ();
    }

    // callback
    void
    NetDeviceFace::ReceiveFromNetDevice (Ptr<NetDevice> device,
                                         Ptr<const Packet> p,
                                         uint16_t protocol,
                                         const Address &from,
                                         const Address &to,
					 NetDevice::PacketType packetType)
    {
      NS_LOG_FUNCTION (device << p << protocol << from << to << packetType);

      if (to != device->GetAddress() && to != device->GetBroadcast())
	NS_LOG_ERROR ("Device has PoA: " << device->GetAddress() << " receiving to " << to << " from: " << from);

      if (protocol == L3Protocol::ICN_ETHERNET_FRAME_TYPE)
	{
	  if (GetPoASharing())
	    {
	      ReceiveICN (p, from, to);
	    }
	  else
	    {
	      ReceiveICN (p);
	    }
	}
      else if (protocol == L3Protocol::NNN_ETHERNET_FRAME_TYPE)
	{
	  if (GetPoASharing())
	    {
	      Receive3N (p, from, to);
	    }
	  else
	    {
	      Receive3N (p);
	    }
	}
      else
	{
	  NS_LOG_WARN ("Received a frame type we cannot handle");
	}
    }

    std::ostream&
    NetDeviceFace::Print (std::ostream& os) const
    {
#ifdef NS3_LOG_ENABLE
      os << "dev[" << GetNode ()->GetId () << "]=net(" << GetId ();

      if (DynamicCast<PointToPointNetDevice> (m_netDevice))
	{
	  // extra debugging information which available ONLY for PointToPointNetDevice's
	  os << ",";
	  os << DynamicCast<PointToPointNetDevice> (m_netDevice)->GetChannel ()->GetDevice (0)->GetNode ()->GetId ();
	  os << "-";
	  os << DynamicCast<PointToPointNetDevice> (m_netDevice)->GetChannel ()->GetDevice (1)->GetNode ()->GetId ();
	}
      os << ")";
#else
      os << "dev=net(" << GetId () << ")";
#endif
      return os;
    }

  } // namespace nnn
} // namespace ns3
