/* -*- Mode: C++; c-file-style: "gnu" -*- */
/*
 * Copyright (c) 2015 Jairo Eduardo Lopez
 *
 *   This file is part of nnnsim.
 *
 *  nnn-aden.h is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  nnn-aden.h is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with nnn-aden.h. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Author: Jairo Eduardo Lopez <jairo@ruri.waseda.jp>
 */

#ifndef _NNN_ADEN_NNN_ADEN_H_
#define _NNN_ADEN_NNN_ADEN_H_

#include "ns3/nnn-pdu.h"
#include "ns3/nnn-en-pdus.h"
#include "ns3/nnn-naming.h"

namespace ns3
{
  namespace nnn
  {

    class ADEN : public ENPDU
    {
    public:
      ADEN ();

      /**
       * \brief Constructor
       *
       *
       * @param name 3N Address Ptr
       **/
      ADEN(Ptr<NNNAddress> name, Time lease_expire);

      /**
       * \brief Constructor
       *
       * @param name 3N Address
       * @param payload Packet Ptr
       **/
      ADEN(const NNNAddress &name, Time lease_expire);

      virtual
      ~ADEN ();

      /**
       * @brief Copy constructor
       */
      ADEN (const ADEN &aden_p);

      /**
       * \brief Get 3N name
       *
       * Gets 3N name.
       **/
      const NNNAddress&
      GetName () const;

      /**
       * @brief Get smart pointer to the 3N name (to avoid extra memory usage)
       */
      Ptr<const NNNAddress>
      GetNamePtr () const;

      /**
       * \brief Set 3N name
       *
       * @param name smart pointer to 3N name
       *
       **/
      void
      SetName (Ptr<NNNAddress> name);

      /**
       * \brief Another variant to set 3N name
       *
       * @param name const reference to 3N name object
       *
       **/
      void
      SetName (const NNNAddress &name);

      /**
       * \brief Get lease time
       *
       * Gets lease time used in the PDU
       **/
      Time
      GetExpireTime() const;

      /**
       * \brief Set lease time
       *
       * @param lease lease time for NNNAddress
       *
       **/
      void
      SetExpireTime (Time lease);

      /**
       * @brief Print ADEN in plain-text to the specified output stream
       */
      void
      Print (std::ostream &os) const;

    private:
      // NO_ASSIGN
      ADEN &
      operator = (const ADEN &other) { return *this; }

    protected:
      Ptr<NNNAddress> m_name;   ///< @brief 3N Address used in the packet
      Time m_lease_expire;      ///< @brief Lease expiration in absolute time
    };

    inline std::ostream &
    operator << (std::ostream &os, const ADEN &i)
    {
      i.Print (os);
      return os;
    }

    /**
     * @brief Class for parsing exception
     */
    class ADENException {};
  } /* namespace nnn */
} /* namespace ns3 */

#endif /* NNN_ADEN_NNN_ADEN_H_ */
