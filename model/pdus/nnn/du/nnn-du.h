/* -*- Mode: C++; c-file-style: "gnu" -*- */
/*
 * Copyright (c) 2015 Waseda University, Sato Laboratory
 *
 *   This file is part of nnnsim.
 *
 *  nnn-du.h is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  nnn-du.h is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with nnn-du.h. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Author: Jairo Eduardo Lopez <jairo@ruri.waseda.jp>
 */

#ifndef _NNN_DU_HEADER_H_
#define _NNN_DU_HEADER_H_

#include "ns3/nnn-pdu.h"
#include "ns3/nnn-data-pdus.h"
#include "ns3/nnn-naming.h"

namespace ns3
{
  namespace nnn
  {
    class DU : public DATAPDU
    {
    public:
      DU ();

      DU(Ptr<const NNNAddress> src, Ptr<const NNNAddress> dst, Ptr<Packet> payload);

      DU(Ptr<const NNNAddress> src, Ptr<const NNNAddress> dst, Ptr<Packet> payload, bool authoritative);

      /**
       * @brief Copy constructor
       */
      DU (const DU &du_p);

      virtual
      ~DU ();

      const NNNAddress&
      GetSrcName () const;

      const NNNAddress&
      GetDstName () const;

      Ptr<const NNNAddress>
      GetSrcNamePtr () const;

      Ptr<const NNNAddress>
      GetDstNamePtr () const;

      bool
      IsAuthoritative () const;

      void
      SetAuthoritative (bool value);

      /**
       * @brief Print DU in plain-text to the specified output stream
       */
      void
      Print (std::ostream &os) const;

      bool
      operator< (const Ptr<DU> &du_p) const;

      Ptr<DATAPDU>
      Copy () const;

    private:
      bool m_authoritative;
    };

    inline std::ostream &
    operator << (std::ostream &os, const DU &i)
    {
      i.Print (os);
      return os;
    }

    /**
     * @brief Class for DU parsing exception
     */
    class DUException {};

  } /* namespace nnn */
} /* namespace ns3 */

#endif /* _NNN_DU_HEADER_H_ */
