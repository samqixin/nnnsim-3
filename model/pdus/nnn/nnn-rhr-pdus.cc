/* -*- Mode: C++; c-file-style: "gnu" -*- */
/*
 * Copyright (c) 2016 Jairo Eduardo Lopez
 *
 *   This file is part of nnnsim.
 *
 *  nnn-rhr-pdus.cc is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  nnn-rhr-pdus.cc is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with nnn-rhr-pdus.cc. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Author: Jairo Eduardo Lopez <jairo@ruri.waseda.jp>
 */

#include "nnn-rhr-pdus.h"

namespace ns3
{
  namespace nnn
  {

    RHRPDU::RHRPDU ()
    : NNNPDU     ()
    , m_hasSource (false)
    , m_src_name (0)
    , m_src_poa  (Address ())
    , m_isSrcFixed  (false)
    , m_src_lease    (Seconds (0))
    , m_hasDestination (false)
    , m_dst_name (0)
    , m_query_name (Create<NNNAddress> ())
    {
    }

    RHRPDU::~RHRPDU ()
    {
      m_dst_name = 0;
      m_src_name = 0;
      m_query_name = 0;
    }

    bool
    RHRPDU::IsSrcFixed () const
    {
      return m_isSrcFixed;
    }

    void
    RHRPDU::SetSrcFixed (bool fixed)
    {
      m_isSrcFixed = fixed;
    }

    bool
    RHRPDU::HasSource () const
    {
      return m_hasSource;
    }

    void
    RHRPDU::SourceSet (bool enable)
    {
      m_hasSource = enable;
    }

    bool
    RHRPDU::HasDestination () const
    {
      return m_hasDestination;
    }

    void
    RHRPDU::DestinationSet (bool enable)
    {
      m_hasDestination = enable;
    }

    const NNNAddress&
    RHRPDU::GetSrcName () const
    {
      if (m_src_name == 0) throw RHRException ();
      return *m_src_name;
    }

    Ptr<const NNNAddress>
    RHRPDU::GetSrcNamePtr () const
    {
      return m_src_name;
    }

    void
    RHRPDU::SetSrcName (Ptr<const NNNAddress> name)
    {
      m_src_name = name;
      SourceSet (true);
    }

    void
    RHRPDU::SetSrcName (const NNNAddress &name)
    {
      m_src_name = Create<NNNAddress> (name);
      SourceSet (true);
    }

    const NNNAddress&
    RHRPDU::GetDstName () const
    {
      if (m_dst_name == 0) throw RHRException ();
      return *m_dst_name;
    }

    Ptr<const NNNAddress>
    RHRPDU::GetDstNamePtr () const
    {
      return m_dst_name;
    }

    void
    RHRPDU::SetDstName (Ptr<const NNNAddress> name)
    {
      m_dst_name = name;
      DestinationSet (true);
    }

    void
    RHRPDU::SetDstName (const NNNAddress &name)
    {
      m_dst_name = Create<NNNAddress> (name);
      DestinationSet (true);
    }

    /////////////////////////////////////////////

    const NNNAddress&
    RHRPDU::GetQueryName () const
    {
      if (m_query_name == 0) throw RHRException ();
      return *m_query_name;
    }

    Ptr<const NNNAddress>
    RHRPDU::GetQueryNamePtr () const
    {
      return m_query_name;
    }

    void
    RHRPDU::SetQueryName (Ptr<const NNNAddress> name)
    {
      m_query_name = name;
    }

    void
    RHRPDU::SetQueryName (const NNNAddress &name)
    {
      m_query_name = Create<NNNAddress> (name);
    }

    Address
    RHRPDU::GetPoa () const
    {
      return m_src_poa;
    }

    void
    RHRPDU::SetPoa (Address poa)
    {
      m_src_poa = poa;
    }

    Time
    RHRPDU::GetSrcLease() const
    {
      return m_src_lease;
    }

    void
    RHRPDU::SetSrcLease (Time lease)
    {
      m_src_lease = lease;
    }

    void
    RHRPDU::Print (std::ostream &os) const
    {
      if (HasSource ())
	{
	  os << "  <IsFixed>" << IsSrcFixed () << "</IsFixed>" << std::endl;
	  os << "  <SrcName>" << GetSrcName () << "</SrcName>" << std::endl;
	  os << "  <Lease>" << GetSrcLease () << "</Lease>" << std::endl;
	}
      os << "  <PoA>" << GetPoa () << "</PoA>" << std::endl;
      if (HasDestination ())
	os << "  <DstName>" << GetDstName () << "</DstName>" << std::endl;
      os << "  <QueryName>" << GetSrcName () << "</QueryName>" << std::endl;
    }

  } /* namespace nnn */
} /* namespace ns3 */
