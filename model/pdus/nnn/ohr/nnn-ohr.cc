/* -*- Mode: C++; c-file-style: "gnu" -*- */
/*
 * Copyright (c) 2016 Jairo Eduardo Lopez
 *
 *   This file is part of nnnsim.
 *
 *  nnn-orhr.cc is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  nnn-orhr.cc is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with nnn-orhr.cc. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Author: Jairo Eduardo Lopez <jairo@ruri.waseda.jp>
 */

#include "ns3/log.h"
#include "nnn-ohr.h"


namespace ns3
{
  NS_LOG_COMPONENT_DEFINE ("nnn.ORHR");
  namespace nnn
  {

    OHR::OHR ()
    : NNNPDU (OHR_NNN, Seconds(0))
    , RHRPDU ()
    , m_dst_distance (0)
    , m_definite_query_info (false)
    , m_isQueryFixed (false)
    , m_query_lease (Seconds (0))
    , m_query_distance (0)
    {
    }

    OHR::OHR (const OHR &ohr_p)
    {
      NS_LOG_FUNCTION("RHR correct copy constructor");
      OHR ();
      // Common RHRPDU
      SetVersion (ohr_p.GetVersion ());
      SetSequence (ohr_p.GetSequence ());
      SetLifetime (ohr_p.GetLifetime ());
      SetSrcLease (ohr_p.GetSrcLease ());
      SetPoa (ohr_p.GetPoa ());
      SourceSet (ohr_p.HasSource ());
      SetSrcName (ohr_p.GetSrcName ());
      SetSrcFixed (ohr_p.IsSrcFixed ());
      DestinationSet (ohr_p.HasDestination ());
      SetDstName (ohr_p.GetDstName ());
      SetQueryName (ohr_p.GetQueryNamePtr ());
      /////////
      SetQueryFixed (ohr_p.IsQueryFixed ());
      SetQueryDistance (ohr_p.GetQueryDistance ());
      SetDefiniteQueryInfo (ohr_p.HasDefiniteQueryInfo ());
      SetDestDistance (ohr_p.GetDestDistance ());
      SetWire (ohr_p.GetWire ());
    }

    OHR::~OHR ()
    {
    }

    bool
    OHR::HasDefiniteQueryInfo () const
    {
      return m_definite_query_info;
    }

    void
    OHR::SetDefiniteQueryInfo (bool enable)
    {
      m_definite_query_info = enable;
    }

    uint32_t
    OHR::GetDestDistance () const
    {
      return m_dst_distance;
    }

    void
    OHR::SetDestDistance (uint32_t dist)
    {
      m_dst_distance = dist;
    }

    bool
    OHR::IsQueryFixed () const
    {
      return m_isQueryFixed;
    }

    void
    OHR::SetQueryFixed (bool enable)
    {
      m_isQueryFixed = enable;
    }

    Time
    OHR::GetQueryLease () const
    {
      return m_query_lease;
    }

    void
    OHR::SetQueryLease (Time lease)
    {
      m_query_lease = lease;
    }

    uint32_t
    OHR::GetQueryDistance () const
    {
      return m_query_distance;
    }

    void
    OHR::SetQueryDistance (uint32_t dist)
    {
      m_query_distance = dist;
    }

    void
    OHR::Print (std::ostream &os) const
    {
      os << "<ORHR>" << std::endl;
      RHRPDU::Print(os);
      os << "  <SrcDistance>" << GetDestDistance () << "</SrcDistance>" << std::endl;
      if (HasDefiniteQueryInfo ())
	{
	  os << "  <QueryFixed>" << IsQueryFixed () << "</QueryFixed>" << std::endl;
	  os << "  <QueryLease>" << GetQueryLease () << "</QueryLease>" << std::endl;
	  os << "  <QueryDistance>" << GetQueryDistance () << "</QueryDistance>" << std::endl;
	}
      os << "</ORHR>" << std::endl;
    }
  } /* namespace nnn */
} /* namespace ns3 */
