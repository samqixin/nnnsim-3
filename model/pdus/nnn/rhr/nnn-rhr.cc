/* -*- Mode: C++; c-file-style: "gnu" -*- */
/*
 * Copyright (c) 2016 Jairo Eduardo Lopez
 *
 *   This file is part of nnnsim.
 *
 *  nnn-rhr.cc is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  nnn-rhr.cc is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with nnn-rhr.cc. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Author: Jairo Eduardo Lopez <jairo@ruri.waseda.jp>
 */
#include "ns3/log.h"
#include "nnn-rhr.h"

namespace ns3
{
  NS_LOG_COMPONENT_DEFINE ("nnn.RHR");
  namespace nnn
  {

    RHR::RHR ()
    : NNNPDU (RHR_NNN, Seconds(0))
    , RHRPDU ()
    {
    }

    RHR::RHR (const RHR &rhr_p)
    {
      NS_LOG_FUNCTION("RHR correct copy constructor");
      RHR ();
      SetVersion (rhr_p.GetVersion ());
      SetLifetime (rhr_p.GetLifetime ());
      SetSequence (rhr_p.GetSequence ());
      SetSrcLease (rhr_p.GetSrcLease ());
      SetPoa (rhr_p.GetPoa ());
      SourceSet (rhr_p.HasSource ());
      SetSrcName (rhr_p.GetSrcName ());
      SetSrcFixed (rhr_p.IsSrcFixed ());
      DestinationSet (rhr_p.HasDestination ());
      SetDstName (rhr_p.GetDstName ());
      SetQueryName (rhr_p.GetQueryNamePtr ());
      SetWire (rhr_p.GetWire ());
    }

    RHR::~RHR ()
    {
    }

    void
    RHR::Print (std::ostream &os) const
    {
      os << "<RHR>" << std::endl;
      RHRPDU::Print(os);
      os << "</RHR>" << std::endl;
    }
  } /* namespace nnn */
} /* namespace ns3 */
