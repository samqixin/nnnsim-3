/* -*- Mode: C++; c-file-style: "gnu" -*- */
/*
 * Copyright (c) 2017 Jairo Eduardo Lopez
 *
 *   This file is part of nnnsim.
 *
 *  nnn-rxbuffer.h is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  nnn-rxbuffer.h is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with nnn-rxbuffer.h. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Author: Jairo Eduardo Lopez <jairo@ruri.waseda.jp>
 */

#ifndef RXBUFFER_NNN_RXBUFFER_H_
#define RXBUFFER_NNN_RXBUFFER_H_

#include "nnn-pdu-rxbuffer.h"

#include "ns3/log.h"
#include "ns3/ptr.h"
#include "ns3/nnn-naming.h"
#include "ns3/nnn-pdus.h"

#include <boost/format.hpp>
#include <boost/tuple/tuple.hpp>

namespace ns3
{
  namespace nnn
  {
    const boost::format rxbuffer_formatter("%-3s|%-14s|%-14s|%-17s|%-17s|%-10s\n");

    class RxBuffer : public Object
    {
    public:

      static TypeId
      GetTypeId ();

      RxBuffer ();

      virtual
      ~RxBuffer ();

      void
      InsertPDU (Ptr<DATAPDU> n_p, Time process);

      void
      RemovePDU (uint32_t pdu_id, uint32_t flowid, uint32_t sequence);

      uint32_t
      RemoveFlowid (uint32_t flowid);

      void
      RemovePDUByFlowidSequence (uint32_t flowid, uint32_t sequence);

      void
      RemovePDUExactFlowidSequence (uint32_t flowid, uint32_t sequence);

      void
      AcknowledgePDU (Ptr<ACKP> n_p);

      bool
      IsAcknowledged (uint32_t pdu_id, uint32_t flowid, uint32_t sequence);

      uint32_t
      Size (uint32_t flowid);

      bool
      IsEmpty ();

      Ptr<DATAPDU>
      RetrieveDATAPDU (uint32_t pdu_id, uint32_t flowid, uint32_t sequence);

      std::list<Ptr<DATAPDU> >
      RetrieveDATAPDUByFlowidLessThanSeq (uint32_t flowid, uint32_t sequence);

      std::list<Ptr<DATAPDU> >
      RetrieveDATAPDUByFlowidGreaterThanSeq (uint32_t flowid, uint32_t sequence, uint32_t window);

      std::list<Ptr<DATAPDU> >
      RetrieveDATAPDUByFlowid (uint32_t flowid);

      std::list<Ptr<DATAPDU> >
      FlushDATAPDUByFlowid (uint32_t flowid);

      void
      Print (std::ostream &os) const;

    private:
      datapdu_buffer internal_pdu;
    };

    inline std::ostream &
    operator << (std::ostream &os, const RxBuffer &rxbuffer)
    {
      os << "RxBuffer:" << std::endl;
      os << ";;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;" << std::endl;
      os << boost::format(rxbuffer_formatter) % "ID" % "Flowid" % "Sequence" % "SRC" % "DST" % "Time";
      os << ";;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;" << std::endl;
      rxbuffer.Print (os);
      os << ";;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;" << std::endl;
      return os;
    }


  } /* namespace nnn */
} /* namespace ns3 */

#endif /* RXBUFFER_NNN_RXBUFFER_H_ */
