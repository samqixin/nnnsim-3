/* -*- Mode:C++; c-file-style:"gnu" -*- */
/*
 * Copyright (c) 2017 Jairo Eduardo Lopez
 *
 *   This file is part of nnnsim.
 *
 *  nnn-tfib-impl.cc is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  nnn-tfib-impl.cc is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with nnn-tfib-impl.cc. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Author: Jairo Eduardo Lopez <jairo@ruri.waseda.jp>
 */

#include "ns3/nnn-face.h"
#include "ns3/nnn-forwarding-strategy.h"

#include "ns3/assert.h"
#include "ns3/log.h"
#include "ns3/names.h"
#include "ns3/node.h"

#include "ns3/nnn-icn-pdus.h"

#include <boost/ref.hpp>
#include <boost/lambda/lambda.hpp>
#include <boost/lambda/bind.hpp>

#include "nnn-tfib-impl.h"
namespace ll = boost::lambda;

namespace ns3
{
  NS_LOG_COMPONENT_DEFINE ("nnn.tfib.TFibImpl");

  namespace nnn
  {
    namespace tfib
    {
      NS_OBJECT_ENSURE_REGISTERED (TFibImpl);

      TypeId
      TFibImpl::GetTypeId (void)
      {
	static TypeId tid = TypeId ("ns3::nnn::tfib::Default") // cheating ns3 object system
	.SetParent<TFib> ()
	.SetGroupName ("Nnn")
	.AddConstructor<TFibImpl> ()
	;
	return tid;
      }

      TFibImpl::TFibImpl ()
      {
      }

      void
      TFibImpl::NotifyNewAggregate ()
      {
	Object::NotifyNewAggregate ();
      }

      void
      TFibImpl::DoDispose (void)
      {
	clear ();
	Object::DoDispose ();
      }


      Ptr<Entry>
      TFibImpl::LongestPrefixMatch (const Interest &interest)
      {
	super::iterator item = super::longest_prefix_match (interest.GetName() );
	// @todo use predicate to search with exclude filters

	if (item == super::end ())
	  return 0;
	else
	  return item->payload ();
      }

      Ptr<Entry>
      TFibImpl::Find (const icn::Name &prefix)
      {
	super::iterator item = super::find_exact (prefix);

	if (item == super::end ())
	  return 0;
	else
	  return item->payload ();
      }

      Ptr<Entry>
      TFibImpl::Add (Ptr<Face> face, const Address& from, const Ptr<const Interest> &interest, Ptr<const NNNAddress> addr)
      {
	NS_LOG_FUNCTION (this->GetObject<Node> ()->GetId () << interest->GetName() << boost::cref(*face) << from << interest->GetSequenceNumber());

	// will add entry if doesn't exists, or just return an iterator to the existing entry
	std::pair< super::iterator, bool > result = super::insert (interest->GetName(), 0);
	if (result.first != super::end ())
	  {
	    if (result.second)
	      {
		Ptr<EntryImpl> newEntry = Create<EntryImpl> (this, interest->GetNamePtr ());
		newEntry->SetTrie (result.first);
		result.first->set_payload (newEntry);
	      }

	    super::modify (result.first,
			   ll::bind (&Entry::AddOrUpdate, ll::_1, face, from, interest, addr));

	    if (result.second)
	      {
		// Make sure to tell the TFIB that we have new information
		NS_ASSERT (this->GetObject<ForwardingStrategy> () != 0);
	      }

	    return result.first->payload ();
	  }
	else
	  return 0;
      }


      void
      TFibImpl::Remove (const Ptr<const icn::Name> &prefix)
      {
	NS_LOG_FUNCTION (this->GetObject<Node> ()->GetId () << boost::cref(*prefix));

	super::iterator tfibEntry = super::find_exact (*prefix);
	if (tfibEntry != super::end ())
	  {
	    // notify forwarding strategy about soon be removed TFIB entry
	    NS_ASSERT (this->GetObject<ForwardingStrategy> () != 0);

	    super::erase (tfibEntry);
	  }
	// else do nothing
      }

      void
      TFibImpl::Print (std::ostream &os) const
      {
	// !!! unordered_set imposes "random" order of item in the same level !!!
	super::parent_trie::const_recursive_iterator item (super::getTrie ());
	super::parent_trie::const_recursive_iterator end (0);
	for (; item != end; item++)
	  {
	    if (item->payload () == 0) continue;

	    os << item->payload ()->GetPrefix () << std::endl;
	    os << *item->payload ();
	  }
      }

      uint32_t
      TFibImpl::GetSize () const
      {
	return super::getPolicy ().size ();
      }

      Ptr<const Entry>
      TFibImpl::Begin () const
      {
	super::parent_trie::const_recursive_iterator item (super::getTrie ());
	super::parent_trie::const_recursive_iterator end (0);
	for (; item != end; item++)
	  {
	    if (item->payload () == 0) continue;
	    break;
	  }

	if (item == end)
	  return End ();
	else
	  return item->payload ();
      }

      Ptr<const Entry>
      TFibImpl::End () const
      {
	return 0;
      }

      Ptr<const Entry>
      TFibImpl::Next (Ptr<const Entry> from) const
      {
	if (from == 0) return 0;

	super::parent_trie::const_recursive_iterator item (*StaticCast<const EntryImpl> (from)->to_iterator ());
	super::parent_trie::const_recursive_iterator end (0);
	for (item++; item != end; item++)
	  {
	    if (item->payload () == 0) continue;
	    break;
	  }

	if (item == end)
	  return End ();
	else
	  return item->payload ();
      }

      Ptr<Entry>
      TFibImpl::Begin ()
      {
	super::parent_trie::recursive_iterator item (super::getTrie ());
	super::parent_trie::recursive_iterator end (0);
	for (; item != end; item++)
	  {
	    if (item->payload () == 0) continue;
	    break;
	  }

	if (item == end)
	  return End ();
	else
	  return item->payload ();
      }

      Ptr<Entry>
      TFibImpl::End ()
      {
	return 0;
      }

      Ptr<Entry>
      TFibImpl::Next (Ptr<Entry> from)
      {
	if (from == 0) return 0;

	super::parent_trie::recursive_iterator item (*StaticCast<EntryImpl> (from)->to_iterator ());
	super::parent_trie::recursive_iterator end (0);
	for (item++; item != end; item++)
	  {
	    if (item->payload () == 0) continue;
	    break;
	  }

	if (item == end)
	  return End ();
	else
	  return item->payload ();
      }
    } // namespace tfib
  } // namespace nnn
} // namespace ns3
