/* -*- Mode: C++; c-file-style: "gnu" -*- */
/*
 * Copyright (c) 2016 Jairo Eduardo Lopez
 *
 *   This file is part of nnnsim.
 *
 *  nnnsim-ohr.cc is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  nnnsim-ohr.cc is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with nnnsim-ohr.cc. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Author: Jairo Eduardo Lopez <jairo@ruri.waseda.jp>
 */

#include "nnnsim-ohr.h"

namespace ns3
{
  namespace nnn
  {
    NS_LOG_COMPONENT_DEFINE ("nnn.wire.nnnSIM.OHR");
    namespace wire
    {
      namespace nnnSIM
      {
	NS_OBJECT_ENSURE_REGISTERED (OHR);

	OHR::OHR ()
	: CommonHeader<nnn::OHR> ()
	  {
	  }

	OHR::OHR(Ptr<nnn::OHR> ohr_p)
	: CommonHeader<nnn::OHR> (ohr_p)
	  {
	  }

	TypeId
	OHR::GetTypeId(void)
	{
	  static TypeId tid = TypeId("ns3::nnn::OHR::nnnSIM")
	      .SetGroupName("Nnn")
	      .SetParent<Header> ()
	      .AddConstructor<OHR> ()
	      ;
	  return tid;
	}

	TypeId
	OHR::GetInstanceTypeId (void) const
	{
	  return GetTypeId ();
	}

	Ptr<Packet>
	OHR::ToWire(Ptr<const nnn::OHR> ohr_p)
	{
	  Ptr<const Packet> p = ohr_p->GetWire ();
	  if (!p)
	    {
	      // Mechanism packets have no payload, make an empty packet
	      Ptr<Packet> packet = Create<Packet> ();
	      OHR wireEncoding (ConstCast<nnn::OHR> (ohr_p));
	      packet-> AddHeader (wireEncoding);
	      ohr_p->SetWire (packet);

	      p = packet;
	    }
	  return p->Copy();
	}

	Ptr<nnn::OHR>
	OHR::FromWire (Ptr<Packet> packet)
	{
	  Ptr<nnn::OHR> ohr_p = Create<nnn::OHR> ();
	  Ptr<Packet> wire = packet->Copy();

	  OHR wireEncoding (ohr_p);
	  packet->RemoveHeader (wireEncoding);

	  // Mechanism packets have no payload, make an empty packet
	  ohr_p->SetWire (wire);

	  return ohr_p;
	}

	uint32_t
	OHR::GetSerializedSize(void) const
	{
	  size_t size = CommonGetSerializedSize() +                /* Common header */
	      1 +                                                  /* Whether the PDU has a source */
	      m_ptr->GetPoa ().GetSerializedSize () +              /* PoA */
	      1 +                                                  /* Is Src fixed name? */
	      8 +                                                  /* 3N Src Lease time */
	      4 +                                                  /* Distance to dest from Src */
	      1 +                                                  /* Whether PDU is directed */
	      1 +                                                  /* Is this definite query information */
	      1 +                                                  /* Is queried 3N name fixed? */
	      8 +                                                  /* Queried 3N Lease time */
	      4 +                                                  /* Distance to queried target from src */
	      NnnSim::SerializedSizeName(m_ptr->GetQueryName ())   /* Queried 3N name */
	      ;

	  if (m_ptr->HasSource ())
	    size += NnnSim::SerializedSizeName(m_ptr->GetSrcName ());

	  if (m_ptr->HasDestination ())
	    size += NnnSim::SerializedSizeName (m_ptr->GetDstName ());

	  return size;
	}

	void
	OHR::Serialize(Buffer::Iterator start) const
	{
	  // Serialize the header
	  CommonSerialize(start);

	  // Remember that CommonSerialize doesn't write the Packet length
	  // Move the iterator forward
	  start.Next(CommonGetSerializedSize() -2);

	  NS_LOG_INFO ("PktID = " << m_ptr->GetPacketId());
	  NS_LOG_INFO ("TTL = " << Seconds(static_cast<uint16_t> (m_ptr->GetLifetime ().ToInteger (Time::S))));
	  NS_LOG_INFO ("Version = " << m_ptr->GetVersion ());
	  NS_LOG_INFO ("Pkt Len = " << GetSerializedSize());

	  // Serialize the packet size
	  start.WriteU16(GetSerializedSize());

	  // Serialize if the 3N Src name is present
	  uint8_t hasSource = 0;

	  if (m_ptr->HasSource ())
	    {
	      NS_LOG_INFO ("3N Src name is present: " << m_ptr->GetSrcName ());
	      hasSource = 1;
	    }
	  else
	    {
	      NS_LOG_INFO ("3N Src name is not set");
	    }

	  start.WriteU8(hasSource);

	  if (m_ptr->HasSource ())
	    {
	      // Serialize source 3N name
	      NnnSim::SerializeName(start, m_ptr->GetSrcName());
	    }

	  // Serialize the PoA name
	  Address tmpaddr = m_ptr->GetPoa ();
	  uint32_t serialSize = tmpaddr.GetSerializedSize ();
	  uint8_t addrSize = tmpaddr.GetLength ();
	  uint8_t buffer[serialSize];

	  NS_LOG_INFO ("Address: " << tmpaddr);
	  NS_LOG_INFO ("Serialized Length: " << serialSize);
	  NS_LOG_INFO ("Address length: " << addrSize);

	  // Use the CopyTo function to get the bit representation
	  tmpaddr.CopyAllTo (buffer, serialSize);

	  // Since the bit representation is in 8 bit chunks, serialize it
	  // accordingly
	  for (uint32_t j = 0; j < serialSize; j++)
	    start.WriteU8 (buffer[j]);

	  // Serialize if the 3N Src name is fixed
	  uint8_t fixedname = 0;

	  if (m_ptr->IsSrcFixed ())
	    {
	      NS_LOG_INFO ("3N Src name is fixed");
	      fixedname = 1;
	    }
	  else
	    {
	      NS_LOG_INFO ("3N Src name is not fixed");
	    }

	  start.WriteU8(fixedname);

	  // Serialize the lease time for the 3N Src name
	  uint64_t lease = static_cast<uint64_t> (m_ptr->GetSrcLease ().ToInteger (Time::S));

	  NS_ASSERT_MSG (0 <= lease &&
			 lease < 0x7fffffffffffffffLL,
			 "Incorrect Lease time (should not be smaller than 0 and larger than UINT64_MAX");

	  NS_LOG_INFO ("Src Lease time = " << lease);
	  // Round lease time to lease and serialize
	  start.WriteU64 (lease);

	  uint32_t distance_to_src = m_ptr->GetDestDistance ();
	  NS_LOG_INFO ("Distance to source: " << distance_to_src);
	  start.WriteU32 (distance_to_src);

	  // Serialize if the 3N Dst name is present
	  uint8_t directedpdu = 0;

	  if (m_ptr->HasDestination ())
	    {
	      NS_LOG_INFO ("Directed OHR");
	      directedpdu = 1;
	    }
	  else
	    {
	      NS_LOG_INFO ("Non-directed OHR");
	    }

	  start.WriteU8(directedpdu);

	  if (m_ptr->HasDestination ())
	    {
	      NnnSim::SerializeName(start, m_ptr->GetDstName ());
	      NS_LOG_INFO ("Destination: " << m_ptr->GetDstName ());
	    }

	  // Serialize if the PDU has definite 3N query information
	  uint8_t definiteQueryInfo = 0;

	  if (m_ptr->HasDefiniteQueryInfo ())
	    {
	      NS_LOG_INFO ("Definite query info is present");
	      definiteQueryInfo = 1;
	    }
	  else
	    {
	      NS_LOG_INFO ("No definite query info");
	    }

	  start.WriteU8(definiteQueryInfo);

	  // Serialize if the 3N query name is fixed
	  uint8_t fixedQueryname = 0;

	  if (m_ptr->IsQueryFixed ())
	    {
	      NS_LOG_INFO ("3N queried name is fixed");
	      fixedQueryname = 1;
	    }
	  else
	    {
	      NS_LOG_INFO ("3N queried name is not fixed");
	    }

	  start.WriteU8(fixedQueryname);

	  // Serialize the lease time for the 3N Src name
	  uint64_t query_lease = static_cast<uint64_t> (m_ptr->GetQueryLease ().ToInteger (Time::S));

	  NS_ASSERT_MSG (0 <= query_lease &&
			 lease < 0x7fffffffffffffffLL,
			 "Incorrect Lease time (should not be smaller than 0 and larger than UINT64_MAX");

	  NS_LOG_INFO ("Queried name Lease time = " << query_lease);
	  // Round lease time to lease and serialize
	  start.WriteU64 (query_lease);

	  uint32_t distance = m_ptr->GetDestDistance ();
	  NS_LOG_INFO ("Distance to query = " << distance);
	  start.WriteU32 (distance);

	  // Serialize 3N name to query
	  NnnSim::SerializeName(start, m_ptr->GetQueryName());

	  NS_LOG_INFO("Finished serialization wrote " << GetSerializedSize ());
	}

	uint32_t
	OHR::Deserialize (Buffer::Iterator start)
	{
	  Buffer::Iterator i = start;

	  // Deserialize the header
	  uint32_t skip = CommonDeserialize (i);

	  NS_LOG_INFO ("PktID = " << m_ptr->GetPacketId());
	  NS_LOG_INFO ("TTL = " << Seconds(static_cast<uint16_t> (m_ptr->GetLifetime ().ToInteger (Time::S))));
	  NS_LOG_INFO ("Version = " << m_ptr->GetVersion ());
	  NS_LOG_INFO ("Pkt len = " << m_packet_len);

	  // Check packet ID
	  if (m_ptr->GetPacketId() != nnn::OHR_NNN)
	    throw new RHRException ();

	  // Move the iterator forward
	  i.Next(skip);

	  uint8_t hasSource = i.ReadU8 ();

	  if (hasSource != 0)
	    {
	      // Deserialize the 3N Src name
	      m_ptr->SetSrcName(NnnSim::DeserializeName(i));
	      NS_LOG_INFO ("3N Src name is present " << m_ptr->GetSrcName ());
	    }
	  else
	    {
	      m_ptr->DestinationSet (false);
	      NS_LOG_INFO ("3N Src name is not set");
	    }

	  // Deserialize the PoA name
	  uint8_t type = i.ReadU8 ();
	  uint8_t length = i.ReadU8 ();

	  // Create a buffer to be able to deserialize PoAs
	  uint8_t buffer[length];

	  for (uint8_t j = 0; j < length; j++)
	    {
	      buffer[j] = i.ReadU8 ();
	    }

	  Address tmp = Address (type, buffer, length);
	  uint32_t serialSize = tmp.GetSerializedSize ();
	  uint8_t addrSize = tmp.GetLength ();

	  NS_LOG_INFO ("Address: " << tmp);
	  NS_LOG_INFO ("Serialized Length: " << serialSize);
	  NS_LOG_INFO ("Address length: " << addrSize);

	  m_ptr->SetPoa(tmp);

	  // Deserialize whether the 3N Src name is fixed
	  uint8_t fixedname = i.ReadU8 ();

	  if (fixedname != 0)
	    {
	      m_ptr->SetSrcFixed (true);
	      NS_LOG_INFO ("3N Src name is fixed");
	    }
	  else
	    {
	      m_ptr->SetSrcFixed (false);
	      NS_LOG_INFO ("3N Src name is not fixed");
	    }

	  // Deserialize the lease time
	  uint64_t lease = i.ReadU64 ();
	  NS_LOG_INFO ("Src Lease time = " << lease);
	  m_ptr->SetSrcLease (Seconds (lease));

	  uint32_t distance_to_src = i.ReadU32();
	  NS_LOG_INFO ("Distance to source: " << distance_to_src);
	  m_ptr->SetDestDistance (distance_to_src);

	  uint8_t directed = i.ReadU8 ();

	  if (directed != 0)
	    {
	      NS_LOG_INFO ("Directed OHR");
	      m_ptr->SetDstName(NnnSim::DeserializeName (i));
	      NS_LOG_INFO ("Destination: " << m_ptr->GetDstName ());

	    }
	  else
	    {
	      NS_LOG_INFO ("Non-directed OHR");
	      m_ptr->DestinationSet (false);
	    }

	  // Serialize if the PDU has definite 3N query information
	  uint8_t definiteQueryInfo = i.ReadU8 ();

	  if (definiteQueryInfo != 0)
	    {
	      NS_LOG_INFO ("Definite query info is present");
	      m_ptr->SetDefiniteQueryInfo (true);
	    }
	  else
	    {
	      NS_LOG_INFO ("No definite query info");
	      m_ptr->SetDefiniteQueryInfo (false);
	    }

	  uint8_t fixedQueryname = i.ReadU8 ();

	  if (fixedQueryname != 0)
	    {
	      NS_LOG_INFO ("3N queried name is fixed");
	      m_ptr->SetQueryFixed(true);
	    }
	  else
	    {
	      NS_LOG_INFO ("3N queried name is not fixed");
	      m_ptr->SetQueryFixed (false);
	    }

	  // Deserialize the lease time
	  uint64_t query_lease = i.ReadU64 ();
	  NS_LOG_INFO ("Queried name Lease time = " << query_lease);
	  m_ptr->SetQueryLease (Seconds (query_lease));

	  // Deserialize the distance calculation
	  uint32_t distance = i.ReadU32 ();
	  NS_LOG_INFO ("Distance to query: " << distance);
	  m_ptr->SetQueryDistance(distance);

	  // Deserialize the 3N name to query
	  m_ptr->SetQueryName(NnnSim::DeserializeName(i));

	  NS_ASSERT (GetSerializedSize ()== (i.GetDistanceFrom (start)));

	  return i.GetDistanceFrom (start);
	}
      } /* namespace nnnSIM */
    } /* namespace wire */
  } /* namespace nnn */
} /* namespace ns3 */
