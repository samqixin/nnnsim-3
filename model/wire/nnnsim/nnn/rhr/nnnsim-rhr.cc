/* -*- Mode: C++; c-file-style: "gnu" -*- */
/*
 * Copyright (c) 2016 Jairo Eduardo Lopez
 *
 *   This file is part of nnnsim.
 *
 *  nnnsim-rhr.cc is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  nnnsim-rhr.cc is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with nnnsim-rhr.cc. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Author: Jairo Eduardo Lopez <jairo@ruri.waseda.jp>
 */

#include "nnnsim-rhr.h"

namespace ns3
{
  namespace nnn
  {
    NS_LOG_COMPONENT_DEFINE ("nnn.wire.nnnSIM.RHR");
    namespace wire
    {
      namespace nnnSIM
      {
	NS_OBJECT_ENSURE_REGISTERED (RHR);

	RHR::RHR ()
	: CommonHeader<nnn::RHR> ()
	  {
	  }

	RHR::RHR(Ptr<nnn::RHR> rhr_p)
	: CommonHeader<nnn::RHR> (rhr_p)
	  {
	  }

	TypeId
	RHR::GetTypeId(void)
	{
	  static TypeId tid = TypeId("ns3::nnn::RHR::nnnSIM")
	      .SetGroupName("Nnn")
	      .SetParent<Header> ()
	      .AddConstructor<RHR> ()
	      ;
	  return tid;
	}

	TypeId
	RHR::GetInstanceTypeId (void) const
	{
	  return GetTypeId ();
	}

	Ptr<Packet>
	RHR::ToWire(Ptr<const nnn::RHR> rhr_p)
	{
	  Ptr<const Packet> p = rhr_p->GetWire ();
	  if (!p)
	    {
	      // Mechanism packets have no payload, make an empty packet
	      Ptr<Packet> packet = Create<Packet> ();
	      RHR wireEncoding (ConstCast<nnn::RHR> (rhr_p));
	      packet-> AddHeader (wireEncoding);
	      rhr_p->SetWire (packet);

	      p = packet;
	    }
	  return p->Copy();
	}

	Ptr<nnn::RHR>
	RHR::FromWire (Ptr<Packet> packet)
	{
	  Ptr<nnn::RHR> rhr_p = Create<nnn::RHR> ();
	  Ptr<Packet> wire = packet->Copy();

	  RHR wireEncoding (rhr_p);
	  packet->RemoveHeader (wireEncoding);

	  // Mechanism packets have no payload, make an empty packet
	  rhr_p->SetWire (wire);

	  return rhr_p;
	}

	uint32_t
	RHR::GetSerializedSize(void) const
	{
	  size_t size = CommonGetSerializedSize() +               /* Common header */
	      1 +                                                 /* Whether the PDU has a source */
	      m_ptr->GetPoa ().GetSerializedSize () +             /* PoA */
	      1 +                                                 /* Is Src fixed name? */
	      8 +                                                 /* 3N Src Lease time */
	      1 +                                                 /* Whether PDU is directed */
	      NnnSim::SerializedSizeName(m_ptr->GetQueryName ())  /* Queried 3N name */
	  ;
	  if (m_ptr->HasSource ())
	    size += NnnSim::SerializedSizeName(m_ptr->GetSrcName ());

	  if (m_ptr->HasDestination ())
	    size += NnnSim::SerializedSizeName (m_ptr->GetDstName ());

	  return size;
	}

	void
	RHR::Serialize(Buffer::Iterator start) const
	{
	  // Serialize the header
	  CommonSerialize(start);

	  // Remember that CommonSerialize doesn't write the Packet length
	  // Move the iterator forward
	  start.Next(CommonGetSerializedSize() -2);

	  NS_LOG_INFO ("PktID = " << m_ptr->GetPacketId());
	  NS_LOG_INFO ("TTL = " << Seconds(static_cast<uint16_t> (m_ptr->GetLifetime ().ToInteger (Time::S))));
	  NS_LOG_INFO ("Version = " << m_ptr->GetVersion ());
	  NS_LOG_INFO ("Pkt Len = " << GetSerializedSize());

	  // Serialize the packet size
	  start.WriteU16(GetSerializedSize());

	  // Serialize if the 3N Src name is present
	  uint8_t hasSource = 0;

	  if (m_ptr->HasSource ())
	    {
	      NS_LOG_INFO ("3N Src name is present: " << m_ptr->GetSrcName ());
	      hasSource = 1;
	    }
	  else
	    {
	      NS_LOG_INFO ("3N Src name is not present");
	    }

	  start.WriteU8 (hasSource);

	  if (m_ptr->HasSource ())
	    {
	      // Serialize source 3N name
	      NnnSim::SerializeName(start, m_ptr->GetSrcName());
	    }

	  // Serialize the PoA name
	  Address tmpaddr = m_ptr->GetPoa ();
	  uint32_t serialSize = tmpaddr.GetSerializedSize ();
	  uint8_t addrSize = tmpaddr.GetLength ();
	  uint8_t buffer[serialSize];

	  NS_LOG_INFO ("Address: " << tmpaddr);
	  NS_LOG_INFO ("Serialized Length: " << serialSize);
	  NS_LOG_INFO ("Address length: " << addrSize);

	  // Use the CopyTo function to get the bit representation
	  tmpaddr.CopyAllTo (buffer, serialSize);

	  // Since the bit representation is in 8 bit chunks, serialize it
	  // accordingly
	  for (uint32_t j = 0; j < serialSize; j++)
	    start.WriteU8 (buffer[j]);

	  // Serialize if the 3N Src name is fixed
	  uint8_t fixedname = 0;

	  if (m_ptr->IsSrcFixed ())
	    {
	      NS_LOG_INFO ("3N Src name is fixed");
	      fixedname = 1;
	    }
	  else
	    {
	      NS_LOG_INFO ("3N Src name is not fixed");
	    }

	  start.WriteU8(fixedname);

	  // Serialize the lease time for the 3N Src name
	  uint64_t lease = static_cast<uint64_t> (m_ptr->GetSrcLease ().ToInteger (Time::S));

	  NS_ASSERT_MSG (0 <= lease &&
			 lease < 0x7fffffffffffffffLL,
			 "Incorrect Lease time (should not be smaller than 0 and larger than UINT64_MAX");

	  NS_LOG_INFO ("Lease time = " << lease);
	  // Round lease time to lease and serialize
	  start.WriteU64 (lease);

	  // Serialize if the 3N Dst name is present
	  uint8_t directedpdu = 0;

	  if (m_ptr->HasDestination ())
	    {
	      NS_LOG_INFO ("Directed RHR");
	      directedpdu = 1;
	    }
	  else
	    {
	      NS_LOG_INFO ("Non-directed RHR");
	    }

	  start.WriteU8(directedpdu);

	  if (m_ptr->HasDestination ())
	    {
	      NnnSim::SerializeName(start, m_ptr->GetDstName ());
	    }

	  // Serialize 3N name to query
	  NnnSim::SerializeName(start, m_ptr->GetQueryName());

	  NS_LOG_INFO("Finished serialization wrote " << GetSerializedSize ());
	}

	uint32_t
	RHR::Deserialize (Buffer::Iterator start)
	{
	  Buffer::Iterator i = start;

	  // Deserialize the header
	  uint32_t skip = CommonDeserialize (i);

	  NS_LOG_INFO ("PktID = " << m_ptr->GetPacketId());
	  NS_LOG_INFO ("TTL = " << Seconds(static_cast<uint16_t> (m_ptr->GetLifetime ().ToInteger (Time::S))));
	  NS_LOG_INFO ("Version = " << m_ptr->GetVersion ());
	  NS_LOG_INFO ("Pkt len = " << m_packet_len);

	  // Check packet ID
	  if (m_ptr->GetPacketId() != nnn::RHR_NNN)
	    throw new RHRException ();

	  // Move the iterator forward
	  i.Next(skip);

	  uint8_t hasSource = i.ReadU8 ();

	  if (hasSource != 0)
	    {
	      // Deserialize the 3N Src name
	      m_ptr->SetSrcName(NnnSim::DeserializeName(i));
	      NS_LOG_INFO ("3N name: " << m_ptr->GetSrcName ());
	    }
	  else
	    {
	      m_ptr->DestinationSet (false);
	      NS_LOG_INFO ("3N name not set");
	    }

	  // Deserialize the PoA name
	  uint8_t type = i.ReadU8 ();
	  uint8_t length = i.ReadU8 ();

	  // Create a buffer to be able to deserialize PoAs
	  uint8_t buffer[length];

	  for (uint8_t j = 0; j < length; j++)
	    {
	      buffer[j] = i.ReadU8 ();
	    }

	  Address tmp = Address (type, buffer, length);

	  NS_LOG_INFO ("Obtained PoA: " << tmp);

	  m_ptr->SetPoa(tmp);

	  // Deserialize whether the 3N Src name is fixed
	  uint8_t fixedname = i.ReadU8 ();

	  if (fixedname != 0)
	    {
	      m_ptr->SetSrcFixed (true);
	    }
	  else
	    {
	      m_ptr->SetSrcFixed (false);
	    }

	  // Deserialize the lease time
	  uint64_t lease = i.ReadU64 ();
	  NS_LOG_INFO ("Lease time = " << lease);
	  m_ptr->SetSrcLease (Seconds (lease));

	  uint8_t directed = i.ReadU8 ();

	  if (directed != 0)
	    {
	      m_ptr->SetDstName(NnnSim::DeserializeName (i));
	      NS_LOG_INFO ("Directed RHR: " << m_ptr->GetDstName ());
	    }
	  else
	    {
	      m_ptr->DestinationSet (false);
	      NS_LOG_INFO ("Non-directed RHR");
	    }

	  // Deserialize the 3N name to query
	  m_ptr->SetQueryName(NnnSim::DeserializeName(i));

	  NS_ASSERT (GetSerializedSize ()== (i.GetDistanceFrom (start)));

	  return i.GetDistanceFrom (start);
	}

      } /* namespace nnnSIM */
    } /* namespace wire */
  } /* namespace nnn */
} /* namespace ns3 */
