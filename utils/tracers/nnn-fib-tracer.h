/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil -*- */
/*
 * Copyright (c) 2018 Jairo Eduardo Lopez
 *
 *   This file is part of nnnsim.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Jairo Eduardo Lopez <jairo@ruri.waseda.jp>
 */

#ifndef UTILS_TRACERS_NNN_FIB_TRACER_H_
#define UTILS_TRACERS_NNN_FIB_TRACER_H_

#include "ns3/ptr.h"
#include "ns3/simple-ref-count.h"
#include "ns3/nstime.h"
#include "ns3/event-id.h"
#include "ns3/node-container.h"
#include "ns3/nnn-icn-name.h"

#include <boost/tuple/tuple.hpp>
#include <boost/shared_ptr.hpp>
#include <list>

namespace ns3
{
  class Node;

  namespace nnn
  {

    class Fib;

    class FibTracer : public SimpleRefCount<FibTracer>
    {
    public:
      FibTracer (boost::shared_ptr<std::ostream> os, Ptr<Node> node);

      FibTracer (boost::shared_ptr<std::ostream> os, const std::string &node);

      virtual
      ~FibTracer ();

      static void
      Install (Ptr<Node> node, const std::string &file);

      static Ptr<FibTracer>
      Install (Ptr<Node> node, boost::shared_ptr<std::ostream> outputStream);

      static void
      Install (const NodeContainer &nodes, const std::string &file);

      static void
      InstallAll (const std::string &file);

      static void
      Destroy ();

      void
      PrintHeader (std::ostream &os) const;

    private:
      void
      Connect ();

      void
      Fib3NPartUpdate (Ptr<const icn::Name>, size_t);

      void
      FibPoAPartUpdate (Ptr<const icn::Name>, size_t);

      std::string m_node;
      Ptr<Node> m_nodePtr;

      boost::shared_ptr<std::ostream> m_os;
    };

  } /* namespace nnn */
} /* namespace ns3 */

#endif /* UTILS_TRACERS_NNN_FIB_TRACER_H_ */
